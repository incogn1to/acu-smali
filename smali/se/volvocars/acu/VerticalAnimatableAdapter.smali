.class public Lse/volvocars/acu/VerticalAnimatableAdapter;
.super Ljava/lang/Object;
.source "VerticalAnimatableAdapter.java"

# interfaces
.implements Lse/volvocars/acu/Animatable;


# instance fields
.field private final mAnimator:Lse/volvocars/acu/Animator;

.field private final mAppName:Landroid/view/View;

.field private final mHome:Landroid/view/View;

.field private final mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

.field private mScreenYOffset:I

.field private final mStatusbarHeight:I

.field private mStatusbarVisible:Z


# direct methods
.method public constructor <init>(ILandroid/view/View;Lse/volvocars/acu/ui/opengl/LauncherGLView;Landroid/view/View;)V
    .locals 1
    .param p1, "statusbarHeight"    # I
    .param p2, "home"    # Landroid/view/View;
    .param p3, "launcher"    # Lse/volvocars/acu/ui/opengl/LauncherGLView;
    .param p4, "appName"    # Landroid/view/View;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Lse/volvocars/acu/Animator;

    invoke-direct {v0}, Lse/volvocars/acu/Animator;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mAnimator:Lse/volvocars/acu/Animator;

    .line 24
    iput p1, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mStatusbarHeight:I

    .line 25
    iput-object p2, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mHome:Landroid/view/View;

    .line 26
    iput-object p3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    .line 27
    iput-object p4, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mAppName:Landroid/view/View;

    .line 28
    return-void
.end method


# virtual methods
.method public animateTo(IZ)V
    .locals 6
    .param p1, "target"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 56
    iput-boolean p2, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mStatusbarVisible:Z

    .line 57
    iget-object v0, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mAnimator:Lse/volvocars/acu/Animator;

    int-to-double v2, p1

    const-wide v4, 0x3fbeb851eb851eb8L    # 0.12

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lse/volvocars/acu/Animator;->animate(Lse/volvocars/acu/Animatable;DD)V

    .line 59
    return-void
.end method

.method public getValue()D
    .locals 2

    .prologue
    .line 63
    iget v0, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mScreenYOffset:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public setValue(D)V
    .locals 7
    .param p1, "value"    # D

    .prologue
    const/high16 v6, 0x420c0000    # 35.0f

    .line 32
    double-to-int v3, p1

    iput v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mScreenYOffset:I

    .line 35
    iget-object v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mHome:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 36
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v3}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 37
    .local v1, "params2":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mAppName:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 39
    .local v2, "params3":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-boolean v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mStatusbarVisible:Z

    if-eqz v3, :cond_0

    .line 40
    iget v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mStatusbarHeight:I

    neg-int v3, v3

    iget v4, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mScreenYOffset:I

    add-int/2addr v3, v4

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 41
    iget v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mStatusbarHeight:I

    neg-int v3, v3

    iget v4, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mScreenYOffset:I

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x28

    int-to-float v3, v3

    iget v4, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mScreenYOffset:I

    int-to-float v4, v4

    iget v5, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mStatusbarHeight:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 46
    :goto_0
    const/16 v3, 0x1e0

    iget v4, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mScreenYOffset:I

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 48
    iget-object v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    iget v4, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mScreenYOffset:I

    invoke-virtual {v3, v4}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setScreenYOffset(I)V

    .line 50
    iget-object v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mHome:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    iget-object v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v3, v1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    iget-object v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mAppName:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    return-void

    .line 43
    :cond_0
    iget v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mScreenYOffset:I

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 44
    iget v3, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mScreenYOffset:I

    add-int/lit8 v3, v3, 0x28

    int-to-float v3, v3

    iget v4, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mScreenYOffset:I

    int-to-float v4, v4

    iget v5, p0, Lse/volvocars/acu/VerticalAnimatableAdapter;->mStatusbarHeight:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_0
.end method
