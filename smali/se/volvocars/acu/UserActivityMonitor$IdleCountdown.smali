.class Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;
.super Ljava/lang/Object;
.source "UserActivityMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/UserActivityMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IdleCountdown"
.end annotation


# static fields
.field private static final WAIT_UNTIL_IDLE:I = 0x1d4c0


# instance fields
.field private final lock:Ljava/lang/Object;

.field private mRestarted:Z

.field final synthetic this$0:Lse/volvocars/acu/UserActivityMonitor;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/UserActivityMonitor;)V
    .locals 1

    .prologue
    .line 78
    iput-object p1, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->this$0:Lse/volvocars/acu/UserActivityMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->mRestarted:Z

    .line 81
    return-void
.end method


# virtual methods
.method public restart()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 88
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->mRestarted:Z

    .line 90
    iget-object v1, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 91
    monitor-exit v0

    .line 92
    return-void

    .line 91
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public run()V
    .locals 5

    .prologue
    .line 104
    iget-object v1, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 105
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->mRestarted:Z

    .line 107
    :goto_0
    iget-boolean v2, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->mRestarted:Z

    if-eqz v2, :cond_0

    .line 108
    const/4 v2, 0x0

    iput-boolean v2, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->mRestarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    :try_start_1
    iget-object v2, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    const-wide/32 v3, 0x1d4c0

    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 111
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 112
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v2, "ActivityMonitor"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Idle mode timer Interrupted while waiting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 120
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 117
    :cond_0
    :try_start_3
    iget-object v2, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->this$0:Lse/volvocars/acu/UserActivityMonitor;

    # getter for: Lse/volvocars/acu/UserActivityMonitor;->mIdle:Z
    invoke-static {v2}, Lse/volvocars/acu/UserActivityMonitor;->access$100(Lse/volvocars/acu/UserActivityMonitor;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 118
    iget-object v2, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->this$0:Lse/volvocars/acu/UserActivityMonitor;

    const/4 v3, 0x1

    # invokes: Lse/volvocars/acu/UserActivityMonitor;->setIdleMode(Z)V
    invoke-static {v2, v3}, Lse/volvocars/acu/UserActivityMonitor;->access$200(Lse/volvocars/acu/UserActivityMonitor;Z)V

    .line 120
    :cond_1
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 121
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 97
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 98
    monitor-exit v0

    .line 100
    return-void

    .line 98
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
