.class Lse/volvocars/acu/ui/HomeTouchListener$GestureListener;
.super Ljava/lang/Object;
.source "HomeTouchListener.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/ui/HomeTouchListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GestureListener"
.end annotation


# static fields
.field private static final SWIPE_MIN_DISTANCE:I = 0x78

.field private static final SWIPE_THRESHOLD_VELOCITY:I = 0x12c


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/ui/HomeTouchListener;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/ui/HomeTouchListener;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lse/volvocars/acu/ui/HomeTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/HomeTouchListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/ui/HomeTouchListener;Lse/volvocars/acu/ui/HomeTouchListener$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/ui/HomeTouchListener;
    .param p2, "x1"    # Lse/volvocars/acu/ui/HomeTouchListener$1;

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lse/volvocars/acu/ui/HomeTouchListener$GestureListener;-><init>(Lse/volvocars/acu/ui/HomeTouchListener;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 120
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/high16 v4, 0x43960000    # 300.0f

    const/high16 v3, 0x42f00000    # 120.0f

    .line 126
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 127
    :cond_0
    const/4 v1, 0x0

    .line 142
    :goto_0
    return v1

    .line 130
    :cond_1
    const/4 v0, 0x0

    .line 132
    .local v0, "fling":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_3

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v4

    if-lez v1, :cond_3

    .line 134
    iget-object v1, p0, Lse/volvocars/acu/ui/HomeTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/HomeTouchListener;

    # getter for: Lse/volvocars/acu/ui/HomeTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;
    invoke-static {v1}, Lse/volvocars/acu/ui/HomeTouchListener;->access$100(Lse/volvocars/acu/ui/HomeTouchListener;)Lse/volvocars/acu/AcuActivity;

    move-result-object v1

    const/16 v2, -0x2e0

    invoke-virtual {v1, v2}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    .line 135
    iget-object v1, p0, Lse/volvocars/acu/ui/HomeTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/HomeTouchListener;

    # getter for: Lse/volvocars/acu/ui/HomeTouchListener;->mHomeView:Lse/volvocars/acu/ui/HomeView;
    invoke-static {v1}, Lse/volvocars/acu/ui/HomeTouchListener;->access$200(Lse/volvocars/acu/ui/HomeTouchListener;)Lse/volvocars/acu/ui/HomeView;

    move-result-object v1

    invoke-virtual {v1}, Lse/volvocars/acu/ui/HomeView;->onStartDrag()V

    .line 136
    const/4 v0, 0x1

    :cond_2
    :goto_1
    move v1, v0

    .line 142
    goto :goto_0

    .line 137
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v4

    if-lez v1, :cond_2

    .line 138
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 147
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 151
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 156
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method
