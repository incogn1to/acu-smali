.class public Lse/volvocars/acu/ui/HomeTouchListener;
.super Ljava/lang/Object;
.source "HomeTouchListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/ui/HomeTouchListener$1;,
        Lse/volvocars/acu/ui/HomeTouchListener$GestureListener;
    }
.end annotation


# static fields
.field private static final DEFREEZE_SCROLLING_THRESHOLD:I = 0x23

.field private static final TAG:Ljava/lang/String; = "HomeTouchListener"


# instance fields
.field private final mAcuActivity:Lse/volvocars/acu/AcuActivity;

.field private mDisableScrolling:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private final mHomeView:Lse/volvocars/acu/ui/HomeView;

.field private mPressDownXPos:I


# direct methods
.method public constructor <init>(Lse/volvocars/acu/AcuActivity;Lse/volvocars/acu/ui/HomeView;)V
    .locals 4
    .param p1, "activity"    # Lse/volvocars/acu/AcuActivity;
    .param p2, "homeView"    # Lse/volvocars/acu/ui/HomeView;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    .line 32
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    new-instance v2, Lse/volvocars/acu/ui/HomeTouchListener$GestureListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lse/volvocars/acu/ui/HomeTouchListener$GestureListener;-><init>(Lse/volvocars/acu/ui/HomeTouchListener;Lse/volvocars/acu/ui/HomeTouchListener$1;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    .line 34
    iput-object p2, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mHomeView:Lse/volvocars/acu/ui/HomeView;

    .line 35
    return-void
.end method

.method static synthetic access$100(Lse/volvocars/acu/ui/HomeTouchListener;)Lse/volvocars/acu/AcuActivity;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/ui/HomeTouchListener;

    .prologue
    .line 15
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    return-object v0
.end method

.method static synthetic access$200(Lse/volvocars/acu/ui/HomeTouchListener;)Lse/volvocars/acu/ui/HomeView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/ui/HomeTouchListener;

    .prologue
    .line 15
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mHomeView:Lse/volvocars/acu/ui/HomeView;

    return-object v0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 43
    iget-object v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v3, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 46
    .local v1, "fling":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v2, v3

    .line 48
    .local v2, "x":I
    iget v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mPressDownXPos:I

    sub-int v0, v3, v2

    .line 50
    .local v0, "currentPos":I
    if-gez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    .line 54
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 103
    :cond_1
    :goto_0
    return v6

    .line 56
    :pswitch_0
    iput v2, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mPressDownXPos:I

    .line 57
    iput-boolean v6, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mDisableScrolling:Z

    goto :goto_0

    .line 60
    :pswitch_1
    iget v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mPressDownXPos:I

    sub-int/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/16 v4, 0x23

    if-le v3, v4, :cond_2

    iget-boolean v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mDisableScrolling:Z

    if-eqz v3, :cond_2

    .line 61
    iput-boolean v5, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mDisableScrolling:Z

    .line 62
    iput v2, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mPressDownXPos:I

    .line 63
    iget v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mPressDownXPos:I

    sub-int v0, v3, v2

    .line 65
    :cond_2
    if-nez v0, :cond_3

    .line 66
    iput v2, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mPressDownXPos:I

    .line 68
    :cond_3
    iget-boolean v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mDisableScrolling:Z

    if-nez v3, :cond_1

    .line 70
    iget-object v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v3, v0}, Lse/volvocars/acu/AcuActivity;->setViewXOffset(I)V

    .line 71
    iget-object v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v3, v6}, Lse/volvocars/acu/AcuActivity;->setNativeState(I)V

    .line 74
    if-lez v0, :cond_1

    .line 75
    iget-object v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mHomeView:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v3}, Lse/volvocars/acu/ui/HomeView;->onStartDrag()V

    .line 76
    iget-object v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mHomeView:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v3, v5}, Lse/volvocars/acu/ui/HomeView;->setArrowVisibility(Z)V

    .line 77
    iget-object v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mHomeView:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v3, v5}, Lse/volvocars/acu/ui/HomeView;->setClockVisibility(Z)V

    .line 78
    iget-object v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mHomeView:Lse/volvocars/acu/ui/HomeView;

    #invoke-virtual {v3, v5}, Lse/volvocars/acu/ui/HomeView;->setWeatherConditionSymbolVisibility(Z)V

    goto :goto_0

    .line 88
    :pswitch_2
    if-nez v1, :cond_1

    .line 92
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/16 v4, 0x170

    if-le v3, v4, :cond_4

    .line 94
    iget-object v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    const/16 v4, -0x2e0

    invoke-virtual {v3, v4}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    goto :goto_0

    .line 97
    :cond_4
    iget-object v3, p0, Lse/volvocars/acu/ui/HomeTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v3, v5}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
