.class public Lse/volvocars/acu/ui/VerticalSwipeGestureListener;
.super Ljava/lang/Object;
.source "VerticalSwipeGestureListener.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# static fields
.field private static final SWIPE_MIN_DISTANCE:I = 0x78

.field private static final SWIPE_THRESHOLD_VELOCITY:I = 0x12c

.field private static final TAG:Ljava/lang/String; = "GestureDetector"


# instance fields
.field private mAcuActivity:Lse/volvocars/acu/AcuActivity;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/AcuActivity;)V
    .locals 0
    .param p1, "activity"    # Lse/volvocars/acu/AcuActivity;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lse/volvocars/acu/ui/VerticalSwipeGestureListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    .line 23
    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x43960000    # 300.0f

    const/high16 v3, 0x42f00000    # 120.0f

    .line 34
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v1, v5

    .line 49
    :goto_0
    return v1

    .line 38
    :cond_1
    const/4 v0, 0x0

    .line 40
    .local v0, "fling":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_3

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v4

    if-lez v1, :cond_3

    .line 41
    iget-object v1, p0, Lse/volvocars/acu/ui/VerticalSwipeGestureListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lse/volvocars/acu/AcuActivity;->setStatusBarVisible(Z)V

    .line 42
    const/4 v0, 0x1

    :cond_2
    :goto_1
    move v1, v0

    .line 49
    goto :goto_0

    .line 43
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    neg-float v1, v1

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v4

    if-lez v1, :cond_2

    .line 44
    iget-object v1, p0, Lse/volvocars/acu/ui/VerticalSwipeGestureListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v1, v5}, Lse/volvocars/acu/AcuActivity;->setStatusBarVisible(Z)V

    .line 45
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 54
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 63
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method
