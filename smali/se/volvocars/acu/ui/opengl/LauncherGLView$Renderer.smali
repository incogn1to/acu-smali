.class Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;
.super Ljava/lang/Object;
.source "LauncherGLView.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView2$Renderer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/ui/opengl/LauncherGLView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Renderer"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer$LoadTextureException;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private volatile mCurrentPos:I

.field private final mHomeView:Lse/volvocars/acu/ui/HomeView;

.field private mLastTimeMillis:J

.field private mNativePositionChangeListener:Lse/volvocars/acu/ui/opengl/OnNativePositionChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lse/volvocars/acu/ui/HomeView;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lse/volvocars/acu/ui/HomeView;

    .prologue
    .line 338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339
    iput-object p1, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mContext:Landroid/content/Context;

    .line 340
    iput-object p2, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mHomeView:Lse/volvocars/acu/ui/HomeView;

    .line 341
    return-void
.end method

.method private notifyOnNativerPositionChangeListeners()V
    .locals 2

    .prologue
    .line 368
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mNativePositionChangeListener:Lse/volvocars/acu/ui/opengl/OnNativePositionChangedListener;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mNativePositionChangeListener:Lse/volvocars/acu/ui/opengl/OnNativePositionChangedListener;

    iget v1, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mCurrentPos:I

    invoke-interface {v0, v1}, Lse/volvocars/acu/ui/opengl/OnNativePositionChangedListener;->onNativePostionChanged(I)V

    .line 371
    :cond_0
    return-void
.end method


# virtual methods
.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    .line 347
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 348
    .local v0, "currentTime":J
    iget-wide v4, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mLastTimeMillis:J

    sub-long v2, v0, v4

    .line 349
    .local v2, "deltaTime":J
    iput-wide v0, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mLastTimeMillis:J

    .line 351
    const-wide/16 v4, 0x64

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 352
    const-wide/16 v2, 0x64

    .line 355
    :cond_0
    long-to-int v4, v2

    invoke-static {v4}, Lse/volvocars/acu/ui/opengl/LauncherLib;->step(I)I

    move-result v4

    iput v4, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mCurrentPos:I

    .line 358
    invoke-direct {p0}, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->notifyOnNativerPositionChangeListeners()V

    .line 361
    iget-object v4, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mHomeView:Lse/volvocars/acu/ui/HomeView;

    if-eqz v4, :cond_1

    .line 362
    iget-object v4, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mHomeView:Lse/volvocars/acu/ui/HomeView;

    invoke-static {}, Lse/volvocars/acu/ui/opengl/LauncherLib;->isRendering()Z

    move-result v5

    invoke-virtual {v4, v5}, Lse/volvocars/acu/ui/HomeView;->showContent(Z)V

    .line 365
    :cond_1
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 0
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 377
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 12
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    .line 383
    const/16 v9, 0x2e0

    const/16 v10, 0x1e0

    const/16 v11, 0x12c

    invoke-static {v9, v10, v11}, Lse/volvocars/acu/ui/opengl/LauncherLib;->onSurfaceCreated(III)Z

    .line 386
    const/4 v0, 0x0

    .line 387
    .local v0, "apkFilePath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 388
    .local v1, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v9, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 390
    .local v6, "packMgmr":Landroid/content/pm/PackageManager;
    :try_start_0
    iget-object v9, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v6, v9, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 396
    if-eqz v1, :cond_2

    .line 397
    iget-object v0, v1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 399
    const-string v5, "assets/sprites/icon_atlas.png"

    .line 400
    .local v5, "iconAtlasPath":Ljava/lang/String;
    const/4 v9, 0x0

    invoke-static {v0, v5, v9}, Lse/volvocars/acu/ui/opengl/LauncherLib;->createTextureFromPNG(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v8

    .line 401
    .local v8, "success":Z
    if-nez v8, :cond_0

    .line 402
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to create texture from "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 403
    .local v4, "errorString":Ljava/lang/String;
    new-instance v9, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer$LoadTextureException;

    invoke-direct {v9, v4}, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer$LoadTextureException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 391
    .end local v4    # "errorString":Ljava/lang/String;
    .end local v5    # "iconAtlasPath":Ljava/lang/String;
    .end local v8    # "success":Z
    :catch_0
    move-exception v9

    move-object v3, v9

    .line 392
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 393
    new-instance v9, Ljava/lang/RuntimeException;

    const-string v10, "Unable to locate assets, aborting..."

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 406
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v5    # "iconAtlasPath":Ljava/lang/String;
    .restart local v8    # "success":Z
    :cond_0
    const-string v2, "assets/sprites/background_atlas.png"

    .line 407
    .local v2, "backgroundAtlasPath":Ljava/lang/String;
    const/4 v9, 0x1

    invoke-static {v0, v2, v9}, Lse/volvocars/acu/ui/opengl/LauncherLib;->createTextureFromPNG(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v8

    .line 408
    if-nez v8, :cond_1

    .line 409
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to create texture from "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 410
    .restart local v4    # "errorString":Ljava/lang/String;
    new-instance v9, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer$LoadTextureException;

    invoke-direct {v9, v4}, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer$LoadTextureException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 413
    .end local v4    # "errorString":Ljava/lang/String;
    :cond_1
    const-string v7, "assets/sprites/particle16x16.png"

    .line 414
    .local v7, "particlePath":Ljava/lang/String;
    const/4 v9, 0x2

    invoke-static {v0, v7, v9}, Lse/volvocars/acu/ui/opengl/LauncherLib;->createTextureFromPNG(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v8

    .line 415
    if-nez v8, :cond_3

    .line 416
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to create texture from "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 417
    .restart local v4    # "errorString":Ljava/lang/String;
    new-instance v9, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer$LoadTextureException;

    invoke-direct {v9, v4}, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer$LoadTextureException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 420
    .end local v2    # "backgroundAtlasPath":Ljava/lang/String;
    .end local v4    # "errorString":Ljava/lang/String;
    .end local v5    # "iconAtlasPath":Ljava/lang/String;
    .end local v7    # "particlePath":Ljava/lang/String;
    .end local v8    # "success":Z
    :cond_2
    const-string v9, "LaucherGLView"

    const-string v10, "Failed to read png-textures"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    iput-wide v9, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mLastTimeMillis:J

    .line 426
    return-void
.end method

.method public setNativePositionChangeListener(Lse/volvocars/acu/ui/opengl/OnNativePositionChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lse/volvocars/acu/ui/opengl/OnNativePositionChangedListener;

    .prologue
    .line 429
    iput-object p1, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->mNativePositionChangeListener:Lse/volvocars/acu/ui/opengl/OnNativePositionChangedListener;

    .line 430
    return-void
.end method
