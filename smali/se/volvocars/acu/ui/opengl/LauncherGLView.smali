.class public final Lse/volvocars/acu/ui/opengl/LauncherGLView;
.super Landroid/opengl/GLSurfaceView2;
.source "LauncherGLView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;,
        Lse/volvocars/acu/ui/opengl/LauncherGLView$ConfigChooser;,
        Lse/volvocars/acu/ui/opengl/LauncherGLView$ContextFactory;,
        Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;
    }
.end annotation


# static fields
.field public static final DIRECTION_LEFT:I = -0x1

.field public static final DIRECTION_RIGHT:I = 0x1

.field public static final GL_STATE_RENDER_ALL:I = 0x1

.field public static final GL_STATE_RENDER_ONLY_BACKGROUND_AND_PARTICLES:I = 0x0

.field private static final GRAPHICS_FORBIDDEN:I = 0x4

.field private static final GRAPHICS_LAUNCH_HIGHLIGHT:I = 0x2

.field private static final GRAPHICS_LEFT_FADE:I = 0x5

.field private static final GRAPHICS_PEARL:I = 0x3

.field private static final GRAPHICS_PEARL_CIRCLE:I = 0x7

.field private static final GRAPHICS_PEARL_SQUARE:I = 0x0

.field private static final GRAPHICS_RIGHT_FADE:I = 0x6

.field private static final GRAPHICS_SELECT_CIRCLE:I = 0x1

.field private static final ICON_WIDTH:I = 0x80

.field public static final PEARL_LEFT_OFFSET:I = 0x41

.field public static final PEARL_VIEW_WIDTH:I = 0x1c2

.field private static final PEARL_WIDTH:I = 0x20

.field private static final PEARL_Y_OFFSET:I = 0x28

.field private static final TAG:Ljava/lang/String; = "LauncherGLView"

.field private static final TEXTURE_ATLAS_BACKGROUND:I = 0x1

.field private static final TEXTURE_ATLAS_ICONS:I = 0x0

.field private static final TEXTURE_PARTICLE:I = 0x2


# instance fields
.field private final mRenderer:Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lse/volvocars/acu/ui/HomeView;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lse/volvocars/acu/ui/HomeView;

    .prologue
    const/4 v1, 0x5

    const/4 v4, 0x0

    .line 105
    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView2;-><init>(Landroid/content/Context;)V

    .line 110
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$ContextFactory;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lse/volvocars/acu/ui/opengl/LauncherGLView$ContextFactory;-><init>(Lse/volvocars/acu/ui/opengl/LauncherGLView$1;)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setEGLContextFactory(Landroid/opengl/GLSurfaceView2$EGLContextFactory;)V

    .line 116
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$ConfigChooser;

    const/4 v2, 0x6

    move v3, v1

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v6}, Lse/volvocars/acu/ui/opengl/LauncherGLView$ConfigChooser;-><init>(IIIIII)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView2$EGLConfigChooser;)V

    .line 117
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;

    invoke-direct {v0, p1, p2}, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;-><init>(Landroid/content/Context;Lse/volvocars/acu/ui/HomeView;)V

    iput-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView;->mRenderer:Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;

    .line 118
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView;->mRenderer:Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setRenderer(Landroid/opengl/GLSurfaceView2$Renderer;)V

    .line 122
    invoke-direct {p0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->loadGraphics()V

    .line 126
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$1;

    invoke-direct {v0, p0}, Lse/volvocars/acu/ui/opengl/LauncherGLView$1;-><init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->queueEvent(Ljava/lang/Runnable;)V

    .line 134
    return-void
.end method

.method static synthetic access$100(Lse/volvocars/acu/ui/opengl/LauncherGLView;IIII)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherGLView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3, p4}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setPearlConstants(IIII)V

    return-void
.end method

.method static synthetic access$300(Ljava/lang/String;Ljavax/microedition/khronos/egl/EGL10;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljavax/microedition/khronos/egl/EGL10;

    .prologue
    .line 70
    invoke-static {p0, p1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->checkEglError(Ljava/lang/String;Ljavax/microedition/khronos/egl/EGL10;)V

    return-void
.end method

.method private static checkEglError(Ljava/lang/String;Ljavax/microedition/khronos/egl/EGL10;)V
    .locals 6
    .param p0, "prompt"    # Ljava/lang/String;
    .param p1, "egl"    # Ljavax/microedition/khronos/egl/EGL10;

    .prologue
    .line 218
    :goto_0
    invoke-interface {p1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .local v0, "error":I
    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    .line 219
    const-string v1, "LauncherGLView"

    const-string v2, "%s: EGL error: 0x%x"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 221
    :cond_0
    return-void
.end method

.method private loadGraphics()V
    .locals 2

    .prologue
    .line 143
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;-><init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;Lse/volvocars/acu/ui/opengl/LauncherGLView$1;)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->queueEvent(Ljava/lang/Runnable;)V

    .line 144
    return-void
.end method

.method private setPearlConstants(IIII)V
    .locals 0
    .param p1, "pearlWidth"    # I
    .param p2, "pearlViewWidth"    # I
    .param p3, "pearlYOffset"    # I
    .param p4, "pearlLeftOffset"    # I

    .prologue
    .line 604
    invoke-static {p1, p2, p3, p4}, Lse/volvocars/acu/ui/opengl/LauncherLib;->setPearlConstants(IIII)V

    .line 605
    return-void
.end method


# virtual methods
.method public addGraphics(IFFFFFF)V
    .locals 9
    .param p1, "id"    # I
    .param p2, "bx"    # F
    .param p3, "by"    # F
    .param p4, "w"    # F
    .param p5, "h"    # F
    .param p6, "pixelWidth"    # F
    .param p7, "pixelHeight"    # F

    .prologue
    .line 503
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$5;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lse/volvocars/acu/ui/opengl/LauncherGLView$5;-><init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;IFFFFFF)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->queueEvent(Ljava/lang/Runnable;)V

    .line 510
    return-void
.end method

.method public addIcon(IFFFFF)V
    .locals 8
    .param p1, "id"    # I
    .param p2, "bx"    # F
    .param p3, "by"    # F
    .param p4, "size"    # F
    .param p5, "width"    # F
    .param p6, "height"    # F

    .prologue
    .line 483
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$4;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lse/volvocars/acu/ui/opengl/LauncherGLView$4;-><init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;IFFFFF)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->queueEvent(Ljava/lang/Runnable;)V

    .line 490
    return-void
.end method

.method public launch()V
    .locals 1

    .prologue
    .line 585
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$8;

    invoke-direct {v0, p0}, Lse/volvocars/acu/ui/opengl/LauncherGLView$8;-><init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->queueEvent(Ljava/lang/Runnable;)V

    .line 592
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 459
    invoke-super {p0}, Landroid/opengl/GLSurfaceView2;->onPause()V

    .line 461
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$3;

    invoke-direct {v0, p0}, Lse/volvocars/acu/ui/opengl/LauncherGLView$3;-><init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->queueEvent(Ljava/lang/Runnable;)V

    .line 468
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 446
    invoke-super {p0}, Landroid/opengl/GLSurfaceView2;->onResume()V

    .line 448
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$2;

    invoke-direct {v0, p0}, Lse/volvocars/acu/ui/opengl/LauncherGLView$2;-><init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->queueEvent(Ljava/lang/Runnable;)V

    .line 454
    return-void
.end method

.method public scrollToPos(I)V
    .locals 0
    .param p1, "targetPosition"    # I

    .prologue
    .line 559
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->scrollToPos(I)I

    .line 560
    return-void
.end method

.method public setAppForbiddenStatus([I)V
    .locals 1
    .param p1, "forbidden"    # [I

    .prologue
    .line 531
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$7;

    invoke-direct {v0, p0, p1}, Lse/volvocars/acu/ui/opengl/LauncherGLView$7;-><init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;[I)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->queueEvent(Ljava/lang/Runnable;)V

    .line 538
    return-void
.end method

.method public setAppIconOrder([I)V
    .locals 1
    .param p1, "order"    # [I

    .prologue
    .line 519
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$6;

    invoke-direct {v0, p0, p1}, Lse/volvocars/acu/ui/opengl/LauncherGLView$6;-><init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;[I)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->queueEvent(Ljava/lang/Runnable;)V

    .line 526
    return-void
.end method

.method public setInsidePearlViewStatus(Z)V
    .locals 0
    .param p1, "inside"    # Z

    .prologue
    .line 609
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->setInsidePearlViewStatus(Z)V

    .line 610
    return-void
.end method

.method public setLineEmitterState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 640
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->setLineEmitterState(Z)V

    .line 641
    return-void
.end method

.method public setNativePositionChangeListener(Lse/volvocars/acu/ui/opengl/OnNativePositionChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lse/volvocars/acu/ui/opengl/OnNativePositionChangedListener;

    .prologue
    .line 620
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView;->mRenderer:Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;->setNativePositionChangeListener(Lse/volvocars/acu/ui/opengl/OnNativePositionChangedListener;)V

    .line 621
    return-void
.end method

.method public setPearlPos(I)I
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 555
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->setPearlPos(I)I

    move-result v0

    return v0
.end method

.method public setPlayButtonEmitterState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 644
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->setPlayButtonEmitterState(Z)V

    .line 645
    return-void
.end method

.method public setPos(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 545
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->setPos(I)V

    .line 546
    return-void
.end method

.method public setScreenXOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 625
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->setScreenXOffset(I)V

    .line 626
    return-void
.end method

.method public setScreenYOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 649
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->setScreenYOffset(I)V

    .line 650
    return-void
.end method

.method public setSelectedStatus(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 614
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->setSelectedStatus(I)V

    .line 615
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 635
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->setState(I)V

    .line 636
    return-void
.end method

.method public setTouchActionState(I)V
    .locals 0
    .param p1, "action"    # I

    .prologue
    .line 576
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->setTouchActionState(I)V

    .line 577
    return-void
.end method

.method public startFling(I)V
    .locals 0
    .param p1, "velocity"    # I

    .prologue
    .line 568
    invoke-static {p1}, Lse/volvocars/acu/ui/opengl/LauncherLib;->startFling(I)V

    .line 569
    return-void
.end method
