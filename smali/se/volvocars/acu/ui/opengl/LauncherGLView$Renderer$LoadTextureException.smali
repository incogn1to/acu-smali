.class Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer$LoadTextureException;
.super Ljava/lang/RuntimeException;
.source "LauncherGLView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/ui/opengl/LauncherGLView$Renderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LoadTextureException"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 436
    invoke-direct {p0}, Ljava/lang/RuntimeException;-><init>()V

    .line 437
    const-string v0, "LauncherGLView"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    return-void
.end method
