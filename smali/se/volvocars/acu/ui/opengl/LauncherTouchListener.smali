.class public final Lse/volvocars/acu/ui/opengl/LauncherTouchListener;
.super Ljava/lang/Object;
.source "LauncherTouchListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;
    }
.end annotation


# static fields
.field private static final ACTION_MIN_TIME:I = 0x5

.field private static final DEFREEZE_SCROLLING_THRESHOLD:I = 0x32

.field private static final HITBOX_PIXELTHRESHOLD:I = 0x32

.field private static final HITBOX_WIDTH:I = 0x96

.field public static final PEARL_VIEW_TOUCH_MARGIN:I = 0x19

.field private static final SELECTION_ABORT_TIME:I = 0x7d0

.field private static final SELECT_ABORT_DISTANCE:I = 0x1e

.field private static final TAG:Ljava/lang/String; = "LauncherTouchListener"

.field private static final TOP_OFFSET:I = 0x84


# instance fields
.field private final hitBoxLeft:Landroid/graphics/Rect;

.field private final hitBoxMiddle:Landroid/graphics/Rect;

.field private final hitBoxRight:Landroid/graphics/Rect;

.field private final mAcuActivity:Lse/volvocars/acu/AcuActivity;

.field private final mAppHandler:Lse/volvocars/acu/AppHandler;

.field private mClickedAppIndex:I

.field private mClickedInsideHomeButton:Z

.field private mClickedInsidePearlView:Z

.field private mCurrentPosAtDown:I

.field private mDisableScrolling:Z

.field private mDownX:D

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHandler:Landroid/os/Handler;

.field private mIsFlingingToHome:Z

.field private mIsMoving:Z

.field private mIsStartingApp:Z

.field private final mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

.field private mPressDownPos:I

.field private final pearlRect:Landroid/graphics/Rect;

.field private final returnToHomeRect:Landroid/graphics/Rect;

.field private triggerDeselect:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/AcuActivity;Lse/volvocars/acu/ui/opengl/LauncherGLView;Lse/volvocars/acu/AppHandler;)V
    .locals 7
    .param p1, "activity"    # Lse/volvocars/acu/AcuActivity;
    .param p2, "glView"    # Lse/volvocars/acu/ui/opengl/LauncherGLView;
    .param p3, "appHandler"    # Lse/volvocars/acu/AppHandler;

    .prologue
    const/16 v6, 0x41

    const/16 v4, 0x13b

    const/16 v3, 0xa5

    const/4 v5, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, -0x7

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v3, v2, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxLeft:Landroid/graphics/Rect;

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    const/16 v1, 0x125

    const/16 v2, 0x1bb

    invoke-direct {v0, v1, v3, v2, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxMiddle:Landroid/graphics/Rect;

    .line 43
    new-instance v0, Landroid/graphics/Rect;

    const/16 v1, 0x251

    const/16 v2, 0x2e7

    invoke-direct {v0, v1, v3, v2, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxRight:Landroid/graphics/Rect;

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    const/16 v1, 0xb7

    const/16 v2, 0x15c

    const/16 v3, 0x26a

    const/16 v4, 0x1e0

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->pearlRect:Landroid/graphics/Rect;

    .line 57
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->pearlRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v6

    sub-int/2addr v1, v6

    iget-object v2, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->pearlRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->pearlRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->pearlRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->returnToHomeRect:Landroid/graphics/Rect;

    .line 65
    iput-boolean v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsidePearlView:Z

    .line 66
    iput-boolean v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsideHomeButton:Z

    .line 67
    iput-boolean v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mDisableScrolling:Z

    .line 68
    const/4 v0, -0x1

    iput v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedAppIndex:I

    .line 77
    iput-boolean v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsStartingApp:Z

    .line 82
    new-instance v0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$1;

    invoke-direct {v0, p0}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$1;-><init>(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)V

    iput-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->triggerDeselect:Ljava/lang/Runnable;

    .line 94
    iput-object p3, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;

    .line 95
    iput-object p1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    .line 96
    iput-object p2, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    .line 97
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    new-instance v2, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;-><init>(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;Lse/volvocars/acu/ui/opengl/LauncherTouchListener$1;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    .line 98
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mHandler:Landroid/os/Handler;

    .line 99
    return-void
.end method

.method static synthetic access$000(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    .prologue
    .line 17
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsMoving:Z

    return v0
.end method

.method static synthetic access$100(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    .prologue
    .line 17
    invoke-direct {p0}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->deselectIcon()V

    return-void
.end method

.method static synthetic access$300(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)I
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    .prologue
    .line 17
    iget v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mPressDownPos:I

    return v0
.end method

.method static synthetic access$302(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;I)I
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener;
    .param p1, "x1"    # I

    .prologue
    .line 17
    iput p1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mPressDownPos:I

    return p1
.end method

.method static synthetic access$400(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Lse/volvocars/acu/AcuActivity;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    .prologue
    .line 17
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    return-object v0
.end method

.method static synthetic access$500(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    .prologue
    .line 17
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsidePearlView:Z

    return v0
.end method

.method static synthetic access$600(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    .prologue
    .line 17
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsideHomeButton:Z

    return v0
.end method

.method static synthetic access$702(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;Z)Z
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsFlingingToHome:Z

    return p1
.end method

.method static synthetic access$800(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Lse/volvocars/acu/AppHandler;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    .prologue
    .line 17
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;

    return-object v0
.end method

.method static synthetic access$900(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Lse/volvocars/acu/ui/opengl/LauncherGLView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    .prologue
    .line 17
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    return-object v0
.end method

.method private actionCancel()V
    .locals 2

    .prologue
    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsMoving:Z

    .line 187
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setTouchActionState(I)V

    .line 188
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->triggerDeselect:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 189
    invoke-direct {p0}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->deselectIcon()V

    .line 190
    return-void
.end method

.method private actionDown(DD)V
    .locals 4
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    const/4 v3, 0x0

    .line 198
    iput-boolean v3, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mDisableScrolling:Z

    .line 199
    iput-wide p1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mDownX:D

    .line 200
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v0}, Lse/volvocars/acu/AcuActivity;->getCurrentPos()I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mCurrentPosAtDown:I

    .line 201
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->pearlRect:Landroid/graphics/Rect;

    double-to-int v1, p1

    double-to-int v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    iput-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsidePearlView:Z

    .line 202
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->returnToHomeRect:Landroid/graphics/Rect;

    double-to-int v1, p1

    double-to-int v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    iput-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsideHomeButton:Z

    .line 203
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    iget-boolean v1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsidePearlView:Z

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setInsidePearlViewStatus(Z)V

    .line 204
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, v3}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setTouchActionState(I)V

    .line 205
    double-to-int v0, p1

    double-to-int v1, p3

    invoke-direct {p0, v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->processSelectionStatus(II)V

    .line 208
    invoke-direct {p0, p1, p2, p3, p4}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->isAppPressed(DD)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsideHomeButton:Z

    if-eqz v0, :cond_1

    .line 209
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mDisableScrolling:Z

    .line 212
    :cond_1
    iput-boolean v3, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsFlingingToHome:Z

    .line 213
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->triggerDeselect:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 214
    return-void
.end method

.method private deselectIcon()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 193
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setSelectedStatus(I)V

    .line 194
    iput v1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedAppIndex:I

    .line 195
    return-void
.end method

.method private goHomeIfHomePressed(DDI)Z
    .locals 4
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "downTime"    # I

    .prologue
    const/4 v3, 0x0

    .line 224
    const/16 v0, 0x7d0

    if-ge p5, v0, :cond_0

    const/4 v0, 0x5

    if-le p5, v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->returnToHomeRect:Landroid/graphics/Rect;

    double-to-int v1, p1

    double-to-int v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsideHomeButton:Z

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v0, v3}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    .line 232
    const/4 v0, 0x1

    .line 234
    :goto_0
    return v0

    :cond_0
    move v0, v3

    goto :goto_0
.end method

.method private isAppPressed(DD)Z
    .locals 9
    .param p1, "xPos"    # D
    .param p3, "yPos"    # D

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 318
    move-wide v0, p1

    .line 319
    .local v0, "x":D
    move-wide v2, p3

    .line 322
    .local v2, "y":D
    iget-object v4, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxLeft:Landroid/graphics/Rect;

    double-to-int v5, v0

    double-to-int v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 323
    iget-object v4, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v4, v7}, Lse/volvocars/acu/AppHandler;->getSelectedApplication(I)Lse/volvocars/acu/model/AppInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    move v4, v8

    .line 331
    :goto_0
    return v4

    :cond_0
    move v4, v7

    .line 323
    goto :goto_0

    .line 325
    :cond_1
    iget-object v4, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxMiddle:Landroid/graphics/Rect;

    double-to-int v5, v0

    double-to-int v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 326
    iget-object v4, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v4, v8}, Lse/volvocars/acu/AppHandler;->getSelectedApplication(I)Lse/volvocars/acu/model/AppInfo;

    move-result-object v4

    if-eqz v4, :cond_2

    move v4, v8

    goto :goto_0

    :cond_2
    move v4, v7

    goto :goto_0

    .line 328
    :cond_3
    iget-object v4, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxRight:Landroid/graphics/Rect;

    double-to-int v5, v0

    double-to-int v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 329
    iget-object v4, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lse/volvocars/acu/AppHandler;->getSelectedApplication(I)Lse/volvocars/acu/model/AppInfo;

    move-result-object v4

    if-eqz v4, :cond_4

    move v4, v8

    goto :goto_0

    :cond_4
    move v4, v7

    goto :goto_0

    :cond_5
    move v4, v7

    .line 331
    goto :goto_0
.end method

.method private moveAction(ZD)V
    .locals 6
    .param p1, "insidePearlArea"    # Z
    .param p2, "x"    # D

    .prologue
    const/4 v4, 0x2

    .line 276
    iget-object v2, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v2, v4}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setTouchActionState(I)V

    .line 278
    iget-wide v2, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mDownX:D

    sub-double v0, p2, v2

    .line 279
    .local v0, "diff":D
    if-nez p1, :cond_1

    .line 280
    iget-object v2, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    iget v3, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mCurrentPosAtDown:I

    int-to-double v3, v3

    sub-double/2addr v3, v0

    double-to-int v3, v3

    invoke-virtual {v2, v3}, Lse/volvocars/acu/AcuActivity;->setCurrentPos(I)V

    .line 283
    iget v2, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedAppIndex:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x403e000000000000L    # 30.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 284
    invoke-direct {p0}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->deselectIcon()V

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-object v2, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    double-to-int v3, p2

    invoke-virtual {v2, v3}, Lse/volvocars/acu/AcuActivity;->setPearlPos(I)I

    .line 289
    iget-object v2, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v2, v4}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setTouchActionState(I)V

    goto :goto_0
.end method

.method private processSelectionStatus(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 300
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxLeft:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AppHandler;->setSelectedApp(I)V

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxMiddle:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 304
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AppHandler;->setSelectedApp(I)V

    goto :goto_0

    .line 306
    :cond_2
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxRight:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AppHandler;->setSelectedApp(I)V

    goto :goto_0
.end method

.method private startAppIfAppPressed(DDI)Z
    .locals 6
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "downTime"    # I

    .prologue
    const/4 v4, 0x0

    const-string v5, "LauncherTouchListener"

    .line 245
    const/16 v0, 0x7d0

    if-ge p5, v0, :cond_3

    const/4 v0, 0x5

    if-le p5, v0, :cond_3

    iget-wide v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mDownX:D

    sub-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_3

    .line 248
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxLeft:Landroid/graphics/Rect;

    double-to-int v1, p1

    double-to-int v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v0, v4}, Lse/volvocars/acu/AppHandler;->startAppInHitbox(I)Z

    move-result v0

    .line 265
    :goto_0
    return v0

    .line 252
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxMiddle:Landroid/graphics/Rect;

    double-to-int v1, p1

    double-to-int v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AppHandler;->startAppInHitbox(I)Z

    move-result v0

    goto :goto_0

    .line 255
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->hitBoxRight:Landroid/graphics/Rect;

    double-to-int v1, p1

    double-to-int v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AppHandler;->startAppInHitbox(I)Z

    move-result v0

    goto :goto_0

    .line 259
    :cond_2
    const-string v0, "LauncherTouchListener"

    const-string v0, "Won\'t start app as no app is pressed."

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move v0, v4

    .line 265
    goto :goto_0

    .line 263
    :cond_3
    const-string v0, "LauncherTouchListener"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Won\'t start app, downtime: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public isStartingApp()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsStartingApp:Z

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 113
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsStartingApp:Z

    if-eqz v0, :cond_0

    move v0, v12

    .line 182
    :goto_0
    return v0

    .line 118
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-double v1, v0

    .line 119
    .local v1, "x":D
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-double v3, v0

    .line 120
    .local v3, "y":D
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v7

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    sub-long/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(J)J

    move-result-wide v7

    long-to-int v5, v7

    .line 122
    .local v5, "downTime":I
    const/4 v6, 0x0

    .line 125
    .local v6, "goHome":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 179
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsStartingApp:Z

    if-nez v0, :cond_2

    if-nez v6, :cond_2

    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsidePearlView:Z

    if-nez v0, :cond_2

    .line 180
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_2
    move v0, v12

    .line 182
    goto :goto_0

    .line 127
    :pswitch_0
    invoke-direct {p0, v1, v2, v3, v4}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->actionDown(DD)V

    .line 128
    iput-boolean v12, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsMoving:Z

    goto :goto_1

    .line 132
    :pswitch_1
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsFlingingToHome:Z

    if-eqz v0, :cond_3

    move v0, v11

    .line 133
    goto :goto_0

    .line 137
    :cond_3
    iget-wide v7, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mDownX:D

    sub-double/2addr v7, v1

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(D)D

    move-result-wide v7

    const-wide/high16 v9, 0x4049000000000000L    # 50.0

    cmpl-double v0, v7, v9

    if-lez v0, :cond_4

    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mDisableScrolling:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsideHomeButton:Z

    if-nez v0, :cond_4

    .line 138
    iput-boolean v11, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mDisableScrolling:Z

    .line 139
    invoke-direct {p0}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->deselectIcon()V

    .line 142
    :cond_4
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mDisableScrolling:Z

    if-nez v0, :cond_5

    .line 143
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsidePearlView:Z

    invoke-direct {p0, v0, v1, v2}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->moveAction(ZD)V

    .line 146
    :cond_5
    const/16 v0, 0x7d0

    if-le v5, v0, :cond_6

    .line 147
    invoke-direct {p0}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->deselectIcon()V

    .line 150
    :cond_6
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsideHomeButton:Z

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->returnToHomeRect:Landroid/graphics/Rect;

    double-to-int v7, v1

    double-to-int v8, v3

    invoke-virtual {v0, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    iput-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsideHomeButton:Z

    goto :goto_1

    .line 158
    :pswitch_2
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsFlingingToHome:Z

    if-eqz v0, :cond_7

    move v0, v11

    .line 159
    goto :goto_0

    :cond_7
    move-object v0, p0

    .line 162
    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->goHomeIfHomePressed(DDI)Z

    move-result v6

    .line 165
    iget-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mDisableScrolling:Z

    if-eqz v0, :cond_8

    move-object v0, p0

    .line 166
    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->startAppIfAppPressed(DDI)Z

    move-result v0

    iput-boolean v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsStartingApp:Z

    .line 174
    :cond_8
    :pswitch_3
    invoke-direct {p0}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->actionCancel()V

    goto :goto_1

    .line 125
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setStartingApp(Z)V
    .locals 0
    .param p1, "starting"    # Z

    .prologue
    .line 102
    iput-boolean p1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsStartingApp:Z

    .line 104
    return-void
.end method
