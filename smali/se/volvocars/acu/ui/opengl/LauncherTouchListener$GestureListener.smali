.class Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;
.super Ljava/lang/Object;
.source "LauncherTouchListener.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/ui/opengl/LauncherTouchListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GestureListener"
.end annotation


# static fields
.field private static final ALWAYS_FLING_TO_HOME_THRESHOLD:I = -0x1f4

.field private static final FLING_MIN_DISTANCE:I = 0xc8

.field private static final FLING_THRESHOLD_VELOCITY:I = 0x1f4

.field private static final MAX_VELOCITY:I = 0x1194

.field private static final MOVE_TO_NEXT_MAX_DISTANCE:I = 0x1f4

.field private static final SWIPE_MIN_DISTANCE:I = 0x64

.field private static final SWIPE_THRESHOLD_VELOCITY:I = 0x1f4


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)V
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;Lse/volvocars/acu/ui/opengl/LauncherTouchListener$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener;
    .param p2, "x1"    # Lse/volvocars/acu/ui/opengl/LauncherTouchListener$1;

    .prologue
    .line 338
    invoke-direct {p0, p1}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;-><init>(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 351
    iget-object v0, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    iget-object v1, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    # getter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;
    invoke-static {v1}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$400(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Lse/volvocars/acu/AcuActivity;

    move-result-object v1

    invoke-virtual {v1}, Lse/volvocars/acu/AcuActivity;->getCurrentPos()I

    move-result v1

    # setter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mPressDownPos:I
    invoke-static {v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$302(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;I)I

    .line 352
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 359
    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    # getter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsidePearlView:Z
    invoke-static {v5}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$500(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    # getter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mClickedInsideHomeButton:Z
    invoke-static {v5}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$600(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 360
    :cond_0
    const/4 v5, 0x0

    .line 403
    :goto_0
    return v5

    .line 363
    :cond_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 364
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 366
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    sub-float v1, v5, v6

    .line 367
    .local v1, "diff":F
    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    # getter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;
    invoke-static {v5}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$400(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Lse/volvocars/acu/AcuActivity;

    move-result-object v5

    invoke-virtual {v5}, Lse/volvocars/acu/AcuActivity;->getCurrentPos()I

    move-result v0

    .line 368
    .local v0, "currentPos":I
    float-to-int v5, v1

    sub-int v3, v0, v5

    .line 369
    .local v3, "flingStartPos":I
    const/4 v5, 0x0

    cmpl-float v5, v1, v5

    if-lez v5, :cond_4

    const/4 v5, -0x1

    move v2, v5

    .line 370
    .local v2, "direction":I
    :goto_1
    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    const/4 v6, 0x0

    # setter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsFlingingToHome:Z
    invoke-static {v5, v6}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$702(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;Z)Z

    .line 375
    const/16 v5, -0x1f4

    if-ge v0, v5, :cond_5

    const/4 v5, 0x0

    cmpg-float v5, v1, v5

    if-gez v5, :cond_5

    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    # getter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mPressDownPos:I
    invoke-static {v5}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$300(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)I

    move-result v5

    const/16 v6, -0x170

    if-gt v5, v6, :cond_5

    .line 377
    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    # getter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;
    invoke-static {v5}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$400(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Lse/volvocars/acu/AcuActivity;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    .line 378
    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    const/4 v6, 0x1

    # setter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsFlingingToHome:Z
    invoke-static {v5, v6}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$702(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;Z)Z

    .line 379
    const/4 v5, 0x1

    goto :goto_0

    .line 369
    .end local v2    # "direction":I
    :cond_4
    const/4 v5, 0x1

    move v2, v5

    goto :goto_1

    .line 383
    .restart local v2    # "direction":I
    :cond_5
    neg-float v5, v1

    const/high16 v6, 0x42c80000    # 100.0f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_6

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x43fa0000    # 500.0f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_6

    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    # getter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;
    invoke-static {v5}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$800(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Lse/volvocars/acu/AppHandler;

    move-result-object v5

    invoke-virtual {v5, v3}, Lse/volvocars/acu/AppHandler;->isLeftmostAppCenteredAtScreenPosition(I)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 386
    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    # getter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAcuActivity:Lse/volvocars/acu/AcuActivity;
    invoke-static {v5}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$400(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Lse/volvocars/acu/AcuActivity;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    .line 387
    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    const/4 v6, 0x1

    # setter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mIsFlingingToHome:Z
    invoke-static {v5, v6}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$702(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;Z)Z

    .line 388
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 390
    :cond_6
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x43480000    # 200.0f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_8

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x43fa0000    # 500.0f

    cmpl-float v5, v5, v6

    if-lez v5, :cond_8

    .line 391
    const v5, 0x458ca000    # 4500.0f

    cmpg-float v5, p3, v5

    if-gez v5, :cond_7

    move v5, p3

    :goto_2
    float-to-int v4, v5

    .line 392
    .local v4, "velocity":I
    int-to-float v5, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    div-int/lit16 v4, v5, 0x1f4

    .line 393
    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    # getter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;
    invoke-static {v5}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$900(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Lse/volvocars/acu/ui/opengl/LauncherGLView;

    move-result-object v5

    invoke-virtual {v5, v4}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->startFling(I)V

    .line 395
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 391
    .end local v4    # "velocity":I
    :cond_7
    const v5, 0x458ca000    # 4500.0f

    goto :goto_2

    .line 398
    :cond_8
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const/high16 v6, 0x43fa0000    # 500.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_9

    .line 400
    iget-object v5, p0, Lse/volvocars/acu/ui/opengl/LauncherTouchListener$GestureListener;->this$0:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    # getter for: Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->mAppHandler:Lse/volvocars/acu/AppHandler;
    invoke-static {v5}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->access$800(Lse/volvocars/acu/ui/opengl/LauncherTouchListener;)Lse/volvocars/acu/AppHandler;

    move-result-object v5

    neg-int v6, v2

    invoke-virtual {v5, v6, v3}, Lse/volvocars/acu/AppHandler;->scrollToNextIconInDirectionWhenScreenPosWas(II)V

    .line 403
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 408
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 412
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 417
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 421
    const/4 v0, 0x0

    return v0
.end method
