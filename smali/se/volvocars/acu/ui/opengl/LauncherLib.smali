.class final Lse/volvocars/acu/ui/opengl/LauncherLib;
.super Ljava/lang/Object;
.source "LauncherLib.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "launcherjni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 14
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native addGraphics(IFFFFFF)V
.end method

.method public static native addIcon(IFFFFF)V
.end method

.method public static native createTextureFromPNG(Ljava/lang/String;Ljava/lang/String;I)Z
.end method

.method public static native destroy()V
.end method

.method public static native isRendering()Z
.end method

.method public static native launch()V
.end method

.method public static native onSurfaceCreated(III)Z
.end method

.method public static native pause()V
.end method

.method public static native resume()V
.end method

.method public static native scrollToPos(I)I
.end method

.method public static native setAppForbiddenStatus([I)V
.end method

.method public static native setAppIconOrder([I)V
.end method

.method public static native setInsidePearlViewStatus(Z)V
.end method

.method public static native setLineEmitterState(Z)V
.end method

.method public static native setPearlConstants(IIII)V
.end method

.method public static native setPearlPos(I)I
.end method

.method public static native setPlayButtonEmitterState(Z)V
.end method

.method public static native setPos(I)V
.end method

.method public static native setScreenXOffset(I)V
.end method

.method public static native setScreenYOffset(I)V
.end method

.method public static native setSelectedStatus(I)V
.end method

.method public static native setState(I)V
.end method

.method public static native setTouchActionState(I)V
.end method

.method public static native startFling(I)V
.end method

.method public static native step(I)I
.end method
