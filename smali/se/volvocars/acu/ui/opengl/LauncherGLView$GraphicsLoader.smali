.class final Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;
.super Ljava/lang/Object;
.source "LauncherGLView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/ui/opengl/LauncherGLView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "GraphicsLoader"
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/ui/opengl/LauncherGLView;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;->this$0:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;Lse/volvocars/acu/ui/opengl/LauncherGLView$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/ui/opengl/LauncherGLView;
    .param p2, "x1"    # Lse/volvocars/acu/ui/opengl/LauncherGLView$1;

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;-><init>(Lse/volvocars/acu/ui/opengl/LauncherGLView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 20

    .prologue
    .line 151
    const/high16 v16, 0x3e000000    # 0.125f

    .line 152
    .local v16, "iconSize":F
    const/16 v18, 0x1f

    .line 153
    .local v18, "numberOfIcons":I
    const/16 v19, 0x4

    .line 154
    .local v19, "rows":I
    const/16 v14, 0x8

    .line 156
    .local v14, "columns":I
    const/4 v4, 0x0

    .line 157
    .local v4, "x":F
    const/4 v5, 0x0

    .line 158
    .local v5, "y":F
    const/4 v3, 0x0

    .line 160
    .local v3, "index":I
    const/4 v15, 0x1

    .local v15, "i":I
    :goto_0
    const/4 v2, 0x4

    if-gt v15, v2, :cond_1

    .line 161
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v5, 0x3e000000    # 0.125f

    int-to-float v6, v15

    mul-float/2addr v5, v6

    sub-float v5, v2, v5

    .line 163
    const/16 v17, 0x0

    .local v17, "j":I
    :goto_1
    const/16 v2, 0x8

    move/from16 v0, v17

    move v1, v2

    if-ge v0, v1, :cond_0

    .line 164
    const/16 v2, 0x1f

    if-gt v3, v2, :cond_0

    .line 165
    const/high16 v2, 0x3e000000    # 0.125f

    move/from16 v0, v17

    int-to-float v0, v0

    move v4, v0

    mul-float/2addr v4, v2

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;->this$0:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    move-object v2, v0

    const/high16 v6, 0x3e000000    # 0.125f

    const/high16 v7, 0x43000000    # 128.0f

    const/high16 v8, 0x43000000    # 128.0f

    invoke-virtual/range {v2 .. v8}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->addIcon(IFFFFF)V

    .line 168
    add-int/lit8 v3, v3, 0x1

    .line 163
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 160
    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 177
    .end local v17    # "j":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;->this$0:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    move-object v6, v0

    const/4 v7, 0x0

    const v8, 0x3d4ffeb0    # 0.05078f

    const/4 v9, 0x0

    const v10, 0x3d4ffeb0    # 0.05078f

    const v11, 0x3d4ffeb0    # 0.05078f

    const/high16 v12, 0x41800000    # 16.0f

    const/high16 v13, 0x41800000    # 16.0f

    invoke-virtual/range {v6 .. v13}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->addGraphics(IFFFFFF)V

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;->this$0:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    move-object v6, v0

    const/4 v7, 0x1

    const v8, 0x3ecf7f8d

    const v9, 0x3aff9700    # 0.0019499958f

    const v10, 0x3e8f7f8d

    const v11, 0x3e8d805e

    const v12, 0x438f8000    # 287.0f

    const v13, 0x438d8000    # 283.0f

    invoke-virtual/range {v6 .. v13}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->addGraphics(IFFFFFF)V

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;->this$0:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    move-object v6, v0

    const/4 v7, 0x2

    const/high16 v8, 0x3e800000    # 0.25f

    const v9, 0x3e01ff30

    const v10, 0x3e1e00d2    # 0.1543f

    const v11, 0x3e1e00d2    # 0.1543f

    const/high16 v12, 0x431e0000    # 158.0f

    const/high16 v13, 0x431e0000    # 158.0f

    invoke-virtual/range {v6 .. v13}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->addGraphics(IFFFFFF)V

    .line 180
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;->this$0:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    move-object v6, v0

    const/4 v7, 0x3

    const v8, 0x3dcffeb0    # 0.10156f

    const/4 v9, 0x0

    const v10, 0x3d4ffeb0    # 0.05078f

    const v11, 0x3d4ffeb0    # 0.05078f

    const/high16 v12, 0x41800000    # 16.0f

    const/high16 v13, 0x41800000    # 16.0f

    invoke-virtual/range {v6 .. v13}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->addGraphics(IFFFFFF)V

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;->this$0:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    move-object v6, v0

    const/4 v7, 0x4

    const/4 v8, 0x0

    const/high16 v9, 0x3e200000    # 0.15625f

    const/high16 v10, 0x3e000000    # 0.125f

    const/high16 v11, 0x3e000000    # 0.125f

    const/high16 v12, 0x43000000    # 128.0f

    const/high16 v13, 0x43000000    # 128.0f

    invoke-virtual/range {v6 .. v13}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->addGraphics(IFFFFFF)V

    .line 182
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;->this$0:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    move-object v6, v0

    const/4 v7, 0x5

    const/high16 v8, 0x3f700000    # 0.9375f

    const/4 v9, 0x0

    const/high16 v10, 0x3d800000    # 0.0625f

    const v11, 0x3aff9724    # 0.00195f

    const/high16 v12, 0x42800000    # 64.0f

    const/high16 v13, 0x43f00000    # 480.0f

    invoke-virtual/range {v6 .. v13}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->addGraphics(IFFFFFF)V

    .line 183
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;->this$0:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    move-object v6, v0

    const/4 v7, 0x6

    const/high16 v8, 0x3f700000    # 0.9375f

    const v9, 0x3b400500

    const/high16 v10, 0x3d800000    # 0.0625f

    const v11, 0x3aff9724    # 0.00195f

    const/high16 v12, 0x42800000    # 64.0f

    const/high16 v13, 0x43f00000    # 480.0f

    invoke-virtual/range {v6 .. v13}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->addGraphics(IFFFFFF)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/ui/opengl/LauncherGLView$GraphicsLoader;->this$0:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    move-object v6, v0

    const/4 v7, 0x7

    const v8, 0x3e4f0069    # 0.20215f

    const/4 v9, 0x0

    const v10, 0x3d4801f7    # 0.04883f

    const v11, 0x3d4801f7    # 0.04883f

    const/high16 v12, 0x41800000    # 16.0f

    const/high16 v13, 0x41800000    # 16.0f

    invoke-virtual/range {v6 .. v13}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->addGraphics(IFFFFFF)V

    .line 186
    return-void
.end method
