.class final Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;
.super Ljava/lang/Object;
.source "HomeView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/ui/HomeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "WeatherWidgetUpdater"
.end annotation


# instance fields
.field private mUiHandler:Landroid/os/Handler;

.field private final mWidget:Lse/volvocars/acu/weather/WeatherWidget;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/weather/WeatherWidget;Landroid/os/Handler;)V
    .locals 2
    .param p1, "widget"    # Lse/volvocars/acu/weather/WeatherWidget;
    .param p2, "uiHandler"    # Landroid/os/Handler;

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    if-nez p1, :cond_0

    .line 185
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Widget cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    iput-object p1, p0, Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    .line 188
    iput-object p2, p0, Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;->mUiHandler:Landroid/os/Handler;

    .line 189
    return-void
.end method

.method static synthetic access$000(Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;)Lse/volvocars/acu/weather/WeatherWidget;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;

    .prologue
    .line 178
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;->mUiHandler:Landroid/os/Handler;

    new-instance v1, Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater$1;

    invoke-direct {v1, p0}, Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater$1;-><init>(Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 200
    return-void
.end method
