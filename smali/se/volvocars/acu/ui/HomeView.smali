.class public Lse/volvocars/acu/ui/HomeView;
.super Landroid/widget/FrameLayout;
.source "HomeView.java"

# interfaces
.implements Lse/volvocars/acu/ui/ArrowAnimationListener;


# annotations
#.annotation system Ldalvik/annotation/MemberClasses;
#    value = {
#        Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;
#    }
#.end annotation


# static fields
.field private static final GPS_MINIMUM_DISTANCE_IN_M:I = 0x1388

.field private static final GPS_MINIMUM_INTERVAL_IN_MS:I = 0x493e0

.field private static final TAG:Ljava/lang/String; = "HomeScreenFragment"

.field private static final WEATHER_UPDATES_IN_MINUTES:I = 0x3c


# instance fields
#.field private connectionChangeReceiver:Lse/volvocars/acu/ui/ConnectionChangeReceiver;

.field private mArrowAnimation:Lse/volvocars/acu/ui/ArrowAnimationView;

.field private mClockWidget:Lse/volvocars/acu/clock/ClockWidget;

.field private mContentVisible:Z

.field private final mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

.field private final mHandler:Landroid/os/Handler;

.field private mIdle:Z

.field private mLauncherGlView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

.field private mLocationManager:Landroid/location/LocationManager;

.field private mMediaWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

#.field private mWeatherFuture:Ljava/util/concurrent/ScheduledFuture;
#    .annotation system Ldalvik/annotation/Signature;
#        value = {
#            "Ljava/util/concurrent/ScheduledFuture",
#            "<*>;"
#        }
#    .end annotation
#.end field

#.field private mWeatherListener:Lse/volvocars/acu/weather/WeatherLocationListener;

#.field private final mWeatherLock:Ljava/lang/Object;

#.field private mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 39
    new-instance v0, Ljava/lang/Object;

    #invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    #iput-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherLock:Ljava/lang/Object;

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lse/volvocars/acu/ui/HomeView;->mContentVisible:Z

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mHandler:Landroid/os/Handler;

    .line 64
    return-void
.end method

.method static synthetic access$100(Lse/volvocars/acu/ui/HomeView;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/ui/HomeView;

    .prologue
    .line 30
    iget-boolean v0, p0, Lse/volvocars/acu/ui/HomeView;->mContentVisible:Z

    return v0
.end method

.method private initArrowAnimation()V
    .locals 1

    .prologue
    .line 133
    const v0, 0x7f070016

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/ui/ArrowAnimationView;

    iput-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mArrowAnimation:Lse/volvocars/acu/ui/ArrowAnimationView;

    .line 134
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mArrowAnimation:Lse/volvocars/acu/ui/ArrowAnimationView;

    invoke-virtual {v0, p0}, Lse/volvocars/acu/ui/ArrowAnimationView;->addArrowAnimationListener(Lse/volvocars/acu/ui/ArrowAnimationListener;)V

    .line 135
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mArrowAnimation:Lse/volvocars/acu/ui/ArrowAnimationView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/ArrowAnimationView;->playAnimation()V

    .line 136
    return-void
.end method

.method private initClockWidget()V
    .locals 3

    .prologue
    .line 166
    const v2, 0x7f070011

    invoke-virtual {p0, v2}, Lse/volvocars/acu/ui/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 167
    .local v1, "timeText":Landroid/widget/TextView;
    const-string v2, "2:32"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    const v2, 0x7f070012

    invoke-virtual {p0, v2}, Lse/volvocars/acu/ui/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 171
    .local v0, "timeFormatText":Landroid/widget/TextView;
    const-string v2, "PM"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    return-void
.end method


#.method private initWeatherWidget()V
#    .locals 2
#
#    .prologue
#    .line 156
#    const v0, 0x7f07005d
#
#    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/HomeView;->findViewById(I)Landroid/view/View;
#
#    move-result-object v0
#
#    check-cast v0, Lse/volvocars/acu/weather/WeatherWidget;
#
#    iput-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;
#
#    .line 157
#    new-instance v0, Lse/volvocars/acu/weather/WeatherLocationListener;
#
#    iget-object v1, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;
#
#    invoke-direct {v0, v1}, Lse/volvocars/acu/weather/WeatherLocationListener;-><init>(Lse/volvocars/acu/weather/WeatherWidget;)V
#
#    iput-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherListener:Lse/volvocars/acu/weather/WeatherLocationListener;
#
#    .line 158
#    invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->startGpsUpdates()V
#
#    .line 159
#    invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->startWeatherUpdater()V
#
#    .line 160
#    new-instance v0, Lse/volvocars/acu/ui/ConnectionChangeReceiver;
#
#    iget-object v1, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;
#
#    invoke-direct {v0, v1}, Lse/volvocars/acu/ui/ConnectionChangeReceiver;-><init>(Lse/volvocars/acu/weather/WeatherWidget;)V
#
#    iput-object v0, p0, Lse/volvocars/acu/ui/HomeView;->connectionChangeReceiver:Lse/volvocars/acu/ui/ConnectionChangeReceiver;
#
#    .line 161
#    invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->startConnectionReceiver()V
#
#    .line 162
#    return-void
#.end method

.method private setLineEmitterState(Z)V
    .locals 1
    .param p1, "run"    # Z

    .prologue
    .line 89
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mLauncherGlView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mLauncherGlView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setLineEmitterState(Z)V

    .line 92
    :cond_0
    return-void
.end method

#.method private startConnectionReceiver()V
#    .locals 4
#
#    .prologue
#    .line 125
#    invoke-virtual {p0}, Lse/volvocars/acu/ui/HomeView;->getContext()Landroid/content/Context;
#
#    move-result-object v0
#
#    iget-object v1, p0, Lse/volvocars/acu/ui/HomeView;->connectionChangeReceiver:Lse/volvocars/acu/ui/ConnectionChangeReceiver;
#
#    new-instance v2, Landroid/content/IntentFilter;
#
#    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"
#
#    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V
#
#    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
#
#    .line 126
#    return-void
#.end method

#.method private startGpsUpdates()V
#    .locals 6
#
#    .prologue
#    .line 120
#    invoke-virtual {p0}, Lse/volvocars/acu/ui/HomeView;->getContext()Landroid/content/Context;
#
#    move-result-object v0
#
#    const-string v1, "location"
#
#    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
#
#    move-result-object v0
#
#    check-cast v0, Landroid/location/LocationManager;
#
#    iput-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mLocationManager:Landroid/location/LocationManager;
#
#    .line 121
#    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mLocationManager:Landroid/location/LocationManager;
#
#    const-string v1, "gps"
#
#    const-wide/32 v2, 0x493e0
#
#    const v4, 0x459c4000    # 5000.0f
#
#    #iget-object v5, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherListener:Lse/volvocars/acu/weather/WeatherLocationListener;
#
#    #invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
#
#    .line 122
#    return-void
#.end method

.method private startLineEmitter()V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lse/volvocars/acu/ui/HomeView;->setLineEmitterState(Z)V

    .line 82
    return-void
.end method

#.method private startWeatherUpdater()V
#    .locals 9
#
#    .prologue
#    .line 95
#    iget-object v8, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherLock:Ljava/lang/Object;
#
#    monitor-enter v8
#
#    .line 96
#    :try_start_0
#    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherFuture:Ljava/util/concurrent/ScheduledFuture;
#
#    if-nez v0, :cond_1
#
#    .line 97
#    const v0, 0x7f07005d
#
#    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/HomeView;->findViewById(I)Landroid/view/View;
#
#    move-result-object v7
#
#    check-cast v7, Lse/volvocars/acu/weather/WeatherWidget;
#
#    .line 98
#    .local v7, "weather":Lse/volvocars/acu/weather/WeatherWidget;
#    if-nez v7, :cond_0
#
#    .line 99
#    monitor-exit v8
#
#    .line 104
#    .end local v7    # "weather":Lse/volvocars/acu/weather/WeatherWidget;
#    :goto_0
#    return-void
#
#    .line 101
#    .restart local v7    # "weather":Lse/volvocars/acu/weather/WeatherWidget;
#    :cond_0
#    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mExecutor:Ljava/util/concurrent/ScheduledExecutorService;
#
#    new-instance v1, Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;
#
#    iget-object v2, p0, Lse/volvocars/acu/ui/HomeView;->mHandler:Landroid/os/Handler;
#
#    invoke-direct {v1, v7, v2}, Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;-><init>(Lse/volvocars/acu/weather/WeatherWidget;Landroid/os/Handler;)V
#
#    const-wide/16 v2, 0x0
#
#    const-wide/16 v4, 0x3c
#
#    sget-object v6, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;
#
#    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
#
#    move-result-object v0
#
#    iput-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherFuture:Ljava/util/concurrent/ScheduledFuture;
#
#    .line 103
#    .end local v7    # "weather":Lse/volvocars/acu/weather/WeatherWidget;
#    :cond_1
#    monitor-exit v8
#
#    goto :goto_0
#
#    :catchall_0
#    move-exception v0
#
#    monitor-exit v8
#    :try_end_0
#    .catchall {:try_start_0 .. :try_end_0} :catchall_0
#
#    throw v0
#.end method

#.method private stopConnectionReceiver()V
#    .locals 2
#
#    .prologue
#    .line 129
#    invoke-virtual {p0}, Lse/volvocars/acu/ui/HomeView;->getContext()Landroid/content/Context;
#
#    move-result-object v0
#
#    iget-object v1, p0, Lse/volvocars/acu/ui/HomeView;->connectionChangeReceiver:Lse/volvocars/acu/ui/ConnectionChangeReceiver;
#
#    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
#
#    .line 130
#    return-void
#.end method

#.method private stopGpsUpdates()V
#    .locals 2
#
#    .prologue
#    .line 116
#    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mLocationManager:Landroid/location/LocationManager;
#
#    iget-object v1, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherListener:Lse/volvocars/acu/weather/WeatherLocationListener;
#
#    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
#
#    .line 117
#    return-void
#.end method

.method private stopLineEmitter()V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lse/volvocars/acu/ui/HomeView;->setLineEmitterState(Z)V

    .line 86
    return-void
.end method

#.method private stopWeatherUpdater()V
#    .locals 3
#
#    .prologue
#    .line 107
#    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherLock:Ljava/lang/Object;
#
#    monitor-enter v0
#
#    .line 108
#    :try_start_0
#    iget-object v1, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherFuture:Ljava/util/concurrent/ScheduledFuture;
#
#    if-eqz v1, :cond_0
#
#    .line 109
#    iget-object v1, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherFuture:Ljava/util/concurrent/ScheduledFuture;
#
#    const/4 v2, 0x0
#
#    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z
#
#    .line 110
#    const/4 v1, 0x0
#
#    iput-object v1, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherFuture:Ljava/util/concurrent/ScheduledFuture;
#
#    .line 112
#    :cond_0
#    monitor-exit v0
#
#    .line 113
#    return-void
#
#    .line 112
#    :catchall_0
#    move-exception v1
#
#    monitor-exit v0
#    :try_end_0
#    .catchall {:try_start_0 .. :try_end_0} :catchall_0
#
#    throw v1
#.end method


# virtual methods
.method public activate()V
    .locals 1

    .prologue
    .line 324
    iget-boolean v0, p0, Lse/volvocars/acu/ui/HomeView;->mIdle:Z

    if-nez v0, :cond_0

    .line 325
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mMediaWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->activate()V

    .line 326
    invoke-virtual {p0}, Lse/volvocars/acu/ui/HomeView;->onActivity()V

    .line 329
    :cond_0
    return-void
.end method

.method public arrowAnimationStarted()V
    .locals 0

    .prologue
    .line 350
    return-void
.end method

.method public arrowAnimationStopped()V
    .locals 0

    .prologue
    .line 343
    invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->stopLineEmitter()V

    .line 345
    return-void
.end method

.method public deactivate()V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mMediaWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->deactivate()V

    .line 336
    invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->stopLineEmitter()V

    .line 337
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mArrowAnimation:Lse/volvocars/acu/ui/ArrowAnimationView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/ArrowAnimationView;->stopAnimation()V

    .line 339
    return-void
.end method

.method public onActivity()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mArrowAnimation:Lse/volvocars/acu/ui/ArrowAnimationView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/ArrowAnimationView;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lse/volvocars/acu/ui/HomeView;->mIdle:Z

    if-nez v0, :cond_0

    .line 237
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mArrowAnimation:Lse/volvocars/acu/ui/ArrowAnimationView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/ArrowAnimationView;->playAnimation()V

    .line 243
    :cond_0
    invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->startLineEmitter()V

    .line 244
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 262
    #invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->stopWeatherUpdater()V

    .line 263
    #invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->stopGpsUpdates()V

    .line 264
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mClockWidget:Lse/volvocars/acu/clock/ClockWidget;

    invoke-virtual {v0}, Lse/volvocars/acu/clock/ClockWidget;->onDestroy()V

    .line 265
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mMediaWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->onDestroy()V

    .line 266
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mArrowAnimation:Lse/volvocars/acu/ui/ArrowAnimationView;

    invoke-virtual {v0, p0}, Lse/volvocars/acu/ui/ArrowAnimationView;->removeArrowAnimationListener(Lse/volvocars/acu/ui/ArrowAnimationListener;)V

    .line 267
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 73
    const v0, 0x7f070015

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/clock/ClockWidget;

    iput-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mClockWidget:Lse/volvocars/acu/clock/ClockWidget;

    .line 74
    const v0, 0x7f070017

    invoke-virtual {p0, v0}, Lse/volvocars/acu/ui/HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/mediaWidget/MediaWidget;

    iput-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mMediaWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    .line 75
    invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->initClockWidget()V

    .line 76
    #invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->initWeatherWidget()V

    .line 77
    invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->initArrowAnimation()V

    .line 78
    return-void
.end method

.method public onPause()V
    .locals 5

    .prologue
    .line 273
    #invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->stopWeatherUpdater()V

    .line 274
    #invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->stopGpsUpdates()V

    .line 275
    #invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->stopConnectionReceiver()V

    .line 276
    invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->stopLineEmitter()V

    .line 277
    iget-object v2, p0, Lse/volvocars/acu/ui/HomeView;->mClockWidget:Lse/volvocars/acu/clock/ClockWidget;

    invoke-virtual {v2}, Lse/volvocars/acu/clock/ClockWidget;->onPause()V

    .line 280
# not touching weather widget
#
#    iget-object v2, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;
#
#    invoke-virtual {v2}, Lse/volvocars/acu/weather/WeatherWidget;->getLastKnownGpsPosition()Lse/volvocars/acu/weather/Coordinate;
#
#    move-result-object v2
#
#    if-eqz v2, :cond_0
#
#    .line 281
#    invoke-virtual {p0}, Lse/volvocars/acu/ui/HomeView;->getContext()Landroid/content/Context;
#
#    move-result-object v2
#
#    const-string v3, "weatherPrefs"
#
#    const/4 v4, 0x0
#
#    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
#
#    move-result-object v1
#
#    .line 282
#    .local v1, "settings":Landroid/content/SharedPreferences;
#    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;
#
#    move-result-object v0
#
#    .line 283
#    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
#    const-string v2, "CurrentGpsPosition"
#
#    iget-object v3, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;
#
#    invoke-virtual {v3}, Lse/volvocars/acu/weather/WeatherWidget;->getLastKnownGpsPosition()Lse/volvocars/acu/weather/Coordinate;
#
#    move-result-object v3
#
#    invoke-virtual {v3}, Lse/volvocars/acu/weather/Coordinate;->toString()Ljava/lang/String;
#
#    move-result-object v3
#
#    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
#
#    .line 284
#    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
#
#    .line 286
#    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
#    .end local v1    # "settings":Landroid/content/SharedPreferences;
#    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 14

    .prologue
    const/16 v11, 0x2c

    const/4 v10, 0x0

    .line 292
    #invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->startGpsUpdates()V

    .line 293
    #iget-object v8, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;

    #invoke-virtual {v8}, Lse/volvocars/acu/weather/WeatherWidget;->reloadSettings()V

    .line 294
    #invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->startWeatherUpdater()V

    .line 295
#    invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->startConnectionReceiver()V

    .line 296
    iget-object v8, p0, Lse/volvocars/acu/ui/HomeView;->mClockWidget:Lse/volvocars/acu/clock/ClockWidget;

    invoke-virtual {v8}, Lse/volvocars/acu/clock/ClockWidget;->onResume()V
    return-void

#    .line 301
#    invoke-virtual {p0}, Lse/volvocars/acu/ui/HomeView;->getContext()Landroid/content/Context;
#
#    move-result-object v8
#
#    const-string v9, "location"
#
#    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
#
#    move-result-object v5
#
#    check-cast v5, Landroid/location/LocationManager;
#
#    .line 302
#    .local v5, "manager":Landroid/location/LocationManager;
#    const-string v8, "gps"
#
#    invoke-virtual {v5, v8}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;
#
#    move-result-object v2
#
#    .line 304
#    .local v2, "location":Landroid/location/Location;
#    if-eqz v2, :cond_0
#
#    .line 306
#    invoke-virtual {p0}, Lse/volvocars/acu/ui/HomeView;->getContext()Landroid/content/Context;
#
#    move-result-object v8
#
#    const-string v9, "weatherPrefs"
#
#    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
#
#    move-result-object v7
#
#    .line 307
#    .local v7, "settings":Landroid/content/SharedPreferences;
#    const-string v8, "CurrentGpsPosition"
#
#    const-string v9, ""
#
#    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
#
#    move-result-object v6
#
#    .line 308
#    .local v6, "selectedLocation":Ljava/lang/String;
#    invoke-virtual {v6}, Ljava/lang/String;->length()I
#
#    move-result v8
#
#    if-lez v8, :cond_1
#
#    const-string v8, ","
#
#    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
#
#    move-result v8
#
#    if-eqz v8, :cond_1
#
#    .line 309
#    invoke-virtual {v6, v11}, Ljava/lang/String;->indexOf(I)I
#
#    move-result v8
#
#    invoke-virtual {v6, v10, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;
#
#    move-result-object v8
#
#    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
#
#    move-result-wide v0
#
#    .line 310
#    .local v0, "lat":D
#    invoke-virtual {v6, v11}, Ljava/lang/String;->indexOf(I)I
#
#    move-result v8
#
#    add-int/lit8 v8, v8, 0x1
#
#    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;
#
#    move-result-object v8
#
#    invoke-static {v8}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
#
#    move-result-wide v3
#
#    .line 311
#    .local v3, "lon":D
#    iget-object v8, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;
#
#    new-instance v9, Lse/volvocars/acu/weather/Coordinate;
#
#    invoke-direct {v9, v0, v1, v3, v4}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V
#
#    invoke-virtual {v8, v9}, Lse/volvocars/acu/weather/WeatherWidget;->setCurrentGpsPosition(Lse/volvocars/acu/weather/Coordinate;)V
#
#    .line 318
#    .end local v0    # "lat":D
#    .end local v3    # "lon":D
#    .end local v6    # "selectedLocation":Ljava/lang/String;
#    .end local v7    # "settings":Landroid/content/SharedPreferences;
#    :cond_0
#    :goto_0
#    return-void
#
#    .line 315
#    .restart local v6    # "selectedLocation":Ljava/lang/String;
#    .restart local v7    # "settings":Landroid/content/SharedPreferences;
#    :cond_1
#    iget-object v8, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;
#
#    new-instance v9, Lse/volvocars/acu/weather/Coordinate;
#
#    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D
#
#    move-result-wide v10
#
#    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D
#
#    move-result-wide v12
#
#    invoke-direct {v9, v10, v11, v12, v13}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V
#
#    invoke-virtual {v8, v9}, Lse/volvocars/acu/weather/WeatherWidget;->setCurrentGpsPosition(Lse/volvocars/acu/weather/Coordinate;)V
#
#    goto :goto_0
.end method

.method public onStartDrag()V
    .locals 2

    .prologue
    .line 250
    invoke-direct {p0}, Lse/volvocars/acu/ui/HomeView;->stopLineEmitter()V

    .line 251
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mArrowAnimation:Lse/volvocars/acu/ui/ArrowAnimationView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/ArrowAnimationView;->stopAnimation()V

    .line 252
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mLauncherGlView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setPlayButtonEmitterState(Z)V

    .line 253
    return-void
.end method

.method public releasePressedState()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 142
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mMediaWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPressedState(Z)V

    .line 143
    #iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;

    #invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/WeatherWidget;->setPressedState(Z)V

    .line 144
    return-void
.end method

.method public setArrowVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 356
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mArrowAnimation:Lse/volvocars/acu/ui/ArrowAnimationView;

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/ArrowAnimationView;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mArrowAnimation:Lse/volvocars/acu/ui/ArrowAnimationView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/ArrowAnimationView;->invalidate()V

    .line 358
    return-void

    .line 356
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public setClockVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 364
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mClockWidget:Lse/volvocars/acu/clock/ClockWidget;

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lse/volvocars/acu/clock/ClockWidget;->setVisibility(I)V

    .line 365
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mClockWidget:Lse/volvocars/acu/clock/ClockWidget;

    invoke-virtual {v0}, Lse/volvocars/acu/clock/ClockWidget;->invalidate()V

    .line 366
    return-void

    .line 364
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public setIdleMode(Z)V
    .locals 1
    .param p1, "idle"    # Z

    .prologue
    .line 226
    #iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;

    #invoke-virtual {v0, p1}, Lse/volvocars/acu/weather/WeatherWidget;->setIdleMode(Z)V

    .line 227
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mClockWidget:Lse/volvocars/acu/clock/ClockWidget;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/clock/ClockWidget;->setIdleMode(Z)V

    .line 228
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mMediaWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setIdleMode(Z)V

    .line 229
    iput-boolean p1, p0, Lse/volvocars/acu/ui/HomeView;->mIdle:Z

    .line 230
    return-void
.end method

.method public setLauncherGlView(Lse/volvocars/acu/ui/opengl/LauncherGLView;)V
    .locals 2
    .param p1, "view"    # Lse/volvocars/acu/ui/opengl/LauncherGLView;

    .prologue
    .line 67
    iput-object p1, p0, Lse/volvocars/acu/ui/HomeView;->mLauncherGlView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    .line 68
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mMediaWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    iget-object v1, p0, Lse/volvocars/acu/ui/HomeView;->mLauncherGlView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setLaucherGLView(Lse/volvocars/acu/ui/opengl/LauncherGLView;)V

    .line 69
    return-void
.end method

.method public setStatusbarVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 151

    #iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;

    #invoke-virtual {v0, p1}, Lse/volvocars/acu/weather/WeatherWidget;->setStatusBarVisible(Z)V

    .line 152
    #invoke-virtual {p0}, Lse/volvocars/acu/ui/HomeView;->invalidate()V

    .line 153
    return-void
.end method

.method public setWeatherConditionSymbolVisibility(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 372

    #iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;

    #invoke-virtual {v0, p1}, Lse/volvocars/acu/weather/WeatherWidget;->setWeatherConditionSymbolVisibility(Z)V

    .line 373
    #iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;

    #invoke-virtual {v0}, Lse/volvocars/acu/weather/WeatherWidget;->invalidate()V

    .line 374
    return-void
.end method

.method public showContent(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 208
    iput-boolean p1, p0, Lse/volvocars/acu/ui/HomeView;->mContentVisible:Z

    .line 209
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView;->mHandler:Landroid/os/Handler;

    new-instance v1, Lse/volvocars/acu/ui/HomeView$1;

    invoke-direct {v1, p0}, Lse/volvocars/acu/ui/HomeView$1;-><init>(Lse/volvocars/acu/ui/HomeView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 219
    return-void
.end method
