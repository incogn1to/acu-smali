.class public Lse/volvocars/acu/ui/ConnectionChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ConnectionChangeReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ConnectionChangeReceiver"


# instance fields
.field private final mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/weather/WeatherWidget;)V
    .locals 0
    .param p1, "mWeatherWidget"    # Lse/volvocars/acu/weather/WeatherWidget;

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 32
    iput-object p1, p0, Lse/volvocars/acu/ui/ConnectionChangeReceiver;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;

    .line 33
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-string v4, "ConnectionChangeReceiver"

    .line 37
    const-string v2, "connectivity"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 39
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    if-eqz v1, :cond_0

    .line 40
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 42
    .local v0, "activeNetInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lse/volvocars/acu/ui/ConnectionChangeReceiver;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;

    if-eqz v2, :cond_1

    .line 43
    const-string v2, "ConnectionChangeReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Connection broadcast received: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", triggering weather update"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    iget-object v2, p0, Lse/volvocars/acu/ui/ConnectionChangeReceiver;->mWeatherWidget:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/WeatherWidget;->updateWeather()V

    .line 51
    .end local v0    # "activeNetInfo":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return-void

    .line 48
    .restart local v0    # "activeNetInfo":Landroid/net/NetworkInfo;
    :cond_1
    const-string v2, "ConnectionChangeReceiver"

    const-string v2, "Connection broadcast received: No active network available"

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
