.class final Lse/volvocars/acu/ui/AppNameView$TitleInfo;
.super Ljava/lang/Object;
.source "AppNameView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/ui/AppNameView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TitleInfo"
.end annotation


# instance fields
.field private final mForbidden:I

.field private final mTitle:Ljava/lang/String;

.field private final mWidthCentered:I

.field private final mWidthOriginal:I


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "widthOriginal"    # I
    .param p3, "widthCentered"    # I

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lse/volvocars/acu/ui/AppNameView$TitleInfo;-><init>(Ljava/lang/String;III)V

    .line 81
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "widthOriginal"    # I
    .param p3, "widthCentered"    # I
    .param p4, "forbidden"    # I

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->mTitle:Ljava/lang/String;

    .line 85
    iput p2, p0, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->mWidthOriginal:I

    .line 86
    iput p3, p0, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->mWidthCentered:I

    .line 87
    iput p4, p0, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->mForbidden:I

    .line 88
    return-void
.end method


# virtual methods
.method public getForbidden()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->mForbidden:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getWidthCentered()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->mWidthCentered:I

    return v0
.end method

.method public getWidthOriginal()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->mWidthOriginal:I

    return v0
.end method
