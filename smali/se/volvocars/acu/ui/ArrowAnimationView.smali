.class public Lse/volvocars/acu/ui/ArrowAnimationView;
.super Landroid/view/View;
.source "ArrowAnimationView.java"


# static fields
.field private static final FRAME_DURATION:I = 0x32

.field private static final FRAME_JUMP:I = 0x1

.field private static final NUMBER_OF_FRAMES:I = 0x1f

.field private static final REPEAT_COUNT:I = 0x3

.field private static final RESOURCE_PREFIX:Ljava/lang/String; = "launcher_arrow_step"

.field private static final TAG:Ljava/lang/String; = "ArrowAnimationView"


# instance fields
.field private currentFrame:I

.field private iterationCounter:I

.field private lastTick:J

.field private mBitmapList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mIsPlaying:Z

.field private mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/ui/ArrowAnimationListener;",
            ">;"
        }
    .end annotation
.end field

.field private mStartPlaying:Z

.field private final mbd:Landroid/graphics/drawable/BitmapDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mContext:Landroid/content/Context;

    .line 34
    iput-boolean v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mIsPlaying:Z

    .line 35
    iput-boolean v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mStartPlaying:Z

    .line 36
    iput v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->currentFrame:I

    .line 37
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->lastTick:J

    .line 38
    iput v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->iterationCounter:I

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mListeners:Ljava/util/List;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mBitmapList:Ljava/util/List;

    .line 49
    iput-object p1, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mContext:Landroid/content/Context;

    .line 50
    invoke-direct {p0}, Lse/volvocars/acu/ui/ArrowAnimationView;->loadAnimation()V

    .line 51
    iget-object v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mbd:Landroid/graphics/drawable/BitmapDrawable;

    .line 52
    return-void
.end method

.method private loadAnimation()V
    .locals 7

    .prologue
    .line 164
    iget-object v4, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mBitmapList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 166
    const/4 v2, 0x0

    .line 167
    .local v2, "resId":I
    const/4 v0, 0x0

    .line 169
    .local v0, "bd":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    const/16 v4, 0x1f

    if-gt v1, v4, :cond_0

    .line 170
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "launcher_arrow_step"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 171
    .local v3, "resourceName":Ljava/lang/String;
    iget-object v4, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "drawable"

    iget-object v6, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 172
    iget-object v4, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .end local v0    # "bd":Landroid/graphics/drawable/BitmapDrawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 173
    .restart local v0    # "bd":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v4, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mBitmapList:Ljava/util/List;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 177
    .end local v3    # "resourceName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private sendAnimationStarted()V
    .locals 4

    .prologue
    .line 71
    iget-object v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/ui/ArrowAnimationListener;

    .line 72
    .local v1, "listener":Lse/volvocars/acu/ui/ArrowAnimationListener;
    if-eqz v1, :cond_0

    .line 73
    invoke-interface {v1}, Lse/volvocars/acu/ui/ArrowAnimationListener;->arrowAnimationStarted()V

    goto :goto_0

    .line 76
    :cond_0
    const-string v2, "ArrowAnimationView"

    const-string v3, "ArrowAnimationListener leaked, should have been removed."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 79
    .end local v1    # "listener":Lse/volvocars/acu/ui/ArrowAnimationListener;
    :cond_1
    return-void
.end method

.method private sendAnimationStopped()V
    .locals 4

    .prologue
    .line 82
    iget-object v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/ui/ArrowAnimationListener;

    .line 83
    .local v1, "listener":Lse/volvocars/acu/ui/ArrowAnimationListener;
    if-eqz v1, :cond_0

    .line 84
    invoke-interface {v1}, Lse/volvocars/acu/ui/ArrowAnimationListener;->arrowAnimationStopped()V

    goto :goto_0

    .line 87
    :cond_0
    const-string v2, "ArrowAnimationView"

    const-string v3, "ArrowAnimationListener leaked, should have been removed."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 90
    .end local v1    # "listener":Lse/volvocars/acu/ui/ArrowAnimationListener;
    :cond_1
    return-void
.end method


# virtual methods
.method public addArrowAnimationListener(Lse/volvocars/acu/ui/ArrowAnimationListener;)V
    .locals 1
    .param p1, "listener"    # Lse/volvocars/acu/ui/ArrowAnimationListener;

    .prologue
    .line 59
    iget-object v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    return-void
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mIsPlaying:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 101
    iget-boolean v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mStartPlaying:Z

    if-eqz v2, :cond_0

    .line 102
    iput v6, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->currentFrame:I

    .line 103
    iput v6, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->iterationCounter:I

    .line 104
    iput-boolean v6, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mStartPlaying:Z

    .line 105
    const/4 v2, 0x1

    iput-boolean v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mIsPlaying:Z

    .line 108
    :cond_0
    iget-boolean v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mIsPlaying:Z

    if-eqz v2, :cond_2

    .line 110
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->lastTick:J

    sub-long v0, v2, v4

    .line 112
    .local v0, "time":J
    const-wide/16 v2, 0x32

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    .line 113
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->lastTick:J

    .line 114
    iget v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->currentFrame:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->currentFrame:I

    .line 116
    iget v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->currentFrame:I

    iget-object v3, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mBitmapList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 117
    iget v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->iterationCounter:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->iterationCounter:I

    .line 118
    iput v6, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->currentFrame:I

    .line 120
    iget v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->iterationCounter:I

    const/4 v3, 0x3

    if-lt v2, v3, :cond_1

    .line 121
    iput-boolean v6, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mIsPlaying:Z

    .line 122
    invoke-direct {p0}, Lse/volvocars/acu/ui/ArrowAnimationView;->sendAnimationStopped()V

    .line 128
    :cond_1
    iget-object v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mBitmapList:Ljava/util/List;

    iget v3, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->currentFrame:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {p1, v2, v7, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 129
    invoke-virtual {p0}, Lse/volvocars/acu/ui/ArrowAnimationView;->postInvalidate()V

    .line 135
    .end local v0    # "time":J
    .end local p0    # "this":Lse/volvocars/acu/ui/ArrowAnimationView;
    :goto_0
    return-void

    .line 133
    .restart local p0    # "this":Lse/volvocars/acu/ui/ArrowAnimationView;
    :cond_2
    iget-object v2, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mBitmapList:Ljava/util/List;

    iget v3, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->currentFrame:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "this":Lse/volvocars/acu/ui/ArrowAnimationView;
    check-cast p0, Landroid/graphics/Bitmap;

    invoke-virtual {p1, p0, v7, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 95
    iget-object v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mbd:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mbd:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lse/volvocars/acu/ui/ArrowAnimationView;->setMeasuredDimension(II)V

    .line 96
    return-void
.end method

.method public playAnimation()V
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x1

    iput-boolean v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mStartPlaying:Z

    .line 144
    invoke-virtual {p0}, Lse/volvocars/acu/ui/ArrowAnimationView;->postInvalidate()V

    .line 145
    invoke-direct {p0}, Lse/volvocars/acu/ui/ArrowAnimationView;->sendAnimationStarted()V

    .line 146
    return-void
.end method

.method public removeArrowAnimationListener(Lse/volvocars/acu/ui/ArrowAnimationListener;)V
    .locals 1
    .param p1, "listener"    # Lse/volvocars/acu/ui/ArrowAnimationListener;

    .prologue
    .line 67
    iget-object v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 68
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 152
    iput-boolean v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mIsPlaying:Z

    .line 153
    iput-boolean v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->mStartPlaying:Z

    .line 154
    iput v0, p0, Lse/volvocars/acu/ui/ArrowAnimationView;->currentFrame:I

    .line 155
    invoke-virtual {p0}, Lse/volvocars/acu/ui/ArrowAnimationView;->postInvalidate()V

    .line 156
    invoke-direct {p0}, Lse/volvocars/acu/ui/ArrowAnimationView;->sendAnimationStopped()V

    .line 157
    return-void
.end method
