.class public Lse/volvocars/acu/ui/AppNameView;
.super Landroid/view/View;
.source "AppNameView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/ui/AppNameView$TitleInfo;
    }
.end annotation


# static fields
.field private static final FONTSIZE_LARGE:I = 0x28

.field private static final FONTSIZE_SMALL:I = 0x1e

.field private static final TEXT_COMPONENT_HEIGHT:I = 0x2d

.field private static final TITLE_WIDTH_MAX:I = 0x140


# instance fields
.field private mAlphaOverride:I

.field private final mColorDefault:I

.field private final mColorHighlight:I

.field private final mColorHightlighForbidden:I

.field private mCurrentPos:I

.field private mHeight:I

.field private mPaint:Landroid/graphics/Paint;

.field private volatile mTitles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/ui/AppNameView$TitleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mWidth:I

.field private mWidthHalf:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v4, 0x7f06001a

    .line 44
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 29
    const/16 v2, 0x2d

    iput v2, p0, Lse/volvocars/acu/ui/AppNameView;->mHeight:I

    .line 30
    const/4 v2, 0x0

    iput v2, p0, Lse/volvocars/acu/ui/AppNameView;->mCurrentPos:I

    .line 46
    invoke-virtual {p0}, Lse/volvocars/acu/ui/AppNameView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lse/volvocars/acu/ui/AppNameView;->mColorHighlight:I

    .line 47
    invoke-virtual {p0}, Lse/volvocars/acu/ui/AppNameView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lse/volvocars/acu/ui/AppNameView;->mColorHightlighForbidden:I

    .line 48
    invoke-virtual {p0}, Lse/volvocars/acu/ui/AppNameView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lse/volvocars/acu/ui/AppNameView;->mColorDefault:I

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "fonts/VolvoSanProLig.otf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 55
    .local v1, "typeface":Landroid/graphics/Typeface;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    .line 56
    iget-object v2, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    iget v3, p0, Lse/volvocars/acu/ui/AppNameView;->mColorHighlight:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 57
    iget-object v2, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x42200000    # 40.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 58
    iget-object v2, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 59
    iget-object v2, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 62
    const-string v2, "window"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 63
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    iput v2, p0, Lse/volvocars/acu/ui/AppNameView;->mWidth:I

    .line 64
    iget v2, p0, Lse/volvocars/acu/ui/AppNameView;->mWidth:I

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lse/volvocars/acu/ui/AppNameView;->mWidthHalf:I

    .line 65
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lse/volvocars/acu/ui/AppNameView;->mTitles:Ljava/util/List;

    .line 66
    return-void
.end method

.method private getClosestHightlightedIndex()I
    .locals 8

    .prologue
    .line 175
    iget-object v6, p0, Lse/volvocars/acu/ui/AppNameView;->mTitles:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    .line 176
    .local v4, "iconCount":I
    const/4 v1, -0x1

    .line 177
    .local v1, "closestIndex":I
    const v0, 0x7fffffff

    .line 179
    .local v0, "closestDistance":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_1

    .line 181
    iget v6, p0, Lse/volvocars/acu/ui/AppNameView;->mCurrentPos:I

    neg-int v6, v6

    mul-int/lit16 v7, v3, 0x12c

    add-int v5, v6, v7

    .line 182
    .local v5, "x":I
    iget v6, p0, Lse/volvocars/acu/ui/AppNameView;->mWidthHalf:I

    sub-int v6, v5, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 184
    .local v2, "currentDistance":I
    if-ge v2, v0, :cond_0

    .line 185
    move v1, v3

    .line 186
    move v0, v2

    .line 179
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 189
    .end local v2    # "currentDistance":I
    .end local v5    # "x":I
    :cond_1
    return v1
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 117
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 119
    invoke-direct {p0}, Lse/volvocars/acu/ui/AppNameView;->getClosestHightlightedIndex()I

    move-result v0

    .line 122
    .local v0, "closestIndex":I
    const/4 v6, -0x1

    if-ne v0, v6, :cond_1

    .line 154
    :cond_0
    return-void

    .line 128
    :cond_1
    iget-object v4, p0, Lse/volvocars/acu/ui/AppNameView;->mTitles:Ljava/util/List;

    .line 131
    .local v4, "titles":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/ui/AppNameView$TitleInfo;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 133
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lse/volvocars/acu/ui/AppNameView$TitleInfo;

    invoke-virtual {v6}, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 135
    .local v2, "title":Ljava/lang/String;
    if-ne v1, v0, :cond_3

    .line 136
    iget-object v7, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lse/volvocars/acu/ui/AppNameView$TitleInfo;

    invoke-virtual {v6}, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->getForbidden()I

    move-result v6

    if-eqz v6, :cond_2

    iget v6, p0, Lse/volvocars/acu/ui/AppNameView;->mColorHightlighForbidden:I

    :goto_1
    invoke-virtual {v7, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 137
    iget-object v6, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    const/high16 v7, 0x42200000    # 40.0f

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 138
    iget-object v6, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    iget v7, p0, Lse/volvocars/acu/ui/AppNameView;->mAlphaOverride:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 139
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lse/volvocars/acu/ui/AppNameView$TitleInfo;

    invoke-virtual {v6}, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->getWidthCentered()I

    move-result v3

    .line 149
    .local v3, "titleWidthHalf":I
    :goto_2
    mul-int/lit16 v6, v1, 0x12c

    iget v7, p0, Lse/volvocars/acu/ui/AppNameView;->mCurrentPos:I

    sub-int/2addr v6, v7

    sub-int v5, v6, v3

    .line 152
    .local v5, "x":I
    int-to-float v6, v5

    iget v7, p0, Lse/volvocars/acu/ui/AppNameView;->mHeight:I

    const/16 v8, 0xa

    sub-int/2addr v7, v8

    int-to-float v7, v7

    iget-object v8, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 136
    .end local v3    # "titleWidthHalf":I
    .end local v5    # "x":I
    :cond_2
    iget v6, p0, Lse/volvocars/acu/ui/AppNameView;->mColorHighlight:I

    goto :goto_1

    .line 142
    :cond_3
    iget-object v6, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    iget v7, p0, Lse/volvocars/acu/ui/AppNameView;->mColorDefault:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 143
    iget-object v6, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    const/high16 v7, 0x41f00000    # 30.0f

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 144
    iget-object v6, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    iget v7, p0, Lse/volvocars/acu/ui/AppNameView;->mAlphaOverride:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 145
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lse/volvocars/acu/ui/AppNameView$TitleInfo;

    invoke-virtual {v6}, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->getWidthOriginal()I

    move-result v3

    .restart local v3    # "titleWidthHalf":I
    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 111
    iget v0, p0, Lse/volvocars/acu/ui/AppNameView;->mWidth:I

    iget v1, p0, Lse/volvocars/acu/ui/AppNameView;->mHeight:I

    invoke-virtual {p0, v0, v1}, Lse/volvocars/acu/ui/AppNameView;->setMeasuredDimension(II)V

    .line 112
    return-void
.end method

.method public setAlphaOverride(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 231
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lse/volvocars/acu/ui/AppNameView;->mAlphaOverride:I

    .line 232
    return-void
.end method

.method public setApps(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/model/AppInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "apps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    const-wide/high16 v11, 0x3fe0000000000000L    # 0.5

    .line 199
    new-instance v2, Landroid/graphics/Paint;

    iget-object v7, p0, Lse/volvocars/acu/ui/AppNameView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {v2, v7}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 201
    .local v2, "paint":Landroid/graphics/Paint;
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 203
    .local v5, "titles":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/ui/AppNameView$TitleInfo;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v0, v7, :cond_1

    .line 205
    const/high16 v7, 0x42200000    # 40.0f

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 207
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lse/volvocars/acu/model/AppInfo;

    invoke-virtual {v7}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    const/high16 v9, 0x43a00000    # 320.0f

    const/4 v10, 0x0

    invoke-virtual {v2, v7, v8, v9, v10}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v1

    .line 208
    .local v1, "maxChars":I
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lse/volvocars/acu/model/AppInfo;

    invoke-virtual {v7}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v6

    .line 209
    .local v6, "trimmedAppTitle":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 210
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lse/volvocars/acu/model/AppInfo;

    invoke-virtual {v7}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v7, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "..."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 213
    :cond_0
    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v7

    float-to-double v7, v7

    mul-double/2addr v7, v11

    double-to-int v3, v7

    .line 215
    .local v3, "titleWidthCentered":I
    const/high16 v7, 0x41f00000    # 30.0f

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 216
    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v7

    float-to-double v7, v7

    mul-double/2addr v7, v11

    double-to-int v4, v7

    .line 218
    .local v4, "titleWidthOriginal":I
    new-instance v7, Lse/volvocars/acu/ui/AppNameView$TitleInfo;

    invoke-direct {v7, v6, v4, v3}, Lse/volvocars/acu/ui/AppNameView$TitleInfo;-><init>(Ljava/lang/String;II)V

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221
    .end local v1    # "maxChars":I
    .end local v3    # "titleWidthCentered":I
    .end local v4    # "titleWidthOriginal":I
    .end local v6    # "trimmedAppTitle":Ljava/lang/String;
    :cond_1
    iput-object v5, p0, Lse/volvocars/acu/ui/AppNameView;->mTitles:Ljava/util/List;

    .line 222
    return-void
.end method

.method public setCurrentPos(I)V
    .locals 0
    .param p1, "pos"    # I

    .prologue
    .line 162
    iput p1, p0, Lse/volvocars/acu/ui/AppNameView;->mCurrentPos:I

    .line 164
    invoke-virtual {p0}, Lse/volvocars/acu/ui/AppNameView;->invalidate()V

    .line 165
    return-void
.end method

.method public setForbiddenArray([I)V
    .locals 9
    .param p1, "enabledArray"    # [I

    .prologue
    .line 236
    iget-object v7, p0, Lse/volvocars/acu/ui/AppNameView;->mTitles:Ljava/util/List;

    .line 237
    .local v7, "titles":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/ui/AppNameView$TitleInfo;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 238
    .local v1, "copy":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/ui/AppNameView$TitleInfo;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    if-ge v3, v8, :cond_1

    .line 239
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lse/volvocars/acu/ui/AppNameView$TitleInfo;

    .line 240
    .local v4, "info":Lse/volvocars/acu/ui/AppNameView$TitleInfo;
    invoke-virtual {v4}, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->getTitle()Ljava/lang/String;

    move-result-object v6

    .line 241
    .local v6, "title":Ljava/lang/String;
    invoke-virtual {v4}, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->getWidthOriginal()I

    move-result v5

    .line 242
    .local v5, "original":I
    invoke-virtual {v4}, Lse/volvocars/acu/ui/AppNameView$TitleInfo;->getWidthCentered()I

    move-result v0

    .line 243
    .local v0, "centered":I
    array-length v8, p1

    if-ge v3, v8, :cond_0

    aget v8, p1, v3

    move v2, v8

    .line 244
    .local v2, "forbidden":I
    :goto_1
    new-instance v8, Lse/volvocars/acu/ui/AppNameView$TitleInfo;

    invoke-direct {v8, v6, v5, v0, v2}, Lse/volvocars/acu/ui/AppNameView$TitleInfo;-><init>(Ljava/lang/String;III)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 243
    .end local v2    # "forbidden":I
    :cond_0
    const/4 v8, 0x0

    move v2, v8

    goto :goto_1

    .line 246
    .end local v0    # "centered":I
    .end local v4    # "info":Lse/volvocars/acu/ui/AppNameView$TitleInfo;
    .end local v5    # "original":I
    .end local v6    # "title":Ljava/lang/String;
    :cond_1
    iput-object v1, p0, Lse/volvocars/acu/ui/AppNameView;->mTitles:Ljava/util/List;

    .line 247
    return-void
.end method
