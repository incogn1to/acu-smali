.class Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater$1;
.super Ljava/lang/Object;
.source "HomeView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;


# direct methods
.method constructor <init>(Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater$1;->this$0:Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 196
    const-string v0, "HomeScreenFragment"

    const-string v1, "Updating current weather info."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater$1;->this$0:Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;

    # getter for: Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;
    invoke-static {v0}, Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;->access$000(Lse/volvocars/acu/ui/HomeView$WeatherWidgetUpdater;)Lse/volvocars/acu/weather/WeatherWidget;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/WeatherWidget;->updateWeather()V

    .line 198
    return-void
.end method
