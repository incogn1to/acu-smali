.class public Lse/volvocars/acu/AcuActivity;
.super Landroid/app/Activity;
.source "AcuActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/AcuActivity$AppNameUpdater;
    }
.end annotation


# static fields
.field static final APPLICATION_NAME_TITLE_TOP_MARGIN:I = 0x28

.field static final APPLICATION_NAME_TITLE_TOP_MARGIN_COMPRESSED:I = 0x5

.field public static final BUILD_TYPE:Ljava/lang/String;

.field private static final DIALOG_ASK_FOR_GPS_SATELLITES_PREF:I = 0x0

.field private static final DIFF_THRESHOLD:I = 0xf

.field public static final EXTRA_STATUSBAR:Ljava/lang/String; = "statusbarVisible"

.field private static final GPS_SATELLITES_PREF:Ljava/lang/String; = "gps_satellites_pref"

.field public static final HOME_SCREEN_POSITION:I = 0x0

.field public static final ICON_OFFSET:I = 0x12c

.field public static final LAUNCHER_SCREEN_POSITION:I = -0x2e0

.field private static final PREFERENCE_LAUNCHER:Ljava/lang/String; = "acu_launcher"

.field public static final SCREEN_HEIGHT:I = 0x1e0

.field public static final SCREEN_HOME:I = 0x0

.field public static final SCREEN_LAUNCHER:I = 0x1

.field public static final SCREEN_WIDTH:I = 0x2e0

.field private static final SCROLL_HORIZONTAL:I = 0x0

.field private static final SCROLL_VERTICAL:I = 0x1

.field static final TAG:Ljava/lang/String; = "AcuActivity"

.field private static volatile mContext:Landroid/content/Context;


# instance fields
.field private mActivityMonitor:Lse/volvocars/acu/UserActivityMonitor;

.field private mAnimatableHelper:Lse/volvocars/acu/AnimatableAdapter;

.field private mAppName:Lse/volvocars/acu/ui/AppNameView;

.field private final mAppNameUpdater:Lse/volvocars/acu/AcuActivity$AppNameUpdater;

.field private mAppOrderDialog:Landroid/app/Dialog;

.field private mAppsHandler:Lse/volvocars/acu/AppHandler;

.field private mCurrentPos:I

.field private mCurrentScreen:I

.field private mDownPosX:I

.field private mDownPosY:I

.field private final mHandler:Landroid/os/Handler;

.field private mHome:Lse/volvocars/acu/ui/HomeView;

.field private mHomeTouchListener:Lse/volvocars/acu/ui/HomeTouchListener;

.field private mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

.field private mLauncherTouchListener:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

.field private mRootFrameLayout:Landroid/widget/FrameLayout;

.field private mScrollMode:I

.field private mStartOnLauncher:Z

.field private mStatusbarHeight:I

.field private mStatusbarVisible:Z

.field private mSwipeGestureListener:Landroid/view/GestureDetector;

.field private mVerticalAnimator:Lse/volvocars/acu/VerticalAnimatableAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lse/volvocars/acu/AcuActivity;->getBuildType()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lse/volvocars/acu/AcuActivity;->BUILD_TYPE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 129
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 77
    const/4 v0, -0x1

    iput v0, p0, Lse/volvocars/acu/AcuActivity;->mScrollMode:I

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lse/volvocars/acu/AcuActivity;->mStartOnLauncher:Z

    .line 91
    const/4 v0, 0x1

    iput v0, p0, Lse/volvocars/acu/AcuActivity;->mCurrentPos:I

    .line 130
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHandler:Landroid/os/Handler;

    .line 131
    new-instance v0, Lse/volvocars/acu/AcuActivity$AppNameUpdater;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lse/volvocars/acu/AcuActivity$AppNameUpdater;-><init>(Lse/volvocars/acu/AcuActivity;Lse/volvocars/acu/AcuActivity$1;)V

    iput-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppNameUpdater:Lse/volvocars/acu/AcuActivity$AppNameUpdater;

    .line 132
    sput-object p0, Lse/volvocars/acu/AcuActivity;->mContext:Landroid/content/Context;

    .line 133
    return-void
.end method

.method static synthetic access$000(Lse/volvocars/acu/AcuActivity;)Lse/volvocars/acu/ui/AppNameView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/AcuActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    return-object v0
.end method

.method static synthetic access$200(Lse/volvocars/acu/AcuActivity;I)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/AcuActivity;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lse/volvocars/acu/AcuActivity;->updateCurrentPos(I)V

    return-void
.end method

.method static synthetic access$300(Lse/volvocars/acu/AcuActivity;)Lse/volvocars/acu/ui/HomeView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/AcuActivity;

    .prologue
    .line 41
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    return-object v0
.end method

.method static synthetic access$400(Lse/volvocars/acu/AcuActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/AcuActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lse/volvocars/acu/AcuActivity;->setGPSsatellitesPreference(Z)V

    return-void
.end method

.method private declared-synchronized askForGPSsatellitesPreference()V
    .locals 1

    .prologue
    .line 700
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lse/volvocars/acu/AcuActivity;->isGPSsatellitesPrefSet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 701
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lse/volvocars/acu/AcuActivity;->showDialog(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 703
    :cond_0
    monitor-exit p0

    return-void

    .line 700
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private createGPSsatellitesEnablerPopUp()Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 748
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 750
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f090055

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 751
    const v1, 0x7f090056

    new-instance v2, Lse/volvocars/acu/AcuActivity$3;

    invoke-direct {v2, p0}, Lse/volvocars/acu/AcuActivity$3;-><init>(Lse/volvocars/acu/AcuActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 760
    const v1, 0x7f090057

    new-instance v2, Lse/volvocars/acu/AcuActivity$4;

    invoke-direct {v2, p0}, Lse/volvocars/acu/AcuActivity$4;-><init>(Lse/volvocars/acu/AcuActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 769
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 771
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method public static getAcuContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lse/volvocars/acu/AcuActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected static getBuildType()Ljava/lang/String;
    .locals 8

    .prologue
    .line 214
    const/4 v1, 0x0

    .line 216
    .local v1, "buildType":Ljava/lang/String;
    :try_start_0
    const-string v4, "android.os.SystemProperties"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 217
    .local v2, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v4, "get"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 218
    .local v3, "get":Ljava/lang/reflect/Method;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "ro.build.type"

    aput-object v6, v4, v5

    invoke-virtual {v3, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    .end local v2    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "get":Ljava/lang/reflect/Method;
    :goto_0
    return-object v1

    .line 219
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method private isGPSsatellitesPrefSet()Z
    .locals 4

    .prologue
    .line 710
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 711
    .local v0, "context":Landroid/content/Context;
    const-string v2, "acu_launcher"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 712
    .local v1, "preferences":Landroid/content/SharedPreferences;
    const-string v2, "gps_satellites_pref"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    return v2
.end method

.method private keyDownRight(Landroid/view/KeyEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 473
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getCurrentScreen()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 474
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 475
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v0}, Lse/volvocars/acu/AppHandler;->goRight()V

    .line 485
    :cond_0
    :goto_0
    return-void

    .line 479
    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 481
    const/16 v0, -0x2e0

    invoke-virtual {p0, v0}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    .line 482
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/HomeView;->deactivate()V

    goto :goto_0
.end method

.method private keyEnter(Landroid/view/KeyEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 502
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getCurrentScreen()I

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 503
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherTouchListener:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v1, v2}, Lse/volvocars/acu/AppHandler;->startAppInHitbox(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->setStartingApp(Z)V

    .line 505
    :cond_0
    return-void
.end method

.method private keyUpLeft(Landroid/view/KeyEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 492
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getCurrentScreen()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 493
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v0}, Lse/volvocars/acu/AppHandler;->goLeft()V

    .line 495
    :cond_0
    return-void
.end method

.method private onActivity()V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mActivityMonitor:Lse/volvocars/acu/UserActivityMonitor;

    invoke-virtual {v0}, Lse/volvocars/acu/UserActivityMonitor;->onActivity()V

    .line 323
    iget v0, p0, Lse/volvocars/acu/AcuActivity;->mCurrentScreen:I

    if-nez v0, :cond_0

    .line 324
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/HomeView;->onActivity()V

    .line 326
    :cond_0
    return-void
.end method

.method private setGPSsatellitesPreference(Z)V
    .locals 5
    .param p1, "enable"    # Z

    .prologue
    .line 722
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 723
    .local v0, "context":Landroid/content/Context;
    const-string v3, "acu_launcher"

    const/4 v4, 0x2

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 725
    .local v2, "preferences":Landroid/content/SharedPreferences;
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "gps"

    invoke-static {v3, v4, p1}, Landroid/provider/Settings$Secure;->setLocationProviderEnabled(Landroid/content/ContentResolver;Ljava/lang/String;Z)V

    .line 727
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 728
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "gps_satellites_pref"

    invoke-interface {v1, v3, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 729
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 730
    return-void
.end method

.method private setScrollModeIfPossible(Landroid/view/MotionEvent;II)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/16 v4, 0xf

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 415
    iget v0, p0, Lse/volvocars/acu/AcuActivity;->mDownPosX:I

    sub-int v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lse/volvocars/acu/AcuActivity;->mDownPosY:I

    sub-int v1, p3, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    sub-int/2addr v1, v4

    if-ge v0, v1, :cond_0

    .line 416
    iput v3, p0, Lse/volvocars/acu/AcuActivity;->mScrollMode:I

    .line 418
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 419
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move v0, v3

    .line 428
    :goto_0
    return v0

    .line 422
    :cond_0
    iget v0, p0, Lse/volvocars/acu/AcuActivity;->mDownPosX:I

    sub-int v0, p2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sub-int/2addr v0, v4

    iget v1, p0, Lse/volvocars/acu/AcuActivity;->mDownPosY:I

    sub-int v1, p3, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-le v0, v1, :cond_1

    .line 423
    iput v2, p0, Lse/volvocars/acu/AcuActivity;->mScrollMode:I

    move v0, v2

    .line 425
    goto :goto_0

    :cond_1
    move v0, v2

    .line 428
    goto :goto_0
.end method

.method private updateCurrentPos(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 305
    iput p1, p0, Lse/volvocars/acu/AcuActivity;->mCurrentPos:I

    .line 306
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppNameUpdater:Lse/volvocars/acu/AcuActivity$AppNameUpdater;

    iget v1, p0, Lse/volvocars/acu/AcuActivity;->mCurrentPos:I

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AcuActivity$AppNameUpdater;->setCurrentPos(I)V

    .line 307
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mAppNameUpdater:Lse/volvocars/acu/AcuActivity$AppNameUpdater;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 308
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 446
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherTouchListener:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->isStartingApp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447
    const-string v0, "AcuActivity"

    const-string v1, "Can\'t dispatch key event as an app is starting!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 465
    :goto_0
    return v0

    .line 450
    :cond_0
    invoke-direct {p0}, Lse/volvocars/acu/AcuActivity;->onActivity()V

    .line 451
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 465
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 454
    :sswitch_0
    invoke-direct {p0, p1}, Lse/volvocars/acu/AcuActivity;->keyEnter(Landroid/view/KeyEvent;)V

    move v0, v2

    .line 455
    goto :goto_0

    .line 458
    :sswitch_1
    invoke-direct {p0, p1}, Lse/volvocars/acu/AcuActivity;->keyDownRight(Landroid/view/KeyEvent;)V

    move v0, v2

    .line 459
    goto :goto_0

    .line 462
    :sswitch_2
    invoke-direct {p0, p1}, Lse/volvocars/acu/AcuActivity;->keyUpLeft(Landroid/view/KeyEvent;)V

    move v0, v2

    .line 463
    goto :goto_0

    .line 451
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_1
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x1

    .line 368
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-eq v3, v5, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    .line 370
    :cond_0
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mActivityMonitor:Lse/volvocars/acu/UserActivityMonitor;

    invoke-virtual {v3}, Lse/volvocars/acu/UserActivityMonitor;->onActivity()V

    .line 373
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v1, v3

    .line 374
    .local v1, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v2, v3

    .line 377
    .local v2, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 406
    :cond_2
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 408
    .local v0, "touch":Z
    iget v3, p0, Lse/volvocars/acu/AcuActivity;->mCurrentScreen:I

    if-nez v3, :cond_3

    if-eqz v0, :cond_3

    .line 409
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v3}, Lse/volvocars/acu/ui/HomeView;->onActivity()V

    :cond_3
    move v3, v0

    .line 411
    .end local v0    # "touch":Z
    :goto_1
    return v3

    .line 379
    :pswitch_0
    iput v1, p0, Lse/volvocars/acu/AcuActivity;->mDownPosX:I

    .line 380
    iput v2, p0, Lse/volvocars/acu/AcuActivity;->mDownPosY:I

    .line 381
    iput v6, p0, Lse/volvocars/acu/AcuActivity;->mScrollMode:I

    .line 382
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mSwipeGestureListener:Landroid/view/GestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 385
    :pswitch_1
    iget v3, p0, Lse/volvocars/acu/AcuActivity;->mScrollMode:I

    if-ne v3, v6, :cond_4

    .line 386
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mSwipeGestureListener:Landroid/view/GestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 387
    invoke-direct {p0, p1, v1, v2}, Lse/volvocars/acu/AcuActivity;->setScrollModeIfPossible(Landroid/view/MotionEvent;II)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v5

    .line 388
    goto :goto_1

    .line 391
    :cond_4
    iget v3, p0, Lse/volvocars/acu/AcuActivity;->mScrollMode:I

    if-ne v3, v5, :cond_2

    .line 392
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mSwipeGestureListener:Landroid/view/GestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move v3, v5

    .line 393
    goto :goto_1

    .line 399
    :pswitch_2
    iget v3, p0, Lse/volvocars/acu/AcuActivity;->mScrollMode:I

    if-ne v3, v5, :cond_2

    .line 400
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mSwipeGestureListener:Landroid/view/GestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move v3, v5

    .line 401
    goto :goto_1

    .line 377
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method getAppHandler()Lse/volvocars/acu/AppHandler;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    return-object v0
.end method

.method public getCurrentPos()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lse/volvocars/acu/AcuActivity;->mCurrentPos:I

    return v0
.end method

.method public getCurrentScreen()I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lse/volvocars/acu/AcuActivity;->mCurrentScreen:I

    return v0
.end method

.method protected getDialog()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppOrderDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method protected isDialogDisplayed()Z
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppOrderDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    return v0
.end method

.method protected isReorderingApps()Z
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v0}, Lse/volvocars/acu/AppHandler;->isReorderingApps()Z

    move-result v0

    return v0
.end method

.method public moveToCorrectStartScreen()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 561
    iget-boolean v0, p0, Lse/volvocars/acu/AcuActivity;->mStartOnLauncher:Z

    if-eqz v0, :cond_1

    .line 563
    const/16 v0, -0x2e0

    invoke-virtual {p0, v0}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    .line 564
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v0}, Lse/volvocars/acu/AppHandler;->moveToFocusIcon()V

    .line 565
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setLineEmitterState(Z)V

    .line 566
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setPlayButtonEmitterState(Z)V

    .line 576
    :cond_0
    :goto_0
    invoke-virtual {p0, v1}, Lse/volvocars/acu/AcuActivity;->setStartOnLauncher(Z)V

    .line 577
    return-void

    .line 569
    :cond_1
    iget-boolean v0, p0, Lse/volvocars/acu/AcuActivity;->mStartOnLauncher:Z

    if-nez v0, :cond_0

    .line 571
    invoke-virtual {p0, v1}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherTouchListener:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->isStartingApp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    const-string v0, "AcuActivity"

    const-string v1, "Can\'t dispatch back key event as an app is starting!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    :goto_0
    return-void

    .line 437
    :cond_0
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getCurrentScreen()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 439
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    .line 441
    :cond_1
    invoke-direct {p0}, Lse/volvocars/acu/AcuActivity;->onActivity()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v10, 0x1e0

    const/16 v8, 0x33

    const/16 v7, 0x2e0

    const/4 v9, 0x0

    const-string v11, "AcuActivity"

    .line 137
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 139
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarHeight:I

    .line 141
    new-instance v4, Landroid/widget/FrameLayout;

    invoke-direct {v4, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lse/volvocars/acu/AcuActivity;->mRootFrameLayout:Landroid/widget/FrameLayout;

    .line 144
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030005

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lse/volvocars/acu/ui/HomeView;

    iput-object v4, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    .line 145
    iget-object v4, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v4, v9}, Lse/volvocars/acu/ui/HomeView;->showContent(Z)V

    .line 146
    new-instance v4, Lse/volvocars/acu/ui/opengl/LauncherGLView;

    iget-object v5, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-direct {v4, p0, v5}, Lse/volvocars/acu/ui/opengl/LauncherGLView;-><init>(Landroid/content/Context;Lse/volvocars/acu/ui/HomeView;)V

    iput-object v4, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    .line 147
    new-instance v4, Lse/volvocars/acu/ui/AppNameView;

    invoke-direct {v4, p0}, Lse/volvocars/acu/ui/AppNameView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    .line 149
    iget-object v4, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    iget-object v5, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v4, v5}, Lse/volvocars/acu/ui/HomeView;->setLauncherGlView(Lse/volvocars/acu/ui/opengl/LauncherGLView;)V

    .line 152
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v7, v10, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 153
    .local v2, "mHomeLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v7, v10, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 154
    .local v1, "mAppNameLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v7, v10, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 155
    .local v3, "mLauncherLayoutParams":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 156
    const/16 v4, 0x28

    iput v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 159
    iget-object v4, p0, Lse/volvocars/acu/AcuActivity;->mRootFrameLayout:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v4, v5, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 160
    iget-object v4, p0, Lse/volvocars/acu/AcuActivity;->mRootFrameLayout:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    invoke-virtual {v4, v5, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 161
    iget-object v4, p0, Lse/volvocars/acu/AcuActivity;->mRootFrameLayout:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v4, v5, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    new-instance v4, Lse/volvocars/acu/VerticalAnimatableAdapter;

    iget v5, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarHeight:I

    iget-object v6, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    iget-object v7, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    iget-object v8, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/VerticalAnimatableAdapter;-><init>(ILandroid/view/View;Lse/volvocars/acu/ui/opengl/LauncherGLView;Landroid/view/View;)V

    iput-object v4, p0, Lse/volvocars/acu/AcuActivity;->mVerticalAnimator:Lse/volvocars/acu/VerticalAnimatableAdapter;

    .line 164
    new-instance v4, Lse/volvocars/acu/AnimatableAdapter;

    iget-object v5, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    iget-object v6, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    invoke-direct {v4, p0, v5, v6}, Lse/volvocars/acu/AnimatableAdapter;-><init>(Lse/volvocars/acu/AcuActivity;Lse/volvocars/acu/ui/opengl/LauncherGLView;Lse/volvocars/acu/ui/AppNameView;)V

    iput-object v4, p0, Lse/volvocars/acu/AcuActivity;->mAnimatableHelper:Lse/volvocars/acu/AnimatableAdapter;

    .line 166
    iget-object v4, p0, Lse/volvocars/acu/AcuActivity;->mRootFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v4}, Lse/volvocars/acu/AcuActivity;->setContentView(Landroid/view/View;)V

    .line 169
    new-instance v4, Lse/volvocars/acu/ui/HomeTouchListener;

    iget-object v5, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-direct {v4, p0, v5}, Lse/volvocars/acu/ui/HomeTouchListener;-><init>(Lse/volvocars/acu/AcuActivity;Lse/volvocars/acu/ui/HomeView;)V

    iput-object v4, p0, Lse/volvocars/acu/AcuActivity;->mHomeTouchListener:Lse/volvocars/acu/ui/HomeTouchListener;

    .line 171
    new-instance v4, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    new-instance v6, Lse/volvocars/acu/ui/VerticalSwipeGestureListener;

    invoke-direct {v6, p0}, Lse/volvocars/acu/ui/VerticalSwipeGestureListener;-><init>(Lse/volvocars/acu/AcuActivity;)V

    invoke-direct {v4, v5, v6}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v4, p0, Lse/volvocars/acu/AcuActivity;->mSwipeGestureListener:Landroid/view/GestureDetector;

    .line 174
    iget-object v4, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    new-instance v5, Lse/volvocars/acu/AcuActivity$1;

    invoke-direct {v5, p0}, Lse/volvocars/acu/AcuActivity$1;-><init>(Lse/volvocars/acu/AcuActivity;)V

    invoke-virtual {v4, v5}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setNativePositionChangeListener(Lse/volvocars/acu/ui/opengl/OnNativePositionChangedListener;)V

    .line 182
    invoke-static {}, Lse/volvocars/acu/OnBootReceiver;->isJustBooted()Z

    move-result v0

    .line 184
    .local v0, "justBooted":Z
    iget-object v4, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    if-eqz v4, :cond_2

    .line 185
    new-instance v4, Lse/volvocars/acu/AppHandler;

    iget-object v5, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    iget-object v6, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    invoke-direct {v4, p0, v5, v6, v0}, Lse/volvocars/acu/AppHandler;-><init>(Lse/volvocars/acu/AcuActivity;Lse/volvocars/acu/ui/opengl/LauncherGLView;Lse/volvocars/acu/ui/AppNameView;Z)V

    iput-object v4, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    .line 186
    iget-object v4, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v4}, Lse/volvocars/acu/AppHandler;->updateAppIconOrder()V

    .line 187
    const-string v4, "AcuActivity"

    const-string v4, "mAppsHandler ready"

    invoke-static {v11, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    new-instance v4, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    iget-object v5, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    iget-object v6, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-direct {v4, p0, v5, v6}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;-><init>(Lse/volvocars/acu/AcuActivity;Lse/volvocars/acu/ui/opengl/LauncherGLView;Lse/volvocars/acu/AppHandler;)V

    iput-object v4, p0, Lse/volvocars/acu/AcuActivity;->mLauncherTouchListener:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    .line 191
    iget-object v4, p0, Lse/volvocars/acu/AcuActivity;->mRootFrameLayout:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lse/volvocars/acu/AcuActivity;->mHomeTouchListener:Lse/volvocars/acu/ui/HomeTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 196
    :goto_0
    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {p0, v9}, Lse/volvocars/acu/AcuActivity;->setStartOnLauncher(Z)V

    .line 198
    const-string v4, "AcuActivity"

    const-string v4, "Setting start on launcher to false as just booted."

    invoke-static {v11, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_0
    iput v9, p0, Lse/volvocars/acu/AcuActivity;->mCurrentScreen:I

    .line 203
    new-instance v4, Lse/volvocars/acu/UserActivityMonitor;

    invoke-direct {v4, p0}, Lse/volvocars/acu/UserActivityMonitor;-><init>(Lse/volvocars/acu/AcuActivity;)V

    iput-object v4, p0, Lse/volvocars/acu/AcuActivity;->mActivityMonitor:Lse/volvocars/acu/UserActivityMonitor;

    .line 205
    if-eqz p1, :cond_1

    const-string v4, "statusbarVisible"

    const/4 v5, 0x1

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_1
    const/4 v4, 0x1

    :goto_1
    invoke-virtual {p0, v4}, Lse/volvocars/acu/AcuActivity;->setStatusBarVisible(Z)V

    .line 207
    invoke-direct {p0}, Lse/volvocars/acu/AcuActivity;->askForGPSsatellitesPreference()V

    .line 208
    return-void

    .line 193
    :cond_2
    const-string v4, "AcuActivity"

    const-string v4, "View is null"

    invoke-static {v11, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    move v4, v9

    .line 205
    goto :goto_1
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 734
    packed-switch p1, :pswitch_data_0

    .line 739
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 736
    :pswitch_0
    invoke-direct {p0}, Lse/volvocars/acu/AcuActivity;->createGPSsatellitesEnablerPopUp()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 734
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 254
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 540
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lse/volvocars/acu/AcuActivity;->removeDialog(I)V

    .line 542
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/HomeView;->onDestroy()V

    .line 546
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    if-eqz v0, :cond_1

    .line 547
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v0}, Lse/volvocars/acu/AppHandler;->close()V

    .line 549
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mActivityMonitor:Lse/volvocars/acu/UserActivityMonitor;

    invoke-virtual {v0}, Lse/volvocars/acu/UserActivityMonitor;->onDestroy()V

    .line 551
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 553
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 525
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 526
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->onPause()V

    .line 529
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    if-eqz v0, :cond_1

    .line 530
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/HomeView;->onPause()V

    .line 533
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppOrderDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppOrderDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 534
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppOrderDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 536
    :cond_2
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/high16 v4, 0x7f0a0000

    .line 259
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getCurrentScreen()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 270
    const/4 v1, 0x0

    iput-object v1, p0, Lse/volvocars/acu/AcuActivity;->mAppOrderDialog:Landroid/app/Dialog;

    .line 273
    :goto_0
    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mAppOrderDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_0

    .line 274
    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mAppOrderDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 276
    :cond_0
    const/4 v1, 0x1

    return v1

    .line 262
    :pswitch_0

    # Help Center is started here
    # v3 - context

    #new-instance v1, Lse/volvocars/acu/HelpCenterDialog;

    sget-object v3, Lse/volvocars/acu/AcuActivity;->mContext:Landroid/content/Context;

    #invoke-direct {v1, v2, v4}, Lse/volvocars/acu/HelpCenterDialog;-><init>(Landroid/content/Context;I)V
    #iput-object v1, p0, Lse/volvocars/acu/AcuActivity;->mAppOrderDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    move-result-object v1

    .local v1, "manager":Landroid/content/pm/PackageManager;
    const-string v2, "com.android.settings"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
    move-result-object v0

    if-eqz v0, :goto_0

    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2
    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 266
    :pswitch_1
    const-string v1, "se.volvocars.acu.apps"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lse/volvocars/acu/AcuActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 267
    .local v0, "settings":Landroid/content/SharedPreferences;
    new-instance v1, Lse/volvocars/acu/AppOrderDialog;

    sget-object v2, Lse/volvocars/acu/AcuActivity;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-direct {v1, v2, v4, v3, v0}, Lse/volvocars/acu/AppOrderDialog;-><init>(Landroid/content/Context;ILse/volvocars/acu/AppHandler;Landroid/content/SharedPreferences;)V

    iput-object v1, p0, Lse/volvocars/acu/AcuActivity;->mAppOrderDialog:Landroid/app/Dialog;

    goto :goto_0

    .line 259
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 509
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 510
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->onResume()V

    .line 513
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    if-eqz v0, :cond_1

    .line 514
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/HomeView;->onResume()V

    .line 516
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v0}, Lse/volvocars/acu/AppHandler;->updateAppIconOrder()V

    .line 517
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->moveToCorrectStartScreen()V

    .line 518
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherTouchListener:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->setStartingApp(Z)V

    .line 519
    invoke-direct {p0}, Lse/volvocars/acu/AcuActivity;->onActivity()V

    .line 520
    invoke-static {}, Lse/volvocars/acu/HomeButtonReceiver;->onReceive()V

    .line 521
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 691
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 693
    const-string v0, "statusbarVisible"

    iget-boolean v1, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarVisible:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 694
    return-void
.end method

.method public scrollToPos(I)V
    .locals 1
    .param p1, "targetPosition"    # I

    .prologue
    .line 297
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->scrollToPos(I)V

    .line 298
    return-void
.end method

.method public setAppNamePosition(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 598
    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    invoke-virtual {v1}, Lse/volvocars/acu/ui/AppNameView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 599
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 600
    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    invoke-virtual {v1, v0}, Lse/volvocars/acu/ui/AppNameView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 601
    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mRootFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->invalidate()V

    .line 602
    return-void
.end method

.method public setCurrentPos(I)V
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 243
    iput p1, p0, Lse/volvocars/acu/AcuActivity;->mCurrentPos:I

    .line 244
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    iget v1, p0, Lse/volvocars/acu/AcuActivity;->mCurrentPos:I

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setPos(I)V

    .line 245
    return-void
.end method

.method public setCurrentScreen(I)V
    .locals 4
    .param p1, "screen"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 640
    if-nez p1, :cond_0

    .line 641
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, v3}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setState(I)V

    .line 642
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/HomeView;->activate()V

    .line 643
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mRootFrameLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mHomeTouchListener:Lse/volvocars/acu/ui/HomeTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 644
    iput v3, p0, Lse/volvocars/acu/AcuActivity;->mCurrentScreen:I

    .line 646
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v0}, Lse/volvocars/acu/AppHandler;->moveToDefaultIcon()V

    .line 647
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/AppNameView;->setAlphaOverride(F)V

    .line 648
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v0, v2}, Lse/volvocars/acu/ui/HomeView;->setArrowVisibility(Z)V

    .line 649
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v0, v2}, Lse/volvocars/acu/ui/HomeView;->setClockVisibility(Z)V

    .line 650
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v0, v2}, Lse/volvocars/acu/ui/HomeView;->setWeatherConditionSymbolVisibility(Z)V

    .line 651
    invoke-virtual {p0, v3}, Lse/volvocars/acu/AcuActivity;->setStartOnLauncher(Z)V

    .line 654
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, v2}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setTouchActionState(I)V

    .line 667
    :goto_0
    return-void

    .line 656
    :cond_0
    if-ne p1, v2, :cond_1

    .line 657
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, v2}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setState(I)V

    .line 658
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mRootFrameLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mLauncherTouchListener:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 659
    iput v2, p0, Lse/volvocars/acu/AcuActivity;->mCurrentScreen:I

    .line 660
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/AppNameView;->setAlphaOverride(F)V

    .line 661
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/HomeView;->deactivate()V

    .line 662
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, v3}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setLineEmitterState(Z)V

    goto :goto_0

    .line 665
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown screen <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setHomePosition(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 590
    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v1}, Lse/volvocars/acu/ui/HomeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 591
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 592
    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v1, v0}, Lse/volvocars/acu/ui/HomeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 593
    iget-object v1, p0, Lse/volvocars/acu/AcuActivity;->mRootFrameLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->invalidate()V

    .line 594
    return-void
.end method

.method setIdleMode(Z)V
    .locals 3
    .param p1, "idle"    # Z

    .prologue
    .line 622
    const-string v0, "AcuActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting idle mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 623
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getCurrentScreen()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 625
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    .line 627
    :cond_0
    new-instance v0, Lse/volvocars/acu/AcuActivity$2;

    invoke-direct {v0, p0, p1}, Lse/volvocars/acu/AcuActivity$2;-><init>(Lse/volvocars/acu/AcuActivity;Z)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/AcuActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 633
    return-void
.end method

.method public setIsStartingApp(Z)V
    .locals 1
    .param p1, "isStarting"    # Z

    .prologue
    .line 239
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherTouchListener:Lse/volvocars/acu/ui/opengl/LauncherTouchListener;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/ui/opengl/LauncherTouchListener;->setStartingApp(Z)V

    .line 240
    return-void
.end method

.method public setNativeState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 674
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setState(I)V

    .line 675
    return-void
.end method

.method public setPearlPos(I)I
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 311
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setPearlPos(I)I

    move-result v0

    return v0
.end method

.method public setStartOnLauncher(Z)V
    .locals 0
    .param p1, "startOnLauncher"    # Z

    .prologue
    .line 682
    iput-boolean p1, p0, Lse/volvocars/acu/AcuActivity;->mStartOnLauncher:Z

    .line 687
    return-void
.end method

.method public setStatusBarVisible(Z)V
    .locals 6
    .param p1, "visible"    # Z

    .prologue
    const/16 v5, 0x400

    const/4 v4, 0x0

    .line 334
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v3}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 335
    .local v1, "params2":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v3}, Lse/volvocars/acu/ui/HomeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 336
    .local v0, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    invoke-virtual {v3}, Lse/volvocars/acu/ui/AppNameView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 338
    .local v2, "params3":Landroid/view/ViewGroup$MarginLayoutParams;
    const/16 v3, 0x1e0

    iput v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 340
    if-eqz p1, :cond_1

    iget-boolean v3, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarVisible:Z

    if-nez v3, :cond_1

    .line 341
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/Window;->clearFlags(I)V

    .line 342
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mVerticalAnimator:Lse/volvocars/acu/VerticalAnimatableAdapter;

    iget v4, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarHeight:I

    invoke-virtual {v3, v4, p1}, Lse/volvocars/acu/VerticalAnimatableAdapter;->animateTo(IZ)V

    .line 343
    iget v3, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarHeight:I

    neg-int v3, v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 344
    iget v3, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarHeight:I

    neg-int v3, v3

    iput v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 345
    iget v3, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarHeight:I

    neg-int v3, v3

    add-int/lit8 v3, v3, 0x28

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 346
    const/4 v3, 0x1

    iput-boolean v3, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarVisible:Z

    .line 358
    :cond_0
    :goto_0
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v3, v0}, Lse/volvocars/acu/ui/HomeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 359
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v3, v1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 360
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    invoke-virtual {v3, v2}, Lse/volvocars/acu/ui/AppNameView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 363
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mHome:Lse/volvocars/acu/ui/HomeView;

    invoke-virtual {v3, p1}, Lse/volvocars/acu/ui/HomeView;->setStatusbarVisible(Z)V

    .line 364
    return-void

    .line 348
    :cond_1
    if-nez p1, :cond_0

    iget-boolean v3, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarVisible:Z

    if-eqz v3, :cond_0

    .line 349
    invoke-virtual {p0}, Lse/volvocars/acu/AcuActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/Window;->addFlags(I)V

    .line 351
    iget v3, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarHeight:I

    add-int/lit8 v3, v3, 0x28

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 352
    iget v3, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarHeight:I

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 353
    iput v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 354
    iget-object v3, p0, Lse/volvocars/acu/AcuActivity;->mVerticalAnimator:Lse/volvocars/acu/VerticalAnimatableAdapter;

    invoke-virtual {v3, v4, p1}, Lse/volvocars/acu/VerticalAnimatableAdapter;->animateTo(IZ)V

    .line 355
    iput-boolean v4, p0, Lse/volvocars/acu/AcuActivity;->mStatusbarVisible:Z

    goto :goto_0
.end method

.method public setTargetPosition(I)V
    .locals 1
    .param p1, "target"    # I

    .prologue
    .line 609
    const/16 v0, -0x2e0

    if-ne p1, v0, :cond_0

    .line 610
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v0}, Lse/volvocars/acu/AppHandler;->moveToDefaultIcon()V

    .line 613
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAnimatableHelper:Lse/volvocars/acu/AnimatableAdapter;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/AnimatableAdapter;->animateTo(I)V

    .line 615
    return-void
.end method

.method public setViewXOffset(I)V
    .locals 3
    .param p1, "x"    # I

    .prologue
    .line 585
    iget-object v0, p0, Lse/volvocars/acu/AcuActivity;->mAnimatableHelper:Lse/volvocars/acu/AnimatableAdapter;

    neg-int v1, p1

    int-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lse/volvocars/acu/AnimatableAdapter;->setValue(D)V

    .line 586
    return-void
.end method
