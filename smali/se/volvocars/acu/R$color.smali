.class public final Lse/volvocars/acu/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final appNameForbidden:I = 0x7f06001a

.field public static final application_title_text:I = 0x7f060025

.field public static final borderColor:I = 0x7f060000

.field public static final color_settings_format:I = 0x7f060026

.field public static final color_widget_text_dark:I = 0x7f060027

.field public static final color_widget_text_light:I = 0x7f060028

.field public static final textClockGray:I = 0x7f060012

.field public static final textClockGrayFaded:I = 0x7f060013

.field public static final textGray:I = 0x7f060014

.field public static final textGrayFaded:I = 0x7f060016

.field public static final textGrayGlow:I = 0x7f060017

.field public static final textGrayMarked:I = 0x7f060015

.field public static final textGrayPressed:I = 0x7f060023

.field public static final textGrayRegular:I = 0x7f060022

.field public static final textGraySelected:I = 0x7f060024

.field public static final textSearchMatch:I = 0x7f060018

.field public static final textWhite:I = 0x7f06000d

.field public static final textWhiteFaded:I = 0x7f06000f

.field public static final textWhiteFadedMuch:I = 0x7f060010

.field public static final textWhiteGlow:I = 0x7f060011

.field public static final textWhiteMarked:I = 0x7f06000e

.field public static final textWhitePressed:I = 0x7f060020

.field public static final textWhiteRegular:I = 0x7f06001f

.field public static final textWhiteSelected:I = 0x7f060021

.field public static final transparent:I = 0x7f060019

.field public static final unitSeparator:I = 0x7f06000b

.field public static final unitSeparatorBg:I = 0x7f06000c

.field public static final weatherAMPMCurrent:I = 0x7f060006

.field public static final weatherAMPMRegular:I = 0x7f060005

.field public static final weatherDate:I = 0x7f06001e

.field public static final weatherDay:I = 0x7f06001d

.field public static final weatherDay24hCurrent:I = 0x7f06000a

.field public static final weatherDay24hRegular:I = 0x7f060009

.field public static final weatherMaxTemp:I = 0x7f06001c

.field public static final weatherMinTemp:I = 0x7f06001b

.field public static final weatherTemp24hCurrent:I = 0x7f060004

.field public static final weatherTemp24hRegular:I = 0x7f060003

.field public static final weatherTime24hCurrent:I = 0x7f060002

.field public static final weatherTime24hRegular:I = 0x7f060001

.field public static final weatherWindSpeedCurrent:I = 0x7f060008

.field public static final weatherWindSpeedRegular:I = 0x7f060007


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
