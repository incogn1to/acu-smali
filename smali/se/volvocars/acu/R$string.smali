.class public final Lse/volvocars/acu/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final APP_AMAP:I = 0x7f090051

.field public static final APP_AUTONAVI:I = 0x7f090050

.field public static final APP_BROWSER:I = 0x7f09003b

.field public static final APP_DEEZER:I = 0x7f09004e

.field public static final APP_ICOYOTE:I = 0x7f09004c

.field public static final APP_IGO:I = 0x7f09004f

.field public static final APP_LIVERADIO:I = 0x7f090049

.field public static final APP_LOCATION:I = 0x7f09003f

.field public static final APP_MARKET:I = 0x7f090046

.field public static final APP_MEDIA:I = 0x7f090042

.field public static final APP_MOVIECLIPS:I = 0x7f090044

.field public static final APP_NAVX_ESSENCE:I = 0x7f090048

.field public static final APP_NAVX_PARKING:I = 0x7f090047

.field public static final APP_ROADTRIP:I = 0x7f09004a

.field public static final APP_SERVICE_BOOKING:I = 0x7f090040

.field public static final APP_SETTINGS:I = 0x7f09003e

.field public static final APP_SMARTLINK:I = 0x7f09004d

.field public static final APP_SPOTIFY:I = 0x7f090043

.field public static final APP_TUNE_IN:I = 0x7f09004b

.field public static final APP_VIDEO_IN:I = 0x7f090041

.field public static final APP_WIKANGO:I = 0x7f090045

.field public static final DISP_TEXT_GEO_LOC_AUTHORIZATION:I = 0x7f090055

.field public static final DISP_TEXT_LABEL_NO:I = 0x7f090057

.field public static final DISP_TEXT_LABEL_YES:I = 0x7f090056

.field public static final _appDrawer_:I = 0x7f09003c

.field public static final _mediaSources_:I = 0x7f09003d

.field public static final app_name:I = 0x7f090000

.field public static final app_order_title:I = 0x7f090005

.field public static final help_center_internet_connection_guide_button:I = 0x7f090052

.field public static final help_center_title:I = 0x7f090053

.field public static final line_in:I = 0x7f090014

.field public static final media_sources_list_title:I = 0x7f090011

.field public static final menu_item_dynamic:I = 0x7f090004

.field public static final menu_item_static:I = 0x7f090003

.field public static final no_artist:I = 0x7f090012

.field public static final no_internet_connection:I = 0x7f090059

.field public static final no_location_match:I = 0x7f090058

.field public static final no_track:I = 0x7f090013

.field public static final start_intent_app_drawer:I = 0x7f090001

.field public static final start_intent_media_sources:I = 0x7f090002

.field public static final tab_my_locations:I = 0x7f090039

.field public static final tab_settings:I = 0x7f09003a

.field public static final too_many_locations:I = 0x7f090038

.field public static final unit_beaufort:I = 0x7f09000b

.field public static final unit_beaufort_short:I = 0x7f09000c

.field public static final unit_celcius:I = 0x7f09000d

.field public static final unit_fahrenheit:I = 0x7f09000e

.field public static final unit_kmh:I = 0x7f09000a

.field public static final unit_knots:I = 0x7f090007

.field public static final unit_mph:I = 0x7f090009

.field public static final unit_mps:I = 0x7f090008

.field public static final unit_separator_temperature:I = 0x7f090010

.field public static final unit_separator_wind:I = 0x7f09000f

.field public static final unit_title:I = 0x7f090006

.field public static final weather_add_location_hint:I = 0x7f090030

.field public static final weather_blizzard:I = 0x7f090027

.field public static final weather_cloudy:I = 0x7f09002a

.field public static final weather_current_location:I = 0x7f090032

.field public static final weather_error_retrieving_data:I = 0x7f090033

.field public static final weather_fog:I = 0x7f090026

.field public static final weather_forecast_no_data:I = 0x7f090037

.field public static final weather_forecast_not_available:I = 0x7f090034

.field public static final weather_forecast_today:I = 0x7f090035

.field public static final weather_forecast_tomorrow:I = 0x7f090036

.field public static final weather_freezing_fog:I = 0x7f090025

.field public static final weather_freezing_rain:I = 0x7f090023

.field public static final weather_heavy_rain:I = 0x7f09001c

.field public static final weather_light_freezing_rain:I = 0x7f090024

.field public static final weather_light_rain:I = 0x7f09001e

.field public static final weather_light_rain_and_thunder:I = 0x7f090018

.field public static final weather_light_sleet:I = 0x7f090020

.field public static final weather_light_snow:I = 0x7f09001b

.field public static final weather_light_snow_and_thunder:I = 0x7f090016

.field public static final weather_light_snow_showers:I = 0x7f090022

.field public static final weather_mist:I = 0x7f090028

.field public static final weather_no_data:I = 0x7f090031

.field public static final weather_overcast:I = 0x7f090029

.field public static final weather_partly_cloudy:I = 0x7f09002b

.field public static final weather_rain:I = 0x7f09001d

.field public static final weather_rain_and_thunder:I = 0x7f090017

.field public static final weather_settings_add_location:I = 0x7f09002e

.field public static final weather_settings_title:I = 0x7f09002f

.field public static final weather_sleet:I = 0x7f09001f

.field public static final weather_smhi_lang_code:I = 0x7f090054

.field public static final weather_snow:I = 0x7f09001a

.field public static final weather_snow_and_thunder:I = 0x7f090015

.field public static final weather_snow_showers:I = 0x7f090021

.field public static final weather_sun:I = 0x7f09002c

.field public static final weather_sunny:I = 0x7f09002d

.field public static final weather_thunder:I = 0x7f090019


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
