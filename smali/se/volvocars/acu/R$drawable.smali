.class public final Lse/volvocars/acu/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final app_item_selector:I = 0x7f020000

.field public static final application_background_425x736px:I = 0x7f020001

.field public static final apporder_list_selector:I = 0x7f020002

.field public static final button_bg_states:I = 0x7f020003

.field public static final button_marked:I = 0x7f020004

.field public static final button_pressed:I = 0x7f020005

.field public static final button_regular:I = 0x7f020006

.field public static final category_bar_bg_736x35px:I = 0x7f020007

.field public static final cloudy_102x250px:I = 0x7f020008

.field public static final cloudy_175x250px:I = 0x7f020009

.field public static final cloudy_400x135px:I = 0x7f02000a

.field public static final delete_location_icon:I = 0x7f02000b

.field public static final divider:I = 0x7f02000c

.field public static final fog_102x250px:I = 0x7f02000d

.field public static final fog_175x250px:I = 0x7f02000e

.field public static final fog_400x135px:I = 0x7f02000f

.field public static final gps_arrow_highlighted_10x10px:I = 0x7f020010

.field public static final gps_arrow_highlighted_12x12px:I = 0x7f020011

.field public static final gps_arrow_not_highlighted_10x10px:I = 0x7f020012

.field public static final gps_arrow_not_highlighted_12x12px:I = 0x7f020013

.field public static final highlight_marked_state_80x80px:I = 0x7f020014

.field public static final highlight_pressed_state_80x80px:I = 0x7f020015

.field public static final icon:I = 0x7f020016

.field public static final idle_cover:I = 0x7f020017

.field public static final launcher_arrow_step1:I = 0x7f020018

.field public static final launcher_arrow_step10:I = 0x7f020019

.field public static final launcher_arrow_step11:I = 0x7f02001a

.field public static final launcher_arrow_step12:I = 0x7f02001b

.field public static final launcher_arrow_step13:I = 0x7f02001c

.field public static final launcher_arrow_step14:I = 0x7f02001d

.field public static final launcher_arrow_step15:I = 0x7f02001e

.field public static final launcher_arrow_step16:I = 0x7f02001f

.field public static final launcher_arrow_step17:I = 0x7f020020

.field public static final launcher_arrow_step18:I = 0x7f020021

.field public static final launcher_arrow_step19:I = 0x7f020022

.field public static final launcher_arrow_step2:I = 0x7f020023

.field public static final launcher_arrow_step20:I = 0x7f020024

.field public static final launcher_arrow_step21:I = 0x7f020025

.field public static final launcher_arrow_step22:I = 0x7f020026

.field public static final launcher_arrow_step23:I = 0x7f020027

.field public static final launcher_arrow_step24:I = 0x7f020028

.field public static final launcher_arrow_step25:I = 0x7f020029

.field public static final launcher_arrow_step26:I = 0x7f02002a

.field public static final launcher_arrow_step27:I = 0x7f02002b

.field public static final launcher_arrow_step28:I = 0x7f02002c

.field public static final launcher_arrow_step29:I = 0x7f02002d

.field public static final launcher_arrow_step3:I = 0x7f02002e

.field public static final launcher_arrow_step30:I = 0x7f02002f

.field public static final launcher_arrow_step31:I = 0x7f020030

.field public static final launcher_arrow_step4:I = 0x7f020031

.field public static final launcher_arrow_step5:I = 0x7f020032

.field public static final launcher_arrow_step6:I = 0x7f020033

.field public static final launcher_arrow_step7:I = 0x7f020034

.field public static final launcher_arrow_step8:I = 0x7f020035

.field public static final launcher_arrow_step9:I = 0x7f020036

.field public static final listitem_marked_32x54px:I = 0x7f020037

.field public static final listitem_pressed_32x54px:I = 0x7f020038

.field public static final locationlist_selector:I = 0x7f020039

.field public static final loose_listitem_markers_50x50px:I = 0x7f02003a

.field public static final music_back_icon:I = 0x7f02003b

.field public static final music_fwd_icon:I = 0x7f02003c

.field public static final music_ipod_iphone_highlighted_50x50:I = 0x7f02003d

.field public static final music_linein_highlighted_50x50:I = 0x7f02003e

.field public static final music_pause_icon:I = 0x7f02003f

.field public static final music_play_icon:I = 0x7f020040

.field public static final music_sdcard_highlighted_50x50:I = 0x7f020041

.field public static final music_usb_highlighted_50x50:I = 0x7f020042

.field public static final music_widget_back_icon_disabled_30x50px:I = 0x7f020043

.field public static final music_widget_back_icon_marked_30x50px:I = 0x7f020044

.field public static final music_widget_back_icon_pressed_30x50px:I = 0x7f020045

.field public static final music_widget_back_icon_regular_30x50px:I = 0x7f020046

.field public static final music_widget_fwd_icon_disabled_30x50px:I = 0x7f020047

.field public static final music_widget_fwd_icon_marked_30x50px:I = 0x7f020048

.field public static final music_widget_fwd_icon_pressed_30x50px:I = 0x7f020049

.field public static final music_widget_fwd_icon_regular_30x50px:I = 0x7f02004a

.field public static final music_widget_pause_icon_disabled_80x80px:I = 0x7f02004b

.field public static final music_widget_pause_icon_marked_80x80px:I = 0x7f02004c

.field public static final music_widget_pause_icon_pressed_80x80px:I = 0x7f02004d

.field public static final music_widget_pause_icon_regular_80x80px:I = 0x7f02004e

.field public static final music_widget_play_icon_disabled_80x80px:I = 0x7f02004f

.field public static final music_widget_play_icon_marked_80x80px:I = 0x7f020050

.field public static final music_widget_play_icon_pressed_80x80px:I = 0x7f020051

.field public static final music_widget_play_icon_regular_80x80px:I = 0x7f020052

.field public static final music_wifi_highlighted_50x50:I = 0x7f020053

.field public static final overcast_102x250px:I = 0x7f020054

.field public static final overcast_175x250px:I = 0x7f020055

.field public static final overcast_400x135px:I = 0x7f020056

.field public static final partly_sunny_102x250px:I = 0x7f020057

.field public static final partly_sunny_175x250px:I = 0x7f020058

.field public static final partly_sunny_400x135px:I = 0x7f020059

.field public static final pearl_not_selected_6x6px:I = 0x7f02005a

.field public static final pearl_selected_6x6px:I = 0x7f02005b

.field public static final popup_bg:I = 0x7f02005c

.field public static final popup_bg_744x319px:I = 0x7f02005d

.field public static final pressed_state_widget_524x133px:I = 0x7f02005e

.field public static final radiobutton_off_50x50px:I = 0x7f02005f

.field public static final radiobutton_on_50x50px:I = 0x7f020060

.field public static final radiobutton_pressed_50x50px:I = 0x7f020061

.field public static final rain_102x250px:I = 0x7f020062

.field public static final rain_175x250px:I = 0x7f020063

.field public static final rain_400x135px:I = 0x7f020064

.field public static final remove_location_icon:I = 0x7f020065

.field public static final scroll_bar:I = 0x7f020066

.field public static final search_icon:I = 0x7f020067

.field public static final seven_days_background_plate_720x250px:I = 0x7f020068

.field public static final seven_days_splitline_1x198px:I = 0x7f020069

.field public static final show_all_applications:I = 0x7f02006a

.field public static final shower_102x250px:I = 0x7f02006b

.field public static final shower_175x250px:I = 0x7f02006c

.field public static final shower_400x135px:I = 0x7f02006d

.field public static final sleet_102x250px:I = 0x7f02006e

.field public static final sleet_175x250px:I = 0x7f02006f

.field public static final sleet_400x135px:I = 0x7f020070

.field public static final snow_102x250px:I = 0x7f020071

.field public static final snow_175x250px:I = 0x7f020072

.field public static final snow_400x135px:I = 0x7f020073

.field public static final snowfall_102x250px:I = 0x7f020074

.field public static final snowfall_175x250px:I = 0x7f020075

.field public static final snowfall_400x135px:I = 0x7f020076

.field public static final sun_102x250px:I = 0x7f020077

.field public static final sun_175x250px:I = 0x7f020078

.field public static final sun_400x135px:I = 0x7f020079

.field public static final tab_marked_244x35px:I = 0x7f02007a

.field public static final tab_pressed_244x35px:I = 0x7f02007b

.field public static final tab_selected_244x35px:I = 0x7f02007c

.field public static final thunder_102x250px:I = 0x7f02007d

.field public static final thunder_175x250px:I = 0x7f02007e

.field public static final thunder_400x135px:I = 0x7f02007f

.field public static final thundershower_102x250px:I = 0x7f020080

.field public static final thundershower_175x250px:I = 0x7f020081

.field public static final thundershower_400x135px:I = 0x7f020082

.field public static final title_splitline_1x60px:I = 0x7f020083

.field public static final toast_bg:I = 0x7f020084

.field public static final trash_can:I = 0x7f020085

.field public static final twentyfour_hrs_rightnow_backgroundplate_175x250px:I = 0x7f020086

.field public static final unit_celcius_icon:I = 0x7f020087

.field public static final unit_celsius_not_selected_50x50px:I = 0x7f020088

.field public static final unit_celsius_selected_50x50px:I = 0x7f020089

.field public static final unit_fahrenheit_icon:I = 0x7f02008a

.field public static final unit_farenheit_not_selected_50x50px:I = 0x7f02008b

.field public static final unit_farenheit_selected_50x50px:I = 0x7f02008c

.field public static final unit_popup_backgroundplate_720x368px:I = 0x7f02008d

.field public static final weather_add_btn:I = 0x7f02008e

.field public static final weather_add_location_button_pressed_50x50px:I = 0x7f02008f

.field public static final weather_add_location_button_regular_50x50px:I = 0x7f020090

.field public static final weather_by_smhi:I = 0x7f020091

.field public static final weather_delete_btn:I = 0x7f020092

.field public static final weather_delete_button_active_50x50px:I = 0x7f020093

.field public static final weather_delete_button_pressed_50x50px:I = 0x7f020094

.field public static final weather_edit_btn:I = 0x7f020095

.field public static final weather_edit_button_active_50x50px:I = 0x7f020096

.field public static final weather_edit_button_pressed_50x50px:I = 0x7f020097

.field public static final weather_edit_button_regular_50x50px:I = 0x7f020098

.field public static final weather_flip_page_icon_24x16px:I = 0x7f020099

.field public static final weather_pearl:I = 0x7f02009a

.field public static final weather_pearl_selected:I = 0x7f02009b

.field public static final weather_search_background:I = 0x7f02009c

.field public static final weather_temporary:I = 0x7f02009d

.field public static final wind_e_highlighted_24x24px:I = 0x7f02009e

.field public static final wind_e_not_highlighted_24x24px:I = 0x7f02009f

.field public static final wind_n_highlighted_24x24px:I = 0x7f0200a0

.field public static final wind_n_not_highlighted_24x24px:I = 0x7f0200a1

.field public static final wind_ne_highlighted_24x24px:I = 0x7f0200a2

.field public static final wind_ne_not_highlighted_24x24px:I = 0x7f0200a3

.field public static final wind_nw_highlighted_24x24px:I = 0x7f0200a4

.field public static final wind_nw_not_highlighted_24x24px:I = 0x7f0200a5

.field public static final wind_s_highlighted_24x24px:I = 0x7f0200a6

.field public static final wind_s_not_highlighted_24x24px:I = 0x7f0200a7

.field public static final wind_se_highlighted_24x24px:I = 0x7f0200a8

.field public static final wind_se_not_highlighted_24x24px:I = 0x7f0200a9

.field public static final wind_sw_highlighted_24x24px:I = 0x7f0200aa

.field public static final wind_sw_not_highlighted_24x24px:I = 0x7f0200ab

.field public static final wind_w_highlighted_24x24px:I = 0x7f0200ac

.field public static final wind_w_not_highlighted_24x24px:I = 0x7f0200ad


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
