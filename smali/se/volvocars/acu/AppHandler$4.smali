.class Lse/volvocars/acu/AppHandler$4;
.super Ljava/lang/Object;
.source "AppHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/AppHandler;->indicateAppBeingStarted(Ljava/util/List;Lse/volvocars/acu/model/AppInfo;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/AppHandler;

.field final synthetic val$app:Lse/volvocars/acu/model/AppInfo;

.field final synthetic val$apps:Ljava/util/List;

.field final synthetic val$index:I


# direct methods
.method constructor <init>(Lse/volvocars/acu/AppHandler;Lse/volvocars/acu/model/AppInfo;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lse/volvocars/acu/AppHandler$4;->this$0:Lse/volvocars/acu/AppHandler;

    iput-object p2, p0, Lse/volvocars/acu/AppHandler$4;->val$app:Lse/volvocars/acu/model/AppInfo;

    iput-object p3, p0, Lse/volvocars/acu/AppHandler$4;->val$apps:Ljava/util/List;

    iput p4, p0, Lse/volvocars/acu/AppHandler$4;->val$index:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 326
    iget-object v2, p0, Lse/volvocars/acu/AppHandler$4;->this$0:Lse/volvocars/acu/AppHandler;

    # getter for: Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;
    invoke-static {v2}, Lse/volvocars/acu/AppHandler;->access$500(Lse/volvocars/acu/AppHandler;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 327
    const/4 v0, 0x0

    .line 329
    .local v0, "movedToDefault":Z
    :try_start_0
    iget-object v3, p0, Lse/volvocars/acu/AppHandler$4;->this$0:Lse/volvocars/acu/AppHandler;

    # getter for: Lse/volvocars/acu/AppHandler;->mReorderApps:Z
    invoke-static {v3}, Lse/volvocars/acu/AppHandler;->access$600(Lse/volvocars/acu/AppHandler;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 330
    iget-object v3, p0, Lse/volvocars/acu/AppHandler$4;->this$0:Lse/volvocars/acu/AppHandler;

    # getter for: Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;
    invoke-static {v3}, Lse/volvocars/acu/AppHandler;->access$700(Lse/volvocars/acu/AppHandler;)Lse/volvocars/acu/model/Apps;

    move-result-object v3

    iget-object v4, p0, Lse/volvocars/acu/AppHandler$4;->val$app:Lse/volvocars/acu/model/AppInfo;

    invoke-virtual {v3, v4}, Lse/volvocars/acu/model/Apps;->appStartedReorder(Lse/volvocars/acu/model/AppInfo;)Z

    move-result v1

    .line 331
    .local v1, "reordered":Z
    iget-object v3, p0, Lse/volvocars/acu/AppHandler$4;->val$apps:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    if-eqz v1, :cond_0

    .line 332
    iget-object v3, p0, Lse/volvocars/acu/AppHandler$4;->this$0:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v3}, Lse/volvocars/acu/AppHandler;->moveToDefaultIcon()V

    .line 333
    const/4 v0, 0x1

    .line 336
    .end local v1    # "reordered":Z
    :cond_0
    if-eqz v0, :cond_1

    .line 337
    iget-object v3, p0, Lse/volvocars/acu/AppHandler$4;->this$0:Lse/volvocars/acu/AppHandler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lse/volvocars/acu/AppHandler;->saveFocusApp(I)V

    .line 341
    :goto_0
    iget-object v3, p0, Lse/volvocars/acu/AppHandler$4;->this$0:Lse/volvocars/acu/AppHandler;

    # getter for: Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;
    invoke-static {v3}, Lse/volvocars/acu/AppHandler;->access$800(Lse/volvocars/acu/AppHandler;)Lse/volvocars/acu/AcuActivity;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lse/volvocars/acu/AcuActivity;->setIsStartingApp(Z)V

    .line 342
    monitor-exit v2

    .line 343
    return-void

    .line 339
    :cond_1
    iget-object v3, p0, Lse/volvocars/acu/AppHandler$4;->this$0:Lse/volvocars/acu/AppHandler;

    iget v4, p0, Lse/volvocars/acu/AppHandler$4;->val$index:I

    invoke-virtual {v3, v4}, Lse/volvocars/acu/AppHandler;->saveFocusApp(I)V

    goto :goto_0

    .line 342
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method
