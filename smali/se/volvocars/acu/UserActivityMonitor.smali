.class public final Lse/volvocars/acu/UserActivityMonitor;
.super Ljava/lang/Object;
.source "UserActivityMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/UserActivityMonitor$1;,
        Lse/volvocars/acu/UserActivityMonitor$IdleIntentReceiver;,
        Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ActivityMonitor"


# instance fields
.field private final mActivity:Lse/volvocars/acu/AcuActivity;

.field private mIdle:Z

.field private final mIdleCountdown:Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;

.field private final mIdleCountdownExecutor:Ljava/util/concurrent/ExecutorService;

.field private mIdleFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private final mIdleReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/AcuActivity;)V
    .locals 3
    .param p1, "activity"    # Lse/volvocars/acu/AcuActivity;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleCountdownExecutor:Ljava/util/concurrent/ExecutorService;

    .line 26
    const/4 v1, 0x1

    iput-boolean v1, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdle:Z

    .line 29
    new-instance v1, Lse/volvocars/acu/UserActivityMonitor$IdleIntentReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lse/volvocars/acu/UserActivityMonitor$IdleIntentReceiver;-><init>(Lse/volvocars/acu/UserActivityMonitor;Lse/volvocars/acu/UserActivityMonitor$1;)V

    iput-object v1, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleReceiver:Landroid/content/BroadcastReceiver;

    .line 32
    iput-object p1, p0, Lse/volvocars/acu/UserActivityMonitor;->mActivity:Lse/volvocars/acu/AcuActivity;

    .line 33
    new-instance v1, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;

    invoke-direct {v1, p0}, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;-><init>(Lse/volvocars/acu/UserActivityMonitor;)V

    iput-object v1, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleCountdown:Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;

    .line 34
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "se.volvocars.acu.IDLE_MODE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 35
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lse/volvocars/acu/UserActivityMonitor;->mActivity:Lse/volvocars/acu/AcuActivity;

    iget-object v2, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lse/volvocars/acu/AcuActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 37
    return-void
.end method

.method static synthetic access$100(Lse/volvocars/acu/UserActivityMonitor;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/UserActivityMonitor;

    .prologue
    .line 18
    iget-boolean v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdle:Z

    return v0
.end method

.method static synthetic access$200(Lse/volvocars/acu/UserActivityMonitor;Z)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/UserActivityMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lse/volvocars/acu/UserActivityMonitor;->setIdleMode(Z)V

    return-void
.end method

.method static synthetic access$300(Lse/volvocars/acu/UserActivityMonitor;)Ljava/util/concurrent/Future;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/UserActivityMonitor;

    .prologue
    .line 18
    iget-object v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;

    return-object v0
.end method

.method static synthetic access$400(Lse/volvocars/acu/UserActivityMonitor;)Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/UserActivityMonitor;

    .prologue
    .line 18
    iget-object v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleCountdown:Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;

    return-object v0
.end method

.method private setIdleMode(Z)V
    .locals 1
    .param p1, "idle"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdle:Z

    .line 64
    iget-object v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/AcuActivity;->setIdleMode(Z)V

    .line 65
    return-void
.end method


# virtual methods
.method public onActivity()V
    .locals 2

    .prologue
    .line 51
    iget-boolean v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdle:Z

    if-eqz v0, :cond_0

    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lse/volvocars/acu/UserActivityMonitor;->setIdleMode(Z)V

    .line 54
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_1

    .line 55
    iget-object v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleCountdown:Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;

    invoke-virtual {v0}, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->restart()V

    .line 60
    :goto_0
    return-void

    .line 57
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleCountdownExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleCountdown:Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 44
    iget-object v0, p0, Lse/volvocars/acu/UserActivityMonitor;->mActivity:Lse/volvocars/acu/AcuActivity;

    iget-object v1, p0, Lse/volvocars/acu/UserActivityMonitor;->mIdleReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AcuActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 45
    return-void
.end method
