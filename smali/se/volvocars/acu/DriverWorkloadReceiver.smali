.class public Lse/volvocars/acu/DriverWorkloadReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DriverWorkloadReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;
    }
.end annotation


# static fields
.field public static final DRIVERWORKLOAD_ACTION:Ljava/lang/String; = "com.parrot.DRIVER_WL"

.field static final DRIVERWORKLOAD_ACTIVATION:Ljava/lang/String; = "activation"

.field static final DRIVERWORKLOAD_WHITELIST:Ljava/lang/String; = "whitelist"

.field private static final TAG:Ljava/lang/String; = "DriverWorkloadBroadcastReceiver"

.field private static mDWActive:Z

.field private static mDWWhitelist:[Ljava/lang/String;

.field private static mWorkloadListener:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 31
    return-void
.end method

.method public static getWorkloadListener()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    sget-object v0, Lse/volvocars/acu/DriverWorkloadReceiver;->mWorkloadListener:Ljava/util/List;

    return-object v0
.end method

.method public static removeWorkLoadListener(Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;)V
    .locals 1
    .param p0, "listener"    # Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;

    .prologue
    .line 47
    sget-object v0, Lse/volvocars/acu/DriverWorkloadReceiver;->mWorkloadListener:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 48
    sget-object v0, Lse/volvocars/acu/DriverWorkloadReceiver;->mWorkloadListener:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 50
    :cond_0
    return-void
.end method

.method public static setWorkloadListener(Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;)V
    .locals 1
    .param p0, "listener"    # Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;

    .prologue
    .line 36
    sget-object v0, Lse/volvocars/acu/DriverWorkloadReceiver;->mWorkloadListener:Ljava/util/List;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lse/volvocars/acu/DriverWorkloadReceiver;->mWorkloadListener:Ljava/util/List;

    .line 39
    :cond_0
    sget-object v0, Lse/volvocars/acu/DriverWorkloadReceiver;->mWorkloadListener:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-string v8, "activation"

    const-string v7, "DriverWorkloadBroadcastReceiver"

    const-string v6, "whitelist"

    .line 58
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "action":Ljava/lang/String;
    const-string v4, "com.parrot.DRIVER_WL"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 61
    const-string v4, "activation"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "whitelist"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 63
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "activation"

    const/4 v5, 0x0

    invoke-virtual {v4, v8, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    sput-boolean v4, Lse/volvocars/acu/DriverWorkloadReceiver;->mDWActive:Z

    .line 64
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "whitelist"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lse/volvocars/acu/DriverWorkloadReceiver;->mDWWhitelist:[Ljava/lang/String;

    .line 66
    sget-object v4, Lse/volvocars/acu/DriverWorkloadReceiver;->mDWWhitelist:[Ljava/lang/String;

    if-nez v4, :cond_0

    .line 67
    const-string v4, "DriverWorkloadBroadcastReceiver"

    const-string v4, "whitelist not of type StringArray, Using a string instead! This is for test purpose only"

    invoke-static {v7, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "whitelist"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lse/volvocars/acu/DriverWorkloadReceiver;->mDWWhitelist:[Ljava/lang/String;

    .line 71
    :cond_0
    invoke-static {}, Lse/volvocars/acu/DriverWorkloadReceiver;->getWorkloadListener()Ljava/util/List;

    move-result-object v3

    .line 72
    .local v3, "listeners":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;>;"
    if-eqz v3, :cond_2

    .line 73
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;

    .line 74
    .local v2, "listener":Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;
    sget-boolean v4, Lse/volvocars/acu/DriverWorkloadReceiver;->mDWActive:Z

    sget-object v5, Lse/volvocars/acu/DriverWorkloadReceiver;->mDWWhitelist:[Ljava/lang/String;

    invoke-interface {v2, v4, v5}, Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;->onDriverWorkloadReceive(Z[Ljava/lang/String;)V

    goto :goto_0

    .line 81
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "listener":Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;
    .end local v3    # "listeners":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;>;"
    :cond_1
    const-string v4, "DriverWorkloadBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown command intent:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_2
    return-void
.end method
