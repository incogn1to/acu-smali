.class public final Lse/volvocars/acu/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final AddFavoriteLayout:I = 0x7f07000a

.field public static final CancelButton:I = 0x7f070010

.field public static final OkButton:I = 0x7f07000e

.field public static final RelativeLayout01:I = 0x7f07000f

.field public static final add_location_view:I = 0x7f070042

.field public static final all_apps:I = 0x7f070006

.field public static final all_apps_title:I = 0x7f070005

.field public static final animation_container:I = 0x7f070016

.field public static final clock_container:I = 0x7f070015

.field public static final condition_symbol:I = 0x7f07005e

.field public static final condition_text:I = 0x7f070061

.field public static final date:I = 0x7f070028

.field public static final day:I = 0x7f070027

.field public static final day1:I = 0x7f070047

.field public static final day2:I = 0x7f070048

.field public static final day3:I = 0x7f070049

.field public static final day4:I = 0x7f07004a

.field public static final day5:I = 0x7f07004b

.field public static final day6:I = 0x7f07004c

.field public static final day7:I = 0x7f07004d

.field public static final delete_location:I = 0x7f070019

.field public static final divider:I = 0x7f07000c

.field public static final edit_location:I = 0x7f07001b

.field public static final fling:I = 0x7f070001

.field public static final frame:I = 0x7f07003c

.field public static final homescreen:I = 0x7f070013

.field public static final hour1:I = 0x7f07004f

.field public static final hour2:I = 0x7f070050

.field public static final hour3:I = 0x7f070051

.field public static final hour4:I = 0x7f070052

.field public static final icon_add:I = 0x7f070045

.field public static final label:I = 0x7f070007

.field public static final listview:I = 0x7f07000d

.field public static final location:I = 0x7f070060

.field public static final locationList:I = 0x7f070043

.field public static final locationSearchList:I = 0x7f070040

.field public static final location_header:I = 0x7f070044

.field public static final location_view:I = 0x7f07001a

.field public static final locations:I = 0x7f07003d

.field public static final locations_area:I = 0x7f07002d

.field public static final locations_country:I = 0x7f07002e

.field public static final locations_title:I = 0x7f07002b

.field public static final locations_view:I = 0x7f070041

.field public static final max:I = 0x7f070029

.field public static final mediaListView:I = 0x7f07001e

.field public static final mediaWidget:I = 0x7f070017

.field public static final media_artist_text:I = 0x7f070024

.field public static final media_info_layout:I = 0x7f070023

.field public static final media_source_icon:I = 0x7f07001c

.field public static final media_source_name:I = 0x7f07001d

.field public static final media_track_text:I = 0x7f070025

.field public static final middle_part:I = 0x7f070014

.field public static final min:I = 0x7f07002a

.field public static final music_container:I = 0x7f07001f

.field public static final nextButton:I = 0x7f070022

.field public static final noInternetTextView:I = 0x7f07003f

.field public static final noMatchTextView:I = 0x7f07003e

.field public static final none:I = 0x7f070000

.field public static final pearls:I = 0x7f070062

.field public static final playButton:I = 0x7f070021

.field public static final previousButton:I = 0x7f070020

.field public static final radiobutton:I = 0x7f070009

.field public static final root_layout:I = 0x7f070053

.field public static final separator:I = 0x7f07003b

.field public static final settings_format_celcius:I = 0x7f070030

.field public static final settings_format_celsius:I = 0x7f070057

.field public static final settings_format_fahrenheit:I = 0x7f070031

.field public static final settings_title:I = 0x7f07002f

.field public static final seven_days_item:I = 0x7f070026

.field public static final seven_days_layout:I = 0x7f070046

.field public static final slide:I = 0x7f070002

.field public static final slideLeft:I = 0x7f070004

.field public static final slideRight:I = 0x7f070003

.field public static final smhi_logo:I = 0x7f07005c

.field public static final split_line:I = 0x7f07002c

.field public static final tab_container:I = 0x7f070056

.field public static final tab_my_locations:I = 0x7f070058

.field public static final tab_settings:I = 0x7f070059

.field public static final temperature:I = 0x7f070037

.field public static final textview:I = 0x7f070008

.field public static final time:I = 0x7f070035

.field public static final timeAMPM:I = 0x7f070036

.field public static final time_text:I = 0x7f070011

.field public static final timeformat_text:I = 0x7f070012

.field public static final title:I = 0x7f07000b

.field public static final title_locations:I = 0x7f070055

.field public static final title_settings:I = 0x7f070054

.field public static final toastText:I = 0x7f070033

.field public static final toast_layout:I = 0x7f070032

.field public static final twenty_four_hour_item:I = 0x7f070034

.field public static final twenty_four_hour_layout:I = 0x7f07004e

.field public static final unit_popup:I = 0x7f07003a

.field public static final weatherWidget:I = 0x7f070018

.field public static final weather_divider:I = 0x7f07005f

.field public static final weather_locations:I = 0x7f07005b

.field public static final weather_settings:I = 0x7f07005a

.field public static final weather_widget:I = 0x7f07005d

.field public static final windArrow:I = 0x7f070038

.field public static final windSpeed:I = 0x7f070039

.field public static final wizard_start_button:I = 0x7f070063


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
