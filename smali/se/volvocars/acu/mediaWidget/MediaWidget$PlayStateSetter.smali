.class final Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;
.super Ljava/lang/Object;
.source "MediaWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/mediaWidget/MediaWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PlayStateSetter"
.end annotation


# instance fields
.field private final isPlaying:Z

.field final synthetic this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/mediaWidget/MediaWidget;Z)V
    .locals 0
    .param p2, "isPlaying"    # Z

    .prologue
    .line 486
    iput-object p1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 487
    iput-boolean p2, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->isPlaying:Z

    .line 488
    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/mediaWidget/MediaWidget;ZLse/volvocars/acu/mediaWidget/MediaWidget$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;
    .param p2, "x1"    # Z
    .param p3, "x2"    # Lse/volvocars/acu/mediaWidget/MediaWidget$1;

    .prologue
    .line 483
    invoke-direct {p0, p1, p2}, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;-><init>(Lse/volvocars/acu/mediaWidget/MediaWidget;Z)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 492
    iget-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->isPlaying:Z

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # getter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mPlayPauseButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$700(Lse/volvocars/acu/mediaWidget/MediaWidget;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02003f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 495
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    const/4 v1, 0x0

    # invokes: Lse/volvocars/acu/mediaWidget/MediaWidget;->setPlayButtonEmitter(Z)V
    invoke-static {v0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$800(Lse/volvocars/acu/mediaWidget/MediaWidget;Z)V

    .line 510
    :cond_0
    :goto_0
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    iget-boolean v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->isPlaying:Z

    # setter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mIsPlaying:Z
    invoke-static {v0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$1202(Lse/volvocars/acu/mediaWidget/MediaWidget;Z)Z

    .line 511
    return-void

    .line 499
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # getter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mPlayPauseButton:Landroid/widget/ImageView;
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$700(Lse/volvocars/acu/mediaWidget/MediaWidget;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f020040

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 501
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # getter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mIdle:Z
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$900(Lse/volvocars/acu/mediaWidget/MediaWidget;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # getter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mActive:Z
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$1000(Lse/volvocars/acu/mediaWidget/MediaWidget;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # invokes: Lse/volvocars/acu/mediaWidget/MediaWidget;->isSourceRelevant()Z
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$1100(Lse/volvocars/acu/mediaWidget/MediaWidget;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    const/4 v1, 0x1

    # invokes: Lse/volvocars/acu/mediaWidget/MediaWidget;->setPlayButtonEmitter(Z)V
    invoke-static {v0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$800(Lse/volvocars/acu/mediaWidget/MediaWidget;Z)V

    goto :goto_0
.end method
