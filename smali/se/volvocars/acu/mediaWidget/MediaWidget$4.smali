.class Lse/volvocars/acu/mediaWidget/MediaWidget$4;
.super Ljava/lang/Object;
.source "MediaWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/mediaWidget/MediaWidget;->setTrackInfo(Lcom/parrot/asteroid/audio/service/Metadata;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

.field final synthetic val$artist:Ljava/lang/String;

.field final synthetic val$track:Ljava/lang/String;


# direct methods
.method constructor <init>(Lse/volvocars/acu/mediaWidget/MediaWidget;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    iput-object p2, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->val$artist:Ljava/lang/String;

    iput-object p3, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->val$track:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const-string v3, ""

    .line 305
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->val$artist:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->val$artist:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 306
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # getter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mArtistText:Landroid/widget/TextView;
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$300(Lse/volvocars/acu/mediaWidget/MediaWidget;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->val$artist:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    :goto_0
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->val$track:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->val$track:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 311
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # getter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mTrackText:Landroid/widget/TextView;
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$400(Lse/volvocars/acu/mediaWidget/MediaWidget;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->val$track:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    :goto_1
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # getter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mArtistText:Landroid/widget/TextView;
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$300(Lse/volvocars/acu/mediaWidget/MediaWidget;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090012

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 313
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # getter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mTrackText:Landroid/widget/TextView;
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$400(Lse/volvocars/acu/mediaWidget/MediaWidget;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$4;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090013

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
