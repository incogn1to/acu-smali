.class public Lse/volvocars/acu/mediaWidget/MediaWidget;
.super Landroid/widget/LinearLayout;
.source "MediaWidget.java"

# interfaces
.implements Lcom/parrot/asteroid/media/MediaObserverInterface;
.implements Lcom/parrot/asteroid/ManagerObserverInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;
    }
.end annotation


# static fields
.field private static final FADE_ALPHA:I = 0x59

.field private static final FULL_ALPHA:I = 0xff

.field protected static final TAG:Ljava/lang/String; = "MediaWidget"


# instance fields
.field private mActive:Z

.field private mArtistText:Landroid/widget/TextView;

.field private mBackButton:Landroid/widget/ImageView;

.field private mContainerView:Landroid/view/View;

.field private mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

.field private mFwdButton:Landroid/widget/ImageView;

.field private mHandler:Landroid/os/Handler;

.field private mIdle:Z

.field private mIsPlaying:Z

.field private mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

.field private mMediaControler:Lcom/parrot/asteroid/media/common/MediaPlayerControler;

.field private mPlayPauseButton:Landroid/widget/ImageView;

.field private final mTextFadeGrayColor:I

.field private final mTextFadeWhiteColor:I

.field private final mTextGrayColor:I

.field private final mTextGrayGlowColor:I

.field private final mTextWhiteColor:I

.field private final mTextWhiteGlowColor:I

.field private mTrackText:Landroid/widget/TextView;

.field private final mTypeface:Landroid/graphics/Typeface;

.field private requestPlayerStatusWhenReady:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    .line 63
    iput-boolean v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mIsPlaying:Z

    .line 64
    iput-boolean v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mIdle:Z

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mActive:Z

    .line 67
    iput-boolean v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->requestPlayerStatusWhenReady:Z

    .line 72
    invoke-virtual {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextFadeWhiteColor:I

    .line 73
    invoke-virtual {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextWhiteColor:I

    .line 74
    invoke-virtual {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextWhiteGlowColor:I

    .line 75
    invoke-virtual {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextFadeGrayColor:I

    .line 76
    invoke-virtual {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextGrayColor:I

    .line 77
    invoke-virtual {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextGrayGlowColor:I

    .line 78
    invoke-virtual {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts/VolvoSanProLig.otf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTypeface:Landroid/graphics/Typeface;

    .line 79
    invoke-direct {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->init()V

    .line 80
    return-void
.end method

.method static synthetic access$000(Lse/volvocars/acu/mediaWidget/MediaWidget;)Lcom/parrot/asteroid/audio/service/Source;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;

    .prologue
    .line 29
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    return-object v0
.end method

.method static synthetic access$100(Lse/volvocars/acu/mediaWidget/MediaWidget;)Lcom/parrot/asteroid/media/common/MediaPlayerControler;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;

    .prologue
    .line 29
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mMediaControler:Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    return-object v0
.end method

.method static synthetic access$1000(Lse/volvocars/acu/mediaWidget/MediaWidget;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;

    .prologue
    .line 29
    iget-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mActive:Z

    return v0
.end method

.method static synthetic access$1100(Lse/volvocars/acu/mediaWidget/MediaWidget;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;

    .prologue
    .line 29
    invoke-direct {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->isSourceRelevant()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1202(Lse/volvocars/acu/mediaWidget/MediaWidget;Z)Z
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mIsPlaying:Z

    return p1
.end method

.method static synthetic access$300(Lse/volvocars/acu/mediaWidget/MediaWidget;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;

    .prologue
    .line 29
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mArtistText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lse/volvocars/acu/mediaWidget/MediaWidget;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;

    .prologue
    .line 29
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTrackText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lse/volvocars/acu/mediaWidget/MediaWidget;Z)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setButtonVisibility(Z)V

    return-void
.end method

.method static synthetic access$600(Lse/volvocars/acu/mediaWidget/MediaWidget;Z)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setButtonsEnabled(Z)V

    return-void
.end method

.method static synthetic access$700(Lse/volvocars/acu/mediaWidget/MediaWidget;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;

    .prologue
    .line 29
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mPlayPauseButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lse/volvocars/acu/mediaWidget/MediaWidget;Z)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPlayButtonEmitter(Z)V

    return-void
.end method

.method static synthetic access$900(Lse/volvocars/acu/mediaWidget/MediaWidget;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/mediaWidget/MediaWidget;

    .prologue
    .line 29
    iget-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mIdle:Z

    return v0
.end method

.method private getButtonsVisibility()Z
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mBackButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mFwdButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mPlayPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 83
    new-instance v0, Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    invoke-virtual {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/parrot/asteroid/media/common/MediaPlayerControler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mMediaControler:Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    .line 84
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mMediaControler:Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    invoke-virtual {v0, p0}, Lcom/parrot/asteroid/media/common/MediaPlayerControler;->addListener(Lcom/parrot/asteroid/media/MediaObserverInterface;)V

    .line 85
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mMediaControler:Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    invoke-virtual {v0, p0}, Lcom/parrot/asteroid/media/common/MediaPlayerControler;->addManagerObserver(Lcom/parrot/asteroid/ManagerObserverInterface;)V

    .line 86
    const-string v0, "MediaWidget"

    const-string v1, "Adding media listener and manager observer."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mHandler:Landroid/os/Handler;

    .line 89
    return-void
.end method

.method private isSourceRelevant()Z
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    if-nez v0, :cond_0

    .line 269
    const/4 v0, 0x0

    .line 271
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setButtonVisibility(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 349
    if-eqz p1, :cond_0

    .line 350
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mBackButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mPlayPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 352
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mFwdButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 358
    :goto_0
    return-void

    .line 354
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mBackButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 355
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mPlayPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mFwdButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setButtonsEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 339
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mFwdButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 340
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mBackButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 341
    return-void
.end method

.method private setPlayButtonEmitter(Z)V
    .locals 1
    .param p1, "state"    # Z

    .prologue
    .line 262
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setPlayButtonEmitterState(Z)V

    .line 265
    :cond_0
    return-void
.end method

.method private setSourceInfo(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 2
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 366
    if-nez p1, :cond_0

    .line 367
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mHandler:Landroid/os/Handler;

    new-instance v1, Lse/volvocars/acu/mediaWidget/MediaWidget$5;

    invoke-direct {v1, p0}, Lse/volvocars/acu/mediaWidget/MediaWidget$5;-><init>(Lse/volvocars/acu/mediaWidget/MediaWidget;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 375
    iput-object p1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    .line 399
    :goto_0
    return-void

    .line 378
    :cond_0
    if-eqz p1, :cond_1

    .line 379
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mHandler:Landroid/os/Handler;

    new-instance v1, Lse/volvocars/acu/mediaWidget/MediaWidget$6;

    invoke-direct {v1, p0}, Lse/volvocars/acu/mediaWidget/MediaWidget$6;-><init>(Lse/volvocars/acu/mediaWidget/MediaWidget;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 388
    :cond_1
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getType()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_2

    .line 389
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mHandler:Landroid/os/Handler;

    new-instance v1, Lse/volvocars/acu/mediaWidget/MediaWidget$7;

    invoke-direct {v1, p0}, Lse/volvocars/acu/mediaWidget/MediaWidget$7;-><init>(Lse/volvocars/acu/mediaWidget/MediaWidget;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 398
    :cond_2
    iput-object p1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    goto :goto_0
.end method

.method private setTrackInfo(Lcom/parrot/asteroid/audio/service/Metadata;)V
    .locals 5
    .param p1, "data"    # Lcom/parrot/asteroid/audio/service/Metadata;

    .prologue
    .line 295
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Metadata;->getArtist()Ljava/lang/String;

    move-result-object v0

    .line 296
    .local v0, "artist":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Metadata;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 297
    .local v2, "track":Ljava/lang/String;
    const/4 v1, 0x0

    .line 298
    .local v1, "lineIn":Z
    iget-object v3, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    invoke-virtual {v3}, Lcom/parrot/asteroid/audio/service/Source;->getType()I

    move-result v3

    const/16 v4, 0x9

    if-ne v3, v4, :cond_0

    .line 299
    const/4 v1, 0x1

    .line 301
    :cond_0
    if-nez v1, :cond_1

    iget-object v3, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    if-eqz v3, :cond_1

    .line 302
    iget-object v3, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mHandler:Landroid/os/Handler;

    new-instance v4, Lse/volvocars/acu/mediaWidget/MediaWidget$4;

    invoke-direct {v4, p0, v0, v2}, Lse/volvocars/acu/mediaWidget/MediaWidget$4;-><init>(Lse/volvocars/acu/mediaWidget/MediaWidget;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 319
    :cond_1
    return-void
.end method


# virtual methods
.method public activate()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 200
    iget-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mIsPlaying:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mIdle:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->isSourceRelevant()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    invoke-direct {p0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPlayButtonEmitter(Z)V

    .line 203
    :cond_0
    iput-boolean v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mActive:Z

    .line 204
    return-void
.end method

.method public deactivate()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 209
    iput-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mActive:Z

    .line 210
    invoke-direct {p0, v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPlayButtonEmitter(Z)V

    .line 211
    return-void
.end method

.method public getButtonsEnabled()Z
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mBackButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mFwdButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPlayState()Z
    .locals 1

    .prologue
    .line 287
    iget-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mIsPlaying:Z

    return v0
.end method

.method public hasSource()Z
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mActive:Z

    return v0
.end method

.method public isInIdleMode()Z
    .locals 1

    .prologue
    .line 275
    iget-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mIdle:Z

    return v0
.end method

.method public isInPressedState()Z
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTrackText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iget v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextWhiteGlowColor:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mArtistText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iget v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextGrayGlowColor:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mContainerView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isWidgetHidden()Z
    .locals 3

    .prologue
    const-string v2, ""

    .line 326
    invoke-direct {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getButtonsVisibility()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mArtistText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTrackText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreatingSource(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 0
    .param p1, "arg0"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 453
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mMediaControler:Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    invoke-virtual {v0, p0}, Lcom/parrot/asteroid/media/common/MediaPlayerControler;->removeListener(Lcom/parrot/asteroid/media/MediaObserverInterface;)V

    .line 223
    const-string v0, "MediaWidget"

    const-string v1, "Destroying"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 94
    const v1, 0x7f07001f

    invoke-virtual {p0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mContainerView:Landroid/view/View;

    .line 95
    const v1, 0x7f070023

    invoke-virtual {p0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 97
    .local v0, "infoView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 98
    new-instance v1, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;

    invoke-direct {v1, p0}, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;-><init>(Lse/volvocars/acu/mediaWidget/MediaWidget;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 100
    const v1, 0x7f070024

    invoke-virtual {p0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mArtistText:Landroid/widget/TextView;

    .line 101
    const v1, 0x7f070025

    invoke-virtual {p0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTrackText:Landroid/widget/TextView;

    .line 103
    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mArtistText:Landroid/widget/TextView;

    iget-object v2, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 104
    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTrackText:Landroid/widget/TextView;

    iget-object v2, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 106
    const v1, 0x7f070020

    invoke-virtual {p0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mBackButton:Landroid/widget/ImageView;

    .line 108
    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mBackButton:Landroid/widget/ImageView;

    new-instance v2, Lse/volvocars/acu/mediaWidget/MediaWidget$1;

    invoke-direct {v2, p0}, Lse/volvocars/acu/mediaWidget/MediaWidget$1;-><init>(Lse/volvocars/acu/mediaWidget/MediaWidget;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    const v1, 0x7f070021

    invoke-virtual {p0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mPlayPauseButton:Landroid/widget/ImageView;

    .line 125
    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mPlayPauseButton:Landroid/widget/ImageView;

    new-instance v2, Lse/volvocars/acu/mediaWidget/MediaWidget$2;

    invoke-direct {v2, p0}, Lse/volvocars/acu/mediaWidget/MediaWidget$2;-><init>(Lse/volvocars/acu/mediaWidget/MediaWidget;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    const v1, 0x7f070022

    invoke-virtual {p0, v1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mFwdButton:Landroid/widget/ImageView;

    .line 141
    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mFwdButton:Landroid/widget/ImageView;

    new-instance v2, Lse/volvocars/acu/mediaWidget/MediaWidget$3;

    invoke-direct {v2, p0}, Lse/volvocars/acu/mediaWidget/MediaWidget$3;-><init>(Lse/volvocars/acu/mediaWidget/MediaWidget;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mMediaControler:Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    invoke-virtual {v1}, Lcom/parrot/asteroid/media/common/MediaPlayerControler;->isManagerReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mMediaControler:Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    invoke-virtual {v1}, Lcom/parrot/asteroid/media/common/MediaPlayerControler;->requestPlayerStatus()Z

    .line 162
    :goto_0
    return-void

    .line 160
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->requestPlayerStatusWhenReady:Z

    goto :goto_0
.end method

.method public onManagerDestroyed(Ljava/lang/Object;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/Object;

    .prologue
    .line 402
    return-void
.end method

.method public onManagerReady(ZLcom/parrot/asteroid/Manager;)V
    .locals 1
    .param p1, "ready"    # Z
    .param p2, "manager"    # Lcom/parrot/asteroid/Manager;

    .prologue
    .line 533
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->requestPlayerStatusWhenReady:Z

    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mMediaControler:Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    invoke-virtual {v0}, Lcom/parrot/asteroid/media/common/MediaPlayerControler;->requestPlayerStatus()Z

    .line 535
    const/4 v0, 0x0

    iput-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->requestPlayerStatusWhenReady:Z

    .line 537
    :cond_0
    return-void
.end method

.method public onMetadataChanged(Lcom/parrot/asteroid/audio/service/Metadata;I)V
    .locals 0
    .param p1, "data"    # Lcom/parrot/asteroid/audio/service/Metadata;
    .param p2, "changeFlag"    # I

    .prologue
    .line 406
    invoke-direct {p0, p1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setTrackInfo(Lcom/parrot/asteroid/audio/service/Metadata;)V

    .line 407
    return-void
.end method

.method public onModeChanged(II)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 456
    return-void
.end method

.method public onPlayerInfoChanged(Lcom/parrot/asteroid/audio/service/PlayerInfo;I)V
    .locals 0
    .param p1, "arg0"    # Lcom/parrot/asteroid/audio/service/PlayerInfo;
    .param p2, "arg1"    # I

    .prologue
    .line 459
    return-void
.end method

.method public onPlayerStatus(Lcom/parrot/asteroid/audio/service/Source;Lcom/parrot/asteroid/audio/service/PlayerInfo;Lcom/parrot/asteroid/audio/service/Metadata;)V
    .locals 2
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;
    .param p2, "info"    # Lcom/parrot/asteroid/audio/service/PlayerInfo;
    .param p3, "data"    # Lcom/parrot/asteroid/audio/service/Metadata;

    .prologue
    .line 411
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 412
    invoke-direct {p0, p1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setSourceInfo(Lcom/parrot/asteroid/audio/service/Source;)V

    .line 413
    invoke-direct {p0, p3}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setTrackInfo(Lcom/parrot/asteroid/audio/service/Metadata;)V

    .line 415
    :cond_0
    invoke-virtual {p0, p2}, Lse/volvocars/acu/mediaWidget/MediaWidget;->onStateChanged(Lcom/parrot/asteroid/audio/service/PlayerInfo;)V

    .line 416
    return-void
.end method

.method public onPreparingSource(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 0
    .param p1, "arg0"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 462
    return-void
.end method

.method public onSourceChanged(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 0
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 449
    invoke-direct {p0, p1}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setSourceInfo(Lcom/parrot/asteroid/audio/service/Source;)V

    .line 450
    return-void
.end method

.method public onSourceError(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 0
    .param p1, "arg0"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 465
    return-void
.end method

.method public onSourceReady(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 0
    .param p1, "arg0"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 468
    return-void
.end method

.method public onSourceRemoved(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 0
    .param p1, "arg0"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 471
    return-void
.end method

.method public onSourceUpdated(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 0
    .param p1, "arg0"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 474
    return-void
.end method

.method public onStateChanged(Lcom/parrot/asteroid/audio/service/PlayerInfo;)V
    .locals 2
    .param p1, "info"    # Lcom/parrot/asteroid/audio/service/PlayerInfo;

    .prologue
    .line 420
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/PlayerInfo;->getState()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 442
    const-string v0, "MediaWidget"

    const-string v1, "onPlayerStatus() : Invalid state value."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    :goto_0
    :pswitch_0
    return-void

    .line 426
    :pswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPlayState(Z)V

    goto :goto_0

    .line 431
    :pswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPlayState(Z)V

    goto :goto_0

    .line 420
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onTrackProgress(II)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 477
    return-void
.end method

.method public setIdleMode(Z)V
    .locals 3
    .param p1, "idle"    # Z

    .prologue
    const/16 v2, 0xff

    const/16 v1, 0x59

    .line 232
    iput-boolean p1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mIdle:Z

    .line 234
    if-eqz p1, :cond_1

    .line 235
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mBackButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 236
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mPlayPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 237
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mFwdButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 239
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTrackText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextFadeWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 240
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mArtistText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextFadeGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 243
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPlayButtonEmitter(Z)V

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mBackButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 247
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mPlayPauseButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 248
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mFwdButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 250
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTrackText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 251
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mArtistText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 254
    iget-boolean v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mIsPlaying:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->isSourceRelevant()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPlayButtonEmitter(Z)V

    goto :goto_0
.end method

.method public setLaucherGLView(Lse/volvocars/acu/ui/opengl/LauncherGLView;)V
    .locals 0
    .param p1, "launcherGLView"    # Lse/volvocars/acu/ui/opengl/LauncherGLView;

    .prologue
    .line 169
    iput-object p1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    .line 170
    return-void
.end method

.method setPlayState(Z)V
    .locals 3
    .param p1, "isPlaying"    # Z

    .prologue
    .line 283
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mHandler:Landroid/os/Handler;

    new-instance v1, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lse/volvocars/acu/mediaWidget/MediaWidget$PlayStateSetter;-><init>(Lse/volvocars/acu/mediaWidget/MediaWidget;ZLse/volvocars/acu/mediaWidget/MediaWidget$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 284
    return-void
.end method

.method public setPressedState(Z)V
    .locals 2
    .param p1, "pressed"    # Z

    .prologue
    .line 177
    if-eqz p1, :cond_0

    .line 178
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTrackText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextWhiteGlowColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 179
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mArtistText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextGrayGlowColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 180
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mContainerView:Landroid/view/View;

    const v1, 0x7f02005e

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 187
    :goto_0
    return-void

    .line 183
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTrackText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 184
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mArtistText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mTextGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 185
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mContainerView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method startMediaPlayer()V
    .locals 4

    .prologue
    const-string v3, "MediaWidget"

    .line 518
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    if-eqz v0, :cond_0

    .line 519
    invoke-virtual {p0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/parrot/asteroid/media/tools/MediaPlayerLauncher;->launch(Landroid/content/Context;Lcom/parrot/asteroid/audio/service/Source;Z)Z

    .line 520
    const-string v0, "MediaWidget"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Launching media source: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;

    invoke-virtual {v1}, Lcom/parrot/asteroid/audio/service/Source;->getSourceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    :goto_0
    return-void

    .line 523
    :cond_0
    const-string v0, "MediaWidget"

    const-string v0, "Can\'t launch media source as it is null!"

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
