.class public Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;
.super Ljava/lang/Object;
.source "MediaWidgetOnTouch.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final SQRA:I = 0x20


# instance fields
.field private mMovedOutsidePressArea:Z

.field private mReleaseSquare:Landroid/graphics/Rect;

.field private mWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/mediaWidget/MediaWidget;)V
    .locals 0
    .param p1, "widget"    # Lse/volvocars/acu/mediaWidget/MediaWidget;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    .line 23
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v6, 0x20

    const/4 v5, 0x1

    const/4 v9, 0x0

    .line 28
    iget-object v4, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v4}, Lse/volvocars/acu/mediaWidget/MediaWidget;->hasSource()Z

    move-result v4

    if-nez v4, :cond_0

    move v4, v9

    .line 64
    :goto_0
    return v4

    .line 32
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 33
    .local v1, "ey":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 35
    .local v0, "ex":F
    float-to-int v3, v1

    .line 36
    .local v3, "y":I
    float-to-int v2, v0

    .line 38
    .local v2, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    move v4, v9

    .line 64
    goto :goto_0

    .line 40
    :pswitch_0
    iget-object v4, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v4, v5}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPressedState(Z)V

    .line 41
    iput-boolean v9, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mMovedOutsidePressArea:Z

    .line 42
    new-instance v4, Landroid/graphics/Rect;

    sub-int v5, v2, v6

    sub-int v6, v3, v6

    add-int/lit8 v7, v2, 0x20

    add-int/lit8 v8, v3, 0x20

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mReleaseSquare:Landroid/graphics/Rect;

    move v4, v9

    .line 43
    goto :goto_0

    .line 45
    :pswitch_1
    iget-object v4, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v4, v9}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPressedState(Z)V

    .line 46
    iput-boolean v5, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mMovedOutsidePressArea:Z

    move v4, v9

    .line 47
    goto :goto_0

    .line 49
    :pswitch_2
    iget-object v4, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mReleaseSquare:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mMovedOutsidePressArea:Z

    if-nez v4, :cond_1

    .line 50
    iget-object v4, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v4, v9}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPressedState(Z)V

    .line 51
    iput-boolean v5, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mMovedOutsidePressArea:Z

    :cond_1
    move v4, v9

    .line 56
    goto :goto_0

    .line 58
    :pswitch_3
    iget-object v4, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v4, v9}, Lse/volvocars/acu/mediaWidget/MediaWidget;->setPressedState(Z)V

    .line 59
    iget-object v4, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mReleaseSquare:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mMovedOutsidePressArea:Z

    if-nez v4, :cond_2

    .line 60
    iget-object v4, p0, Lse/volvocars/acu/mediaWidget/MediaWidgetOnTouch;->mWidget:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-virtual {v4}, Lse/volvocars/acu/mediaWidget/MediaWidget;->startMediaPlayer()V

    :cond_2
    move v4, v5

    .line 62
    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
