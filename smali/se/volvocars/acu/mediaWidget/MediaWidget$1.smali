.class Lse/volvocars/acu/mediaWidget/MediaWidget$1;
.super Ljava/lang/Object;
.source "MediaWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/mediaWidget/MediaWidget;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;


# direct methods
.method constructor <init>(Lse/volvocars/acu/mediaWidget/MediaWidget;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$1;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 112
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$1;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # getter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mCurrentSource:Lcom/parrot/asteroid/audio/service/Source;
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$000(Lse/volvocars/acu/mediaWidget/MediaWidget;)Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$1;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # getter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mMediaControler:Lcom/parrot/asteroid/media/common/MediaPlayerControler;
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$100(Lse/volvocars/acu/mediaWidget/MediaWidget;)Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/parrot/asteroid/media/common/MediaPlayerControler;->isManagerReady()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Lse/volvocars/acu/mediaWidget/MediaWidget$1;->this$0:Lse/volvocars/acu/mediaWidget/MediaWidget;

    # getter for: Lse/volvocars/acu/mediaWidget/MediaWidget;->mMediaControler:Lcom/parrot/asteroid/media/common/MediaPlayerControler;
    invoke-static {v0}, Lse/volvocars/acu/mediaWidget/MediaWidget;->access$100(Lse/volvocars/acu/mediaWidget/MediaWidget;)Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/parrot/asteroid/media/common/MediaPlayerControler;->previousMedia()Z

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    const-string v0, "MediaWidget"

    const-string v1, "previousMedia() : The MediaManager isn\'t ready."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
