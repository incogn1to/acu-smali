.class public Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;
.super Landroid/widget/BaseAdapter;
.source "MediaSourcesList.java"

# interfaces
.implements Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;
.implements Landroid/widget/ListAdapter;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/mediasources/MediaSourcesList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaSourceAdapter"
.end annotation


# instance fields
.field private mContext:Lse/volvocars/acu/mediasources/MediaSourcesList;

.field private mManager:Lcom/parrot/asteroid/media/MediaManager;

.field private mSourceSet:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lse/volvocars/acu/model/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTypeface:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/mediasources/MediaSourcesList;)V
    .locals 2
    .param p1, "ctx"    # Lse/volvocars/acu/mediasources/MediaSourcesList;

    .prologue
    .line 100
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 101
    iput-object p1, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mContext:Lse/volvocars/acu/mediasources/MediaSourcesList;

    .line 102
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    .line 103
    iget-object v0, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mContext:Lse/volvocars/acu/mediasources/MediaSourcesList;

    invoke-virtual {v0}, Lse/volvocars/acu/mediasources/MediaSourcesList;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts/VolvoSanProLig.otf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mTypeface:Landroid/graphics/Typeface;

    .line 104
    iget-object v0, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mContext:Lse/volvocars/acu/mediasources/MediaSourcesList;

    invoke-static {v0}, Lcom/parrot/asteroid/media/MediaManagerFactory;->getMediaManager(Landroid/content/Context;)Lcom/parrot/asteroid/media/MediaManager;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mManager:Lcom/parrot/asteroid/media/MediaManager;

    .line 105
    return-void
.end method

.method private getDrawableBySourceType(I)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "sourceType"    # I

    .prologue
    const v3, 0x7f020053

    const v2, 0x7f02003e

    .line 152
    iget-object v1, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mContext:Lse/volvocars/acu/mediasources/MediaSourcesList;

    invoke-virtual {v1}, Lse/volvocars/acu/mediasources/MediaSourcesList;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 153
    .local v0, "res":Landroid/content/res/Resources;
    packed-switch p1, :pswitch_data_0

    .line 167
    :pswitch_0
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_0
    return-object v1

    .line 155
    :pswitch_1
    const v1, 0x7f02003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 157
    :pswitch_2
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 159
    :pswitch_3
    const v1, 0x7f020041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 161
    :pswitch_4
    const v1, 0x7f020042

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 163
    :pswitch_5
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 165
    :pswitch_6
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public addAppInfo(Lse/volvocars/acu/model/AppInfo;)V
    .locals 5
    .param p1, "info"    # Lse/volvocars/acu/model/AppInfo;

    .prologue
    .line 174
    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v3, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 175
    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v3, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 176
    const/4 v2, 0x0

    .line 178
    .local v2, "p":I
    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/model/AppInfo;

    .line 179
    .local v0, "i":Lse/volvocars/acu/model/AppInfo;
    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mManager:Lcom/parrot/asteroid/media/MediaManager;

    invoke-virtual {v3}, Lcom/parrot/asteroid/media/MediaManager;->getCurrentSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mManager:Lcom/parrot/asteroid/media/MediaManager;

    invoke-virtual {v3}, Lcom/parrot/asteroid/media/MediaManager;->getCurrentSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v3

    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getMediaSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/parrot/asteroid/audio/service/Source;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 181
    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    goto :goto_0

    .line 185
    .end local v0    # "i":Lse/volvocars/acu/model/AppInfo;
    :cond_1
    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    iget-object v4, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v4, v2}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 186
    invoke-virtual {p0}, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->update()V

    .line 188
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "p":I
    :cond_2
    return-void
.end method

.method public getAppInfoFromIdentifier(Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;
    .locals 3
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 203
    iget-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/model/AppInfo;

    .line 204
    .local v1, "info":Lse/volvocars/acu/model/AppInfo;
    invoke-virtual {v1}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v1

    .line 208
    .end local v1    # "info":Lse/volvocars/acu/model/AppInfo;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mContext:Lse/volvocars/acu/mediasources/MediaSourcesList;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 123
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v8, 0x7f07001d

    .line 130
    if-nez p2, :cond_0

    .line 131
    invoke-virtual {p0}, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 132
    .local v5, "vi":Landroid/view/LayoutInflater;
    const v6, 0x7f030007

    const/4 v7, 0x0

    invoke-virtual {v5, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 133
    .local v3, "ll":Landroid/view/View;
    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 134
    .local v4, "tv":Landroid/widget/TextView;
    iget-object v6, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 140
    .end local v5    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v6, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v6, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/model/AppInfo;

    .line 141
    .local v1, "info":Lse/volvocars/acu/model/AppInfo;
    invoke-virtual {v1}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    const v6, 0x7f07001c

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 146
    .local v2, "iv":Landroid/widget/ImageView;
    invoke-virtual {v1}, Lse/volvocars/acu/model/AppInfo;->getMediaSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v6

    invoke-virtual {v6}, Lcom/parrot/asteroid/audio/service/Source;->getType()I

    move-result v6

    invoke-direct {p0, v6}, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->getDrawableBySourceType(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 147
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 148
    return-object v3

    .line 136
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    .end local v1    # "info":Lse/volvocars/acu/model/AppInfo;
    .end local v2    # "iv":Landroid/widget/ImageView;
    .end local v3    # "ll":Landroid/view/View;
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_0
    move-object v3, p2

    .line 137
    .restart local v3    # "ll":Landroid/view/View;
    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .restart local v4    # "tv":Landroid/widget/TextView;
    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 220
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v1, p3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/model/AppInfo;

    .line 221
    .local v0, "app":Lse/volvocars/acu/model/AppInfo;
    iget-object v1, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mContext:Lse/volvocars/acu/mediasources/MediaSourcesList;

    invoke-static {v0, v1}, Lse/volvocars/acu/AppHandler;->startApp(Lse/volvocars/acu/model/AppInfo;Landroid/content/Context;)Z

    .line 222
    iget-object v1, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mContext:Lse/volvocars/acu/mediasources/MediaSourcesList;

    invoke-virtual {v1}, Lse/volvocars/acu/mediasources/MediaSourcesList;->finish()V

    .line 223
    return-void
.end method

.method public removeAllMediaDevices()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 193
    invoke-virtual {p0}, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->update()V

    .line 194
    return-void
.end method

.method public removeAppInfo(Lse/volvocars/acu/model/AppInfo;)V
    .locals 1
    .param p1, "info"    # Lse/volvocars/acu/model/AppInfo;

    .prologue
    .line 213
    iget-object v0, p0, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->mSourceSet:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 214
    invoke-virtual {p0}, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->update()V

    .line 215
    return-void
.end method

.method public update()V
    .locals 0

    .prologue
    .line 108
    invoke-virtual {p0}, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;->notifyDataSetChanged()V

    .line 109
    return-void
.end method
