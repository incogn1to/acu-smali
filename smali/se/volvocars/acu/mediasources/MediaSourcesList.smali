.class public Lse/volvocars/acu/mediasources/MediaSourcesList;
.super Landroid/app/Activity;
.source "MediaSourcesList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaSourcesList"


# instance fields
.field private mListView:Landroid/widget/ListView;

.field private mManager:Lcom/parrot/asteroid/media/MediaManager;

.field private mMediaDeviceObserver:Lse/volvocars/acu/model/MediaDeviceObserver;

.field private mSourceAdapter:Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;

.field private mTypeface:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 93
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-string v4, "MediaSourcesList"

    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const v2, 0x7f030008

    invoke-virtual {p0, v2}, Lse/volvocars/acu/mediasources/MediaSourcesList;->setContentView(I)V

    .line 52
    new-instance v2, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;

    invoke-direct {v2, p0}, Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;-><init>(Lse/volvocars/acu/mediasources/MediaSourcesList;)V

    iput-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mSourceAdapter:Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;

    .line 54
    invoke-static {p0}, Lcom/parrot/asteroid/media/MediaManagerFactory;->getMediaManager(Landroid/content/Context;)Lcom/parrot/asteroid/media/MediaManager;

    move-result-object v2

    iput-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mManager:Lcom/parrot/asteroid/media/MediaManager;

    .line 56
    const v2, 0x7f07001e

    invoke-virtual {p0, v2}, Lse/volvocars/acu/mediasources/MediaSourcesList;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mListView:Landroid/widget/ListView;

    .line 58
    iget-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mSourceAdapter:Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 59
    new-instance v2, Lse/volvocars/acu/model/MediaDeviceObserver;

    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mSourceAdapter:Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;

    invoke-direct {v2, v3}, Lse/volvocars/acu/model/MediaDeviceObserver;-><init>(Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;)V

    iput-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mMediaDeviceObserver:Lse/volvocars/acu/model/MediaDeviceObserver;

    .line 60
    const-string v2, "MediaSourcesList"

    const-string v2, "Adding media manager observer"

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iget-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mManager:Lcom/parrot/asteroid/media/MediaManager;

    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mMediaDeviceObserver:Lse/volvocars/acu/model/MediaDeviceObserver;

    invoke-virtual {v2, v3}, Lcom/parrot/asteroid/media/MediaManager;->addManagerObserver(Lcom/parrot/asteroid/ManagerObserverInterface;)V

    .line 63
    iget-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mManager:Lcom/parrot/asteroid/media/MediaManager;

    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mMediaDeviceObserver:Lse/volvocars/acu/model/MediaDeviceObserver;

    invoke-virtual {v2, v3}, Lcom/parrot/asteroid/media/MediaManager;->addListener(Lcom/parrot/asteroid/media/MediaObserverInterface;)Z

    move-result v0

    .line 64
    .local v0, "addedListener":Z
    const-string v2, "MediaSourcesList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Added media listener: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iget-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mSourceAdapter:Lse/volvocars/acu/mediasources/MediaSourcesList$MediaSourceAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 67
    invoke-virtual {p0}, Lse/volvocars/acu/mediasources/MediaSourcesList;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "fonts/VolvoSanProLig.otf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    iput-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mTypeface:Landroid/graphics/Typeface;

    .line 68
    const v2, 0x7f07000b

    invoke-virtual {p0, v2}, Lse/volvocars/acu/mediasources/MediaSourcesList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 69
    .local v1, "tv":Landroid/widget/TextView;
    iget-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 70
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 84
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 91
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 74
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 75
    iget-object v1, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mMediaDeviceObserver:Lse/volvocars/acu/model/MediaDeviceObserver;

    iget-object v2, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mManager:Lcom/parrot/asteroid/media/MediaManager;

    invoke-virtual {v2}, Lcom/parrot/asteroid/media/MediaManager;->isManagerReady()Z

    move-result v2

    iget-object v3, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mManager:Lcom/parrot/asteroid/media/MediaManager;

    invoke-virtual {v1, v2, v3}, Lse/volvocars/acu/model/MediaDeviceObserver;->onManagerReady(ZLcom/parrot/asteroid/Manager;)V

    .line 76
    iget-object v1, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mManager:Lcom/parrot/asteroid/media/MediaManager;

    invoke-virtual {v1}, Lcom/parrot/asteroid/media/MediaManager;->getCurrentSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v0

    .line 77
    .local v0, "cs":Lcom/parrot/asteroid/audio/service/Source;
    if-eqz v0, :cond_0

    .line 78
    iget-object v1, p0, Lse/volvocars/acu/mediasources/MediaSourcesList;->mMediaDeviceObserver:Lse/volvocars/acu/model/MediaDeviceObserver;

    invoke-virtual {v1, v0}, Lse/volvocars/acu/model/MediaDeviceObserver;->onSourceReady(Lcom/parrot/asteroid/audio/service/Source;)V

    .line 80
    :cond_0
    return-void
.end method
