.class final Lse/volvocars/acu/AppHandler$AppIconOrderUpdater;
.super Ljava/lang/Object;
.source "AppHandler.java"

# interfaces
.implements Lse/volvocars/acu/model/AppInfoChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/AppHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AppIconOrderUpdater"
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/AppHandler;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/AppHandler;)V
    .locals 0

    .prologue
    .line 466
    iput-object p1, p0, Lse/volvocars/acu/AppHandler$AppIconOrderUpdater;->this$0:Lse/volvocars/acu/AppHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/AppHandler;Lse/volvocars/acu/AppHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/AppHandler;
    .param p2, "x1"    # Lse/volvocars/acu/AppHandler$1;

    .prologue
    .line 466
    invoke-direct {p0, p1}, Lse/volvocars/acu/AppHandler$AppIconOrderUpdater;-><init>(Lse/volvocars/acu/AppHandler;)V

    return-void
.end method


# virtual methods
.method public appInfoHasChanged()V
    .locals 2

    .prologue
    .line 469
    const-string v0, "AppHandler"

    const-string v1, "Callback appInfoHasChanged."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    iget-object v0, p0, Lse/volvocars/acu/AppHandler$AppIconOrderUpdater;->this$0:Lse/volvocars/acu/AppHandler;

    # getter for: Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;
    invoke-static {v0}, Lse/volvocars/acu/AppHandler;->access$500(Lse/volvocars/acu/AppHandler;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 471
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/AppHandler$AppIconOrderUpdater;->this$0:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v1}, Lse/volvocars/acu/AppHandler;->updateAppIconOrder()V

    .line 472
    monitor-exit v0

    .line 473
    return-void

    .line 472
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
