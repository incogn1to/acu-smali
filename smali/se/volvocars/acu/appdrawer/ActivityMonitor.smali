.class public final Lse/volvocars/acu/appdrawer/ActivityMonitor;
.super Ljava/lang/Object;
.source "ActivityMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;
    }
.end annotation


# static fields
.field public static final IDLE_MODE_BROADCAST:Ljava/lang/String; = "se.volvocars.acu.IDLE_MODE"

.field public static final IDLE_MODE_EXTRA:Ljava/lang/String; = "idle"

.field private static final TAG:Ljava/lang/String; = "ActivityMonitorAppDrawer"


# instance fields
.field private final mActivity:Lse/volvocars/acu/appdrawer/AppDrawerActivity;

.field private mIdle:Z

.field private final mIdleCountdown:Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;

.field private final mIdleCountdownExecutor:Ljava/util/concurrent/ExecutorService;

.field private mIdleFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)V
    .locals 1
    .param p1, "activity"    # Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleCountdownExecutor:Ljava/util/concurrent/ExecutorService;

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdle:Z

    .line 29
    iput-object p1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mActivity:Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    .line 30
    new-instance v0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;

    invoke-direct {v0, p0}, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;-><init>(Lse/volvocars/acu/appdrawer/ActivityMonitor;)V

    iput-object v0, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleCountdown:Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lse/volvocars/acu/appdrawer/ActivityMonitor;Z)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/appdrawer/ActivityMonitor;
    .param p1, "x1"    # Z

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lse/volvocars/acu/appdrawer/ActivityMonitor;->setIdleMode(Z)V

    return-void
.end method

.method private setIdleMode(Z)V
    .locals 4
    .param p1, "idle"    # Z

    .prologue
    .line 64
    const-string v1, "ActivityMonitorAppDrawer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting idle mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iput-boolean p1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdle:Z

    .line 66
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mActivity:Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    invoke-virtual {v1, p1}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->setIdleMode(Z)V

    .line 67
    if-eqz p1, :cond_0

    .line 68
    new-instance v0, Landroid/content/Intent;

    const-string v1, "se.volvocars.acu.IDLE_MODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 69
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "idle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 70
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mActivity:Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    invoke-virtual {v1, v0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 72
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method


# virtual methods
.method public onActivity()V
    .locals 3

    .prologue
    .line 37
    iget-boolean v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdle:Z

    if-eqz v1, :cond_0

    .line 38
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lse/volvocars/acu/appdrawer/ActivityMonitor;->setIdleMode(Z)V

    .line 40
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;

    invoke-interface {v1}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v1

    if-nez v1, :cond_1

    .line 41
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleCountdown:Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;

    invoke-virtual {v1}, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->restart()V

    .line 49
    :goto_0
    return-void

    .line 44
    :cond_1
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleCountdownExecutor:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleCountdown:Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 45
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 46
    .local v0, "e":Ljava/util/concurrent/RejectedExecutionException;
    const-string v1, "ActivityMonitorAppDrawer"

    const-string v2, "Idle countdown excecution couldn\'t be scheduled."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleCountdown:Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;

    invoke-virtual {v0}, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->stop()V

    .line 55
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;

    .line 56
    return-void
.end method
