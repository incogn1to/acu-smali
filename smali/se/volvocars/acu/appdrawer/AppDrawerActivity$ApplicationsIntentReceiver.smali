.class Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AppDrawerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/appdrawer/AppDrawerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ApplicationsIntentReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/appdrawer/AppDrawerActivity;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsIntentReceiver;->this$0:Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/appdrawer/AppDrawerActivity;Lse/volvocars/acu/appdrawer/AppDrawerActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/appdrawer/AppDrawerActivity;
    .param p2, "x1"    # Lse/volvocars/acu/appdrawer/AppDrawerActivity$1;

    .prologue
    .line 301
    invoke-direct {p0, p1}, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsIntentReceiver;-><init>(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 304
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsIntentReceiver;->this$0:Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    # invokes: Lse/volvocars/acu/appdrawer/AppDrawerActivity;->loadApplications()V
    invoke-static {v0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->access$200(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)V

    .line 305
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsIntentReceiver;->this$0:Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    # invokes: Lse/volvocars/acu/appdrawer/AppDrawerActivity;->bindApplications()V
    invoke-static {v0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->access$300(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)V

    .line 306
    const-string v0, "AllAppsDrawer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recived intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    return-void
.end method
