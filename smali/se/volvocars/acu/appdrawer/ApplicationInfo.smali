.class Lse/volvocars/acu/appdrawer/ApplicationInfo;
.super Ljava/lang/Object;
.source "ApplicationInfo.java"


# instance fields
.field filtered:Z

.field icon:Landroid/graphics/drawable/Drawable;

.field intent:Landroid/content/Intent;

.field title:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    if-ne p0, p1, :cond_0

    move v2, v5

    .line 71
    :goto_0
    return v2

    .line 66
    :cond_0
    instance-of v2, p1, Lse/volvocars/acu/appdrawer/ApplicationInfo;

    if-nez v2, :cond_1

    move v2, v4

    .line 67
    goto :goto_0

    .line 70
    :cond_1
    move-object v0, p1

    check-cast v0, Lse/volvocars/acu/appdrawer/ApplicationInfo;

    move-object v1, v0

    .line 71
    .local v1, "that":Lse/volvocars/acu/appdrawer/ApplicationInfo;
    iget-object v2, p0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->title:Ljava/lang/CharSequence;

    iget-object v3, v1, Lse/volvocars/acu/appdrawer/ApplicationInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Lse/volvocars/acu/appdrawer/ApplicationInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v5

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 79
    iget-object v2, p0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->title:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    move v1, v2

    .line 80
    .local v1, "result":I
    :goto_0
    iget-object v2, p0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "name":Ljava/lang/String;
    mul-int/lit8 v2, v1, 0x1f

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    :cond_0
    add-int v1, v2, v3

    .line 82
    return v1

    .end local v0    # "name":Ljava/lang/String;
    .end local v1    # "result":I
    :cond_1
    move v1, v3

    .line 79
    goto :goto_0
.end method

.method final setActivity(Landroid/content/ComponentName;I)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "launchFlags"    # I

    .prologue
    .line 55
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->intent:Landroid/content/Intent;

    .line 56
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->intent:Landroid/content/Intent;

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 58
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 59
    return-void
.end method
