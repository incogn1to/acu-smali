.class Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;
.super Ljava/lang/Object;
.source "ActivityMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/appdrawer/ActivityMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IdleCountdown"
.end annotation


# static fields
.field private static final WAIT_UNTIL_IDLE:I = 0x1d4c0


# instance fields
.field private final lock:Ljava/lang/Object;

.field private mRestarted:Z

.field private mStopped:Z

.field final synthetic this$0:Lse/volvocars/acu/appdrawer/ActivityMonitor;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/appdrawer/ActivityMonitor;)V
    .locals 1

    .prologue
    .line 85
    iput-object p1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->this$0:Lse/volvocars/acu/appdrawer/ActivityMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->mRestarted:Z

    .line 87
    return-void
.end method


# virtual methods
.method public restart()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 94
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->mRestarted:Z

    .line 95
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 96
    monitor-exit v0

    .line 97
    return-void

    .line 96
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 109
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 110
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->mRestarted:Z

    .line 111
    const/4 v2, 0x0

    iput-boolean v2, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->mStopped:Z

    .line 112
    :goto_0
    iget-boolean v2, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->mRestarted:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->mStopped:Z

    if-nez v2, :cond_0

    .line 113
    const/4 v2, 0x0

    iput-boolean v2, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->mRestarted:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :try_start_1
    iget-object v2, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    const-wide/32 v3, 0x1d4c0

    invoke-virtual {v2, v3, v4}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 117
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v2, "ActivityMonitorAppDrawer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Idle mode timer Interrupted while waiting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 122
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 121
    :cond_0
    :try_start_3
    iget-object v2, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->this$0:Lse/volvocars/acu/appdrawer/ActivityMonitor;

    iget-boolean v3, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->mStopped:Z

    if-nez v3, :cond_1

    move v3, v6

    :goto_1
    # invokes: Lse/volvocars/acu/appdrawer/ActivityMonitor;->setIdleMode(Z)V
    invoke-static {v2, v3}, Lse/volvocars/acu/appdrawer/ActivityMonitor;->access$000(Lse/volvocars/acu/appdrawer/ActivityMonitor;Z)V

    .line 122
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 123
    return-void

    :cond_1
    move v3, v5

    .line 121
    goto :goto_1
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    monitor-enter v0

    .line 101
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->mStopped:Z

    .line 102
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/ActivityMonitor$IdleCountdown;->lock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 103
    monitor-exit v0

    .line 105
    return-void

    .line 103
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
