.class public Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AppDrawerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/appdrawer/AppDrawerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ApplicationsAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lse/volvocars/acu/appdrawer/ApplicationInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private mOldBounds:Landroid/graphics/Rect;

.field final synthetic this$0:Lse/volvocars/acu/appdrawer/AppDrawerActivity;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/appdrawer/AppDrawerActivity;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/appdrawer/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 318
    .local p3, "apps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/appdrawer/ApplicationInfo;>;"
    iput-object p1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsAdapter;->this$0:Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    .line 319
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 315
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsAdapter;->mOldBounds:Landroid/graphics/Rect;

    .line 320
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 24
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 324
    # getter for: Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplications:Ljava/util/List;
    invoke-static {}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->access$400()Ljava/util/List;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lse/volvocars/acu/appdrawer/ApplicationInfo;

    .line 326
    .local v12, "info":Lse/volvocars/acu/appdrawer/ApplicationInfo;
    move-object/from16 v19, p2

    .line 328
    .local v19, "v":Landroid/view/View;
    if-nez v19, :cond_0

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsAdapter;->this$0:Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v11

    .line 330
    .local v11, "inflater":Landroid/view/LayoutInflater;
    const v21, 0x7f030001

    const/16 v22, 0x0

    move-object v0, v11

    move/from16 v1, v21

    move-object/from16 v2, p3

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v19

    .line 331
    const v21, 0x7f070007

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 332
    .local v13, "label":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsAdapter;->this$0:Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    move-object/from16 v21, v0

    # getter for: Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mTypeface:Landroid/graphics/Typeface;
    invoke-static/range {v21 .. v21}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->access$500(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)Landroid/graphics/Typeface;

    move-result-object v21

    move-object v0, v13

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 335
    .end local v11    # "inflater":Landroid/view/LayoutInflater;
    .end local v13    # "label":Landroid/widget/TextView;
    :cond_0
    iget-object v8, v12, Lse/volvocars/acu/appdrawer/ApplicationInfo;->icon:Landroid/graphics/drawable/Drawable;

    .line 337
    .local v8, "icon":Landroid/graphics/drawable/Drawable;
    move-object v0, v12

    iget-boolean v0, v0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->filtered:Z

    move/from16 v21, v0

    if-nez v21, :cond_3

    .line 338
    invoke-virtual/range {p0 .. p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsAdapter;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 339
    .local v16, "resources":Landroid/content/res/Resources;
    const/high16 v21, 0x1050000

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v20, v0

    .line 340
    .local v20, "width":I
    const/high16 v21, 0x1050000

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move v7, v0

    .line 342
    .local v7, "height":I
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    .line 343
    .local v10, "iconWidth":I
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    .line 345
    .local v9, "iconHeight":I
    move-object v0, v8

    instance-of v0, v0, Landroid/graphics/drawable/PaintDrawable;

    move/from16 v21, v0

    if-eqz v21, :cond_1

    .line 346
    move-object v0, v8

    check-cast v0, Landroid/graphics/drawable/PaintDrawable;

    move-object v14, v0

    .line 347
    .local v14, "painter":Landroid/graphics/drawable/PaintDrawable;
    move-object v0, v14

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/PaintDrawable;->setIntrinsicWidth(I)V

    .line 348
    invoke-virtual {v14, v7}, Landroid/graphics/drawable/PaintDrawable;->setIntrinsicHeight(I)V

    .line 351
    .end local v14    # "painter":Landroid/graphics/drawable/PaintDrawable;
    :cond_1
    if-lez v20, :cond_3

    if-lez v7, :cond_3

    .line 352
    move v0, v10

    int-to-float v0, v0

    move/from16 v21, v0

    move v0, v9

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v15, v21, v22

    .line 354
    .local v15, "ratio":F
    if-le v10, v9, :cond_4

    .line 355
    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v21, v21, v15

    move/from16 v0, v21

    float-to-int v0, v0

    move v7, v0

    .line 360
    :cond_2
    :goto_0
    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v21

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_5

    sget-object v21, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object/from16 v5, v21

    .line 363
    .local v5, "c":Landroid/graphics/Bitmap$Config;
    :goto_1
    move/from16 v0, v20

    move v1, v7

    move-object v2, v5

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 364
    .local v18, "thumb":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/Canvas;

    move-object v0, v6

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 365
    .local v6, "canvas":Landroid/graphics/Canvas;
    new-instance v21, Landroid/graphics/PaintFlagsDrawFilter;

    const/16 v22, 0x4

    const/16 v23, 0x0

    invoke-direct/range {v21 .. v23}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    move-object v0, v6

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    .line 371
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsAdapter;->mOldBounds:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 372
    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object v0, v8

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v20

    move v4, v7

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 373
    invoke-virtual {v8, v6}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 374
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsAdapter;->mOldBounds:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object v0, v8

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 375
    new-instance v21, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, v21

    move-object v1, v12

    iput-object v0, v1, Lse/volvocars/acu/appdrawer/ApplicationInfo;->icon:Landroid/graphics/drawable/Drawable;

    .line 376
    iget-object v8, v12, Lse/volvocars/acu/appdrawer/ApplicationInfo;->icon:Landroid/graphics/drawable/Drawable;

    .line 377
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object v1, v12

    iput-boolean v0, v1, Lse/volvocars/acu/appdrawer/ApplicationInfo;->filtered:Z

    .line 381
    .end local v5    # "c":Landroid/graphics/Bitmap$Config;
    .end local v6    # "canvas":Landroid/graphics/Canvas;
    .end local v7    # "height":I
    .end local v9    # "iconHeight":I
    .end local v10    # "iconWidth":I
    .end local v15    # "ratio":F
    .end local v16    # "resources":Landroid/content/res/Resources;
    .end local v18    # "thumb":Landroid/graphics/Bitmap;
    .end local v20    # "width":I
    :cond_3
    const v21, 0x7f070007

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 382
    .local v17, "textView":Landroid/widget/TextView;
    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move-object v2, v8

    move-object/from16 v3, v22

    move-object/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 383
    move-object v0, v12

    iget-object v0, v0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->title:Ljava/lang/CharSequence;

    move-object/from16 v21, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 385
    return-object v19

    .line 356
    .end local v17    # "textView":Landroid/widget/TextView;
    .restart local v7    # "height":I
    .restart local v9    # "iconHeight":I
    .restart local v10    # "iconWidth":I
    .restart local v15    # "ratio":F
    .restart local v16    # "resources":Landroid/content/res/Resources;
    .restart local v20    # "width":I
    :cond_4
    if-le v9, v10, :cond_2

    .line 357
    move v0, v7

    int-to-float v0, v0

    move/from16 v21, v0

    mul-float v21, v21, v15

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v20, v0

    goto/16 :goto_0

    .line 360
    :cond_5
    sget-object v21, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move-object/from16 v5, v21

    goto/16 :goto_1
.end method
