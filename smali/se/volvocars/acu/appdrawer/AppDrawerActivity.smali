.class public Lse/volvocars/acu/appdrawer/AppDrawerActivity;
.super Landroid/app/Activity;
.source "AppDrawerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationLauncher;,
        Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsAdapter;,
        Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsIntentReceiver;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "AllAppsDrawer"

.field private static mApplications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/appdrawer/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mActivityMonitor:Lse/volvocars/acu/appdrawer/ActivityMonitor;

.field private final mApplicationsReceiver:Landroid/content/BroadcastReceiver;

.field private mDriverWlListener:Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;

.field private mGrid:Landroid/widget/GridView;

.field private mHiddenApps:[Ljava/lang/String;

.field private mHomeButtonListener:Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;

.field private mTypeface:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 73
    new-instance v0, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsIntentReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsIntentReceiver;-><init>(Lse/volvocars/acu/appdrawer/AppDrawerActivity;Lse/volvocars/acu/appdrawer/AppDrawerActivity$1;)V

    iput-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplicationsReceiver:Landroid/content/BroadcastReceiver;

    .line 85
    new-instance v0, Lse/volvocars/acu/appdrawer/ActivityMonitor;

    invoke-direct {v0, p0}, Lse/volvocars/acu/appdrawer/ActivityMonitor;-><init>(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)V

    iput-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mActivityMonitor:Lse/volvocars/acu/appdrawer/ActivityMonitor;

    .line 86
    return-void
.end method

.method static synthetic access$200(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    .prologue
    .line 63
    invoke-direct {p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->loadApplications()V

    return-void
.end method

.method static synthetic access$300(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    .prologue
    .line 63
    invoke-direct {p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->bindApplications()V

    return-void
.end method

.method static synthetic access$400()Ljava/util/List;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplications:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)Landroid/graphics/Typeface;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/appdrawer/AppDrawerActivity;

    .prologue
    .line 63
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method private bindApplications()V
    .locals 3

    .prologue
    .line 227
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    if-nez v0, :cond_0

    .line 228
    const v0, 0x7f070006

    invoke-virtual {p0, v0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    .line 230
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    new-instance v1, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsAdapter;

    sget-object v2, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplications:Ljava/util/List;

    invoke-direct {v1, p0, p0, v2}, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationsAdapter;-><init>(Lse/volvocars/acu/appdrawer/AppDrawerActivity;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 231
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelection(I)V

    .line 232
    return-void
.end method

.method private loadApplications()V
    .locals 10

    .prologue
    .line 240
    invoke-virtual {p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 242
    .local v6, "manager":Landroid/content/pm/PackageManager;
    new-instance v5, Landroid/content/Intent;

    const-string v7, "android.intent.action.MAIN"

    const/4 v8, 0x0

    invoke-direct {v5, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 243
    .local v5, "mainIntent":Landroid/content/Intent;
    const-string v7, "android.intent.category.LAUNCHER"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    const/4 v7, 0x0

    invoke-virtual {v6, v5, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 246
    .local v1, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v7, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-direct {v7, v6}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v1, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 248
    if-eqz v1, :cond_2

    .line 249
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 251
    .local v2, "count":I
    sget-object v7, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplications:Ljava/util/List;

    if-nez v7, :cond_0

    .line 252
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v7, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplications:Ljava/util/List;

    .line 254
    :cond_0
    sget-object v7, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplications:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 256
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_2

    .line 257
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 259
    .local v4, "info":Landroid/content/pm/ResolveInfo;
    iget-object v7, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v7}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->pkgOnBlackList(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 260
    new-instance v0, Lse/volvocars/acu/appdrawer/ApplicationInfo;

    invoke-direct {v0}, Lse/volvocars/acu/appdrawer/ApplicationInfo;-><init>()V

    .line 262
    .local v0, "application":Lse/volvocars/acu/appdrawer/ApplicationInfo;
    invoke-virtual {v4, v6}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    iput-object v7, v0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->title:Ljava/lang/CharSequence;

    .line 263
    new-instance v7, Landroid/content/ComponentName;

    iget-object v8, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v9, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/high16 v8, 0x10200000

    invoke-virtual {v0, v7, v8}, Lse/volvocars/acu/appdrawer/ApplicationInfo;->setActivity(Landroid/content/ComponentName;I)V

    .line 268
    iget-object v7, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v7, v6}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    iput-object v7, v0, Lse/volvocars/acu/appdrawer/ApplicationInfo;->icon:Landroid/graphics/drawable/Drawable;

    .line 270
    sget-object v7, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplications:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    .end local v0    # "application":Lse/volvocars/acu/appdrawer/ApplicationInfo;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 274
    .end local v2    # "count":I
    .end local v3    # "i":I
    .end local v4    # "info":Landroid/content/pm/ResolveInfo;
    :cond_2
    return-void
.end method

.method private pkgOnBlackList(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 278
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mHiddenApps:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 279
    .local v3, "string":Ljava/lang/String;
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 280
    const/4 v4, 0x1

    .line 284
    .end local v3    # "string":Ljava/lang/String;
    :goto_1
    return v4

    .line 278
    .restart local v3    # "string":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 284
    .end local v3    # "string":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private registerIntentReceivers()V
    .locals 2

    .prologue
    .line 195
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 196
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 197
    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 198
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 199
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplicationsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 202
    new-instance v1, Lse/volvocars/acu/appdrawer/AppDrawerActivity$1;

    invoke-direct {v1, p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity$1;-><init>(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)V

    iput-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mHomeButtonListener:Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;

    .line 208
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mHomeButtonListener:Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;

    invoke-static {v1}, Lse/volvocars/acu/HomeButtonReceiver;->setHomeButtonListener(Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;)V

    .line 210
    new-instance v1, Lse/volvocars/acu/appdrawer/AppDrawerActivity$2;

    invoke-direct {v1, p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity$2;-><init>(Lse/volvocars/acu/appdrawer/AppDrawerActivity;)V

    iput-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mDriverWlListener:Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;

    .line 219
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mDriverWlListener:Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;

    invoke-static {v1}, Lse/volvocars/acu/DriverWorkloadReceiver;->setWorkloadListener(Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;)V

    .line 221
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x1

    .line 125
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mActivityMonitor:Lse/volvocars/acu/appdrawer/ActivityMonitor;

    invoke-virtual {v1}, Lse/volvocars/acu/appdrawer/ActivityMonitor;->onActivity()V

    .line 128
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x13

    if-ne v1, v2, :cond_2

    .line 129
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 130
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getSelectedItemPosition()I

    move-result v1

    sub-int v0, v1, v3

    .line 132
    .local v0, "pos":I
    if-gez v0, :cond_0

    .line 133
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getCount()I

    move-result v1

    sub-int v0, v1, v3

    .line 135
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setSelection(I)V

    .line 151
    .end local v0    # "pos":I
    :cond_1
    :goto_0
    return v3

    .line 137
    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x14

    if-ne v1, v2, :cond_4

    .line 138
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 139
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getSelectedItemPosition()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 141
    .restart local v0    # "pos":I
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 142
    const/4 v0, 0x0

    .line 145
    :cond_3
    iget-object v1, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    invoke-virtual {v1, v0}, Landroid/widget/GridView;->setSelection(I)V

    goto :goto_0

    .line 148
    .end local v0    # "pos":I
    :cond_4
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 156
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mActivityMonitor:Lse/volvocars/acu/appdrawer/ActivityMonitor;

    invoke-virtual {v0}, Lse/volvocars/acu/appdrawer/ActivityMonitor;->onActivity()V

    .line 157
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected getApplications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/appdrawer/ApplicationInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    sget-object v0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplications:Ljava/util/List;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    invoke-virtual {p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "fonts/VolvoSanProLig.otf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    iput-object v2, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mTypeface:Landroid/graphics/Typeface;

    .line 93
    invoke-virtual {p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f050000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mHiddenApps:[Ljava/lang/String;

    .line 95
    const/high16 v2, 0x7f030000

    invoke-virtual {p0, v2}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->setContentView(I)V

    .line 97
    const v2, 0x7f070005

    invoke-virtual {p0, v2}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 98
    .local v1, "textView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09003c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 99
    .local v0, "str":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v2, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 102
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->setDefaultKeyMode(I)V

    .line 105
    invoke-direct {p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->registerIntentReceivers()V

    .line 107
    invoke-direct {p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->loadApplications()V

    .line 109
    invoke-direct {p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->bindApplications()V

    .line 110
    iget-object v2, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setVisibility(I)V

    .line 111
    iget-object v2, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mGrid:Landroid/widget/GridView;

    new-instance v3, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationLauncher;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lse/volvocars/acu/appdrawer/AppDrawerActivity$ApplicationLauncher;-><init>(Lse/volvocars/acu/appdrawer/AppDrawerActivity;Lse/volvocars/acu/appdrawer/AppDrawerActivity$1;)V

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 114
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 174
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 179
    sget-object v2, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplications:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 180
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 181
    sget-object v2, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplications:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/appdrawer/ApplicationInfo;

    iget-object v2, v2, Lse/volvocars/acu/appdrawer/ApplicationInfo;->icon:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 180
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 185
    :cond_0
    iget-object v2, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mApplicationsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 187
    iget-object v2, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mDriverWlListener:Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;

    invoke-static {v2}, Lse/volvocars/acu/DriverWorkloadReceiver;->removeWorkLoadListener(Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;)V

    .line 188
    iget-object v2, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mHomeButtonListener:Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;

    invoke-static {v2}, Lse/volvocars/acu/HomeButtonReceiver;->removeHomeButtonListener(Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;)V

    .line 189
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 168
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 169
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mActivityMonitor:Lse/volvocars/acu/appdrawer/ActivityMonitor;

    invoke-virtual {v0}, Lse/volvocars/acu/appdrawer/ActivityMonitor;->pause()V

    .line 170
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 163
    iget-object v0, p0, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->mActivityMonitor:Lse/volvocars/acu/appdrawer/ActivityMonitor;

    invoke-virtual {v0}, Lse/volvocars/acu/appdrawer/ActivityMonitor;->onActivity()V

    .line 164
    return-void
.end method

.method setIdleMode(Z)V
    .locals 3
    .param p1, "idle"    # Z

    .prologue
    .line 292
    const-string v0, "AllAppsDrawer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting idle mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    if-eqz p1, :cond_0

    .line 294
    invoke-virtual {p0}, Lse/volvocars/acu/appdrawer/AppDrawerActivity;->finish()V

    .line 296
    :cond_0
    return-void
.end method
