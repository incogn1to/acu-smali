.class Lse/volvocars/acu/Animator$ThreadedTask;
.super Landroid/os/AsyncTask;
.source "Animator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ThreadedTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/Animator;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/Animator;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lse/volvocars/acu/Animator$ThreadedTask;->this$0:Lse/volvocars/acu/Animator;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/Animator;Lse/volvocars/acu/Animator$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/Animator;
    .param p2, "x1"    # Lse/volvocars/acu/Animator$1;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lse/volvocars/acu/Animator$ThreadedTask;-><init>(Lse/volvocars/acu/Animator;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 141
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lse/volvocars/acu/Animator$ThreadedTask;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 145
    :goto_0
    iget-object v3, p0, Lse/volvocars/acu/Animator$ThreadedTask;->this$0:Lse/volvocars/acu/Animator;

    # getter for: Lse/volvocars/acu/Animator;->animations:Ljava/util/List;
    invoke-static {v3}, Lse/volvocars/acu/Animator;->access$100(Lse/volvocars/acu/Animator;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 146
    iget-object v3, p0, Lse/volvocars/acu/Animator$ThreadedTask;->this$0:Lse/volvocars/acu/Animator;

    # getter for: Lse/volvocars/acu/Animator;->animations:Ljava/util/List;
    invoke-static {v3}, Lse/volvocars/acu/Animator;->access$100(Lse/volvocars/acu/Animator;)Ljava/util/List;

    move-result-object v3

    monitor-enter v3

    .line 147
    :try_start_0
    iget-object v4, p0, Lse/volvocars/acu/Animator$ThreadedTask;->this$0:Lse/volvocars/acu/Animator;

    # getter for: Lse/volvocars/acu/Animator;->animations:Ljava/util/List;
    invoke-static {v4}, Lse/volvocars/acu/Animator;->access$100(Lse/volvocars/acu/Animator;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 148
    .local v2, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lse/volvocars/acu/Animator$Animation;>;"
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 149
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/Animator$Animation;

    .line 151
    .local v0, "animation":Lse/volvocars/acu/Animator$Animation;
    invoke-virtual {v0}, Lse/volvocars/acu/Animator$Animation;->getProgress()F

    move-result v4

    float-to-double v4, v4

    invoke-virtual {v0}, Lse/volvocars/acu/Animator$Animation;->getStep()D

    move-result-wide v6

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    double-to-float v4, v4

    invoke-virtual {v0, v4}, Lse/volvocars/acu/Animator$Animation;->setProgress(F)V

    .line 154
    invoke-virtual {v0}, Lse/volvocars/acu/Animator$Animation;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v4

    # getter for: Lse/volvocars/acu/Animator$Animation;->mProgress:F
    invoke-static {v0}, Lse/volvocars/acu/Animator$Animation;->access$200(Lse/volvocars/acu/Animator$Animation;)F

    move-result v5

    invoke-interface {v4, v5}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    .line 155
    .local v1, "animationPosition":F
    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, v1

    float-to-double v4, v4

    invoke-virtual {v0}, Lse/volvocars/acu/Animator$Animation;->getStartValue()D

    move-result-wide v6

    mul-double/2addr v4, v6

    float-to-double v6, v1

    invoke-virtual {v0}, Lse/volvocars/acu/Animator$Animation;->getEndValue()D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Lse/volvocars/acu/Animator$Animation;->setNextValue(D)V

    goto :goto_1

    .line 157
    .end local v0    # "animation":Lse/volvocars/acu/Animator$Animation;
    .end local v1    # "animationPosition":F
    .end local v2    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lse/volvocars/acu/Animator$Animation;>;"
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v2    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lse/volvocars/acu/Animator$Animation;>;"
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 160
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, ""

    aput-object v5, v3, v4

    invoke-virtual {p0, v3}, Lse/volvocars/acu/Animator$ThreadedTask;->publishProgress([Ljava/lang/Object;)V

    .line 163
    const-wide/16 v3, 0x1e

    :try_start_2
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 164
    :catch_0
    move-exception v3

    goto :goto_0

    .line 169
    .end local v2    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lse/volvocars/acu/Animator$Animation;>;"
    :cond_1
    const/4 v3, 0x0

    return-object v3
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 141
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lse/volvocars/acu/Animator$ThreadedTask;->onProgressUpdate([Ljava/lang/String;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/String;)V
    .locals 6
    .param p1, "values"    # [Ljava/lang/String;

    .prologue
    .line 174
    iget-object v2, p0, Lse/volvocars/acu/Animator$ThreadedTask;->this$0:Lse/volvocars/acu/Animator;

    # getter for: Lse/volvocars/acu/Animator;->animations:Ljava/util/List;
    invoke-static {v2}, Lse/volvocars/acu/Animator;->access$100(Lse/volvocars/acu/Animator;)Ljava/util/List;

    move-result-object v2

    monitor-enter v2

    .line 176
    :try_start_0
    iget-object v3, p0, Lse/volvocars/acu/Animator$ThreadedTask;->this$0:Lse/volvocars/acu/Animator;

    # getter for: Lse/volvocars/acu/Animator;->animations:Ljava/util/List;
    invoke-static {v3}, Lse/volvocars/acu/Animator;->access$100(Lse/volvocars/acu/Animator;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 177
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lse/volvocars/acu/Animator$Animation;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 178
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/Animator$Animation;

    .line 180
    .local v0, "animation":Lse/volvocars/acu/Animator$Animation;
    invoke-virtual {v0}, Lse/volvocars/acu/Animator$Animation;->getProgress()F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 181
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 183
    invoke-virtual {v0}, Lse/volvocars/acu/Animator$Animation;->getEndValue()D

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lse/volvocars/acu/Animator$Animation;->setNextValue(D)V

    .line 186
    :cond_0
    invoke-virtual {v0}, Lse/volvocars/acu/Animator$Animation;->getComponent()Lse/volvocars/acu/Animatable;

    move-result-object v3

    invoke-virtual {v0}, Lse/volvocars/acu/Animator$Animation;->getNextValue()D

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lse/volvocars/acu/Animatable;->setValue(D)V

    goto :goto_0

    .line 188
    .end local v0    # "animation":Lse/volvocars/acu/Animator$Animation;
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lse/volvocars/acu/Animator$Animation;>;"
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lse/volvocars/acu/Animator$Animation;>;"
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 189
    return-void
.end method
