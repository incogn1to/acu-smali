.class Lse/volvocars/acu/UserActivityMonitor$IdleIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UserActivityMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/UserActivityMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IdleIntentReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/UserActivityMonitor;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/UserActivityMonitor;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lse/volvocars/acu/UserActivityMonitor$IdleIntentReceiver;->this$0:Lse/volvocars/acu/UserActivityMonitor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/UserActivityMonitor;Lse/volvocars/acu/UserActivityMonitor$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/UserActivityMonitor;
    .param p2, "x1"    # Lse/volvocars/acu/UserActivityMonitor$1;

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lse/volvocars/acu/UserActivityMonitor$IdleIntentReceiver;-><init>(Lse/volvocars/acu/UserActivityMonitor;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    .line 132
    const-string v1, "ActivityMonitor"

    const-string v2, "*****************Idle mode broadcast received!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const-string v1, "idle"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 134
    .local v0, "idle":Z
    iget-object v1, p0, Lse/volvocars/acu/UserActivityMonitor$IdleIntentReceiver;->this$0:Lse/volvocars/acu/UserActivityMonitor;

    # getter for: Lse/volvocars/acu/UserActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;
    invoke-static {v1}, Lse/volvocars/acu/UserActivityMonitor;->access$300(Lse/volvocars/acu/UserActivityMonitor;)Ljava/util/concurrent/Future;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lse/volvocars/acu/UserActivityMonitor$IdleIntentReceiver;->this$0:Lse/volvocars/acu/UserActivityMonitor;

    # getter for: Lse/volvocars/acu/UserActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;
    invoke-static {v1}, Lse/volvocars/acu/UserActivityMonitor;->access$300(Lse/volvocars/acu/UserActivityMonitor;)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 135
    iget-object v1, p0, Lse/volvocars/acu/UserActivityMonitor$IdleIntentReceiver;->this$0:Lse/volvocars/acu/UserActivityMonitor;

    # getter for: Lse/volvocars/acu/UserActivityMonitor;->mIdleCountdown:Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;
    invoke-static {v1}, Lse/volvocars/acu/UserActivityMonitor;->access$400(Lse/volvocars/acu/UserActivityMonitor;)Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;

    move-result-object v1

    invoke-virtual {v1}, Lse/volvocars/acu/UserActivityMonitor$IdleCountdown;->stop()V

    .line 136
    iget-object v1, p0, Lse/volvocars/acu/UserActivityMonitor$IdleIntentReceiver;->this$0:Lse/volvocars/acu/UserActivityMonitor;

    # getter for: Lse/volvocars/acu/UserActivityMonitor;->mIdleFuture:Ljava/util/concurrent/Future;
    invoke-static {v1}, Lse/volvocars/acu/UserActivityMonitor;->access$300(Lse/volvocars/acu/UserActivityMonitor;)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 138
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/UserActivityMonitor$IdleIntentReceiver;->this$0:Lse/volvocars/acu/UserActivityMonitor;

    # invokes: Lse/volvocars/acu/UserActivityMonitor;->setIdleMode(Z)V
    invoke-static {v1, v0}, Lse/volvocars/acu/UserActivityMonitor;->access$200(Lse/volvocars/acu/UserActivityMonitor;Z)V

    .line 142
    return-void
.end method
