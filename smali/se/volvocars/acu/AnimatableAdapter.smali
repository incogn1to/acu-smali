.class Lse/volvocars/acu/AnimatableAdapter;
.super Ljava/lang/Object;
.source "AnimatableAdapter.java"

# interfaces
.implements Lse/volvocars/acu/Animatable;


# static fields
.field private static final TAG:Ljava/lang/String; = "AnimatableAdapter"


# instance fields
.field private final mAcuActivity:Lse/volvocars/acu/AcuActivity;

.field private final mAnimator:Lse/volvocars/acu/Animator;

.field private final mAppName:Lse/volvocars/acu/ui/AppNameView;

.field private final mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

.field private mScreenXOffset:I


# direct methods
.method public constructor <init>(Lse/volvocars/acu/AcuActivity;Lse/volvocars/acu/ui/opengl/LauncherGLView;Lse/volvocars/acu/ui/AppNameView;)V
    .locals 1
    .param p1, "activity"    # Lse/volvocars/acu/AcuActivity;
    .param p2, "launcher"    # Lse/volvocars/acu/ui/opengl/LauncherGLView;
    .param p3, "appName"    # Lse/volvocars/acu/ui/AppNameView;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lse/volvocars/acu/Animator;

    invoke-direct {v0}, Lse/volvocars/acu/Animator;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/AnimatableAdapter;->mAnimator:Lse/volvocars/acu/Animator;

    .line 23
    iput-object p1, p0, Lse/volvocars/acu/AnimatableAdapter;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    .line 24
    iput-object p2, p0, Lse/volvocars/acu/AnimatableAdapter;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    .line 25
    iput-object p3, p0, Lse/volvocars/acu/AnimatableAdapter;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    .line 26
    return-void
.end method


# virtual methods
.method public animateTo(I)V
    .locals 6
    .param p1, "target"    # I

    .prologue
    .line 58
    iget-object v0, p0, Lse/volvocars/acu/AnimatableAdapter;->mAnimator:Lse/volvocars/acu/Animator;

    int-to-double v2, p1

    const-wide v4, 0x3fb999999999999aL    # 0.1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lse/volvocars/acu/Animator;->animate(Lse/volvocars/acu/Animatable;DD)V

    .line 59
    return-void
.end method

.method public getValue()D
    .locals 2

    .prologue
    .line 63
    iget v0, p0, Lse/volvocars/acu/AnimatableAdapter;->mScreenXOffset:I

    int-to-double v0, v0

    return-wide v0
.end method

.method public setValue(D)V
    .locals 4
    .param p1, "value"    # D

    .prologue
    const/4 v2, 0x1

    .line 32
    double-to-int v0, p1

    iput v0, p0, Lse/volvocars/acu/AnimatableAdapter;->mScreenXOffset:I

    .line 36
    iget-object v0, p0, Lse/volvocars/acu/AnimatableAdapter;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    iget v1, p0, Lse/volvocars/acu/AnimatableAdapter;->mScreenXOffset:I

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setScreenXOffset(I)V

    .line 38
    iget-object v0, p0, Lse/volvocars/acu/AnimatableAdapter;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    iget v1, p0, Lse/volvocars/acu/AnimatableAdapter;->mScreenXOffset:I

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AcuActivity;->setHomePosition(I)V

    .line 39
    iget-object v0, p0, Lse/volvocars/acu/AnimatableAdapter;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    iget v1, p0, Lse/volvocars/acu/AnimatableAdapter;->mScreenXOffset:I

    add-int/lit16 v1, v1, 0x2e0

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AcuActivity;->setAppNamePosition(I)V

    .line 44
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_0

    .line 45
    iget-object v0, p0, Lse/volvocars/acu/AnimatableAdapter;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AcuActivity;->setCurrentScreen(I)V

    .line 52
    :goto_0
    return-void

    .line 46
    :cond_0
    const-wide/high16 v0, -0x3f79000000000000L    # -736.0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_1

    .line 47
    iget-object v0, p0, Lse/volvocars/acu/AnimatableAdapter;->mAcuActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v0, v2}, Lse/volvocars/acu/AcuActivity;->setCurrentScreen(I)V

    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/AnimatableAdapter;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0, v2}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setState(I)V

    .line 50
    iget-object v0, p0, Lse/volvocars/acu/AnimatableAdapter;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Lse/volvocars/acu/AnimatableAdapter;->mScreenXOffset:I

    int-to-float v2, v2

    const/high16 v3, 0x44380000    # 736.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lse/volvocars/acu/ui/AppNameView;->setAlphaOverride(F)V

    goto :goto_0
.end method

.method public setXOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 55
    iput p1, p0, Lse/volvocars/acu/AnimatableAdapter;->mScreenXOffset:I

    .line 56
    return-void
.end method
