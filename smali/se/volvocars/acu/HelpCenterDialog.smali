.class public Lse/volvocars/acu/HelpCenterDialog;
.super Landroid/app/Dialog;
.source "HelpCenterDialog.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 33
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v3, 0x7f03001a

    invoke-virtual {p0, v3}, Lse/volvocars/acu/HelpCenterDialog;->setContentView(I)V

    .line 42
    const v3, 0x7f07000b

    invoke-virtual {p0, v3}, Lse/volvocars/acu/HelpCenterDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 43
    .local v2, "tv":Landroid/widget/TextView;
    invoke-virtual {p0}, Lse/volvocars/acu/HelpCenterDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    const-string v4, "fonts/VolvoSanProLig.otf"

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 44
    .local v0, "mTypeface":Landroid/graphics/Typeface;
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 47
    const v3, 0x7f070063

    invoke-virtual {p0, v3}, Lse/volvocars/acu/HelpCenterDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 48
    .local v1, "okButton":Landroid/widget/Button;
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 51
    new-instance v3, Lse/volvocars/acu/HelpCenterDialog$1;

    invoke-direct {v3, p0}, Lse/volvocars/acu/HelpCenterDialog$1;-><init>(Lse/volvocars/acu/HelpCenterDialog;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    return-void
.end method
