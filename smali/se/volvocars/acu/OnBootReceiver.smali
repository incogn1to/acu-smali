.class public Lse/volvocars/acu/OnBootReceiver;
.super Landroid/content/BroadcastReceiver;
.source "OnBootReceiver.java"


# static fields
.field private static mBootJustReceived:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static isJustBooted()Z
    .locals 2

    .prologue
    .line 30
    sget-boolean v0, Lse/volvocars/acu/OnBootReceiver;->mBootJustReceived:Z

    .line 31
    .local v0, "result":Z
    const/4 v1, 0x0

    sput-boolean v1, Lse/volvocars/acu/OnBootReceiver;->mBootJustReceived:Z

    .line 32
    return v0
.end method

.method public static setJustBootedForTesting(Z)V
    .locals 0
    .param p0, "booted"    # Z

    .prologue
    .line 40
    sput-boolean p0, Lse/volvocars/acu/OnBootReceiver;->mBootJustReceived:Z

    .line 41
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 19
    const-string v0, "Boot"

    const-string v1, "Boot rceived"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    const/4 v0, 0x1

    sput-boolean v0, Lse/volvocars/acu/OnBootReceiver;->mBootJustReceived:Z

    .line 22
    return-void
.end method
