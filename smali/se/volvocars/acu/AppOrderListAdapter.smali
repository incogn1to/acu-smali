.class public Lse/volvocars/acu/AppOrderListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AppOrderListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private mActiveItem:I

.field private mViews:[Lse/volvocars/acu/AppOrderItemView;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutResourceId"    # I

    .prologue
    const v6, 0x7f030002

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 15
    iput-object v5, p0, Lse/volvocars/acu/AppOrderListAdapter;->mViews:[Lse/volvocars/acu/AppOrderItemView;

    .line 34
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 35
    .local v2, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {v2, v6, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 36
    .local v4, "v":Landroid/view/View;
    move-object v0, v4

    check-cast v0, Lse/volvocars/acu/AppOrderItemView;

    move-object v3, v0

    .line 37
    .local v3, "staticAppOrder":Lse/volvocars/acu/AppOrderItemView;
    invoke-virtual {v2, v6, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 38
    move-object v0, v4

    check-cast v0, Lse/volvocars/acu/AppOrderItemView;

    move-object v1, v0

    .line 41
    .local v1, "dynamicAppOrder":Lse/volvocars/acu/AppOrderItemView;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090003

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lse/volvocars/acu/AppOrderItemView;->setText(Ljava/lang/String;)V

    .line 42
    invoke-virtual {v3, v7}, Lse/volvocars/acu/AppOrderItemView;->setState(I)V

    .line 43
    invoke-virtual {v3, v7}, Lse/volvocars/acu/AppOrderItemView;->setId(I)V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090004

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lse/volvocars/acu/AppOrderItemView;->setText(Ljava/lang/String;)V

    .line 45
    invoke-virtual {v1, v8}, Lse/volvocars/acu/AppOrderItemView;->setState(I)V

    .line 46
    invoke-virtual {v1, v8}, Lse/volvocars/acu/AppOrderItemView;->setId(I)V

    .line 50
    const/4 v5, 0x2

    new-array v5, v5, [Lse/volvocars/acu/AppOrderItemView;

    aput-object v3, v5, v7

    aput-object v1, v5, v8

    iput-object v5, p0, Lse/volvocars/acu/AppOrderListAdapter;->mViews:[Lse/volvocars/acu/AppOrderItemView;

    .line 55
    return-void
.end method


# virtual methods
.method public getActiveItem()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lse/volvocars/acu/AppOrderListAdapter;->mActiveItem:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lse/volvocars/acu/AppOrderListAdapter;->mViews:[Lse/volvocars/acu/AppOrderItemView;

    array-length v0, v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 85
    iget-object v0, p0, Lse/volvocars/acu/AppOrderListAdapter;->mViews:[Lse/volvocars/acu/AppOrderItemView;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public setActiveItem(I)V
    .locals 3
    .param p1, "pos"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 63
    if-ltz p1, :cond_0

    if-le p1, v1, :cond_1

    .line 64
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Argument has to be between 0 and 1."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_1
    if-nez p1, :cond_2

    .line 67
    iget-object v0, p0, Lse/volvocars/acu/AppOrderListAdapter;->mViews:[Lse/volvocars/acu/AppOrderItemView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AppOrderItemView;->setState(I)V

    .line 68
    iget-object v0, p0, Lse/volvocars/acu/AppOrderListAdapter;->mViews:[Lse/volvocars/acu/AppOrderItemView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Lse/volvocars/acu/AppOrderItemView;->setState(I)V

    .line 74
    :goto_0
    iput p1, p0, Lse/volvocars/acu/AppOrderListAdapter;->mActiveItem:I

    .line 75
    return-void

    .line 70
    :cond_2
    iget-object v0, p0, Lse/volvocars/acu/AppOrderListAdapter;->mViews:[Lse/volvocars/acu/AppOrderItemView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v2}, Lse/volvocars/acu/AppOrderItemView;->setState(I)V

    .line 71
    iget-object v0, p0, Lse/volvocars/acu/AppOrderListAdapter;->mViews:[Lse/volvocars/acu/AppOrderItemView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AppOrderItemView;->setState(I)V

    goto :goto_0
.end method
