.class public Lse/volvocars/acu/AppOrderItemView;
.super Landroid/widget/RelativeLayout;
.source "AppOrderItemView.java"


# static fields
.field private static final STATE_OFF:I


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field private mRadioButtonOff:Landroid/graphics/drawable/Drawable;

.field private mRadioButtonOn:Landroid/graphics/drawable/Drawable;

.field private mTextView:Landroid/widget/TextView;

.field private mTypeface:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts/VolvoSanProLig.otf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/AppOrderItemView;->mTypeface:Landroid/graphics/Typeface;

    .line 38
    return-void
.end method


# virtual methods
.method public isTurnedOn()Z
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lse/volvocars/acu/AppOrderItemView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/AppOrderItemView;->mRadioButtonOn:Landroid/graphics/drawable/Drawable;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 66
    const v0, 0x7f070008

    invoke-virtual {p0, v0}, Lse/volvocars/acu/AppOrderItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lse/volvocars/acu/AppOrderItemView;->mTextView:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lse/volvocars/acu/AppOrderItemView;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lse/volvocars/acu/AppOrderItemView;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 69
    const v0, 0x7f070009

    invoke-virtual {p0, v0}, Lse/volvocars/acu/AppOrderItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lse/volvocars/acu/AppOrderItemView;->mImageView:Landroid/widget/ImageView;

    .line 72
    invoke-virtual {p0}, Lse/volvocars/acu/AppOrderItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/AppOrderItemView;->mRadioButtonOn:Landroid/graphics/drawable/Drawable;

    .line 73
    invoke-virtual {p0}, Lse/volvocars/acu/AppOrderItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/AppOrderItemView;->mRadioButtonOff:Landroid/graphics/drawable/Drawable;

    .line 74
    iget-object v0, p0, Lse/volvocars/acu/AppOrderItemView;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lse/volvocars/acu/AppOrderItemView;->mRadioButtonOn:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 75
    return-void
.end method

.method public setState(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lse/volvocars/acu/AppOrderItemView;->mImageView:Landroid/widget/ImageView;

    if-nez p1, :cond_0

    iget-object v1, p0, Lse/volvocars/acu/AppOrderItemView;->mRadioButtonOff:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 55
    return-void

    .line 54
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/AppOrderItemView;->mRadioButtonOn:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 46
    iget-object v0, p0, Lse/volvocars/acu/AppOrderItemView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    return-void
.end method
