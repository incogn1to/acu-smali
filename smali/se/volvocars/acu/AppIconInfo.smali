.class public Lse/volvocars/acu/AppIconInfo;
.super Ljava/lang/Object;
.source "AppIconInfo.java"


# instance fields
.field private final x:F

.field private final y:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0
    .param p1, "xcord"    # F
    .param p2, "ycord"    # F

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lse/volvocars/acu/AppIconInfo;->x:F

    .line 16
    iput p2, p0, Lse/volvocars/acu/AppIconInfo;->y:F

    .line 17
    return-void
.end method


# virtual methods
.method public getX()F
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lse/volvocars/acu/AppIconInfo;->x:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lse/volvocars/acu/AppIconInfo;->y:F

    return v0
.end method
