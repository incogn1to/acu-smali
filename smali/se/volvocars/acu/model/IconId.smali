.class public final enum Lse/volvocars/acu/model/IconId;
.super Ljava/lang/Enum;
.source "IconId.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lse/volvocars/acu/model/IconId;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_ALL_APPS:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_AMAP:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_AUTONAVI:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_BROWSER:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_DEEZER:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_ICOYOTE:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_IGO:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_IN:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_LOCATION:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_MEDIA_DEVICE:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_MEDIA_LINEIN:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_MEDIA_SD:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_MEDIA_SHOWER:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_MEDIA_USB:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_MEDIA_WIFI:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_MOVIECLIPS:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_MUSIC:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_NAVX_ESSENCE:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_NAVX_PARKING:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_ORANGE_LIVE_RADIO:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_PARROT_MARKET:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_REAL_VNC:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_ROADTRIP:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_SERVICE_BOOKING:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_SETTINGS:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_SMARTLINK:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_SPOTIFY:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_TUNE_IN:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_VIDEO:Lse/volvocars/acu/model/IconId;

.field public static final enum ICON_WIKANGOH:Lse/volvocars/acu/model/IconId;


# instance fields
.field private final id:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 10
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_SETTINGS"

    invoke-direct {v0, v1, v4, v4}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_SETTINGS:Lse/volvocars/acu/model/IconId;

    .line 11
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_LOCATION"

    invoke-direct {v0, v1, v5, v5}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_LOCATION:Lse/volvocars/acu/model/IconId;

    .line 12
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_IN"

    invoke-direct {v0, v1, v6, v6}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_IN:Lse/volvocars/acu/model/IconId;

    .line 13
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_MEDIA_SD"

    invoke-direct {v0, v1, v7, v7}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_SD:Lse/volvocars/acu/model/IconId;

    .line 14
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_MEDIA_WIFI"

    invoke-direct {v0, v1, v8, v8}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_WIFI:Lse/volvocars/acu/model/IconId;

    .line 15
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_MEDIA_DEVICE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_DEVICE:Lse/volvocars/acu/model/IconId;

    .line 16
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_MEDIA_LINEIN"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_LINEIN:Lse/volvocars/acu/model/IconId;

    .line 17
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_MEDIA_USB"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_USB:Lse/volvocars/acu/model/IconId;

    .line 18
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_REAL_VNC"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_REAL_VNC:Lse/volvocars/acu/model/IconId;

    .line 19
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_MEDIA_SHOWER"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_SHOWER:Lse/volvocars/acu/model/IconId;

    .line 20
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_BROWSER"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_BROWSER:Lse/volvocars/acu/model/IconId;

    .line 21
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_VIDEO"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_VIDEO:Lse/volvocars/acu/model/IconId;

    .line 22
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_MUSIC"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_MUSIC:Lse/volvocars/acu/model/IconId;

    .line 23
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_ALL_APPS"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_ALL_APPS:Lse/volvocars/acu/model/IconId;

    .line 24
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_SERVICE_BOOKING"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_SERVICE_BOOKING:Lse/volvocars/acu/model/IconId;

    .line 25
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_IGO"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_IGO:Lse/volvocars/acu/model/IconId;

    .line 26
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_DEEZER"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_DEEZER:Lse/volvocars/acu/model/IconId;

    .line 27
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_ICOYOTE"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_ICOYOTE:Lse/volvocars/acu/model/IconId;

    .line 28
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_TUNE_IN"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_TUNE_IN:Lse/volvocars/acu/model/IconId;

    .line 29
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_SPOTIFY"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_SPOTIFY:Lse/volvocars/acu/model/IconId;

    .line 30
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_ROADTRIP"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_ROADTRIP:Lse/volvocars/acu/model/IconId;

    .line 31
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_ORANGE_LIVE_RADIO"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_ORANGE_LIVE_RADIO:Lse/volvocars/acu/model/IconId;

    .line 32
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_NAVX_PARKING"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_NAVX_PARKING:Lse/volvocars/acu/model/IconId;

    .line 33
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_NAVX_ESSENCE"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_NAVX_ESSENCE:Lse/volvocars/acu/model/IconId;

    .line 34
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_WIKANGOH"

    const/16 v2, 0x18

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_WIKANGOH:Lse/volvocars/acu/model/IconId;

    .line 35
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_PARROT_MARKET"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_PARROT_MARKET:Lse/volvocars/acu/model/IconId;

    .line 36
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_SMARTLINK"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_SMARTLINK:Lse/volvocars/acu/model/IconId;

    .line 37
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_MOVIECLIPS"

    const/16 v2, 0x1b

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_MOVIECLIPS:Lse/volvocars/acu/model/IconId;

    .line 38
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_AUTONAVI"

    const/16 v2, 0x1c

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_AUTONAVI:Lse/volvocars/acu/model/IconId;

    .line 39
    new-instance v0, Lse/volvocars/acu/model/IconId;

    const-string v1, "ICON_AMAP"

    const/16 v2, 0x1d

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lse/volvocars/acu/model/IconId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/IconId;->ICON_AMAP:Lse/volvocars/acu/model/IconId;

    .line 9
    const/16 v0, 0x1e

    new-array v0, v0, [Lse/volvocars/acu/model/IconId;

    sget-object v1, Lse/volvocars/acu/model/IconId;->ICON_SETTINGS:Lse/volvocars/acu/model/IconId;

    aput-object v1, v0, v4

    sget-object v1, Lse/volvocars/acu/model/IconId;->ICON_LOCATION:Lse/volvocars/acu/model/IconId;

    aput-object v1, v0, v5

    sget-object v1, Lse/volvocars/acu/model/IconId;->ICON_IN:Lse/volvocars/acu/model/IconId;

    aput-object v1, v0, v6

    sget-object v1, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_SD:Lse/volvocars/acu/model/IconId;

    aput-object v1, v0, v7

    sget-object v1, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_WIFI:Lse/volvocars/acu/model/IconId;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_DEVICE:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_LINEIN:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_USB:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_REAL_VNC:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_SHOWER:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_BROWSER:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_VIDEO:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_MUSIC:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_ALL_APPS:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_SERVICE_BOOKING:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_IGO:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_DEEZER:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_ICOYOTE:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_TUNE_IN:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_SPOTIFY:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_ROADTRIP:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_ORANGE_LIVE_RADIO:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_NAVX_PARKING:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_NAVX_ESSENCE:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_WIKANGOH:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_PARROT_MARKET:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_SMARTLINK:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_MOVIECLIPS:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_AUTONAVI:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_AMAP:Lse/volvocars/acu/model/IconId;

    aput-object v2, v0, v1

    sput-object v0, Lse/volvocars/acu/model/IconId;->$VALUES:[Lse/volvocars/acu/model/IconId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lse/volvocars/acu/model/IconId;->id:I

    .line 46
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lse/volvocars/acu/model/IconId;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lse/volvocars/acu/model/IconId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    .end local p0    # "name":Ljava/lang/String;
    check-cast p0, Lse/volvocars/acu/model/IconId;

    return-object p0
.end method

.method public static values()[Lse/volvocars/acu/model/IconId;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lse/volvocars/acu/model/IconId;->$VALUES:[Lse/volvocars/acu/model/IconId;

    invoke-virtual {v0}, [Lse/volvocars/acu/model/IconId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lse/volvocars/acu/model/IconId;

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lse/volvocars/acu/model/IconId;->id:I

    return v0
.end method
