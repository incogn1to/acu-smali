.class public final Lse/volvocars/acu/model/Apps;
.super Ljava/lang/Object;
.source "Apps.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/model/Apps$1;,
        Lse/volvocars/acu/model/Apps$AppReloader;
    }
.end annotation


# static fields
.field private static final ALLOW_DUMMY:Z = false

.field private static final TAG:Ljava/lang/String; = "Apps"


# instance fields
.field private mApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/model/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mAppsLock:Ljava/lang/Object;

.field private final mModel:Lse/volvocars/acu/model/ApplicationModel;

.field private final mModelLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "justBooted"    # Z

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/model/Apps;->mModelLock:Ljava/lang/Object;

    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/model/Apps;->mAppsLock:Ljava/lang/Object;

    .line 26
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/model/Apps;->mApps:Ljava/util/List;

    .line 29
    new-instance v0, Lse/volvocars/acu/model/ApplicationModel;

    const-string v1, "FactorySettings.json"

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2, p2}, Lse/volvocars/acu/model/ApplicationModel;-><init>(Landroid/content/Context;Ljava/lang/String;ZZ)V

    iput-object v0, p0, Lse/volvocars/acu/model/Apps;->mModel:Lse/volvocars/acu/model/ApplicationModel;

    .line 30
    iget-object v0, p0, Lse/volvocars/acu/model/Apps;->mModel:Lse/volvocars/acu/model/ApplicationModel;

    new-instance v1, Lse/volvocars/acu/model/Apps$AppReloader;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lse/volvocars/acu/model/Apps$AppReloader;-><init>(Lse/volvocars/acu/model/Apps;Lse/volvocars/acu/model/Apps$1;)V

    invoke-virtual {v0, v1}, Lse/volvocars/acu/model/ApplicationModel;->addAppInfoChangeListener(Lse/volvocars/acu/model/AppInfoChangeListener;)V

    .line 31
    invoke-direct {p0}, Lse/volvocars/acu/model/Apps;->loadApps()V

    .line 32
    return-void
.end method

.method static synthetic access$100(Lse/volvocars/acu/model/Apps;)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/model/Apps;

    .prologue
    .line 16
    invoke-direct {p0}, Lse/volvocars/acu/model/Apps;->loadApps()V

    return-void
.end method

.method private loadApps()V
    .locals 7

    .prologue
    .line 57
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 58
    .local v1, "apps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    iget-object v4, p0, Lse/volvocars/acu/model/Apps;->mModelLock:Ljava/lang/Object;

    monitor-enter v4

    .line 59
    :try_start_0
    iget-object v5, p0, Lse/volvocars/acu/model/Apps;->mModel:Lse/volvocars/acu/model/ApplicationModel;

    invoke-virtual {v5}, Lse/volvocars/acu/model/ApplicationModel;->getAppInfoList()Ljava/util/List;

    move-result-object v3

    .line 60
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/model/AppInfo;

    .line 61
    .local v0, "app":Lse/volvocars/acu/model/AppInfo;
    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->isHidden()Z

    move-result v5

    if-nez v5, :cond_0

    .line 63
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    .end local v0    # "app":Lse/volvocars/acu/model/AppInfo;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "list":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    :catchall_0
    move-exception v5

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 66
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "list":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    :cond_1
    :try_start_1
    const-string v5, "Apps"

    const-string v6, "Added non hidden apps"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    iget-object v4, p0, Lse/volvocars/acu/model/Apps;->mAppsLock:Ljava/lang/Object;

    monitor-enter v4

    .line 74
    :try_start_2
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lse/volvocars/acu/model/Apps;->mApps:Ljava/util/List;

    .line 75
    monitor-exit v4

    .line 76
    return-void

    .line 75
    :catchall_1
    move-exception v5

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v5
.end method


# virtual methods
.method public addAppInfoChangeListener(Lse/volvocars/acu/model/AppInfoChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lse/volvocars/acu/model/AppInfoChangeListener;

    .prologue
    .line 35
    iget-object v0, p0, Lse/volvocars/acu/model/Apps;->mModelLock:Ljava/lang/Object;

    monitor-enter v0

    .line 36
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/model/Apps;->mModel:Lse/volvocars/acu/model/ApplicationModel;

    invoke-virtual {v1, p1}, Lse/volvocars/acu/model/ApplicationModel;->addAppInfoChangeListener(Lse/volvocars/acu/model/AppInfoChangeListener;)V

    .line 37
    monitor-exit v0

    .line 38
    return-void

    .line 37
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public appStartedReorder(Lse/volvocars/acu/model/AppInfo;)Z
    .locals 3
    .param p1, "app"    # Lse/volvocars/acu/model/AppInfo;

    .prologue
    .line 102
    iget-object v0, p0, Lse/volvocars/acu/model/Apps;->mModelLock:Ljava/lang/Object;

    monitor-enter v0

    .line 103
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/model/Apps;->mModel:Lse/volvocars/acu/model/ApplicationModel;

    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lse/volvocars/acu/model/ApplicationModel;->appStartedReorder(Ljava/lang/String;)Z

    move-result v1

    monitor-exit v0

    return v1

    .line 104
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public close()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lse/volvocars/acu/model/Apps;->mModelLock:Ljava/lang/Object;

    monitor-enter v0

    .line 92
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/model/Apps;->mModel:Lse/volvocars/acu/model/ApplicationModel;

    invoke-virtual {v1}, Lse/volvocars/acu/model/ApplicationModel;->destroy()V

    .line 93
    monitor-exit v0

    .line 94
    return-void

    .line 93
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getApps()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/model/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lse/volvocars/acu/model/Apps;->mAppsLock:Ljava/lang/Object;

    monitor-enter v0

    .line 48
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/model/Apps;->mApps:Ljava/util/List;

    monitor-exit v0

    return-object v1

    .line 49
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeAppInfoChangeListener(Lse/volvocars/acu/model/AppInfoChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lse/volvocars/acu/model/AppInfoChangeListener;

    .prologue
    .line 41
    iget-object v0, p0, Lse/volvocars/acu/model/Apps;->mModelLock:Ljava/lang/Object;

    monitor-enter v0

    .line 42
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/model/Apps;->mModel:Lse/volvocars/acu/model/ApplicationModel;

    invoke-virtual {v1, p1}, Lse/volvocars/acu/model/ApplicationModel;->removeAppInfoChangeListener(Lse/volvocars/acu/model/AppInfoChangeListener;)V

    .line 43
    monitor-exit v0

    .line 44
    return-void

    .line 43
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
