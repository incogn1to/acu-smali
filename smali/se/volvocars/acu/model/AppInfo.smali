.class public Lse/volvocars/acu/model/AppInfo;
.super Ljava/lang/Object;
.source "AppInfo.java"


# static fields
.field public static final APP_DRAWER_IDENTIFIER:Ljava/lang/String; = "_appDrawer_"

.field public static final MEDIA_LIST_IDENTIFIERSTART:Ljava/lang/String; = "com.parrot.mediaList:"

.field public static final MEDIA_PLAYER_IDENTIFIERSTART:Ljava/lang/String; = "com.parrot.mediaplayer:"

.field public static final MEDIA_SOURCES_IDENTIFIER:Ljava/lang/String; = "_mediaSources_"

.field public static final SETTINGS_PACKAGE:Ljava/lang/String; = "com.android.settings"


# instance fields
.field private final appIconId:I

.field private final appTitle:Ljava/lang/String;

.field private final databaseId:J

.field private final hidden:Z

.field private final launchMode:Lse/volvocars/acu/model/LaunchMode;

.field private final mediaSource:Lcom/parrot/asteroid/audio/service/Source;

.field private final startIntent:Landroid/content/Intent;

.field private final uniqueIdentifier:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;IZLjava/lang/String;Landroid/content/ComponentName;IJ)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "iconId"    # I
    .param p3, "hidden"    # Z
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "className"    # Landroid/content/ComponentName;
    .param p6, "launchFlags"    # I
    .param p7, "id"    # J

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lse/volvocars/acu/model/AppInfo;->appTitle:Ljava/lang/String;

    .line 46
    iput p2, p0, Lse/volvocars/acu/model/AppInfo;->appIconId:I

    .line 47
    iput-boolean p3, p0, Lse/volvocars/acu/model/AppInfo;->hidden:Z

    .line 48
    iput-object p4, p0, Lse/volvocars/acu/model/AppInfo;->uniqueIdentifier:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lse/volvocars/acu/model/AppInfo;->mediaSource:Lcom/parrot/asteroid/audio/service/Source;

    .line 50
    sget-object v0, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_INTENT:Lse/volvocars/acu/model/LaunchMode;

    iput-object v0, p0, Lse/volvocars/acu/model/AppInfo;->launchMode:Lse/volvocars/acu/model/LaunchMode;

    .line 52
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lse/volvocars/acu/model/AppInfo;->startIntent:Landroid/content/Intent;

    .line 53
    iget-object v0, p0, Lse/volvocars/acu/model/AppInfo;->startIntent:Landroid/content/Intent;

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    iget-object v0, p0, Lse/volvocars/acu/model/AppInfo;->startIntent:Landroid/content/Intent;

    invoke-virtual {v0, p5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 55
    iget-object v0, p0, Lse/volvocars/acu/model/AppInfo;->startIntent:Landroid/content/Intent;

    invoke-virtual {v0, p6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 57
    iput-wide p7, p0, Lse/volvocars/acu/model/AppInfo;->databaseId:J

    .line 58
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZLjava/lang/String;Landroid/content/Intent;Lcom/parrot/asteroid/audio/service/Source;Lse/volvocars/acu/model/LaunchMode;J)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "iconId"    # I
    .param p3, "hidden"    # Z
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "startIntent"    # Landroid/content/Intent;
    .param p6, "source"    # Lcom/parrot/asteroid/audio/service/Source;
    .param p7, "launchMode"    # Lse/volvocars/acu/model/LaunchMode;
    .param p8, "id"    # J

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lse/volvocars/acu/model/AppInfo;->appTitle:Ljava/lang/String;

    .line 73
    iput p2, p0, Lse/volvocars/acu/model/AppInfo;->appIconId:I

    .line 74
    iput-boolean p3, p0, Lse/volvocars/acu/model/AppInfo;->hidden:Z

    .line 75
    iput-object p4, p0, Lse/volvocars/acu/model/AppInfo;->uniqueIdentifier:Ljava/lang/String;

    .line 76
    iput-object p5, p0, Lse/volvocars/acu/model/AppInfo;->startIntent:Landroid/content/Intent;

    .line 77
    iput-wide p8, p0, Lse/volvocars/acu/model/AppInfo;->databaseId:J

    .line 78
    iput-object p6, p0, Lse/volvocars/acu/model/AppInfo;->mediaSource:Lcom/parrot/asteroid/audio/service/Source;

    .line 79
    iput-object p7, p0, Lse/volvocars/acu/model/AppInfo;->launchMode:Lse/volvocars/acu/model/LaunchMode;

    .line 80
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZLjava/lang/String;Lcom/parrot/asteroid/audio/service/Source;Lse/volvocars/acu/model/LaunchMode;J)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "iconId"    # I
    .param p3, "hidden"    # Z
    .param p4, "mediaIdentifier"    # Ljava/lang/String;
    .param p5, "source"    # Lcom/parrot/asteroid/audio/service/Source;
    .param p6, "launchMode"    # Lse/volvocars/acu/model/LaunchMode;
    .param p7, "id"    # J

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object p1, p0, Lse/volvocars/acu/model/AppInfo;->appTitle:Ljava/lang/String;

    .line 95
    iput p2, p0, Lse/volvocars/acu/model/AppInfo;->appIconId:I

    .line 96
    iput-boolean p3, p0, Lse/volvocars/acu/model/AppInfo;->hidden:Z

    .line 97
    iput-object p4, p0, Lse/volvocars/acu/model/AppInfo;->uniqueIdentifier:Ljava/lang/String;

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lse/volvocars/acu/model/AppInfo;->startIntent:Landroid/content/Intent;

    .line 99
    iput-object p5, p0, Lse/volvocars/acu/model/AppInfo;->mediaSource:Lcom/parrot/asteroid/audio/service/Source;

    .line 100
    iput-object p6, p0, Lse/volvocars/acu/model/AppInfo;->launchMode:Lse/volvocars/acu/model/LaunchMode;

    .line 101
    iput-wide p7, p0, Lse/volvocars/acu/model/AppInfo;->databaseId:J

    .line 102
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 186
    if-ne p0, p1, :cond_0

    move v2, v5

    .line 194
    :goto_0
    return v2

    .line 189
    :cond_0
    instance-of v2, p1, Lse/volvocars/acu/model/AppInfo;

    if-nez v2, :cond_1

    move v2, v4

    .line 190
    goto :goto_0

    .line 193
    :cond_1
    move-object v0, p1

    check-cast v0, Lse/volvocars/acu/model/AppInfo;

    move-object v1, v0

    .line 194
    .local v1, "that":Lse/volvocars/acu/model/AppInfo;
    iget-object v2, p0, Lse/volvocars/acu/model/AppInfo;->uniqueIdentifier:Ljava/lang/String;

    iget-object v3, v1, Lse/volvocars/acu/model/AppInfo;->uniqueIdentifier:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lse/volvocars/acu/model/AppInfo;->appTitle:Ljava/lang/String;

    iget-object v3, v1, Lse/volvocars/acu/model/AppInfo;->appTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v5

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_0
.end method

.method public getAppIconId()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lse/volvocars/acu/model/AppInfo;->appIconId:I

    return v0
.end method

.method public getAppTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lse/volvocars/acu/model/AppInfo;->appTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getDatabaseId()J
    .locals 2

    .prologue
    .line 150
    iget-wide v0, p0, Lse/volvocars/acu/model/AppInfo;->databaseId:J

    return-wide v0
.end method

.method public getLaunchMode()Lse/volvocars/acu/model/LaunchMode;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lse/volvocars/acu/model/AppInfo;->launchMode:Lse/volvocars/acu/model/LaunchMode;

    return-object v0
.end method

.method public getMediaSource()Lcom/parrot/asteroid/audio/service/Source;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lse/volvocars/acu/model/AppInfo;->mediaSource:Lcom/parrot/asteroid/audio/service/Source;

    return-object v0
.end method

.method public getStartIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lse/volvocars/acu/model/AppInfo;->startIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getUniqueidentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lse/volvocars/acu/model/AppInfo;->uniqueIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 200
    iget-object v3, p0, Lse/volvocars/acu/model/AppInfo;->appTitle:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lse/volvocars/acu/model/AppInfo;->appTitle:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    move v2, v3

    .line 201
    .local v2, "result":I
    :goto_0
    iget-object v3, p0, Lse/volvocars/acu/model/AppInfo;->uniqueIdentifier:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lse/volvocars/acu/model/AppInfo;->uniqueIdentifier:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    move v1, v3

    .line 202
    .local v1, "packageNameResult":I
    :goto_1
    const/16 v0, 0xb2

    .line 203
    .local v0, "coolNumber":I
    xor-int v3, v2, v1

    xor-int/lit16 v2, v3, 0xb2

    .line 204
    return v2

    .end local v0    # "coolNumber":I
    .end local v1    # "packageNameResult":I
    .end local v2    # "result":I
    :cond_0
    move v2, v4

    .line 200
    goto :goto_0

    .restart local v2    # "result":I
    :cond_1
    move v1, v4

    .line 201
    goto :goto_1
.end method

.method public isHidden()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lse/volvocars/acu/model/AppInfo;->hidden:Z

    return v0
.end method

.method public setHidden(ZJ)Lse/volvocars/acu/model/AppInfo;
    .locals 10
    .param p1, "isHidden"    # Z
    .param p2, "databaseId"    # J

    .prologue
    .line 180
    new-instance v0, Lse/volvocars/acu/model/AppInfo;

    iget-object v1, p0, Lse/volvocars/acu/model/AppInfo;->appTitle:Ljava/lang/String;

    iget v2, p0, Lse/volvocars/acu/model/AppInfo;->appIconId:I

    iget-object v4, p0, Lse/volvocars/acu/model/AppInfo;->uniqueIdentifier:Ljava/lang/String;

    iget-object v5, p0, Lse/volvocars/acu/model/AppInfo;->startIntent:Landroid/content/Intent;

    iget-object v6, p0, Lse/volvocars/acu/model/AppInfo;->mediaSource:Lcom/parrot/asteroid/audio/service/Source;

    iget-object v7, p0, Lse/volvocars/acu/model/AppInfo;->launchMode:Lse/volvocars/acu/model/LaunchMode;

    move v3, p1

    move-wide v8, p2

    invoke-direct/range {v0 .. v9}, Lse/volvocars/acu/model/AppInfo;-><init>(Ljava/lang/String;IZLjava/lang/String;Landroid/content/Intent;Lcom/parrot/asteroid/audio/service/Source;Lse/volvocars/acu/model/LaunchMode;J)V

    return-object v0
.end method

.method public setMediaSource(Lcom/parrot/asteroid/audio/service/Source;)Lse/volvocars/acu/model/AppInfo;
    .locals 10
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 162
    new-instance v0, Lse/volvocars/acu/model/AppInfo;

    iget-object v1, p0, Lse/volvocars/acu/model/AppInfo;->appTitle:Ljava/lang/String;

    iget v2, p0, Lse/volvocars/acu/model/AppInfo;->appIconId:I

    iget-boolean v3, p0, Lse/volvocars/acu/model/AppInfo;->hidden:Z

    iget-object v4, p0, Lse/volvocars/acu/model/AppInfo;->uniqueIdentifier:Ljava/lang/String;

    iget-object v5, p0, Lse/volvocars/acu/model/AppInfo;->startIntent:Landroid/content/Intent;

    iget-object v7, p0, Lse/volvocars/acu/model/AppInfo;->launchMode:Lse/volvocars/acu/model/LaunchMode;

    iget-wide v8, p0, Lse/volvocars/acu/model/AppInfo;->databaseId:J

    move-object v6, p1

    invoke-direct/range {v0 .. v9}, Lse/volvocars/acu/model/AppInfo;-><init>(Ljava/lang/String;IZLjava/lang/String;Landroid/content/Intent;Lcom/parrot/asteroid/audio/service/Source;Lse/volvocars/acu/model/LaunchMode;J)V

    return-object v0
.end method
