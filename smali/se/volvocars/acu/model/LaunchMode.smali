.class public final enum Lse/volvocars/acu/model/LaunchMode;
.super Ljava/lang/Enum;
.source "LaunchMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lse/volvocars/acu/model/LaunchMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lse/volvocars/acu/model/LaunchMode;

.field public static final enum LAUNCH_WITH_INTENT:Lse/volvocars/acu/model/LaunchMode;

.field public static final enum LAUNCH_WITH_MEDIA_LIST:Lse/volvocars/acu/model/LaunchMode;

.field public static final enum LAUNCH_WITH_MEDIA_PLAYER:Lse/volvocars/acu/model/LaunchMode;


# instance fields
.field private final mode:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lse/volvocars/acu/model/LaunchMode;

    const-string v1, "LAUNCH_WITH_INTENT"

    invoke-direct {v0, v1, v2, v2}, Lse/volvocars/acu/model/LaunchMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_INTENT:Lse/volvocars/acu/model/LaunchMode;

    .line 10
    new-instance v0, Lse/volvocars/acu/model/LaunchMode;

    const-string v1, "LAUNCH_WITH_MEDIA_PLAYER"

    invoke-direct {v0, v1, v3, v3}, Lse/volvocars/acu/model/LaunchMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_MEDIA_PLAYER:Lse/volvocars/acu/model/LaunchMode;

    .line 11
    new-instance v0, Lse/volvocars/acu/model/LaunchMode;

    const-string v1, "LAUNCH_WITH_MEDIA_LIST"

    invoke-direct {v0, v1, v4, v4}, Lse/volvocars/acu/model/LaunchMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_MEDIA_LIST:Lse/volvocars/acu/model/LaunchMode;

    .line 8
    const/4 v0, 0x3

    new-array v0, v0, [Lse/volvocars/acu/model/LaunchMode;

    sget-object v1, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_INTENT:Lse/volvocars/acu/model/LaunchMode;

    aput-object v1, v0, v2

    sget-object v1, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_MEDIA_PLAYER:Lse/volvocars/acu/model/LaunchMode;

    aput-object v1, v0, v3

    sget-object v1, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_MEDIA_LIST:Lse/volvocars/acu/model/LaunchMode;

    aput-object v1, v0, v4

    sput-object v0, Lse/volvocars/acu/model/LaunchMode;->$VALUES:[Lse/volvocars/acu/model/LaunchMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "mode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17
    iput p3, p0, Lse/volvocars/acu/model/LaunchMode;->mode:I

    .line 18
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lse/volvocars/acu/model/LaunchMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lse/volvocars/acu/model/LaunchMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    .end local p0    # "name":Ljava/lang/String;
    check-cast p0, Lse/volvocars/acu/model/LaunchMode;

    return-object p0
.end method

.method public static values()[Lse/volvocars/acu/model/LaunchMode;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lse/volvocars/acu/model/LaunchMode;->$VALUES:[Lse/volvocars/acu/model/LaunchMode;

    invoke-virtual {v0}, [Lse/volvocars/acu/model/LaunchMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lse/volvocars/acu/model/LaunchMode;

    return-object v0
.end method


# virtual methods
.method public getMode()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lse/volvocars/acu/model/LaunchMode;->mode:I

    return v0
.end method
