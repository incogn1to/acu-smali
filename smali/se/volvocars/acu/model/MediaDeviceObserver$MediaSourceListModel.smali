.class public interface abstract Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;
.super Ljava/lang/Object;
.source "MediaDeviceObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/model/MediaDeviceObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MediaSourceListModel"
.end annotation


# virtual methods
.method public abstract addAppInfo(Lse/volvocars/acu/model/AppInfo;)V
.end method

.method public abstract getAppInfoFromIdentifier(Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;
.end method

.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract removeAllMediaDevices()V
.end method

.method public abstract removeAppInfo(Lse/volvocars/acu/model/AppInfo;)V
.end method
