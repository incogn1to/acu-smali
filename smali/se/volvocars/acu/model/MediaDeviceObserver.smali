.class public Lse/volvocars/acu/model/MediaDeviceObserver;
.super Ljava/lang/Object;
.source "MediaDeviceObserver.java"

# interfaces
.implements Lcom/parrot/asteroid/media/MediaObserverInterface;
.implements Lcom/parrot/asteroid/ManagerObserverInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaDeviceObserver"


# instance fields
.field private final mModel:Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;)V
    .locals 2
    .param p1, "model"    # Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    if-nez p1, :cond_0

    .line 42
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input model cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    iput-object p1, p0, Lse/volvocars/acu/model/MediaDeviceObserver;->mModel:Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;

    .line 45
    return-void
.end method

.method private addSourceToModel(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 13
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    const-string v12, "MediaDeviceObserver"

    .line 52
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getType()I

    move-result v11

    .line 58
    .local v11, "sourceType":I
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->isBrowsable()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_MEDIA_LIST:Lse/volvocars/acu/model/LaunchMode;

    move-object v6, v1

    .line 59
    .local v6, "launchMode":Lse/volvocars/acu/model/LaunchMode;
    :goto_0
    invoke-direct {p0, v11}, Lse/volvocars/acu/model/MediaDeviceObserver;->getSourceIconId(I)Lse/volvocars/acu/model/IconId;

    move-result-object v9

    .line 60
    .local v9, "iconId":Lse/volvocars/acu/model/IconId;
    if-nez v9, :cond_1

    .line 61
    const-string v1, "MediaDeviceObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "New media source created, but not type we are concerned with: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :goto_1
    return-void

    .line 58
    .end local v6    # "launchMode":Lse/volvocars/acu/model/LaunchMode;
    .end local v9    # "iconId":Lse/volvocars/acu/model/IconId;
    :cond_0
    sget-object v1, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_MEDIA_PLAYER:Lse/volvocars/acu/model/LaunchMode;

    move-object v6, v1

    goto :goto_0

    .line 65
    .restart local v6    # "launchMode":Lse/volvocars/acu/model/LaunchMode;
    .restart local v9    # "iconId":Lse/volvocars/acu/model/IconId;
    :cond_1
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->isBrowsable()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "com.parrot.mediaList:"

    move-object v10, v1

    .line 66
    .local v10, "identifierStart":Ljava/lang/String;
    :goto_2
    new-instance v0, Lse/volvocars/acu/model/AppInfo;

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getSourceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Lse/volvocars/acu/model/IconId;->getId()I

    move-result v2

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v7, -0x1

    move-object v5, p1

    invoke-direct/range {v0 .. v8}, Lse/volvocars/acu/model/AppInfo;-><init>(Ljava/lang/String;IZLjava/lang/String;Lcom/parrot/asteroid/audio/service/Source;Lse/volvocars/acu/model/LaunchMode;J)V

    .line 69
    .local v0, "info":Lse/volvocars/acu/model/AppInfo;
    const-string v1, "MediaDeviceObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device ready, adding it: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v1, p0, Lse/volvocars/acu/model/MediaDeviceObserver;->mModel:Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;

    invoke-interface {v1, v0}, Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;->addAppInfo(Lse/volvocars/acu/model/AppInfo;)V

    goto :goto_1

    .line 65
    .end local v0    # "info":Lse/volvocars/acu/model/AppInfo;
    .end local v10    # "identifierStart":Ljava/lang/String;
    :cond_2
    const-string v1, "com.parrot.mediaplayer:"

    move-object v10, v1

    goto :goto_2
.end method

.method private getSourceIconId(I)Lse/volvocars/acu/model/IconId;
    .locals 1
    .param p1, "sourceType"    # I

    .prologue
    .line 75
    packed-switch p1, :pswitch_data_0

    .line 91
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 77
    :pswitch_1
    sget-object v0, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_DEVICE:Lse/volvocars/acu/model/IconId;

    goto :goto_0

    .line 79
    :pswitch_2
    sget-object v0, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_LINEIN:Lse/volvocars/acu/model/IconId;

    goto :goto_0

    .line 81
    :pswitch_3
    sget-object v0, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_SD:Lse/volvocars/acu/model/IconId;

    goto :goto_0

    .line 83
    :pswitch_4
    sget-object v0, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_USB:Lse/volvocars/acu/model/IconId;

    goto :goto_0

    .line 86
    :pswitch_5
    sget-object v0, Lse/volvocars/acu/model/IconId;->ICON_MEDIA_WIFI:Lse/volvocars/acu/model/IconId;

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private removeSource(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 6
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 144
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->isBrowsable()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "com.parrot.mediaList:"

    move-object v1, v3

    .line 145
    .local v1, "identifierStart":Ljava/lang/String;
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "identifier":Ljava/lang/String;
    const-string v3, "MediaDeviceObserver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Trying to remove source: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v3, p0, Lse/volvocars/acu/model/MediaDeviceObserver;->mModel:Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;

    invoke-interface {v3, v0}, Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;->getAppInfoFromIdentifier(Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v2

    .line 148
    .local v2, "info":Lse/volvocars/acu/model/AppInfo;
    iget-object v3, p0, Lse/volvocars/acu/model/MediaDeviceObserver;->mModel:Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;

    invoke-interface {v3, v2}, Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;->removeAppInfo(Lse/volvocars/acu/model/AppInfo;)V

    .line 149
    return-void

    .line 144
    .end local v0    # "identifier":Ljava/lang/String;
    .end local v1    # "identifierStart":Ljava/lang/String;
    .end local v2    # "info":Lse/volvocars/acu/model/AppInfo;
    :cond_0
    const-string v3, "com.parrot.mediaplayer:"

    move-object v1, v3

    goto :goto_0
.end method


# virtual methods
.method public onCreatingSource(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 0
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 167
    return-void
.end method

.method public onManagerDestroyed(Ljava/lang/Object;)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/Object;

    .prologue
    .line 170
    return-void
.end method

.method public onManagerReady(ZLcom/parrot/asteroid/Manager;)V
    .locals 7
    .param p1, "ready"    # Z
    .param p2, "manager"    # Lcom/parrot/asteroid/Manager;

    .prologue
    const-string v6, "MediaDeviceObserver"

    .line 98
    iget-object v4, p0, Lse/volvocars/acu/model/MediaDeviceObserver;->mModel:Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;

    invoke-interface {v4}, Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/parrot/asteroid/media/MediaManagerFactory;->getMediaManager(Landroid/content/Context;)Lcom/parrot/asteroid/media/MediaManager;

    move-result-object v1

    .line 99
    .local v1, "media":Lcom/parrot/asteroid/media/MediaManager;
    const-string v4, "MediaDeviceObserver"

    const-string v4, "Media Manager ready!"

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {v1}, Lcom/parrot/asteroid/media/MediaManager;->getAllSource()Ljava/util/ArrayList;

    move-result-object v3

    .line 101
    .local v3, "sources":Ljava/util/List;, "Ljava/util/List<Lcom/parrot/asteroid/audio/service/Source;>;"
    if-nez v3, :cond_1

    .line 102
    const-string v4, "MediaDeviceObserver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MediaManager.getAllSource() returned <null>. ready="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_0
    return-void

    .line 105
    :cond_1
    const-string v4, "MediaDeviceObserver"

    const-string v4, "Removing all currently present media devices."

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v4, p0, Lse/volvocars/acu/model/MediaDeviceObserver;->mModel:Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;

    invoke-interface {v4}, Lse/volvocars/acu/model/MediaDeviceObserver$MediaSourceListModel;->removeAllMediaDevices()V

    .line 107
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/parrot/asteroid/audio/service/Source;

    .line 108
    .local v2, "s":Lcom/parrot/asteroid/audio/service/Source;
    invoke-virtual {v2}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_2

    invoke-virtual {v2}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_3

    .line 109
    :cond_2
    const-string v4, "MediaDeviceObserver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Source precent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/parrot/asteroid/audio/service/Source;->getSourceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/parrot/asteroid/audio/service/Source;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    invoke-direct {p0, v2}, Lse/volvocars/acu/model/MediaDeviceObserver;->addSourceToModel(Lcom/parrot/asteroid/audio/service/Source;)V

    goto :goto_0

    .line 112
    :cond_3
    const-string v4, "MediaDeviceObserver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Source precent that isn\'t ready: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/parrot/asteroid/audio/service/Source;->getSourceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onMetadataChanged(Lcom/parrot/asteroid/audio/service/Metadata;I)V
    .locals 0
    .param p1, "arg0"    # Lcom/parrot/asteroid/audio/service/Metadata;
    .param p2, "arg1"    # I

    .prologue
    .line 173
    return-void
.end method

.method public onModeChanged(II)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 177
    return-void
.end method

.method public onPlayerInfoChanged(Lcom/parrot/asteroid/audio/service/PlayerInfo;I)V
    .locals 0
    .param p1, "arg0"    # Lcom/parrot/asteroid/audio/service/PlayerInfo;
    .param p2, "arg1"    # I

    .prologue
    .line 181
    return-void
.end method

.method public onPlayerStatus(Lcom/parrot/asteroid/audio/service/Source;Lcom/parrot/asteroid/audio/service/PlayerInfo;Lcom/parrot/asteroid/audio/service/Metadata;)V
    .locals 0
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;
    .param p2, "arg1"    # Lcom/parrot/asteroid/audio/service/PlayerInfo;
    .param p3, "arg2"    # Lcom/parrot/asteroid/audio/service/Metadata;

    .prologue
    .line 184
    return-void
.end method

.method public onPreparingSource(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 0
    .param p1, "arg0"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 187
    return-void
.end method

.method public onSourceChanged(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 3
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    const-string v2, "MediaDeviceObserver"

    .line 153
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 154
    :cond_0
    const-string v0, "MediaDeviceObserver"

    const-string v0, "Adding device on source changed, with state ready or active."

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-direct {p0, p1}, Lse/volvocars/acu/model/MediaDeviceObserver;->addSourceToModel(Lcom/parrot/asteroid/audio/service/Source;)V

    .line 161
    :cond_1
    :goto_0
    const-string v0, "MediaDeviceObserver"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSourceChanged, state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    return-void

    .line 157
    :cond_2
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 158
    :cond_3
    const-string v0, "MediaDeviceObserver"

    const-string v0, "Remove source with problematic state."

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-direct {p0, p1}, Lse/volvocars/acu/model/MediaDeviceObserver;->removeSource(Lcom/parrot/asteroid/audio/service/Source;)V

    goto :goto_0
.end method

.method public onSourceError(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 0
    .param p1, "arg0"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 190
    return-void
.end method

.method public onSourceReady(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 0
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lse/volvocars/acu/model/MediaDeviceObserver;->addSourceToModel(Lcom/parrot/asteroid/audio/service/Source;)V

    .line 122
    return-void
.end method

.method public onSourceRemoved(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 3
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    .line 139
    invoke-direct {p0, p1}, Lse/volvocars/acu/model/MediaDeviceObserver;->removeSource(Lcom/parrot/asteroid/audio/service/Source;)V

    .line 140
    const-string v0, "MediaDeviceObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device removed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getSourceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    return-void
.end method

.method public onSourceUpdated(Lcom/parrot/asteroid/audio/service/Source;)V
    .locals 3
    .param p1, "source"    # Lcom/parrot/asteroid/audio/service/Source;

    .prologue
    const-string v2, "MediaDeviceObserver"

    .line 126
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 127
    :cond_0
    const-string v0, "MediaDeviceObserver"

    const-string v0, "Adding device on source updated, with state ready or active."

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-direct {p0, p1}, Lse/volvocars/acu/model/MediaDeviceObserver;->addSourceToModel(Lcom/parrot/asteroid/audio/service/Source;)V

    .line 134
    :cond_1
    :goto_0
    const-string v0, "MediaDeviceObserver"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSourceUpdated, state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    return-void

    .line 130
    :cond_2
    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/parrot/asteroid/audio/service/Source;->getState()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 131
    :cond_3
    const-string v0, "MediaDeviceObserver"

    const-string v0, "Remove source with problematic state."

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-direct {p0, p1}, Lse/volvocars/acu/model/MediaDeviceObserver;->removeSource(Lcom/parrot/asteroid/audio/service/Source;)V

    goto :goto_0
.end method

.method public onStateChanged(Lcom/parrot/asteroid/audio/service/PlayerInfo;)V
    .locals 0
    .param p1, "playerInfo"    # Lcom/parrot/asteroid/audio/service/PlayerInfo;

    .prologue
    .line 193
    return-void
.end method

.method public onTrackProgress(II)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 196
    return-void
.end method
