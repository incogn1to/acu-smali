.class final Lse/volvocars/acu/model/ApplicationModel;
.super Ljava/lang/Object;
.source "ApplicationModel.java"


# static fields
.field public static final PREFS_FIRST_START:Ljava/lang/String; = "firstStart"

.field private static final TAG:Ljava/lang/String; = "LauncherApplicationModel"


# instance fields
.field private final allowDummy:Z

.field private mAppInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/model/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mAppInfoChangedListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/model/AppInfoChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mAppInfoLock:Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field private mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

.field private mDatabaseListener:Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZZ)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "factorySettingsFile"    # Ljava/lang/String;
    .param p3, "allowDummy"    # Z
    .param p4, "justBooted"    # Z

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoChangedListeners:Ljava/util/List;

    .line 60
    iput-boolean p3, p0, Lse/volvocars/acu/model/ApplicationModel;->allowDummy:Z

    .line 61
    iput-object p1, p0, Lse/volvocars/acu/model/ApplicationModel;->mContext:Landroid/content/Context;

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    .line 65
    invoke-direct {p0, p2}, Lse/volvocars/acu/model/ApplicationModel;->createFirstRuntimeDataAndSyncWithDatabase(Ljava/lang/String;)V

    .line 67
    new-instance v0, Lse/volvocars/acu/model/ApplicationModel$1;

    invoke-direct {v0, p0}, Lse/volvocars/acu/model/ApplicationModel$1;-><init>(Lse/volvocars/acu/model/ApplicationModel;)V

    iput-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabaseListener:Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;

    .line 73
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v0

    .line 74
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    iget-object v2, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabaseListener:Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;

    invoke-virtual {v1, v2}, Lse/volvocars/acu/db/ApplicationsDatabase;->addOnDatabaseChangedListener(Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;)V

    .line 75
    monitor-exit v0

    .line 78
    return-void

    .line 75
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method static synthetic access$000(Lse/volvocars/acu/model/ApplicationModel;)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/model/ApplicationModel;

    .prologue
    .line 31
    invoke-direct {p0}, Lse/volvocars/acu/model/ApplicationModel;->updateAppInfoList()V

    return-void
.end method

.method private addAppDrawer()V
    .locals 11

    .prologue
    const-string v1, "_appDrawer_"

    .line 132
    iget-object v10, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v10

    .line 133
    :try_start_0
    new-instance v5, Landroid/content/Intent;

    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 134
    .local v5, "launchIntent":Landroid/content/Intent;
    new-instance v0, Lse/volvocars/acu/model/AppInfo;

    const-string v1, "_appDrawer_"

    invoke-virtual {p0, v1}, Lse/volvocars/acu/model/ApplicationModel;->getAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_ALL_APPS:Lse/volvocars/acu/model/IconId;

    invoke-virtual {v2}, Lse/volvocars/acu/model/IconId;->getId()I

    move-result v2

    const/4 v3, 0x0

    const-string v4, "_appDrawer_"

    const/4 v6, 0x0

    sget-object v7, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_INTENT:Lse/volvocars/acu/model/LaunchMode;

    const-wide/16 v8, -0x1

    invoke-direct/range {v0 .. v9}, Lse/volvocars/acu/model/AppInfo;-><init>(Ljava/lang/String;IZLjava/lang/String;Landroid/content/Intent;Lcom/parrot/asteroid/audio/service/Source;Lse/volvocars/acu/model/LaunchMode;J)V

    .line 139
    .local v0, "appDrawer":Lse/volvocars/acu/model/AppInfo;
    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lse/volvocars/acu/model/ApplicationModel;->getAppInfoFromIdentifier(Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v1

    if-nez v1, :cond_0

    .line 140
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_0
    const-string v1, "LauncherApplicationModel"

    const-string v2, "Adding app drawer app!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    monitor-exit v10

    .line 146
    return-void

    .line 144
    .end local v0    # "appDrawer":Lse/volvocars/acu/model/AppInfo;
    .end local v5    # "launchIntent":Landroid/content/Intent;
    :catchall_0
    move-exception v1

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private addMusicSources(Ljava/util/List;I)V
    .locals 13
    .param p2, "order"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/db/StoredAppInfo;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p1, "storedApps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    const-string v10, "LauncherApplicationModel"

    const-string v4, "_mediaSources_"

    .line 154
    const-string v1, "_mediaSources_"

    invoke-virtual {p0, v4}, Lse/volvocars/acu/model/ApplicationModel;->getAppInfoFromIdentifier(Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v1

    if-nez v1, :cond_0

    .line 156
    new-instance v5, Landroid/content/Intent;

    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 157
    .local v5, "launchIntent":Landroid/content/Intent;
    new-instance v0, Lse/volvocars/acu/model/AppInfo;

    const-string v1, "_mediaSources_"

    invoke-virtual {p0, v4}, Lse/volvocars/acu/model/ApplicationModel;->getAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lse/volvocars/acu/model/IconId;->ICON_MUSIC:Lse/volvocars/acu/model/IconId;

    invoke-virtual {v2}, Lse/volvocars/acu/model/IconId;->getId()I

    move-result v2

    const/4 v3, 0x0

    const-string v6, "_mediaSources_"

    const/4 v6, 0x0

    sget-object v7, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_INTENT:Lse/volvocars/acu/model/LaunchMode;

    const-wide/16 v8, -0x1

    invoke-direct/range {v0 .. v9}, Lse/volvocars/acu/model/AppInfo;-><init>(Ljava/lang/String;IZLjava/lang/String;Landroid/content/Intent;Lcom/parrot/asteroid/audio/service/Source;Lse/volvocars/acu/model/LaunchMode;J)V

    .line 160
    .local v0, "musicSourcesAppInfo":Lse/volvocars/acu/model/AppInfo;
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lse/volvocars/acu/model/ApplicationModel;->getStoredAppInfoFromIdentifier(Ljava/util/List;Ljava/lang/String;)Lse/volvocars/acu/db/StoredAppInfo;

    move-result-object v12

    .line 163
    .local v12, "storedMusicSourcesApp":Lse/volvocars/acu/db/StoredAppInfo;
    if-nez v12, :cond_1

    .line 164
    const-string v1, "LauncherApplicationModel"

    const-string v1, "Adding Music Sources"

    invoke-static {v10, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v7

    .line 166
    .local v7, "appName":Ljava/lang/String;
    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v8

    .line 167
    .local v8, "appPkg":Ljava/lang/String;
    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->isHidden()Z

    move-result v9

    .line 168
    .local v9, "appHidden":Z
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    new-instance v6, Lse/volvocars/acu/db/StoredAppInfo;

    add-int/lit8 v2, p2, 0x1

    int-to-long v10, v2

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/db/StoredAppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ZJ)V

    invoke-virtual {v1, v6}, Lse/volvocars/acu/db/ApplicationsDatabase;->addApplication(Lse/volvocars/acu/db/StoredAppInfo;)J

    .line 178
    .end local v0    # "musicSourcesAppInfo":Lse/volvocars/acu/model/AppInfo;
    .end local v5    # "launchIntent":Landroid/content/Intent;
    .end local v7    # "appName":Ljava/lang/String;
    .end local v8    # "appPkg":Ljava/lang/String;
    .end local v9    # "appHidden":Z
    .end local v12    # "storedMusicSourcesApp":Lse/volvocars/acu/db/StoredAppInfo;
    :cond_0
    :goto_0
    return-void

    .line 170
    .restart local v0    # "musicSourcesAppInfo":Lse/volvocars/acu/model/AppInfo;
    .restart local v5    # "launchIntent":Landroid/content/Intent;
    .restart local v12    # "storedMusicSourcesApp":Lse/volvocars/acu/db/StoredAppInfo;
    :cond_1
    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12}, Lse/volvocars/acu/db/StoredAppInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 171
    const-string v1, "LauncherApplicationModel"

    const-string v1, "Music Sources application already exists in database but name has been updated"

    invoke-static {v10, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lse/volvocars/acu/db/ApplicationsDatabase;->updateAppNameColumn(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private createAppInfoFromFactroySettingAndResolveInfo(Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;Landroid/content/pm/ResolveInfo;)Lse/volvocars/acu/model/AppInfo;
    .locals 17
    .param p1, "setting"    # Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    .param p2, "resolveInfo"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 370
    new-instance v7, Landroid/content/ComponentName;

    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object v4, v0

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object v5, v0

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v7, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    .local v7, "componentName":Landroid/content/ComponentName;
    const/4 v8, 0x0

    .line 375
    .local v8, "flags":I
    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getStartUpFlags()[I

    move-result-object v11

    .local v11, "arr$":[I
    move-object v0, v11

    array-length v0, v0

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_0
    move v0, v14

    move/from16 v1, v16

    if-ge v0, v1, :cond_0

    aget v13, v11, v14

    .line 376
    .local v13, "i":I
    or-int/2addr v8, v13

    .line 375
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 380
    .end local v13    # "i":I
    :cond_0
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getAppIconId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lse/volvocars/acu/model/IconId;->valueOf(Ljava/lang/String;)Lse/volvocars/acu/model/IconId;

    move-result-object v15

    .line 381
    .local v15, "iconId":Lse/volvocars/acu/model/IconId;
    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getAppName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object v1, v4

    invoke-virtual {v0, v1}, Lse/volvocars/acu/model/ApplicationModel;->getAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 382
    .local v3, "name":Ljava/lang/String;
    new-instance v2, Lse/volvocars/acu/model/AppInfo;

    invoke-virtual {v15}, Lse/volvocars/acu/model/IconId;->getId()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-wide/16 v9, -0x1

    invoke-direct/range {v2 .. v10}, Lse/volvocars/acu/model/AppInfo;-><init>(Ljava/lang/String;IZLjava/lang/String;Landroid/content/ComponentName;IJ)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .local v2, "info":Lse/volvocars/acu/model/AppInfo;
    move-object v4, v2

    .line 389
    .end local v2    # "info":Lse/volvocars/acu/model/AppInfo;
    .end local v3    # "name":Ljava/lang/String;
    .end local v15    # "iconId":Lse/volvocars/acu/model/IconId;
    :goto_1
    return-object v4

    .line 386
    :catch_0
    move-exception v4

    move-object v12, v4

    .line 387
    .local v12, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "LauncherApplicationModel"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Couldn\'t add app as id isn\'t correct: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getAppName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", iconId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getAppIconId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private createAppInfoIfInstalled(Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;Ljava/util/List;)Lse/volvocars/acu/model/AppInfo;
    .locals 6
    .param p1, "setting"    # Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Lse/volvocars/acu/model/AppInfo;"
        }
    .end annotation

    .prologue
    .local p2, "installedApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v4, 0x0

    const-string v5, "LauncherApplicationModel"

    .line 272
    invoke-virtual {p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 273
    const-string v2, "LauncherApplicationModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Won\'t create appInfo for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getAppName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " as it isn\'t enabled in the factory settings."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v4

    .line 294
    :goto_0
    return-object v2

    .line 277
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 279
    .local v1, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 280
    invoke-direct {p0, p1, v1}, Lse/volvocars/acu/model/ApplicationModel;->createAppInfoFromFactroySettingAndResolveInfo(Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;Landroid/content/pm/ResolveInfo;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v2

    goto :goto_0

    .line 284
    .end local v1    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_2
    iget-boolean v2, p0, Lse/volvocars/acu/model/ApplicationModel;->allowDummy:Z

    if-eqz v2, :cond_3

    .line 287
    invoke-direct {p0, p1}, Lse/volvocars/acu/model/ApplicationModel;->createDummyAppInfoFromFactroySetting(Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v2

    goto :goto_0

    .line 291
    :cond_3
    const-string v2, "LauncherApplicationModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can\'t add application "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getAppName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " as it isn\'t found on the android system"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v4

    .line 294
    goto :goto_0
.end method

.method private createDummyAppInfoFromFactroySetting(Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;)Lse/volvocars/acu/model/AppInfo;
    .locals 13
    .param p1, "setting"    # Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;

    .prologue
    const/4 v12, 0x0

    .line 305
    :try_start_0
    invoke-virtual {p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getAppIconId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lse/volvocars/acu/model/IconId;->valueOf(Ljava/lang/String;)Lse/volvocars/acu/model/IconId;

    move-result-object v11

    .line 306
    .local v11, "iconId":Lse/volvocars/acu/model/IconId;
    invoke-virtual {p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lse/volvocars/acu/model/ApplicationModel;->getAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 307
    .local v1, "name":Ljava/lang/String;
    new-instance v0, Lse/volvocars/acu/model/AppInfo;

    invoke-virtual {v11}, Lse/volvocars/acu/model/IconId;->getId()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getPackageName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const/4 v6, 0x0

    sget-object v7, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_INTENT:Lse/volvocars/acu/model/LaunchMode;

    const-wide/16 v8, -0x1

    invoke-direct/range {v0 .. v9}, Lse/volvocars/acu/model/AppInfo;-><init>(Ljava/lang/String;IZLjava/lang/String;Landroid/content/Intent;Lcom/parrot/asteroid/audio/service/Source;Lse/volvocars/acu/model/LaunchMode;J)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .local v0, "info":Lse/volvocars/acu/model/AppInfo;
    move-object v2, v0

    .line 314
    .end local v0    # "info":Lse/volvocars/acu/model/AppInfo;
    .end local v1    # "name":Ljava/lang/String;
    .end local v11    # "iconId":Lse/volvocars/acu/model/IconId;
    :goto_0
    return-object v2

    .line 311
    :catch_0
    move-exception v2

    move-object v10, v2

    .line 312
    .local v10, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "LauncherApplicationModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t add app as id isn\'t correct: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", iconId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->getAppIconId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v12

    .line 314
    goto :goto_0
.end method

.method private createFirstRuntimeDataAndSyncWithDatabase(Ljava/lang/String;)V
    .locals 4
    .param p1, "factorySettingsFile"    # Ljava/lang/String;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lse/volvocars/acu/model/ApplicationModel;->loadFactorySettings(Ljava/lang/String;)[Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;

    move-result-object v1

    .line 86
    .local v1, "factorySettings":[Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    invoke-direct {p0, v1}, Lse/volvocars/acu/model/ApplicationModel;->getAppsFromFactorySettings([Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;)Ljava/util/List;

    move-result-object v0

    .line 88
    .local v0, "factorySettingApps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    invoke-direct {p0}, Lse/volvocars/acu/model/ApplicationModel;->initiateDataBaseConnection()V

    .line 89
    invoke-direct {p0, v0}, Lse/volvocars/acu/model/ApplicationModel;->syncDatabase(Ljava/util/List;)V

    .line 90
    const-string v2, "LauncherApplicationModel"

    const-string v3, "Just booted, syncing database."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v2, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v2

    .line 93
    :try_start_0
    iget-object v3, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 94
    iget-object v3, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 98
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    invoke-direct {p0}, Lse/volvocars/acu/model/ApplicationModel;->updateAppInfoList()V

    .line 101
    return-void

    .line 96
    :cond_0
    :try_start_1
    iput-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    goto :goto_0

    .line 98
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private getAppInfoFromIdentifier(Ljava/util/List;Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;
    .locals 3
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/model/AppInfo;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lse/volvocars/acu/model/AppInfo;"
        }
    .end annotation

    .prologue
    .line 413
    .local p1, "appInfos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/model/AppInfo;

    .line 414
    .local v1, "info":Lse/volvocars/acu/model/AppInfo;
    invoke-virtual {v1}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v1

    .line 418
    .end local v1    # "info":Lse/volvocars/acu/model/AppInfo;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getAppsFromFactorySettings([Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;)Ljava/util/List;
    .locals 11
    .param p1, "factorySettings"    # [Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/model/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    iget-object v9, p0, Lse/volvocars/acu/model/ApplicationModel;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 242
    .local v7, "manager":Landroid/content/pm/PackageManager;
    new-instance v6, Landroid/content/Intent;

    const-string v9, "android.intent.action.MAIN"

    const/4 v10, 0x0

    invoke-direct {v6, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 243
    .local v6, "mainIntent":Landroid/content/Intent;
    const-string v9, "android.intent.category.LAUNCHER"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    const/4 v9, 0x0

    invoke-virtual {v7, v6, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    .line 246
    .local v4, "installedApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 248
    .local v1, "factorySettingApps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    if-eqz p1, :cond_1

    array-length v9, p1

    if-lez v9, :cond_1

    .line 249
    move-object v0, p1

    .local v0, "arr$":[Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v8, v0, v2

    .line 250
    .local v8, "setting":Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    invoke-direct {p0, v8, v4}, Lse/volvocars/acu/model/ApplicationModel;->createAppInfoIfInstalled(Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;Ljava/util/List;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v3

    .line 251
    .local v3, "info":Lse/volvocars/acu/model/AppInfo;
    if-eqz v3, :cond_0

    .line 252
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 256
    .end local v0    # "arr$":[Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    .end local v2    # "i$":I
    .end local v3    # "info":Lse/volvocars/acu/model/AppInfo;
    .end local v5    # "len$":I
    .end local v8    # "setting":Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    :cond_1
    const-string v9, "LauncherApplicationModel"

    const-string v10, "Can\'t find any settings from factory!"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :cond_2
    return-object v1
.end method

.method private getStoredAppInfoFromIdentifier(Ljava/util/List;Ljava/lang/String;)Lse/volvocars/acu/db/StoredAppInfo;
    .locals 3
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/db/StoredAppInfo;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lse/volvocars/acu/db/StoredAppInfo;"
        }
    .end annotation

    .prologue
    .line 429
    .local p1, "appInfos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/db/StoredAppInfo;

    .line 430
    .local v1, "info":Lse/volvocars/acu/db/StoredAppInfo;
    invoke-virtual {v1}, Lse/volvocars/acu/db/StoredAppInfo;->getPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v1

    .line 434
    .end local v1    # "info":Lse/volvocars/acu/db/StoredAppInfo;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getStringResource(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 355
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "string"

    const-string v3, "se.volvocars.acu"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 356
    .local v0, "appNameId":I
    if-eqz v0, :cond_0

    .line 357
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 359
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initiateDataBaseConnection()V
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v0

    .line 230
    :try_start_0
    new-instance v1, Lse/volvocars/acu/db/ApplicationsDatabase;

    iget-object v2, p0, Lse/volvocars/acu/model/ApplicationModel;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lse/volvocars/acu/db/ApplicationsDatabase;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    .line 231
    monitor-exit v0

    .line 232
    return-void

    .line 231
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private loadFactorySettings(Ljava/lang/String;)[Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    .locals 8
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const-string v6, "LauncherApplicationModel"

    .line 612
    new-instance v2, Lcom/google/gson/Gson;

    invoke-direct {v2}, Lcom/google/gson/Gson;-><init>()V

    .line 613
    .local v2, "gson":Lcom/google/gson/Gson;
    iget-object v4, p0, Lse/volvocars/acu/model/ApplicationModel;->mContext:Landroid/content/Context;

    invoke-static {p1, v4}, Lse/volvocars/acu/util/FileHandler;->readAssetsFile(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 614
    .local v3, "text":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 615
    const-string v4, "LauncherApplicationModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Couldn\'t read assets file: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v7

    .line 628
    :goto_0
    return-object v4

    .line 619
    :cond_0
    :try_start_0
    const-class v4, Lse/volvocars/acu/gsonparser/FactorySettings;

    invoke-virtual {v2, v3, v4}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/gsonparser/FactorySettings;

    .line 620
    .local v1, "factorySettingsGson":Lse/volvocars/acu/gsonparser/FactorySettings;
    invoke-virtual {v1}, Lse/volvocars/acu/gsonparser/FactorySettings;->getFactorySettings()[Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    :try_end_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/gson/JsonParseException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    goto :goto_0

    .line 622
    .end local v1    # "factorySettingsGson":Lse/volvocars/acu/gsonparser/FactorySettings;
    :catch_0
    move-exception v4

    move-object v0, v4

    .line 623
    .local v0, "e":Lcom/google/gson/JsonSyntaxException;
    const-string v4, "LauncherApplicationModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not read factory settings file, syntax error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/gson/JsonSyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "e":Lcom/google/gson/JsonSyntaxException;
    :goto_1
    move-object v4, v7

    .line 628
    goto :goto_0

    .line 625
    :catch_1
    move-exception v4

    move-object v0, v4

    .line 626
    .local v0, "e":Lcom/google/gson/JsonParseException;
    const-string v4, "LauncherApplicationModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Could not read factory settings file, parse exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/gson/JsonParseException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private sendAppInfoChangedCallback()V
    .locals 3

    .prologue
    .line 481
    iget-object v2, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoChangedListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/model/AppInfoChangeListener;

    .line 482
    .local v1, "listener":Lse/volvocars/acu/model/AppInfoChangeListener;
    if-eqz v1, :cond_0

    .line 483
    invoke-interface {v1}, Lse/volvocars/acu/model/AppInfoChangeListener;->appInfoHasChanged()V

    goto :goto_0

    .line 486
    .end local v1    # "listener":Lse/volvocars/acu/model/AppInfoChangeListener;
    :cond_1
    return-void
.end method

.method private syncDatabase(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/model/AppInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 186
    .local p1, "factorySettingApps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    iget-object v12, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v12

    .line 187
    :try_start_0
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->getAllApps()Ljava/util/List;

    move-result-object v11

    .line 188
    .local v11, "storedAppInfos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lse/volvocars/acu/db/StoredAppInfo;

    .line 190
    .local v6, "app":Lse/volvocars/acu/db/StoredAppInfo;
    invoke-virtual {v6}, Lse/volvocars/acu/db/StoredAppInfo;->getPackage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lse/volvocars/acu/model/ApplicationModel;->getAppInfoFromIdentifier(Ljava/util/List;Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Lse/volvocars/acu/db/StoredAppInfo;->getPackage()Ljava/lang/String;

    move-result-object v0

    const-string v4, "_mediaSources_"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v6}, Lse/volvocars/acu/db/StoredAppInfo;->getId()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lse/volvocars/acu/db/ApplicationsDatabase;->removeApplication(J)I

    goto :goto_0

    .line 222
    .end local v6    # "app":Lse/volvocars/acu/db/StoredAppInfo;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "storedAppInfos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    :catchall_0
    move-exception v0

    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 196
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v11    # "storedAppInfos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    :cond_1
    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    .line 199
    .local v10, "order":I
    invoke-direct {p0, v11, v10}, Lse/volvocars/acu/model/ApplicationModel;->addMusicSources(Ljava/util/List;I)V

    .line 201
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lse/volvocars/acu/model/AppInfo;

    .line 203
    .local v6, "app":Lse/volvocars/acu/model/AppInfo;
    invoke-virtual {v6}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v11, v0}, Lse/volvocars/acu/model/ApplicationModel;->getStoredAppInfoFromIdentifier(Ljava/util/List;Ljava/lang/String;)Lse/volvocars/acu/db/StoredAppInfo;

    move-result-object v0

    if-nez v0, :cond_3

    .line 204
    invoke-virtual {v6}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v1

    .line 205
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {v6}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v2

    .line 206
    .local v2, "pkg":Ljava/lang/String;
    invoke-virtual {v6}, Lse/volvocars/acu/model/AppInfo;->isHidden()Z

    move-result v3

    .line 207
    .local v3, "hidden":Z
    iget-object v13, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    new-instance v0, Lse/volvocars/acu/db/StoredAppInfo;

    int-to-long v4, v10

    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/db/StoredAppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ZJ)V

    invoke-virtual {v13, v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->addApplication(Lse/volvocars/acu/db/StoredAppInfo;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    add-int/lit8 v10, v10, -0x1

    .line 209
    goto :goto_1

    .line 213
    .end local v1    # "name":Ljava/lang/String;
    .end local v2    # "pkg":Ljava/lang/String;
    .end local v3    # "hidden":Z
    :cond_3
    :try_start_2
    invoke-virtual {v6}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v11, v0}, Lse/volvocars/acu/model/ApplicationModel;->getStoredAppInfoFromIdentifier(Ljava/util/List;Ljava/lang/String;)Lse/volvocars/acu/db/StoredAppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/db/StoredAppInfo;->getName()Ljava/lang/String;

    move-result-object v7

    .line 214
    .local v7, "currentStoredName":Ljava/lang/String;
    invoke-virtual {v6}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 215
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v6}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lse/volvocars/acu/db/ApplicationsDatabase;->updateAppNameColumn(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 217
    .end local v7    # "currentStoredName":Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object v8, v0

    .line 218
    .local v8, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    const-string v0, "LauncherApplicationModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Couldn\'t update new app name in database since app doesn\'t exist there: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v6}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 222
    .end local v6    # "app":Lse/volvocars/acu/model/AppInfo;
    .end local v8    # "e":Ljava/lang/IllegalArgumentException;
    :cond_4
    monitor-exit v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 223
    return-void
.end method

.method private tryUpdatingMediaSource(Lse/volvocars/acu/model/AppInfo;)V
    .locals 5
    .param p1, "app"    # Lse/volvocars/acu/model/AppInfo;

    .prologue
    .line 578
    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->getMediaSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 579
    const-string v2, "LauncherApplicationModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Updating media source: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lse/volvocars/acu/model/ApplicationModel;->getAppInfoFromIdentifier(Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v0

    .line 581
    .local v0, "info":Lse/volvocars/acu/model/AppInfo;
    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->getMediaSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v2

    invoke-virtual {v0, v2}, Lse/volvocars/acu/model/AppInfo;->setMediaSource(Lcom/parrot/asteroid/audio/service/Source;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v1

    .line 582
    .local v1, "updated":Lse/volvocars/acu/model/AppInfo;
    iget-object v2, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 583
    iget-object v2, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 586
    .end local v0    # "info":Lse/volvocars/acu/model/AppInfo;
    .end local v1    # "updated":Lse/volvocars/acu/model/AppInfo;
    :cond_0
    return-void
.end method

.method private updateAppInfoList()V
    .locals 9

    .prologue
    .line 495
    iget-object v5, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v5

    .line 496
    :try_start_0
    iget-object v6, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v6}, Lse/volvocars/acu/db/ApplicationsDatabase;->getAllApps()Ljava/util/List;

    move-result-object v3

    .line 497
    .local v3, "infos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 498
    .local v4, "updated":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/db/StoredAppInfo;

    .line 499
    .local v2, "info":Lse/volvocars/acu/db/StoredAppInfo;
    invoke-virtual {v2}, Lse/volvocars/acu/db/StoredAppInfo;->getPackage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lse/volvocars/acu/model/ApplicationModel;->getAppInfoFromIdentifier(Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v0

    .line 500
    .local v0, "found":Lse/volvocars/acu/model/AppInfo;
    if-eqz v0, :cond_0

    .line 501
    invoke-virtual {v2}, Lse/volvocars/acu/db/StoredAppInfo;->isHidden()Z

    move-result v6

    invoke-virtual {v2}, Lse/volvocars/acu/db/StoredAppInfo;->getId()J

    move-result-wide v7

    invoke-virtual {v0, v6, v7, v8}, Lse/volvocars/acu/model/AppInfo;->setHidden(ZJ)Lse/volvocars/acu/model/AppInfo;

    move-result-object v0

    .line 502
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 513
    .end local v0    # "found":Lse/volvocars/acu/model/AppInfo;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "info":Lse/volvocars/acu/db/StoredAppInfo;
    .end local v3    # "infos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    .end local v4    # "updated":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    :catchall_0
    move-exception v6

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 509
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v3    # "infos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    .restart local v4    # "updated":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    :cond_1
    :try_start_1
    iput-object v4, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    .line 511
    invoke-direct {p0}, Lse/volvocars/acu/model/ApplicationModel;->addAppDrawer()V

    .line 512
    invoke-direct {p0}, Lse/volvocars/acu/model/ApplicationModel;->sendAppInfoChangedCallback()V

    .line 513
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 515
    return-void
.end method


# virtual methods
.method public addAppInfo(Lse/volvocars/acu/model/AppInfo;)V
    .locals 10
    .param p1, "app"    # Lse/volvocars/acu/model/AppInfo;

    .prologue
    const-string v1, "LauncherApplicationModel"

    .line 551
    iget-object v9, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v9

    .line 552
    :try_start_0
    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lse/volvocars/acu/model/ApplicationModel;->getAppInfoFromIdentifier(Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v1

    if-nez v1, :cond_2

    .line 553
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 555
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v1}, Lse/volvocars/acu/db/ApplicationsDatabase;->getAllApps()Ljava/util/List;

    move-result-object v8

    .line 556
    .local v8, "infos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v8, v1}, Lse/volvocars/acu/model/ApplicationModel;->getStoredAppInfoFromIdentifier(Ljava/util/List;Ljava/lang/String;)Lse/volvocars/acu/db/StoredAppInfo;

    move-result-object v1

    if-nez v1, :cond_1

    .line 557
    new-instance v0, Lse/volvocars/acu/db/StoredAppInfo;

    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->isHidden()Z

    move-result v3

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/db/StoredAppInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ZJ)V

    .line 559
    .local v0, "storedApp":Lse/volvocars/acu/db/StoredAppInfo;
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v1, v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->addApplication(Lse/volvocars/acu/db/StoredAppInfo;)J

    move-result-wide v6

    .line 560
    .local v6, "id":J
    const-wide/16 v1, -0x1

    cmp-long v1, v6, v1

    if-eqz v1, :cond_0

    .line 561
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v1, v6, v7}, Lse/volvocars/acu/db/ApplicationsDatabase;->updateLastUsedColumn(J)I

    .line 574
    .end local v0    # "storedApp":Lse/volvocars/acu/db/StoredAppInfo;
    .end local v6    # "id":J
    .end local v8    # "infos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    :cond_0
    :goto_0
    monitor-exit v9

    .line 575
    return-void

    .line 565
    .restart local v8    # "infos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    :cond_1
    const-string v1, "LauncherApplicationModel"

    const-string v2, "Added app already exists in database, just updating."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    invoke-direct {p0}, Lse/volvocars/acu/model/ApplicationModel;->updateAppInfoList()V

    goto :goto_0

    .line 574
    .end local v8    # "infos":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    :catchall_0
    move-exception v1

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 569
    :cond_2
    :try_start_1
    const-string v1, "LauncherApplicationModel"

    const-string v2, "Trying to add an app that is already added to the system."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    invoke-direct {p0, p1}, Lse/volvocars/acu/model/ApplicationModel;->tryUpdatingMediaSource(Lse/volvocars/acu/model/AppInfo;)V

    .line 571
    invoke-direct {p0}, Lse/volvocars/acu/model/ApplicationModel;->updateAppInfoList()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public addAppInfoChangeListener(Lse/volvocars/acu/model/AppInfoChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lse/volvocars/acu/model/AppInfoChangeListener;

    .prologue
    const-string v1, "LauncherApplicationModel"

    .line 455
    if-nez p1, :cond_0

    .line 456
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "AppInfoChangeListener can\'t be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoChangedListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 459
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoChangedListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 460
    const-string v0, "LauncherApplicationModel"

    const-string v0, "Added AppInfoChangeListener"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    :goto_0
    return-void

    .line 462
    :cond_1
    const-string v0, "LauncherApplicationModel"

    const-string v0, "Didn\'t add appInfoChangeListener as it was already added."

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public appStartedReorder(Ljava/lang/String;)Z
    .locals 7
    .param p1, "uniqueIdentifier"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const-string v2, "LauncherApplicationModel"

    .line 525
    iget-object v2, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v2

    .line 526
    :try_start_0
    invoke-virtual {p0, p1}, Lse/volvocars/acu/model/ApplicationModel;->getAppInfoFromIdentifier(Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v0

    .line 527
    .local v0, "app":Lse/volvocars/acu/model/AppInfo;
    if-nez v0, :cond_0

    .line 528
    const-string v3, "LauncherApplicationModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Couldn\'t reorganize order of apps, as app with package name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " couldn\'t be found."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v2, v6

    .line 539
    :goto_0
    return v2

    .line 534
    :cond_0
    :try_start_1
    iget-object v3, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getDatabaseId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lse/volvocars/acu/db/ApplicationsDatabase;->updateLastUsedColumn(J)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 536
    const/4 v3, 0x1

    :try_start_2
    monitor-exit v2

    move v2, v3

    goto :goto_0

    .line 537
    :catch_0
    move-exception v3

    move-object v1, v3

    .line 538
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "LauncherApplicationModel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Couldn\'t update last used in database as app doesn\'t exist there: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    monitor-exit v2

    move v2, v6

    goto :goto_0

    .line 541
    .end local v0    # "app":Lse/volvocars/acu/model/AppInfo;
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public destroy()V
    .locals 3

    .prologue
    .line 635
    const-string v0, "LauncherApplicationModel"

    const-string v1, "OnDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v0

    .line 637
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    iget-object v2, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabaseListener:Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;

    invoke-virtual {v1, v2}, Lse/volvocars/acu/db/ApplicationsDatabase;->removeOnDatabaseChangeListener(Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;)V

    .line 638
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v1}, Lse/volvocars/acu/db/ApplicationsDatabase;->close()V

    .line 639
    monitor-exit v0

    .line 640
    return-void

    .line 639
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected getAppInfoFromIdentifier(Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 400
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v0

    .line 401
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    invoke-direct {p0, v1, p1}, Lse/volvocars/acu/model/ApplicationModel;->getAppInfoFromIdentifier(Ljava/util/List;Ljava/lang/String;)Lse/volvocars/acu/model/AppInfo;

    move-result-object v1

    monitor-exit v0

    return-object v1

    .line 402
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getAppInfoList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/model/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 443
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v0

    .line 444
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v0

    return-object v1

    .line 445
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getAppName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "givenName"    # Ljava/lang/String;

    .prologue
    .line 347
    invoke-direct {p0, p1}, Lse/volvocars/acu/model/ApplicationModel;->getStringResource(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 348
    .local v0, "name":Ljava/lang/String;
    if-nez v0, :cond_0

    move-object v1, p1

    .line 351
    :goto_0
    return-object v1

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 643
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public removeAllMediaDevices()V
    .locals 6

    .prologue
    .line 647
    iget-object v3, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v3

    .line 648
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 649
    .local v2, "toRemove":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    iget-object v4, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/model/AppInfo;

    .line 650
    .local v1, "info":Lse/volvocars/acu/model/AppInfo;
    invoke-virtual {v1}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.parrot.mediaList:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.parrot.mediaplayer:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 652
    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 659
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "info":Lse/volvocars/acu/model/AppInfo;
    .end local v2    # "toRemove":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 656
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "toRemove":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    :cond_2
    :try_start_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/model/AppInfo;

    .line 657
    .restart local v1    # "info":Lse/volvocars/acu/model/AppInfo;
    invoke-virtual {p0, v1}, Lse/volvocars/acu/model/ApplicationModel;->removeAppInfo(Lse/volvocars/acu/model/AppInfo;)V

    goto :goto_1

    .line 659
    .end local v1    # "info":Lse/volvocars/acu/model/AppInfo;
    :cond_3
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 660
    return-void
.end method

.method public removeAppInfo(Lse/volvocars/acu/model/AppInfo;)V
    .locals 5
    .param p1, "app"    # Lse/volvocars/acu/model/AppInfo;

    .prologue
    const-string v1, "LauncherApplicationModel"

    .line 594
    iget-object v1, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoLock:Ljava/lang/Object;

    monitor-enter v1

    .line 595
    if-nez p1, :cond_0

    .line 596
    :try_start_0
    const-string v2, "LauncherApplicationModel"

    const-string v3, "tried to remove app that is null."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    monitor-exit v1

    .line 606
    :goto_0
    return-void

    .line 599
    :cond_0
    iget-object v2, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfo:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 601
    :try_start_1
    iget-object v2, p0, Lse/volvocars/acu/model/ApplicationModel;->mDatabase:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {p1}, Lse/volvocars/acu/model/AppInfo;->getDatabaseId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lse/volvocars/acu/db/ApplicationsDatabase;->removeApplication(J)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 605
    :goto_1
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 602
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 603
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    const-string v2, "LauncherApplicationModel"

    const-string v3, "Tried to remove an app that isn\'t in the database, but is in the model."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public removeAppInfoChangeListener(Lse/volvocars/acu/model/AppInfoChangeListener;)V
    .locals 2
    .param p1, "listener"    # Lse/volvocars/acu/model/AppInfoChangeListener;

    .prologue
    .line 471
    iget-object v0, p0, Lse/volvocars/acu/model/ApplicationModel;->mAppInfoChangedListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    const-string v0, "LauncherApplicationModel"

    const-string v1, "Couldn\'t remove the appInfoChangeListener as it wasn\'t registered."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    :cond_0
    return-void
.end method
