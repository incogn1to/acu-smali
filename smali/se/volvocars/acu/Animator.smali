.class public Lse/volvocars/acu/Animator;
.super Ljava/lang/Object;
.source "Animator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/Animator$1;,
        Lse/volvocars/acu/Animator$ThreadedTask;,
        Lse/volvocars/acu/Animator$Animation;
    }
.end annotation


# static fields
.field public static final STEP:D = 0.03999999910593033


# instance fields
.field private animations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/Animator$Animation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/Animator;->animations:Ljava/util/List;

    .line 141
    return-void
.end method

.method static synthetic access$100(Lse/volvocars/acu/Animator;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/Animator;

    .prologue
    .line 20
    iget-object v0, p0, Lse/volvocars/acu/Animator;->animations:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public animate(Lse/volvocars/acu/Animatable;D)V
    .locals 6
    .param p1, "component"    # Lse/volvocars/acu/Animatable;
    .param p2, "endValue"    # D

    .prologue
    .line 37
    const-wide v4, 0x3fa47ae140000000L    # 0.03999999910593033

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    invoke-virtual/range {v0 .. v5}, Lse/volvocars/acu/Animator;->animate(Lse/volvocars/acu/Animatable;DD)V

    .line 38
    return-void
.end method

.method public animate(Lse/volvocars/acu/Animatable;DD)V
    .locals 10
    .param p1, "component"    # Lse/volvocars/acu/Animatable;
    .param p2, "endValue"    # D
    .param p4, "step"    # D

    .prologue
    const/4 v9, 0x1

    .line 48
    iget-object v7, p0, Lse/volvocars/acu/Animator;->animations:Ljava/util/List;

    monitor-enter v7

    .line 49
    :try_start_0
    iget-object v0, p0, Lse/volvocars/acu/Animator;->animations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 51
    .local v6, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lse/volvocars/acu/Animator$Animation;>;"
    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/Animator$Animation;

    invoke-virtual {v0}, Lse/volvocars/acu/Animator$Animation;->getComponent()Lse/volvocars/acu/Animatable;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 53
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    .line 58
    :cond_1
    iget-object v8, p0, Lse/volvocars/acu/Animator;->animations:Ljava/util/List;

    new-instance v0, Lse/volvocars/acu/Animator$Animation;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/Animator$Animation;-><init>(Lse/volvocars/acu/Animatable;DD)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    iget-object v0, p0, Lse/volvocars/acu/Animator;->animations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v9, :cond_2

    .line 61
    new-instance v0, Lse/volvocars/acu/Animator$ThreadedTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lse/volvocars/acu/Animator$ThreadedTask;-><init>(Lse/volvocars/acu/Animator;Lse/volvocars/acu/Animator$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lse/volvocars/acu/Animator$ThreadedTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 63
    :cond_2
    monitor-exit v7

    .line 64
    return-void

    .line 63
    .end local v6    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lse/volvocars/acu/Animator$Animation;>;"
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
