.class final Lse/volvocars/acu/Animator$Animation;
.super Ljava/lang/Object;
.source "Animator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Animation"
.end annotation


# instance fields
.field private mComponent:Lse/volvocars/acu/Animatable;

.field private mEndValue:D

.field private final mInterpolator:Landroid/view/animation/Interpolator;

.field private mNextValue:D

.field private mProgress:F

.field private mStartValue:D

.field private mStep:D


# direct methods
.method public constructor <init>(Lse/volvocars/acu/Animatable;D)V
    .locals 3
    .param p1, "component"    # Lse/volvocars/acu/Animatable;
    .param p2, "endValue"    # D

    .prologue
    const/4 v2, 0x0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput v2, p0, Lse/volvocars/acu/Animator$Animation;->mProgress:F

    .line 75
    const-wide v0, 0x3fa47ae140000000L    # 0.03999999910593033

    iput-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mStep:D

    .line 77
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    invoke-virtual {v0}, Landroid/view/animation/TranslateAnimation;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/Animator$Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 116
    iput-object p1, p0, Lse/volvocars/acu/Animator$Animation;->mComponent:Lse/volvocars/acu/Animatable;

    .line 117
    invoke-interface {p1}, Lse/volvocars/acu/Animatable;->getValue()D

    move-result-wide v0

    iput-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mStartValue:D

    .line 118
    iput-wide p2, p0, Lse/volvocars/acu/Animator$Animation;->mEndValue:D

    .line 119
    iget-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mStartValue:D

    iput-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mNextValue:D

    .line 120
    return-void
.end method

.method public constructor <init>(Lse/volvocars/acu/Animatable;DD)V
    .locals 3
    .param p1, "component"    # Lse/volvocars/acu/Animatable;
    .param p2, "endValue"    # D
    .param p4, "step"    # D

    .prologue
    const/4 v2, 0x0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput v2, p0, Lse/volvocars/acu/Animator$Animation;->mProgress:F

    .line 75
    const-wide v0, 0x3fa47ae140000000L    # 0.03999999910593033

    iput-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mStep:D

    .line 77
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v2, v2, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    invoke-virtual {v0}, Landroid/view/animation/TranslateAnimation;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/Animator$Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 129
    iput-object p1, p0, Lse/volvocars/acu/Animator$Animation;->mComponent:Lse/volvocars/acu/Animatable;

    .line 130
    invoke-interface {p1}, Lse/volvocars/acu/Animatable;->getValue()D

    move-result-wide v0

    iput-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mStartValue:D

    .line 131
    iput-wide p2, p0, Lse/volvocars/acu/Animator$Animation;->mEndValue:D

    .line 132
    iput-wide p4, p0, Lse/volvocars/acu/Animator$Animation;->mStep:D

    .line 133
    iget-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mStartValue:D

    iput-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mNextValue:D

    .line 134
    return-void
.end method

.method static synthetic access$200(Lse/volvocars/acu/Animator$Animation;)F
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/Animator$Animation;

    .prologue
    .line 70
    iget v0, p0, Lse/volvocars/acu/Animator$Animation;->mProgress:F

    return v0
.end method


# virtual methods
.method public getComponent()Lse/volvocars/acu/Animatable;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lse/volvocars/acu/Animator$Animation;->mComponent:Lse/volvocars/acu/Animatable;

    return-object v0
.end method

.method public getEndValue()D
    .locals 2

    .prologue
    .line 88
    iget-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mEndValue:D

    return-wide v0
.end method

.method public getInterpolator()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lse/volvocars/acu/Animator$Animation;->mInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method public getNextValue()D
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mNextValue:D

    return-wide v0
.end method

.method public getProgress()F
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lse/volvocars/acu/Animator$Animation;->mProgress:F

    return v0
.end method

.method public getStartValue()D
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mStartValue:D

    return-wide v0
.end method

.method public getStep()D
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lse/volvocars/acu/Animator$Animation;->mStep:D

    return-wide v0
.end method

.method public setNextValue(D)V
    .locals 0
    .param p1, "nextValue"    # D

    .prologue
    .line 96
    iput-wide p1, p0, Lse/volvocars/acu/Animator$Animation;->mNextValue:D

    .line 97
    return-void
.end method

.method public setProgress(F)V
    .locals 0
    .param p1, "progress"    # F

    .prologue
    .line 104
    iput p1, p0, Lse/volvocars/acu/Animator$Animation;->mProgress:F

    .line 105
    return-void
.end method
