.class public Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
.super Ljava/lang/Object;
.source "FactorySettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/gsonparser/FactorySettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AppFactorySettings"
.end annotation


# instance fields
.field private appIconId:Ljava/lang/String;

.field private appName:Ljava/lang/String;

.field private enabled:Z

.field private launchMode:Ljava/lang/String;

.field private packageName:Ljava/lang/String;

.field private startUpFlags:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-string v1, ""

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-string v0, ""

    iput-object v1, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->appName:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v1, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->packageName:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->enabled:Z

    return-void
.end method


# virtual methods
.method public getAppIconId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->appIconId:Ljava/lang/String;

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public getLaunchMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->launchMode:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getStartUpFlags()[I
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->startUpFlags:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "this":Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    check-cast p0, [I

    return-object p0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->enabled:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "title: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->appName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", packageName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", appIconId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->appIconId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startUpFlags: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->startUpFlags:[I

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", launchMode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->launchMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enabled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
