.class public Lse/volvocars/acu/gsonparser/FactorySettings;
.super Ljava/lang/Object;
.source "FactorySettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    }
.end annotation


# instance fields
.field private factorySettings:[Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method


# virtual methods
.method public getFactorySettings()[Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lse/volvocars/acu/gsonparser/FactorySettings;->factorySettings:[Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;

    invoke-virtual {v0}, [Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;->clone()Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "this":Lse/volvocars/acu/gsonparser/FactorySettings;
    check-cast p0, [Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "factorySettings: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/gsonparser/FactorySettings;->factorySettings:[Lse/volvocars/acu/gsonparser/FactorySettings$AppFactorySettings;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
