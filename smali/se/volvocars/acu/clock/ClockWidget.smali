.class public Lse/volvocars/acu/clock/ClockWidget;
.super Landroid/widget/LinearLayout;
.source "ClockWidget.java"


# static fields
.field private static final AM_PM_STYLE:I = 0x2

.field private static final AM_PM_STYLE_GONE:I = 0x2

.field private static final AM_PM_STYLE_NORMAL:I = 0x0

.field private static final AM_PM_STYLE_SMALL:I = 0x1

.field protected static final TAG:Ljava/lang/String; = "ClockWidget"

.field private static TIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;


# instance fields
.field private mClockFormat:Ljava/text/SimpleDateFormat;

.field private mClockFormatString:Ljava/lang/String;

.field private final mTextGrayColor:I

.field private final mTextWhiteColor:I

.field private final mTimeChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mTimeFormat:Landroid/widget/TextView;

.field private mTimeView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lse/volvocars/acu/clock/ClockWidget;->TIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    .line 35
    sget-object v0, Lse/volvocars/acu/clock/ClockWidget;->TIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 36
    sget-object v0, Lse/volvocars/acu/clock/ClockWidget;->TIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 37
    sget-object v0, Lse/volvocars/acu/clock/ClockWidget;->TIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    new-instance v0, Lse/volvocars/acu/clock/ClockWidget$1;

    invoke-direct {v0, p0}, Lse/volvocars/acu/clock/ClockWidget$1;-><init>(Lse/volvocars/acu/clock/ClockWidget;)V

    iput-object v0, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 65
    invoke-virtual {p0}, Lse/volvocars/acu/clock/ClockWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/clock/ClockWidget;->mTextWhiteColor:I

    .line 67
    invoke-virtual {p0}, Lse/volvocars/acu/clock/ClockWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/clock/ClockWidget;->mTextGrayColor:I

    .line 68
    return-void
.end method

.method private final getSmallTime(Ljava/util/Date;)Ljava/lang/CharSequence;
    .locals 21
    .param p1, "now"    # Ljava/util/Date;

    .prologue
    .line 159
    invoke-virtual/range {p0 .. p0}, Lse/volvocars/acu/clock/ClockWidget;->getContext()Landroid/content/Context;

    move-result-object v9

    .line 160
    .local v9, "context":Landroid/content/Context;
    invoke-static {v9}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v7

    .line 163
    .local v7, "b24":Z
    if-eqz v7, :cond_2

    .line 164
    const v16, 0x1040077

    .line 169
    .local v16, "res":I
    :goto_0
    const v3, 0xef00

    .line 170
    .local v3, "MAGIC1":C
    const v4, 0xef01

    .line 173
    .local v4, "MAGIC2":C
    move-object v0, v9

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 174
    .local v10, "format":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/clock/ClockWidget;->mClockFormatString:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object v0, v10

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_7

    .line 181
    const/4 v5, -0x1

    .line 182
    .local v5, "a":I
    const/4 v15, 0x0

    .line 183
    .local v15, "quoted":Z
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v19

    move v0, v12

    move/from16 v1, v19

    if-ge v0, v1, :cond_1

    .line 184
    invoke-virtual {v10, v12}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 186
    .local v8, "c":C
    const/16 v19, 0x27

    move v0, v8

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 187
    if-nez v15, :cond_3

    const/16 v19, 0x1

    move/from16 v15, v19

    .line 189
    :cond_0
    :goto_2
    if-nez v15, :cond_4

    const/16 v19, 0x61

    move v0, v8

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 190
    move v5, v12

    .line 195
    .end local v8    # "c":C
    :cond_1
    if-ltz v5, :cond_6

    .line 197
    move v6, v5

    .line 198
    .local v6, "b":I
    :goto_3
    if-lez v5, :cond_5

    const/16 v19, 0x1

    sub-int v19, v5, v19

    move-object v0, v10

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 199
    add-int/lit8 v5, v5, -0x1

    goto :goto_3

    .line 166
    .end local v3    # "MAGIC1":C
    .end local v4    # "MAGIC2":C
    .end local v5    # "a":I
    .end local v6    # "b":I
    .end local v10    # "format":Ljava/lang/String;
    .end local v12    # "i":I
    .end local v15    # "quoted":Z
    .end local v16    # "res":I
    :cond_2
    const v16, 0x1040076

    .restart local v16    # "res":I
    goto :goto_0

    .line 187
    .restart local v3    # "MAGIC1":C
    .restart local v4    # "MAGIC2":C
    .restart local v5    # "a":I
    .restart local v8    # "c":C
    .restart local v10    # "format":Ljava/lang/String;
    .restart local v12    # "i":I
    .restart local v15    # "quoted":Z
    :cond_3
    const/16 v19, 0x0

    move/from16 v15, v19

    goto :goto_2

    .line 183
    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 201
    .end local v8    # "c":C
    .restart local v6    # "b":I
    :cond_5
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v20, 0x0

    move-object v0, v10

    move/from16 v1, v20

    move v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const v20, 0xef00

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v10, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "a"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const v20, 0xef01

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v19

    add-int/lit8 v20, v6, 0x1

    move-object v0, v10

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 206
    .end local v6    # "b":I
    :cond_6
    new-instance v18, Ljava/text/SimpleDateFormat;

    move-object/from16 v0, v18

    move-object v1, v10

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .local v18, "sdf":Ljava/text/SimpleDateFormat;
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/clock/ClockWidget;->mClockFormat:Ljava/text/SimpleDateFormat;

    .line 207
    move-object v0, v10

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/clock/ClockWidget;->mClockFormatString:Ljava/lang/String;

    .line 211
    .end local v5    # "a":I
    .end local v12    # "i":I
    .end local v15    # "quoted":Z
    :goto_4
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v17

    .line 214
    .local v17, "result":Ljava/lang/String;
    const v19, 0xef00

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v13

    .line 215
    .local v13, "magic1":I
    const v19, 0xef01

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v14

    .line 216
    .local v14, "magic2":I
    if-ltz v13, :cond_8

    if-le v14, v13, :cond_8

    .line 217
    new-instance v11, Landroid/text/SpannableStringBuilder;

    move-object v0, v11

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 219
    .local v11, "formatted":Landroid/text/SpannableStringBuilder;
    add-int/lit8 v19, v14, 0x1

    move-object v0, v11

    move v1, v13

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    move-object/from16 v19, v11

    .line 232
    .end local v11    # "formatted":Landroid/text/SpannableStringBuilder;
    :goto_5
    return-object v19

    .line 209
    .end local v13    # "magic1":I
    .end local v14    # "magic2":I
    .end local v17    # "result":Ljava/lang/String;
    .end local v18    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/clock/ClockWidget;->mClockFormat:Ljava/text/SimpleDateFormat;

    move-object/from16 v18, v0

    .restart local v18    # "sdf":Ljava/text/SimpleDateFormat;
    goto :goto_4

    .restart local v13    # "magic1":I
    .restart local v14    # "magic2":I
    .restart local v17    # "result":Ljava/lang/String;
    :cond_8
    move-object/from16 v19, v17

    .line 232
    goto :goto_5
.end method

.method private setTime()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 121
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 123
    .local v0, "now":Ljava/util/Date;
    invoke-virtual {p0}, Lse/volvocars/acu/clock/ClockWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeFormat:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    :goto_0
    iget-object v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lse/volvocars/acu/clock/ClockWidget;->getSmallTime(Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    const/16 v1, 0x3c

    invoke-virtual {v0}, Ljava/util/Date;->getSeconds()I

    move-result v2

    sub-int/2addr v1, v2

    return v1

    .line 126
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeFormat:Landroid/widget/TextView;

    const-string v2, "%Tp"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeFormat:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public forceClockUpdate()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lse/volvocars/acu/clock/ClockWidget;->setTime()I

    .line 114
    return-void
.end method

.method public isInIdleMode()Z
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iget v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTextWhiteColor:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeFormat:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iget v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTextGrayColor:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 72
    const v1, 0x7f070011

    invoke-virtual {p0, v1}, Lse/volvocars/acu/clock/ClockWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeView:Landroid/widget/TextView;

    .line 73
    const v1, 0x7f070012

    invoke-virtual {p0, v1}, Lse/volvocars/acu/clock/ClockWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeFormat:Landroid/widget/TextView;

    .line 75
    invoke-virtual {p0}, Lse/volvocars/acu/clock/ClockWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "fonts/VolvoSanProLig.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 77
    .local v0, "typeface":Landroid/graphics/Typeface;
    iget-object v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeFormat:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 78
    iget-object v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 79
    invoke-virtual {p0}, Lse/volvocars/acu/clock/ClockWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeFormat:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p0}, Lse/volvocars/acu/clock/ClockWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 97
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 104
    invoke-virtual {p0}, Lse/volvocars/acu/clock/ClockWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeChangedReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Lse/volvocars/acu/clock/ClockWidget;->TIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 105
    invoke-virtual {p0}, Lse/volvocars/acu/clock/ClockWidget;->forceClockUpdate()V

    .line 106
    return-void
.end method

.method public setIdleMode(Z)V
    .locals 2
    .param p1, "idle"    # Z

    .prologue
    .line 141
    if-eqz p1, :cond_0

    .line 143
    iget-object v0, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeView:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTextWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 144
    iget-object v0, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeFormat:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTextGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 151
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeView:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTextWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 149
    iget-object v0, p0, Lse/volvocars/acu/clock/ClockWidget;->mTimeFormat:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/clock/ClockWidget;->mTextGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
