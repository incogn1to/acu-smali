.class public Lse/volvocars/acu/HomeButtonReceiver;
.super Ljava/lang/Object;
.source "HomeButtonReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;
    }
.end annotation


# static fields
.field private static mHomeButtonListener:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public static getHomebuttonListener()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    sget-object v0, Lse/volvocars/acu/HomeButtonReceiver;->mHomeButtonListener:Ljava/util/List;

    return-object v0
.end method

.method public static onReceive()V
    .locals 4

    .prologue
    .line 41
    invoke-static {}, Lse/volvocars/acu/HomeButtonReceiver;->getHomebuttonListener()Ljava/util/List;

    move-result-object v2

    .line 42
    .local v2, "listeners":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;>;"
    if-eqz v2, :cond_0

    .line 43
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;

    .line 44
    .local v1, "listener":Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;
    invoke-interface {v1}, Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;->onHomeButtonPress()V

    goto :goto_0

    .line 47
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;
    :cond_0
    return-void
.end method

.method public static removeHomeButtonListener(Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;)V
    .locals 1
    .param p0, "listener"    # Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;

    .prologue
    .line 35
    sget-object v0, Lse/volvocars/acu/HomeButtonReceiver;->mHomeButtonListener:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 36
    sget-object v0, Lse/volvocars/acu/HomeButtonReceiver;->mHomeButtonListener:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 38
    :cond_0
    return-void
.end method

.method public static setHomeButtonListener(Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;)V
    .locals 1
    .param p0, "listener"    # Lse/volvocars/acu/HomeButtonReceiver$HomeButtonListener;

    .prologue
    .line 24
    sget-object v0, Lse/volvocars/acu/HomeButtonReceiver;->mHomeButtonListener:Ljava/util/List;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lse/volvocars/acu/HomeButtonReceiver;->mHomeButtonListener:Ljava/util/List;

    .line 27
    :cond_0
    sget-object v0, Lse/volvocars/acu/HomeButtonReceiver;->mHomeButtonListener:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method
