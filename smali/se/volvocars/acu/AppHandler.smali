.class public final Lse/volvocars/acu/AppHandler;
.super Ljava/lang/Object;
.source "AppHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/AppHandler$WorkloadHandler;,
        Lse/volvocars/acu/AppHandler$AppIconOrderUpdater;
    }
.end annotation


# static fields
.field private static final DEFAULT_ICON:I = 0x0

.field private static final DEFAULT_REORDER_APPS:Z = false

.field private static final DELAY_AFTER_START_APP:I = 0x3e8

.field private static final DELAY_BEFORE_START_APP:I = 0x15e

.field private static final DELAY_WAIT_FOR_SCROLL:I = 0x226

.field public static final HITBOX_CENTER:I = 0x1

.field public static final HITBOX_LEFT:I = 0x0

.field public static final HITBOX_RIGHT:I = 0x2

.field private static final KEY_APP_FOCUS:Ljava/lang/String; = "focus_app"

.field public static final KEY_REORDER:Ljava/lang/String; = "reorder_apps"

.field public static final PREFERENCE_APPS:Ljava/lang/String; = "se.volvocars.acu.apps"

.field private static final TAG:Ljava/lang/String; = "AppHandler"

.field private static final TIME_THRESHOLD_FOR_DPAD_TO_HOME:I = 0x320


# instance fields
.field private final mActivity:Lse/volvocars/acu/AcuActivity;

.field private mAllowDpadCross:Z

.field private final mAppName:Lse/volvocars/acu/ui/AppNameView;

.field private final mApps:Lse/volvocars/acu/model/Apps;

.field private final mAppsLock:Ljava/lang/Object;

.field private final mDpadTimerHandler:Landroid/os/Handler;

.field private final mDpadTimerRunnable:Ljava/lang/Runnable;

.field private mDriverWlActive:Z

.field private mDriverWlWhitelist:[Ljava/lang/String;

.field private mFocusIcon:I

.field private final mHandler:Landroid/os/Handler;

.field private final mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

.field private mReorderApps:Z


# direct methods
.method public constructor <init>(Lse/volvocars/acu/AcuActivity;Lse/volvocars/acu/ui/opengl/LauncherGLView;Lse/volvocars/acu/ui/AppNameView;Z)V
    .locals 3
    .param p1, "activity"    # Lse/volvocars/acu/AcuActivity;
    .param p2, "launcher"    # Lse/volvocars/acu/ui/opengl/LauncherGLView;
    .param p3, "nameView"    # Lse/volvocars/acu/ui/AppNameView;
    .param p4, "justBooted"    # Z

    .prologue
    const/4 v2, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lse/volvocars/acu/AppHandler;->mAllowDpadCross:Z

    .line 75
    new-instance v0, Lse/volvocars/acu/AppHandler$1;

    invoke-direct {v0, p0}, Lse/volvocars/acu/AppHandler$1;-><init>(Lse/volvocars/acu/AppHandler;)V

    iput-object v0, p0, Lse/volvocars/acu/AppHandler;->mDpadTimerRunnable:Ljava/lang/Runnable;

    .line 91
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/AppHandler;->mHandler:Landroid/os/Handler;

    .line 92
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/AppHandler;->mDpadTimerHandler:Landroid/os/Handler;

    .line 93
    iput-object p1, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    .line 94
    iput-object p2, p0, Lse/volvocars/acu/AppHandler;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    .line 95
    iput-object p3, p0, Lse/volvocars/acu/AppHandler;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    .line 96
    new-instance v0, Lse/volvocars/acu/model/Apps;

    invoke-virtual {p2}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p4}, Lse/volvocars/acu/model/Apps;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    .line 97
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    new-instance v1, Lse/volvocars/acu/AppHandler$AppIconOrderUpdater;

    invoke-direct {v1, p0, v2}, Lse/volvocars/acu/AppHandler$AppIconOrderUpdater;-><init>(Lse/volvocars/acu/AppHandler;Lse/volvocars/acu/AppHandler$1;)V

    invoke-virtual {v0, v1}, Lse/volvocars/acu/model/Apps;->addAppInfoChangeListener(Lse/volvocars/acu/model/AppInfoChangeListener;)V

    .line 98
    new-instance v0, Lcom/parrot/asteroid/media/common/MediaPlayerControler;

    invoke-virtual {p2}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/parrot/asteroid/media/common/MediaPlayerControler;-><init>(Landroid/content/Context;)V

    .line 99
    new-instance v0, Lse/volvocars/acu/AppHandler$WorkloadHandler;

    invoke-direct {v0, p0, v2}, Lse/volvocars/acu/AppHandler$WorkloadHandler;-><init>(Lse/volvocars/acu/AppHandler;Lse/volvocars/acu/AppHandler$1;)V

    invoke-static {v0}, Lse/volvocars/acu/DriverWorkloadReceiver;->setWorkloadListener(Lse/volvocars/acu/DriverWorkloadReceiver$WorkloadListener;)V

    .line 101
    invoke-virtual {p0}, Lse/volvocars/acu/AppHandler;->loadSettings()V

    .line 102
    invoke-direct {p0}, Lse/volvocars/acu/AppHandler;->loadFocusApp()V

    .line 103
    return-void
.end method

.method static synthetic access$002(Lse/volvocars/acu/AppHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/AppHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lse/volvocars/acu/AppHandler;->mAllowDpadCross:Z

    return p1
.end method

.method static synthetic access$1002(Lse/volvocars/acu/AppHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/AppHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lse/volvocars/acu/AppHandler;->mDriverWlActive:Z

    return p1
.end method

.method static synthetic access$1100(Lse/volvocars/acu/AppHandler;)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/AppHandler;

    .prologue
    .line 33
    invoke-direct {p0}, Lse/volvocars/acu/AppHandler;->updateForbiddenIcons()V

    return-void
.end method

.method static synthetic access$300(Lse/volvocars/acu/AppHandler;)Lse/volvocars/acu/ui/opengl/LauncherGLView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/AppHandler;

    .prologue
    .line 33
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    return-object v0
.end method

.method static synthetic access$400(Lse/volvocars/acu/AppHandler;I)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/AppHandler;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lse/volvocars/acu/AppHandler;->startApplication(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lse/volvocars/acu/AppHandler;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/AppHandler;

    .prologue
    .line 33
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$600(Lse/volvocars/acu/AppHandler;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/AppHandler;

    .prologue
    .line 33
    iget-boolean v0, p0, Lse/volvocars/acu/AppHandler;->mReorderApps:Z

    return v0
.end method

.method static synthetic access$700(Lse/volvocars/acu/AppHandler;)Lse/volvocars/acu/model/Apps;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/AppHandler;

    .prologue
    .line 33
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    return-object v0
.end method

.method static synthetic access$800(Lse/volvocars/acu/AppHandler;)Lse/volvocars/acu/AcuActivity;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/AppHandler;

    .prologue
    .line 33
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    return-object v0
.end method

.method static synthetic access$902(Lse/volvocars/acu/AppHandler;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/AppHandler;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lse/volvocars/acu/AppHandler;->mDriverWlWhitelist:[Ljava/lang/String;

    return-object p1
.end method

.method private allowAllApps()V
    .locals 5

    .prologue
    .line 451
    const/4 v1, 0x0

    .line 452
    .local v1, "i":I
    iget-object v4, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    invoke-virtual {v4}, Lse/volvocars/acu/model/Apps;->getApps()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lse/volvocars/acu/model/AppInfo;

    .line 453
    .local v3, "info":Lse/volvocars/acu/model/AppInfo;
    invoke-virtual {v3}, Lse/volvocars/acu/model/AppInfo;->isHidden()Z

    move-result v4

    if-nez v4, :cond_0

    .line 454
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 457
    .end local v3    # "info":Lse/volvocars/acu/model/AppInfo;
    :cond_1
    new-array v0, v1, [I

    .line 458
    .local v0, "enabledArray":[I
    iget-object v4, p0, Lse/volvocars/acu/AppHandler;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v4, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setAppForbiddenStatus([I)V

    .line 459
    iget-object v4, p0, Lse/volvocars/acu/AppHandler;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    invoke-virtual {v4, v0}, Lse/volvocars/acu/ui/AppNameView;->setForbiddenArray([I)V

    .line 460
    return-void
.end method

.method private allowOnlyWhitelistedApps()V
    .locals 9

    .prologue
    .line 433
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 434
    .local v1, "forbiddenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v8, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    invoke-virtual {v8}, Lse/volvocars/acu/model/Apps;->getApps()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lse/volvocars/acu/model/AppInfo;

    .line 435
    .local v5, "info":Lse/volvocars/acu/model/AppInfo;
    invoke-virtual {v5}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v6

    .line 436
    .local v6, "pkg":Ljava/lang/String;
    invoke-direct {p0, v6}, Lse/volvocars/acu/AppHandler;->onWhitelist(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v8, 0x0

    :goto_1
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v8, 0x1

    goto :goto_1

    .line 438
    .end local v5    # "info":Lse/volvocars/acu/model/AppInfo;
    .end local v6    # "pkg":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v0, v8, [I

    .line 439
    .local v0, "forbiddenArray":[I
    const/4 v2, 0x0

    .line 440
    .local v2, "i":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 441
    .local v7, "val":Ljava/lang/Integer;
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v0, v2

    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_2

    .line 443
    .end local v7    # "val":Ljava/lang/Integer;
    :cond_2
    iget-object v8, p0, Lse/volvocars/acu/AppHandler;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v8, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setAppForbiddenStatus([I)V

    .line 444
    iget-object v8, p0, Lse/volvocars/acu/AppHandler;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    invoke-virtual {v8, v0}, Lse/volvocars/acu/ui/AppNameView;->setForbiddenArray([I)V

    .line 445
    return-void
.end method

.method private findSelectedApplicationIndex(I)I
    .locals 2
    .param p1, "hitbox"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    monitor-enter v0

    .line 171
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v1}, Lse/volvocars/acu/AcuActivity;->getCurrentPos()I

    move-result v1

    invoke-virtual {p0, p1, v1}, Lse/volvocars/acu/AppHandler;->findSelectedAppAtScreenPosition(II)I

    move-result v1

    monitor-exit v0

    return v1

    .line 172
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getIconTargetPosition(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 585
    mul-int/lit16 v0, p1, 0x12c

    const/16 v1, 0x170

    sub-int/2addr v0, v1

    return v0
.end method

.method private indicateAppBeingStarted(Ljava/util/List;Lse/volvocars/acu/model/AppInfo;I)V
    .locals 4
    .param p2, "app"    # Lse/volvocars/acu/model/AppInfo;
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/model/AppInfo;",
            ">;",
            "Lse/volvocars/acu/model/AppInfo;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 323
    .local p1, "apps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mHandler:Landroid/os/Handler;

    new-instance v1, Lse/volvocars/acu/AppHandler$4;

    invoke-direct {v1, p0, p2, p1, p3}, Lse/volvocars/acu/AppHandler$4;-><init>(Lse/volvocars/acu/AppHandler;Lse/volvocars/acu/model/AppInfo;Ljava/util/List;I)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 345
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/AcuActivity;->setStartOnLauncher(Z)V

    .line 347
    return-void
.end method

.method private isAppStartable(Landroid/content/Intent;Lse/volvocars/acu/model/LaunchMode;)Z
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "mode"    # Lse/volvocars/acu/model/LaunchMode;

    .prologue
    .line 255
    move-object v2, p1

    .line 256
    .local v2, "target":Landroid/content/Intent;
    sget-object v3, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_MEDIA_PLAYER:Lse/volvocars/acu/model/LaunchMode;

    if-ne p2, v3, :cond_1

    .line 257
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "target":Landroid/content/Intent;
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 258
    .restart local v2    # "target":Landroid/content/Intent;
    const-string v3, "com.parrot.mediaplayer"

    const-string v4, "com.parrot.mediaplayer.MediaPlayerActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    :cond_0
    :goto_0
    iget-object v3, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v3}, Lse/volvocars/acu/AcuActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 265
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    const/high16 v3, 0x10000

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 268
    .local v1, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 269
    const/4 v3, 0x1

    .line 272
    :goto_1
    return v3

    .line 259
    .end local v0    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v1    # "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_1
    sget-object v3, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_MEDIA_LIST:Lse/volvocars/acu/model/LaunchMode;

    if-ne p2, v3, :cond_0

    .line 260
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "target":Landroid/content/Intent;
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 261
    .restart local v2    # "target":Landroid/content/Intent;
    const-string v3, "com.parrot.mediaList"

    const-string v4, "com.parrot.mediaList.MediaListApp"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 271
    .restart local v0    # "packageManager":Landroid/content/pm/PackageManager;
    .restart local v1    # "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_2
    const-string v3, "AppHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "App with intent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " could not be found on system."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private loadFocusApp()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 116
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    const-string v2, "se.volvocars.acu.apps"

    invoke-virtual {v1, v2, v3}, Lse/volvocars/acu/AcuActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 117
    .local v0, "settings":Landroid/content/SharedPreferences;
    const-string v1, "focus_app"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lse/volvocars/acu/AppHandler;->mFocusIcon:I

    .line 118
    iget v1, p0, Lse/volvocars/acu/AppHandler;->mFocusIcon:I

    invoke-direct {p0, v1}, Lse/volvocars/acu/AppHandler;->moveToIcon(I)V

    .line 119
    return-void
.end method

.method private moveToIcon(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 574
    invoke-direct {p0, p1}, Lse/volvocars/acu/AppHandler;->getIconTargetPosition(I)I

    move-result v0

    .line 575
    .local v0, "targetPosition":I
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v1, v0}, Lse/volvocars/acu/AcuActivity;->setCurrentPos(I)V

    .line 577
    return-void
.end method

.method private onWhitelist(Ljava/lang/String;)Z
    .locals 8
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    const-string v6, ":"

    .line 595
    iget-object v5, p0, Lse/volvocars/acu/AppHandler;->mDriverWlWhitelist:[Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 596
    move-object v3, p1

    .line 597
    .local v3, "packageName":Ljava/lang/String;
    const-string v5, ":"

    invoke-virtual {p1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 598
    const-string v5, ":"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v3, v5, v7

    .line 600
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mDriverWlWhitelist:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_3

    aget-object v4, v0, v1

    .line 601
    .local v4, "string":Ljava/lang/String;
    if-eqz v4, :cond_1

    invoke-virtual {v4, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 603
    const/4 v5, 0x1

    .line 612
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "string":Ljava/lang/String;
    :goto_1
    return v5

    .line 600
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "packageName":Ljava/lang/String;
    .restart local v4    # "string":Ljava/lang/String;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 609
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "string":Ljava/lang/String;
    :cond_2
    const-string v5, "AppHandler"

    const-string v6, "Whitelist is null"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move v5, v7

    .line 612
    goto :goto_1
.end method

.method private scrollToIcon(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 563
    invoke-direct {p0, p1}, Lse/volvocars/acu/AppHandler;->getIconTargetPosition(I)I

    move-result v0

    .line 564
    .local v0, "targetPosition":I
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v1, v0}, Lse/volvocars/acu/AcuActivity;->scrollToPos(I)V

    .line 565
    return-void
.end method

.method private startApp(Lse/volvocars/acu/model/AppInfo;)Z
    .locals 1
    .param p1, "app"    # Lse/volvocars/acu/model/AppInfo;

    .prologue
    .line 355
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lse/volvocars/acu/AppHandler;->startApp(Lse/volvocars/acu/model/AppInfo;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static startApp(Lse/volvocars/acu/model/AppInfo;Landroid/content/Context;)Z
    .locals 10
    .param p0, "app"    # Lse/volvocars/acu/model/AppInfo;
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/high16 v9, 0x20000000

    const/high16 v8, 0x10000000

    const/high16 v7, 0x20000

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 358
    sget-object v3, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_INTENT:Lse/volvocars/acu/model/LaunchMode;

    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getLaunchMode()Lse/volvocars/acu/model/LaunchMode;

    move-result-object v4

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getStartIntent()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 361
    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v3

    const-string v4, "_appDrawer_"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 362
    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getStartIntent()Landroid/content/Intent;

    move-result-object v1

    .line 363
    .local v1, "newintent":Landroid/content/Intent;
    const/high16 v3, 0x800000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 364
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .end local v1    # "newintent":Landroid/content/Intent;
    :goto_0
    move v3, v5

    .line 401
    :goto_1
    return v3

    .line 366
    :cond_0
    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getStartIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 371
    :cond_1
    sget-object v3, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_MEDIA_LIST:Lse/volvocars/acu/model/LaunchMode;

    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getLaunchMode()Lse/volvocars/acu/model/LaunchMode;

    move-result-object v4

    if-ne v3, v4, :cond_2

    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getMediaSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 372
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.parrot.mediaList"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 373
    .restart local v1    # "newintent":Landroid/content/Intent;
    const-string v3, "source"

    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getMediaSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 374
    const-string v3, "android.intent.category.DEFAULT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 375
    invoke-virtual {v1, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 376
    invoke-virtual {v1, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 377
    invoke-virtual {v1, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 378
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move v3, v5

    .line 379
    goto :goto_1

    .line 381
    .end local v1    # "newintent":Landroid/content/Intent;
    :cond_2
    sget-object v3, Lse/volvocars/acu/model/LaunchMode;->LAUNCH_WITH_MEDIA_PLAYER:Lse/volvocars/acu/model/LaunchMode;

    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getLaunchMode()Lse/volvocars/acu/model/LaunchMode;

    move-result-object v4

    if-ne v3, v4, :cond_4

    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getMediaSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 382
    invoke-static {p1}, Lcom/parrot/asteroid/media/MediaManagerFactory;->getMediaManager(Landroid/content/Context;)Lcom/parrot/asteroid/media/MediaManager;

    move-result-object v0

    .line 383
    .local v0, "mediaManager":Lcom/parrot/asteroid/media/MediaManager;
    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getMediaSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v2

    .line 384
    .local v2, "source":Lcom/parrot/asteroid/audio/service/Source;
    invoke-virtual {v0}, Lcom/parrot/asteroid/media/MediaManager;->getCurrentSource()Lcom/parrot/asteroid/audio/service/Source;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/parrot/asteroid/audio/service/Source;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 386
    invoke-virtual {v0}, Lcom/parrot/asteroid/media/MediaManager;->play()Z

    .line 392
    :goto_2
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.parrot.mediaplayer"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 393
    .restart local v1    # "newintent":Landroid/content/Intent;
    const-string v3, "source"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 394
    const-string v3, "android.intent.category.DEFAULT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    invoke-virtual {v1, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 396
    invoke-virtual {v1, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 397
    invoke-virtual {v1, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 398
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    move v3, v5

    .line 399
    goto :goto_1

    .line 389
    .end local v1    # "newintent":Landroid/content/Intent;
    :cond_3
    invoke-virtual {v0, v2, v6}, Lcom/parrot/asteroid/media/MediaManager;->launchSource(Lcom/parrot/asteroid/audio/service/Source;Z)V

    goto :goto_2

    .end local v0    # "mediaManager":Lcom/parrot/asteroid/media/MediaManager;
    .end local v2    # "source":Lcom/parrot/asteroid/audio/service/Source;
    :cond_4
    move v3, v6

    .line 401
    goto/16 :goto_1
.end method

.method private startApplication(I)Z
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 300
    iget-object v3, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 301
    :try_start_0
    iget-object v4, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    invoke-virtual {v4}, Lse/volvocars/acu/model/Apps;->getApps()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 303
    .local v1, "apps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    :try_start_1
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/model/AppInfo;

    .line 304
    .local v0, "app":Lse/volvocars/acu/model/AppInfo;
    invoke-direct {p0, v0}, Lse/volvocars/acu/AppHandler;->startApp(Lse/volvocars/acu/model/AppInfo;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 305
    invoke-direct {p0, v1, v0, p1}, Lse/volvocars/acu/AppHandler;->indicateAppBeingStarted(Ljava/util/List;Lse/volvocars/acu/model/AppInfo;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 306
    const/4 v4, 0x1

    :try_start_2
    monitor-exit v3

    move v3, v4

    .line 311
    .end local v0    # "app":Lse/volvocars/acu/model/AppInfo;
    .end local p0    # "this":Lse/volvocars/acu/AppHandler;
    :goto_0
    return v3

    .line 308
    .restart local p0    # "this":Lse/volvocars/acu/AppHandler;
    :catch_0
    move-exception v4

    move-object v2, v4

    .line 309
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    const-string v4, "AppHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Tried to start application "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "this":Lse/volvocars/acu/AppHandler;
    check-cast p0, Lse/volvocars/acu/model/AppInfo;

    invoke-virtual {p0}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " but an error occured: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    .end local v2    # "e":Landroid/content/ActivityNotFoundException;
    :cond_0
    const/4 v4, 0x0

    monitor-exit v3

    move v3, v4

    goto :goto_0

    .line 312
    .end local v1    # "apps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4
.end method

.method private updateForbiddenIcons()V
    .locals 2

    .prologue
    .line 419
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    monitor-enter v0

    .line 420
    :try_start_0
    iget-boolean v1, p0, Lse/volvocars/acu/AppHandler;->mDriverWlActive:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mDriverWlWhitelist:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 421
    invoke-direct {p0}, Lse/volvocars/acu/AppHandler;->allowOnlyWhitelistedApps()V

    .line 426
    :goto_0
    monitor-exit v0

    .line 427
    return-void

    .line 424
    :cond_0
    invoke-direct {p0}, Lse/volvocars/acu/AppHandler;->allowAllApps()V

    goto :goto_0

    .line 426
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 408
    iget-object v0, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    monitor-enter v0

    .line 409
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    if-eqz v1, :cond_0

    .line 410
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    invoke-virtual {v1}, Lse/volvocars/acu/model/Apps;->close()V

    .line 412
    :cond_0
    monitor-exit v0

    .line 413
    return-void

    .line 412
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public findSelectedAppAtScreenPosition(II)I
    .locals 6
    .param p1, "hitbox"    # I
    .param p2, "position"    # I

    .prologue
    .line 184
    iget-object v3, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 185
    const/high16 v0, 0x43960000    # 300.0f

    .line 186
    .local v0, "iconOffset":F
    int-to-float v4, p2

    int-to-float v5, p1

    mul-float/2addr v5, v0

    add-float v2, v4, v5

    .line 188
    .local v2, "pos":F
    div-float v4, v2, v0

    float-to-double v4, v4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Math;->rint(D)D

    move-result-wide v4

    double-to-int v1, v4

    .line 190
    .local v1, "index":I
    iget-object v4, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    invoke-virtual {v4}, Lse/volvocars/acu/model/Apps;->getApps()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    if-gt v1, v4, :cond_0

    if-gez v1, :cond_1

    .line 191
    :cond_0
    const/4 v1, -0x1

    .line 193
    :cond_1
    monitor-exit v3

    return v1

    .line 194
    .end local v1    # "index":I
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public getSelectedApplication(I)Lse/volvocars/acu/model/AppInfo;
    .locals 3
    .param p1, "hitbox"    # I

    .prologue
    .line 154
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 155
    :try_start_0
    invoke-direct {p0, p1}, Lse/volvocars/acu/AppHandler;->findSelectedApplicationIndex(I)I

    move-result v0

    .line 156
    .local v0, "index":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 157
    const/4 v2, 0x0

    monitor-exit v1

    move-object v1, v2

    .line 159
    .end local p0    # "this":Lse/volvocars/acu/AppHandler;
    :goto_0
    return-object v1

    .restart local p0    # "this":Lse/volvocars/acu/AppHandler;
    :cond_0
    iget-object v2, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    invoke-virtual {v2}, Lse/volvocars/acu/model/Apps;->getApps()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "this":Lse/volvocars/acu/AppHandler;
    check-cast p0, Lse/volvocars/acu/model/AppInfo;

    monitor-exit v1

    move-object v1, p0

    goto :goto_0

    .line 160
    .end local v0    # "index":I
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public goLeft()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 495
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 496
    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0, v2}, Lse/volvocars/acu/AppHandler;->findSelectedApplicationIndex(I)I

    move-result v0

    .line 497
    .local v0, "currentIndex":I
    if-lez v0, :cond_1

    .line 499
    sub-int v2, v0, v3

    invoke-direct {p0, v2}, Lse/volvocars/acu/AppHandler;->scrollToIcon(I)V

    .line 502
    if-ne v0, v3, :cond_0

    .line 503
    const/4 v2, 0x0

    iput-boolean v2, p0, Lse/volvocars/acu/AppHandler;->mAllowDpadCross:Z

    .line 504
    iget-object v2, p0, Lse/volvocars/acu/AppHandler;->mDpadTimerHandler:Landroid/os/Handler;

    iget-object v3, p0, Lse/volvocars/acu/AppHandler;->mDpadTimerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 505
    iget-object v2, p0, Lse/volvocars/acu/AppHandler;->mDpadTimerHandler:Landroid/os/Handler;

    iget-object v3, p0, Lse/volvocars/acu/AppHandler;->mDpadTimerRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x320

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 520
    :cond_0
    :goto_0
    monitor-exit v1

    .line 521
    return-void

    .line 514
    :cond_1
    iget-boolean v2, p0, Lse/volvocars/acu/AppHandler;->mAllowDpadCross:Z

    if-eqz v2, :cond_2

    .line 515
    iget-object v2, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lse/volvocars/acu/AcuActivity;->setTargetPosition(I)V

    goto :goto_0

    .line 520
    .end local v0    # "currentIndex":I
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 517
    .restart local v0    # "currentIndex":I
    :cond_2
    :try_start_1
    const-string v2, "AppHandler"

    const-string v3, "Not allowed to move to HomeScreen yet. Have to wait 800ms"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public goRight()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 480
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 481
    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0, v2}, Lse/volvocars/acu/AppHandler;->findSelectedApplicationIndex(I)I

    move-result v0

    .line 482
    .local v0, "currentIndex":I
    iget-object v2, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    invoke-virtual {v2}, Lse/volvocars/acu/model/Apps;->getApps()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v3

    if-ge v0, v2, :cond_0

    .line 484
    add-int/lit8 v2, v0, 0x1

    invoke-direct {p0, v2}, Lse/volvocars/acu/AppHandler;->scrollToIcon(I)V

    .line 488
    :cond_0
    monitor-exit v1

    .line 489
    return-void

    .line 488
    .end local v0    # "currentIndex":I
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method isDefaultIconCentered()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 540
    invoke-direct {p0, v2}, Lse/volvocars/acu/AppHandler;->getIconTargetPosition(I)I

    move-result v0

    .line 541
    .local v0, "defaultIconPosition":I
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    invoke-virtual {v1}, Lse/volvocars/acu/AcuActivity;->getCurrentPos()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public isLeftmostAppCenteredAtScreenPosition(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    .line 144
    invoke-virtual {p0, v1, p1}, Lse/volvocars/acu/AppHandler;->findSelectedAppAtScreenPosition(II)I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReorderingApps()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lse/volvocars/acu/AppHandler;->mReorderApps:Z

    return v0
.end method

.method public loadSettings()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 109
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    const-string v2, "se.volvocars.acu.apps"

    invoke-virtual {v1, v2, v3}, Lse/volvocars/acu/AcuActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 110
    .local v0, "settings":Landroid/content/SharedPreferences;
    const-string v1, "reorder_apps"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lse/volvocars/acu/AppHandler;->mReorderApps:Z

    .line 111
    return-void
.end method

.method public moveToDefaultIcon()V
    .locals 1

    .prologue
    .line 528
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lse/volvocars/acu/AppHandler;->moveToIcon(I)V

    .line 529
    return-void
.end method

.method public moveToFocusIcon()V
    .locals 1

    .prologue
    .line 536
    iget v0, p0, Lse/volvocars/acu/AppHandler;->mFocusIcon:I

    invoke-direct {p0, v0}, Lse/volvocars/acu/AppHandler;->moveToIcon(I)V

    .line 537
    return-void
.end method

.method public saveFocusApp(I)V
    .locals 5
    .param p1, "focusApp"    # I

    .prologue
    .line 126
    iput p1, p0, Lse/volvocars/acu/AppHandler;->mFocusIcon:I

    .line 127
    iget-object v2, p0, Lse/volvocars/acu/AppHandler;->mActivity:Lse/volvocars/acu/AcuActivity;

    const-string v3, "se.volvocars.acu.apps"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lse/volvocars/acu/AcuActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 128
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 129
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "focus_app"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 130
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 131
    return-void
.end method

.method public scrollToNextIconInDirectionWhenScreenPosWas(II)V
    .locals 3
    .param p1, "direction"    # I
    .param p2, "pos"    # I

    .prologue
    const/4 v2, 0x1

    .line 551
    invoke-virtual {p0, v2, p2}, Lse/volvocars/acu/AppHandler;->findSelectedAppAtScreenPosition(II)I

    move-result v0

    .line 552
    .local v0, "currentIcon":I
    add-int/2addr v0, p1

    .line 553
    if-ltz v0, :cond_0

    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    invoke-virtual {v1}, Lse/volvocars/acu/model/Apps;->getApps()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    .line 554
    invoke-direct {p0, v0}, Lse/volvocars/acu/AppHandler;->scrollToIcon(I)V

    .line 556
    :cond_0
    return-void
.end method

.method public setSelectedApp(I)V
    .locals 3
    .param p1, "hitbox"    # I

    .prologue
    .line 635
    iget-object v1, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    monitor-enter v1

    .line 636
    :try_start_0
    invoke-direct {p0, p1}, Lse/volvocars/acu/AppHandler;->findSelectedApplicationIndex(I)I

    move-result v0

    .line 637
    .local v0, "index":I
    iget-object v2, p0, Lse/volvocars/acu/AppHandler;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v2, v0}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setSelectedStatus(I)V

    .line 638
    monitor-exit v1

    .line 639
    return-void

    .line 638
    .end local v0    # "index":I
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public startAppInHitbox(I)Z
    .locals 11
    .param p1, "hitbox"    # I

    .prologue
    const/4 v10, 0x1

    const-string v5, "AppHandler"

    .line 203
    iget-object v5, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    monitor-enter v5

    .line 204
    :try_start_0
    invoke-direct {p0, p1}, Lse/volvocars/acu/AppHandler;->findSelectedApplicationIndex(I)I

    move-result v2

    .line 205
    .local v2, "clickResult":I
    const/4 v0, 0x1

    .line 206
    .local v0, "appAllowed":Z
    if-ltz v2, :cond_3

    .line 207
    iget-object v6, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    invoke-virtual {v6}, Lse/volvocars/acu/model/Apps;->getApps()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lse/volvocars/acu/model/AppInfo;

    .line 209
    .local v3, "clickedApp":Lse/volvocars/acu/model/AppInfo;
    iget-boolean v6, p0, Lse/volvocars/acu/AppHandler;->mDriverWlActive:Z

    if-eqz v6, :cond_0

    invoke-virtual {v3}, Lse/volvocars/acu/model/AppInfo;->getUniqueidentifier()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lse/volvocars/acu/AppHandler;->onWhitelist(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 210
    const/4 v0, 0x0

    .line 211
    const-string v6, "AppHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " was not found on the whitelist and will not be started"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_0
    invoke-virtual {v3}, Lse/volvocars/acu/model/AppInfo;->getStartIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v3}, Lse/volvocars/acu/model/AppInfo;->getLaunchMode()Lse/volvocars/acu/model/LaunchMode;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lse/volvocars/acu/AppHandler;->isAppStartable(Landroid/content/Intent;Lse/volvocars/acu/model/LaunchMode;)Z

    move-result v1

    .line 214
    .local v1, "appRunnable":Z
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 215
    const/4 v4, 0x0

    .line 217
    .local v4, "delay":I
    if-eq p1, v10, :cond_1

    .line 218
    const/16 v4, 0x226

    .line 219
    invoke-direct {p0, v2}, Lse/volvocars/acu/AppHandler;->scrollToIcon(I)V

    .line 223
    :cond_1
    iget-object v6, p0, Lse/volvocars/acu/AppHandler;->mHandler:Landroid/os/Handler;

    new-instance v7, Lse/volvocars/acu/AppHandler$2;

    invoke-direct {v7, p0}, Lse/volvocars/acu/AppHandler$2;-><init>(Lse/volvocars/acu/AppHandler;)V

    int-to-long v8, v4

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 231
    iget-object v6, p0, Lse/volvocars/acu/AppHandler;->mHandler:Landroid/os/Handler;

    new-instance v7, Lse/volvocars/acu/AppHandler$3;

    invoke-direct {v7, p0, v2}, Lse/volvocars/acu/AppHandler$3;-><init>(Lse/volvocars/acu/AppHandler;I)V

    add-int/lit16 v8, v4, 0x15e

    int-to-long v8, v8

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 238
    monitor-exit v5

    move v5, v10

    .line 244
    .end local v1    # "appRunnable":Z
    .end local v3    # "clickedApp":Lse/volvocars/acu/model/AppInfo;
    .end local v4    # "delay":I
    :goto_0
    return v5

    .line 240
    .restart local v1    # "appRunnable":Z
    .restart local v3    # "clickedApp":Lse/volvocars/acu/model/AppInfo;
    :cond_2
    const-string v6, "AppHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lse/volvocars/acu/model/AppInfo;->getAppTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " was not started, not runnable or not on whitelist."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    .end local v1    # "appRunnable":Z
    .end local v3    # "clickedApp":Lse/volvocars/acu/model/AppInfo;
    :goto_1
    const/4 v6, 0x0

    monitor-exit v5

    move v5, v6

    goto :goto_0

    .line 242
    :cond_3
    const-string v6, "AppHandler"

    const-string v7, "No application found"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 245
    .end local v0    # "appAllowed":Z
    .end local v2    # "clickResult":I
    :catchall_0
    move-exception v6

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method updateAppIconOrder()V
    .locals 5

    .prologue
    .line 280
    iget-object v4, p0, Lse/volvocars/acu/AppHandler;->mAppsLock:Ljava/lang/Object;

    monitor-enter v4

    .line 281
    :try_start_0
    iget-object v3, p0, Lse/volvocars/acu/AppHandler;->mApps:Lse/volvocars/acu/model/Apps;

    invoke-virtual {v3}, Lse/volvocars/acu/model/Apps;->getApps()Ljava/util/List;

    move-result-object v0

    .line 282
    .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-array v2, v3, [I

    .line 283
    .local v2, "orderArray":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 284
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lse/volvocars/acu/model/AppInfo;

    invoke-virtual {v3}, Lse/volvocars/acu/model/AppInfo;->getAppIconId()I

    move-result v3

    aput v3, v2, v1

    .line 283
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 287
    :cond_0
    iget-object v3, p0, Lse/volvocars/acu/AppHandler;->mLauncherGLView:Lse/volvocars/acu/ui/opengl/LauncherGLView;

    invoke-virtual {v3, v2}, Lse/volvocars/acu/ui/opengl/LauncherGLView;->setAppIconOrder([I)V

    .line 288
    invoke-direct {p0}, Lse/volvocars/acu/AppHandler;->updateForbiddenIcons()V

    .line 289
    iget-object v3, p0, Lse/volvocars/acu/AppHandler;->mAppName:Lse/volvocars/acu/ui/AppNameView;

    invoke-virtual {v3, v0}, Lse/volvocars/acu/ui/AppNameView;->setApps(Ljava/util/List;)V

    .line 291
    monitor-exit v4

    .line 292
    return-void

    .line 291
    .end local v0    # "apps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/model/AppInfo;>;"
    .end local v1    # "i":I
    .end local v2    # "orderArray":[I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method
