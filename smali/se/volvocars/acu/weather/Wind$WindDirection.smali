.class public final enum Lse/volvocars/acu/weather/Wind$WindDirection;
.super Ljava/lang/Enum;
.source "Wind.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/Wind;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WindDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lse/volvocars/acu/weather/Wind$WindDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lse/volvocars/acu/weather/Wind$WindDirection;

.field public static final enum E:Lse/volvocars/acu/weather/Wind$WindDirection;

.field public static final enum N:Lse/volvocars/acu/weather/Wind$WindDirection;

.field public static final enum NE:Lse/volvocars/acu/weather/Wind$WindDirection;

.field public static final enum NW:Lse/volvocars/acu/weather/Wind$WindDirection;

.field public static final enum S:Lse/volvocars/acu/weather/Wind$WindDirection;

.field public static final enum SE:Lse/volvocars/acu/weather/Wind$WindDirection;

.field public static final enum SW:Lse/volvocars/acu/weather/Wind$WindDirection;

.field public static final enum W:Lse/volvocars/acu/weather/Wind$WindDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 45
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindDirection;

    const-string v1, "W"

    invoke-direct {v0, v1, v3}, Lse/volvocars/acu/weather/Wind$WindDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindDirection;->W:Lse/volvocars/acu/weather/Wind$WindDirection;

    .line 46
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindDirection;

    const-string v1, "NW"

    invoke-direct {v0, v1, v4}, Lse/volvocars/acu/weather/Wind$WindDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindDirection;->NW:Lse/volvocars/acu/weather/Wind$WindDirection;

    .line 47
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindDirection;

    const-string v1, "N"

    invoke-direct {v0, v1, v5}, Lse/volvocars/acu/weather/Wind$WindDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindDirection;->N:Lse/volvocars/acu/weather/Wind$WindDirection;

    .line 48
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindDirection;

    const-string v1, "NE"

    invoke-direct {v0, v1, v6}, Lse/volvocars/acu/weather/Wind$WindDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindDirection;->NE:Lse/volvocars/acu/weather/Wind$WindDirection;

    .line 49
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindDirection;

    const-string v1, "E"

    invoke-direct {v0, v1, v7}, Lse/volvocars/acu/weather/Wind$WindDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindDirection;->E:Lse/volvocars/acu/weather/Wind$WindDirection;

    .line 50
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindDirection;

    const-string v1, "SE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lse/volvocars/acu/weather/Wind$WindDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindDirection;->SE:Lse/volvocars/acu/weather/Wind$WindDirection;

    .line 51
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindDirection;

    const-string v1, "S"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lse/volvocars/acu/weather/Wind$WindDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindDirection;->S:Lse/volvocars/acu/weather/Wind$WindDirection;

    .line 52
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindDirection;

    const-string v1, "SW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lse/volvocars/acu/weather/Wind$WindDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindDirection;->SW:Lse/volvocars/acu/weather/Wind$WindDirection;

    .line 44
    const/16 v0, 0x8

    new-array v0, v0, [Lse/volvocars/acu/weather/Wind$WindDirection;

    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->W:Lse/volvocars/acu/weather/Wind$WindDirection;

    aput-object v1, v0, v3

    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->NW:Lse/volvocars/acu/weather/Wind$WindDirection;

    aput-object v1, v0, v4

    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->N:Lse/volvocars/acu/weather/Wind$WindDirection;

    aput-object v1, v0, v5

    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->NE:Lse/volvocars/acu/weather/Wind$WindDirection;

    aput-object v1, v0, v6

    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->E:Lse/volvocars/acu/weather/Wind$WindDirection;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lse/volvocars/acu/weather/Wind$WindDirection;->SE:Lse/volvocars/acu/weather/Wind$WindDirection;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lse/volvocars/acu/weather/Wind$WindDirection;->S:Lse/volvocars/acu/weather/Wind$WindDirection;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lse/volvocars/acu/weather/Wind$WindDirection;->SW:Lse/volvocars/acu/weather/Wind$WindDirection;

    aput-object v2, v0, v1

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindDirection;->$VALUES:[Lse/volvocars/acu/weather/Wind$WindDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lse/volvocars/acu/weather/Wind$WindDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    const-class v0, Lse/volvocars/acu/weather/Wind$WindDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    .end local p0    # "name":Ljava/lang/String;
    check-cast p0, Lse/volvocars/acu/weather/Wind$WindDirection;

    return-object p0
.end method

.method public static values()[Lse/volvocars/acu/weather/Wind$WindDirection;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lse/volvocars/acu/weather/Wind$WindDirection;->$VALUES:[Lse/volvocars/acu/weather/Wind$WindDirection;

    invoke-virtual {v0}, [Lse/volvocars/acu/weather/Wind$WindDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lse/volvocars/acu/weather/Wind$WindDirection;

    return-object v0
.end method
