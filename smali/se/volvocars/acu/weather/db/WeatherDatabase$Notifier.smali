.class final Lse/volvocars/acu/weather/db/WeatherDatabase$Notifier;
.super Ljava/lang/Object;
.source "WeatherDatabase.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/db/WeatherDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Notifier"
.end annotation


# instance fields
.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p1, "listeners":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput-object p1, p0, Lse/volvocars/acu/weather/db/WeatherDatabase$Notifier;->mListeners:Ljava/util/List;

    .line 146
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 150
    iget-object v3, p0, Lse/volvocars/acu/weather/db/WeatherDatabase$Notifier;->mListeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;

    .line 152
    .local v2, "listener":Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;
    :try_start_0
    invoke-interface {v2}, Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;->onDatabaseChanged()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "cause":Ljava/lang/Exception;
    # getter for: Lse/volvocars/acu/weather/db/WeatherDatabase;->TAG:Ljava/lang/String;
    invoke-static {}, Lse/volvocars/acu/weather/db/WeatherDatabase;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Caught exception during callback: "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 157
    .end local v0    # "cause":Ljava/lang/Exception;
    .end local v2    # "listener":Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;
    :cond_0
    return-void
.end method
