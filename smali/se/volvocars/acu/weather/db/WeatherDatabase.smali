.class public final Lse/volvocars/acu/weather/db/WeatherDatabase;
.super Ljava/lang/Object;
.source "WeatherDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;,
        Lse/volvocars/acu/weather/db/WeatherDatabase$Notifier;
    }
.end annotation


# static fields
.field private static final CONNECTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lse/volvocars/acu/weather/db/WeatherDatabase;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final CONNECTIONS_LOCK:Ljava/lang/Object;

.field private static final EXECUTOR:Ljava/util/concurrent/Executor;

.field public static final FORECAST_AIR_PRESSURE:Ljava/lang/String; = "air_pressure"

.field public static final FORECAST_DAY_NIGHT:Ljava/lang/String; = "day_night"

.field public static final FORECAST_GUST:Ljava/lang/String; = "wind_gust"

.field public static final FORECAST_ISSUEDATE:Ljava/lang/String; = "issuedate"

.field public static final FORECAST_LATITUDE:Ljava/lang/String; = "latitude"

.field public static final FORECAST_LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final FORECAST_NEXTUPDATE:Ljava/lang/String; = "nextupdate"

.field public static final FORECAST_PERCIPITATION:Ljava/lang/String; = "percipitation"

.field public static final FORECAST_REL_HUMIDITY:Ljava/lang/String; = "rel_humid"

.field public static final FORECAST_TEMPERATURE:Ljava/lang/String; = "temp"

.field public static final FORECAST_VALID4DATE:Ljava/lang/String; = "validfordate"

.field public static final FORECAST_WEATHER_SYMBOL:Ljava/lang/String; = "weather_symbol"

.field public static final FORECAST_WINDDIR:Ljava/lang/String; = "wind_dir"

.field public static final FORECAST_WINDSPEED:Ljava/lang/String; = "wind_speed"

.field private static final SQL_AND:Ljava/lang/String; = " AND "

.field private static final SQL_ARG:Ljava/lang/String; = " ? "

.field private static final SQL_BETWEEN:Ljava/lang/String; = " BETWEEN "

.field private static final SQL_EQU:Ljava/lang/String; = " = "

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mDbHelper:Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mListenersLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lse/volvocars/acu/weather/db/WeatherDatabase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lse/volvocars/acu/weather/db/WeatherDatabase;->TAG:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lse/volvocars/acu/weather/db/WeatherDatabase;->CONNECTIONS_LOCK:Ljava/lang/Object;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lse/volvocars/acu/weather/db/WeatherDatabase;->CONNECTIONS:Ljava/util/List;

    .line 67
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lse/volvocars/acu/weather/db/WeatherDatabase;->EXECUTOR:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mListenersLock:Ljava/lang/Object;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mListeners:Ljava/util/List;

    .line 89
    iput-object p1, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mContext:Landroid/content/Context;

    .line 90
    sget-object v0, Lse/volvocars/acu/weather/db/WeatherDatabase;->CONNECTIONS_LOCK:Ljava/lang/Object;

    monitor-enter v0

    .line 91
    :try_start_0
    sget-object v1, Lse/volvocars/acu/weather/db/WeatherDatabase;->CONNECTIONS:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    invoke-virtual {p0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->reopen()V

    .line 94
    return-void

    .line 92
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lse/volvocars/acu/weather/db/WeatherDatabase;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private assertConnectionStatus()V
    .locals 2

    .prologue
    .line 205
    invoke-virtual {p0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Connection is closed, use open() to reopen a connection. Note that listeners still receives change updates even when the connection is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 208
    :cond_0
    return-void
.end method

.method private delete(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "whereClause"    # Ljava/lang/String;
    .param p2, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 271
    invoke-direct {p0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->assertConnectionStatus()V

    .line 272
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "weather"

    invoke-virtual {v0, v1, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private insert(Landroid/content/ContentValues;)J
    .locals 3
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 276
    invoke-direct {p0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->assertConnectionStatus()V

    .line 277
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "weather"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method private notifyListeners()V
    .locals 7

    .prologue
    .line 109
    sget-object v3, Lse/volvocars/acu/weather/db/WeatherDatabase;->CONNECTIONS_LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 110
    :try_start_0
    sget-object v4, Lse/volvocars/acu/weather/db/WeatherDatabase;->CONNECTIONS:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 111
    .end local p0    # "this":Lse/volvocars/acu/weather/db/WeatherDatabase;
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lse/volvocars/acu/weather/db/WeatherDatabase;>;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 112
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/weather/db/WeatherDatabase;

    .line 113
    .local v0, "connection":Lse/volvocars/acu/weather/db/WeatherDatabase;
    if-nez v0, :cond_0

    .line 114
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 122
    .end local v0    # "connection":Lse/volvocars/acu/weather/db/WeatherDatabase;
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lse/volvocars/acu/weather/db/WeatherDatabase;>;>;"
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 117
    .restart local v0    # "connection":Lse/volvocars/acu/weather/db/WeatherDatabase;
    .restart local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lse/volvocars/acu/weather/db/WeatherDatabase;>;>;"
    :cond_0
    :try_start_1
    iget-object v4, v0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mListenersLock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 118
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    iget-object v5, v0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mListeners:Ljava/util/List;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 119
    .local v1, "copy":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;>;"
    sget-object v5, Lse/volvocars/acu/weather/db/WeatherDatabase;->EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v6, Lse/volvocars/acu/weather/db/WeatherDatabase$Notifier;

    invoke-direct {v6, v1}, Lse/volvocars/acu/weather/db/WeatherDatabase$Notifier;-><init>(Ljava/util/List;)V

    invoke-interface {v5, v6}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 120
    monitor-exit v4

    goto :goto_0

    .end local v1    # "copy":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;>;"
    :catchall_1
    move-exception v5

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    throw v5

    .line 122
    .end local v0    # "connection":Lse/volvocars/acu/weather/db/WeatherDatabase;
    :cond_1
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 123
    return-void
.end method


# virtual methods
.method public addOnDatabaseChangedListener(Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;

    .prologue
    .line 97
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mListenersLock:Ljava/lang/Object;

    monitor-enter v0

    .line 98
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    monitor-exit v0

    .line 100
    return-void

    .line 99
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public addValues(Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "cv"    # Landroid/content/ContentValues;

    .prologue
    .line 131
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/db/WeatherDatabase;->insert(Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 132
    .local v0, "id":J
    invoke-direct {p0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->notifyListeners()V

    .line 133
    return-wide v0
.end method

.method public clearExpiredData()I
    .locals 8

    .prologue
    .line 301
    const-wide/32 v0, 0x1499700

    .line 302
    .local v0, "sixHoursInMs":J
    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/32 v6, 0x1499700

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 303
    .local v2, "whereArgs":[Ljava/lang/String;
    const-string v3, "nextupdate < ?"

    invoke-direct {p0, v3, v2}, Lse/volvocars/acu/weather/db/WeatherDatabase;->delete(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    return v3
.end method

.method public clearOldData(Lse/volvocars/acu/weather/settings/Location;J)I
    .locals 5
    .param p1, "loc"    # Lse/volvocars/acu/weather/settings/Location;
    .param p2, "issuedate"    # J

    .prologue
    .line 314
    const-string v1, "longitude = ? AND latitude = ? AND issuedate != ?"

    .line 318
    .local v1, "whereClause":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v3

    invoke-virtual {v3}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v3

    invoke-virtual {v3}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x2

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 324
    .local v0, "whereArgs":[Ljava/lang/String;
    invoke-direct {p0, v1, v0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->delete(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 194
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 196
    iput-object v1, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 198
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDbHelper:Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;

    if-eqz v0, :cond_1

    .line 199
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDbHelper:Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;->close()V

    .line 200
    iput-object v1, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDbHelper:Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;

    .line 202
    :cond_1
    return-void
.end method

.method public findWeather(DDJJ)Landroid/database/Cursor;
    .locals 12
    .param p1, "lon"    # D
    .param p3, "lat"    # D
    .param p5, "timeFrom"    # J
    .param p7, "timeTo"    # J

    .prologue
    .line 237
    const/4 v9, 0x0

    .line 238
    .local v9, "retcur":Landroid/database/Cursor;
    const-string v3, "longitude =  ?  AND latitude =  ?  AND validfordate BETWEEN  ?  AND  ? "

    .line 242
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static/range {p3 .. p4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-static/range {p7 .. p8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 248
    .local v4, "selArgs":[Ljava/lang/String;
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "weather"

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "validfordate ASC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 250
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 252
    const-wide/32 v10, 0x1499700

    .line 253
    .local v10, "sixHours":J
    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static/range {p3 .. p4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    const-wide/32 v1, 0x1499700

    add-long v1, v1, p7

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 259
    .restart local v4    # "selArgs":[Ljava/lang/String;
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 260
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "weather"

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "validfordate ASC"

    const-string v8, "1"

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 262
    .end local v10    # "sixHours":J
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 263
    sget-object v0, Lse/volvocars/acu/weather/db/WeatherDatabase;->TAG:Ljava/lang/String;

    const-string v1, "Couldn\'t find any weather in database."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    :goto_0
    return-object v9

    .line 265
    :cond_1
    sget-object v0, Lse/volvocars/acu/weather/db/WeatherDatabase;->TAG:Ljava/lang/String;

    const-string v1, "Found weather in database"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public findWeather(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;)Landroid/database/Cursor;
    .locals 9
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;
    .param p2, "from"    # Ljava/util/Date;
    .param p3, "to"    # Ljava/util/Date;

    .prologue
    .line 224
    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v1

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v3

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lse/volvocars/acu/weather/db/WeatherDatabase;->findWeather(DDJJ)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getNextUpdateForLocation(Lse/volvocars/acu/weather/settings/Location;)J
    .locals 13
    .param p1, "loc"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v12, 0x0

    .line 329
    const-string v3, "longitude = ? AND latitude = ?"

    .line 331
    .local v3, "whereClause":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v12

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    .line 335
    .local v4, "whereArgs":[Ljava/lang/String;
    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "nextupdate"

    aput-object v0, v2, v12

    .line 336
    .local v2, "col":[Ljava/lang/String;
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "weather"

    const-string v8, "1"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 337
    .local v9, "cur":Landroid/database/Cursor;
    const-wide/16 v10, 0x0

    .line 338
    .local v10, "retval":J
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    invoke-interface {v9, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 341
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 342
    return-wide v10
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDbHelper:Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeOnDatabaseChangeListener(Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lse/volvocars/acu/weather/db/WeatherDatabase$OnDatabaseChangedListener;

    .prologue
    .line 103
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mListenersLock:Ljava/lang/Object;

    monitor-enter v0

    .line 104
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 105
    monitor-exit v0

    .line 106
    return-void

    .line 105
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public reopen()V
    .locals 7

    .prologue
    .line 168
    iget-object v3, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v3, :cond_0

    iget-object v3, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDbHelper:Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;

    if-eqz v3, :cond_1

    .line 169
    :cond_0
    invoke-virtual {p0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->close()V

    .line 172
    :cond_1
    :try_start_0
    new-instance v3, Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;

    iget-object v4, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDbHelper:Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;

    .line 173
    iget-object v3, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDbHelper:Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;

    invoke-virtual {v3}, Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iput-object v3, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_0
    return-void

    .line 174
    :catch_0
    move-exception v3

    move-object v1, v3

    .line 178
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/databases/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Weather.db"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 181
    .local v0, "dbPath":Ljava/lang/String;
    invoke-static {v0}, Lse/volvocars/acu/db/DatabaseErrorHandler;->deleteDatabaseFile(Ljava/lang/String;)V

    .line 182
    iget-object v3, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDbHelper:Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;

    invoke-virtual {v3}, Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iput-object v3, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 183
    .end local v0    # "dbPath":Ljava/lang/String;
    :catch_1
    move-exception v3

    move-object v2, v3

    .line 185
    .local v2, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public update(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "whereClause"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 281
    invoke-direct {p0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->assertConnectionStatus()V

    .line 282
    iget-object v0, p0, Lse/volvocars/acu/weather/db/WeatherDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "weather"

    invoke-virtual {v0, v1, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method
