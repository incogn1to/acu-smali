.class Lse/volvocars/acu/weather/db/WeatherDbOpenHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "WeatherDbOpenHelper.java"


# static fields
.field private static final DATABASE_VERSION:I = 0x1

.field private static final SQL_COL_DELIM:Ljava/lang/String; = ", "

.field private static final SQL_DOUBLE:Ljava/lang/String; = " DOUBLE "

.field private static final SQL_INT:Ljava/lang/String; = " INT "

.field private static final SQL_LONG:Ljava/lang/String; = " LONG "

.field private static final SQL_STRING:Ljava/lang/String; = " STRING "

.field public static final WEATHER_DATABASE:Ljava/lang/String; = "Weather.db"

.field public static final WEATHER_TABLE_NAME:Ljava/lang/String; = "weather"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const-string v0, "Weather.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 26
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 30
    const-string v0, "CREATE TABLE weather (_id INTEGER PRIMARY KEY AUTOINCREMENT, longitude DOUBLE , latitude DOUBLE , validfordate LONG , issuedate LONG , nextupdate LONG , weather_symbol INT , day_night STRING , temp DOUBLE , wind_dir INT , wind_speed DOUBLE , wind_gust INT , rel_humid INT , percipitation DOUBLE , air_pressure INT );"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 52
    return-void
.end method
