.class public interface abstract Lse/volvocars/acu/weather/WeatherProvider;
.super Ljava/lang/Object;
.source "WeatherProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;
    }
.end annotation


# virtual methods
.method public abstract getLocation()Lse/volvocars/acu/weather/settings/Location;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract runWeatherSearch(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;)V
.end method

.method public abstract searchLocation(Ljava/lang/CharSequence;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract searchLocation(Ljava/lang/CharSequence;Landroid/location/Location;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Landroid/location/Location;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation
.end method

.method public abstract searchLocation(Lse/volvocars/acu/weather/Coordinate;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lse/volvocars/acu/weather/Coordinate;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setContext(Landroid/content/Context;)V
.end method

.method public abstract setLocation(DD)V
.end method

.method public abstract setLocation(Lse/volvocars/acu/weather/settings/Location;)V
.end method
