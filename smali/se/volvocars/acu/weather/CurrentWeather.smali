.class public final Lse/volvocars/acu/weather/CurrentWeather;
.super Ljava/lang/Object;
.source "CurrentWeather.java"


# instance fields
.field private final mCondition:Lse/volvocars/acu/weather/WeatherCondition;

.field private final mForecastCondition:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field private final mLocation:Lse/volvocars/acu/weather/settings/Location;

.field private mMaxtemp:Lse/volvocars/acu/weather/Temperature;

.field private mMinTemp:Lse/volvocars/acu/weather/Temperature;

.field private final mTemperature:Lse/volvocars/acu/weather/Temperature;

.field private final mTimestamp:J

.field private mWind:Lse/volvocars/acu/weather/Wind;


# direct methods
.method constructor <init>(Lse/volvocars/acu/weather/settings/Location;Lse/volvocars/acu/weather/Temperature;Lse/volvocars/acu/weather/Temperature;Lse/volvocars/acu/weather/WeatherCondition;Lse/volvocars/acu/weather/Wind;)V
    .locals 5
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;
    .param p2, "minTemp"    # Lse/volvocars/acu/weather/Temperature;
    .param p3, "maxTemp"    # Lse/volvocars/acu/weather/Temperature;
    .param p4, "cond"    # Lse/volvocars/acu/weather/WeatherCondition;
    .param p5, "wind"    # Lse/volvocars/acu/weather/Wind;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lse/volvocars/acu/weather/CurrentWeather;->mTimestamp:J

    .line 27
    iput-object p1, p0, Lse/volvocars/acu/weather/CurrentWeather;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    .line 28
    iput-object p2, p0, Lse/volvocars/acu/weather/CurrentWeather;->mMinTemp:Lse/volvocars/acu/weather/Temperature;

    .line 29
    iput-object p3, p0, Lse/volvocars/acu/weather/CurrentWeather;->mMaxtemp:Lse/volvocars/acu/weather/Temperature;

    .line 30
    sget-object v0, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    sget-object v1, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {p2, v1}, Lse/volvocars/acu/weather/Temperature;->get(Lse/volvocars/acu/weather/Temperature$Unit;)D

    move-result-wide v1

    sget-object v3, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {p3, v3}, Lse/volvocars/acu/weather/Temperature;->get(Lse/volvocars/acu/weather/Temperature$Unit;)D

    move-result-wide v3

    add-double/2addr v1, v3

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    invoke-static {v0, v1, v2}, Lse/volvocars/acu/weather/Temperature;->from(Lse/volvocars/acu/weather/Temperature$Unit;D)Lse/volvocars/acu/weather/Temperature;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/CurrentWeather;->mTemperature:Lse/volvocars/acu/weather/Temperature;

    .line 31
    iput-object p4, p0, Lse/volvocars/acu/weather/CurrentWeather;->mCondition:Lse/volvocars/acu/weather/WeatherCondition;

    .line 32
    invoke-static {p4}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->fromWeatherCondition(Lse/volvocars/acu/weather/WeatherCondition;)Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/CurrentWeather;->mForecastCondition:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 33
    iput-object p5, p0, Lse/volvocars/acu/weather/CurrentWeather;->mWind:Lse/volvocars/acu/weather/Wind;

    .line 34
    return-void
.end method


# virtual methods
.method public getCondition()Lse/volvocars/acu/weather/WeatherCondition;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lse/volvocars/acu/weather/CurrentWeather;->mCondition:Lse/volvocars/acu/weather/WeatherCondition;

    return-object v0
.end method

.method public getForecastCondition()Lse/volvocars/acu/weather/settings/WeatherForecastCondition;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lse/volvocars/acu/weather/CurrentWeather;->mForecastCondition:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    return-object v0
.end method

.method public getLocation()Lse/volvocars/acu/weather/settings/Location;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lse/volvocars/acu/weather/CurrentWeather;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    return-object v0
.end method

.method public getTemperature()Lse/volvocars/acu/weather/Temperature;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lse/volvocars/acu/weather/CurrentWeather;->mTemperature:Lse/volvocars/acu/weather/Temperature;

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lse/volvocars/acu/weather/CurrentWeather;->mTimestamp:J

    return-wide v0
.end method

.method public getWind()Lse/volvocars/acu/weather/Wind;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lse/volvocars/acu/weather/CurrentWeather;->mWind:Lse/volvocars/acu/weather/Wind;

    return-object v0
.end method

.method public maxTemp()Lse/volvocars/acu/weather/Temperature;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lse/volvocars/acu/weather/CurrentWeather;->mMaxtemp:Lse/volvocars/acu/weather/Temperature;

    return-object v0
.end method

.method public minTemp()Lse/volvocars/acu/weather/Temperature;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lse/volvocars/acu/weather/CurrentWeather;->mMinTemp:Lse/volvocars/acu/weather/Temperature;

    return-object v0
.end method
