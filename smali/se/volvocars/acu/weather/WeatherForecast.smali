.class public Lse/volvocars/acu/weather/WeatherForecast;
.super Ljava/lang/Object;
.source "WeatherForecast.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;
    }
.end annotation


# instance fields
.field private mForecastList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mIssueDate:J


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "issuedate"    # J

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-wide p1, p0, Lse/volvocars/acu/weather/WeatherForecast;->mIssueDate:J

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/WeatherForecast;->mForecastList:Ljava/util/ArrayList;

    .line 45
    return-void
.end method


# virtual methods
.method public addWeatherData(JDIID)V
    .locals 11
    .param p1, "time"    # J
    .param p3, "temperature"    # D
    .param p5, "weather"    # I
    .param p6, "direction"    # I
    .param p7, "speed"    # D

    .prologue
    .line 48
    iget-object v10, p0, Lse/volvocars/acu/weather/WeatherForecast;->mForecastList:Ljava/util/ArrayList;

    new-instance v0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move/from16 v6, p5

    move/from16 v7, p6

    move-wide/from16 v8, p7

    invoke-direct/range {v0 .. v9}, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;-><init>(Lse/volvocars/acu/weather/WeatherForecast;JDIID)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    return-void
.end method

.method public getForecastData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherForecast;->mForecastList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getIssueDate()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lse/volvocars/acu/weather/WeatherForecast;->mIssueDate:J

    return-wide v0
.end method
