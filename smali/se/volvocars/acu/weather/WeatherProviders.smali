.class public final Lse/volvocars/acu/weather/WeatherProviders;
.super Ljava/lang/Object;
.source "WeatherProviders.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAvailableProviders()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 24
    .local v0, "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "SMHI"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 25
    const-string v1, "Dummy"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    return-object v0
.end method

.method public static getDefaultProvider(Landroid/content/Context;)Lse/volvocars/acu/weather/WeatherProvider;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    const-string v1, "SMHI"

    invoke-static {v1}, Lse/volvocars/acu/weather/WeatherProviders;->getProvider(Ljava/lang/String;)Lse/volvocars/acu/weather/WeatherProvider;

    move-result-object v0

    .line 49
    .local v0, "provider":Lse/volvocars/acu/weather/WeatherProvider;
    invoke-interface {v0, p0}, Lse/volvocars/acu/weather/WeatherProvider;->setContext(Landroid/content/Context;)V

    .line 50
    return-object v0
.end method

.method public static getProvider(Ljava/lang/String;)Lse/volvocars/acu/weather/WeatherProvider;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    const-string v0, "Dummy"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 35
    new-instance v0, Lse/volvocars/acu/weather/DummyProvider;

    invoke-direct {v0}, Lse/volvocars/acu/weather/DummyProvider;-><init>()V

    .line 37
    :goto_0
    return-object v0

    .line 36
    :cond_0
    const-string v0, "SMHI"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    new-instance v0, Lse/volvocars/acu/weather/SmhiProvider;

    invoke-direct {v0}, Lse/volvocars/acu/weather/SmhiProvider;-><init>()V

    goto :goto_0

    .line 39
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No provider named <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "> available."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
