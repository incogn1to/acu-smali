.class public Lse/volvocars/acu/weather/settings/LocationAdapter;
.super Landroid/widget/ArrayAdapter;
.source "LocationAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lse/volvocars/acu/weather/settings/Location;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LocationAdapter"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEmptyIndex:I

.field private mLayoutResourceId:I

.field private mLimit:I

.field private mLocationData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation
.end field

.field private final mRowHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p3, "data":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/settings/Location;>;"
    invoke-static {p3}, Lse/volvocars/acu/weather/settings/LocationAdapter;->assertNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLocationData:Ljava/util/List;

    .line 27
    const/16 v0, 0x1e

    iput v0, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLimit:I

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    .line 34
    iput-object p3, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLocationData:Ljava/util/List;

    .line 35
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mContext:Landroid/content/Context;

    .line 36
    iput p2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLayoutResourceId:I

    .line 37
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mRowHeight:I

    .line 38
    return-void
.end method

.method private static assertNotNull(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "object":Ljava/lang/Object;, "TT;"
    if-nez p0, :cond_0

    .line 42
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    return-object p0
.end method

.method private uniqueLocation(Lse/volvocars/acu/weather/settings/Location;)Z
    .locals 3
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    .line 146
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getLocationData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/Location;

    .line 147
    .local v1, "item":Lse/volvocars/acu/weather/settings/Location;
    invoke-virtual {v1, p1}, Lse/volvocars/acu/weather/settings/Location;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 148
    const/4 v2, 0x0

    .line 151
    .end local v1    # "item":Lse/volvocars/acu/weather/settings/Location;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/settings/Location;>;"
    if-nez p1, :cond_1

    .line 115
    :cond_0
    return-void

    .line 109
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/Location;

    .line 110
    .local v1, "item":Lse/volvocars/acu/weather/settings/Location;
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->listFull()Z

    move-result v2

    if-nez v2, :cond_0

    .line 113
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getLocationData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 135
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getLocationData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getLocationData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 138
    :cond_0
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getLayoutResourceId()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLayoutResourceId:I

    return v0
.end method

.method public getLimit()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLimit:I

    return v0
.end method

.method public getLocationData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLocationData:Ljava/util/List;

    return-object v0
.end method

.method public getLocationString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 92
    .end local p0    # "this":Lse/volvocars/acu/weather/settings/LocationAdapter;
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lse/volvocars/acu/weather/settings/Location;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 93
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lse/volvocars/acu/weather/settings/Location;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/Location;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 98
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 50
    iget v5, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    if-ne p1, v5, :cond_0

    .line 51
    new-instance v0, Landroid/view/View;

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 52
    .local v0, "dummyView":Landroid/view/View;
    new-instance v5, Landroid/widget/AbsListView$LayoutParams;

    const/4 v6, -0x1

    iget v7, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mRowHeight:I

    invoke-direct {v5, v6, v7}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v5, v0

    .line 67
    .end local v0    # "dummyView":Landroid/view/View;
    :goto_0
    return-object v5

    .line 56
    :cond_0
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 57
    .local v2, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getLayoutResourceId()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v2, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 59
    .local v4, "row":Landroid/view/View;
    new-instance v1, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;

    invoke-direct {v1}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;-><init>()V

    .line 60
    .local v1, "holder":Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;
    const v5, 0x7f07001a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lse/volvocars/acu/weather/settings/ListItemTextView;

    invoke-virtual {v1, v5}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->setLocationView(Lse/volvocars/acu/weather/settings/ListItemTextView;)V

    .line 62
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getLocationData()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lse/volvocars/acu/weather/settings/Location;

    .line 63
    .local v3, "locationItem":Lse/volvocars/acu/weather/settings/Location;
    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->getLocationView()Lse/volvocars/acu/weather/settings/ListItemTextView;

    move-result-object v5

    invoke-virtual {v5}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setWidthLong()V

    .line 64
    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->getLocationView()Lse/volvocars/acu/weather/settings/ListItemTextView;

    move-result-object v5

    invoke-virtual {v5, v3}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setLocation(Lse/volvocars/acu/weather/settings/Location;)V

    .line 65
    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->getLocationView()Lse/volvocars/acu/weather/settings/ListItemTextView;

    move-result-object v5

    invoke-virtual {v5}, Lse/volvocars/acu/weather/settings/ListItemTextView;->trimAndUpdate()V

    move-object v5, v4

    .line 67
    goto :goto_0
.end method

.method public bridge synthetic insert(Ljava/lang/Object;I)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # I

    .prologue
    .line 21
    check-cast p1, Lse/volvocars/acu/weather/settings/Location;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lse/volvocars/acu/weather/settings/LocationAdapter;->insert(Lse/volvocars/acu/weather/settings/Location;I)V

    return-void
.end method

.method public insert(Lse/volvocars/acu/weather/settings/Location;I)V
    .locals 3
    .param p1, "object"    # Lse/volvocars/acu/weather/settings/Location;
    .param p2, "index"    # I

    .prologue
    .line 160
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->listFull()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/LocationAdapter;->uniqueLocation(Lse/volvocars/acu/weather/settings/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-super {p0, p1, p2}, Landroid/widget/ArrayAdapter;->insert(Ljava/lang/Object;I)V

    .line 165
    :goto_0
    return-void

    .line 163
    :cond_0
    const-string v0, "LocationAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duplicate location: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - skip insert action"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public listFull()Z
    .locals 2

    .prologue
    .line 126
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getLocationData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLimit:I

    if-ge v0, v1, :cond_0

    .line 127
    const/4 v0, 0x0

    .line 129
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public newCurrentDragIndex(I)V
    .locals 5
    .param p1, "newIndex"    # I

    .prologue
    .line 168
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getCount()I

    move-result v2

    if-lt p1, v2, :cond_1

    .line 169
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Index too small or too large! Index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 172
    :cond_1
    iget v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    if-gez v2, :cond_2

    .line 173
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v3, "Saved value for currentDragIndex is illegal, you must call startDrag first."

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 175
    :cond_2
    iget v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getCount()I

    move-result v3

    if-lt v2, v3, :cond_3

    .line 176
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Saved value for currentDragIndex is out of bounds: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 179
    :cond_3
    iget v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    if-ge p1, v2, :cond_4

    .line 180
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLocationData:Ljava/util/List;

    iget v3, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/Location;

    .line 181
    .local v1, "item2":Lse/volvocars/acu/weather/settings/Location;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/weather/settings/Location;

    .line 183
    .local v0, "item1":Lse/volvocars/acu/weather/settings/Location;
    invoke-virtual {p0, v1, p1}, Lse/volvocars/acu/weather/settings/LocationAdapter;->insert(Lse/volvocars/acu/weather/settings/Location;I)V

    .line 184
    iget v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    invoke-virtual {p0, v0, v2}, Lse/volvocars/acu/weather/settings/LocationAdapter;->insert(Lse/volvocars/acu/weather/settings/Location;I)V

    .line 193
    :goto_0
    iput p1, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    .line 194
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->notifyDataSetChanged()V

    .line 197
    return-void

    .line 186
    .end local v0    # "item1":Lse/volvocars/acu/weather/settings/Location;
    .end local v1    # "item2":Lse/volvocars/acu/weather/settings/Location;
    :cond_4
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/weather/settings/Location;

    .line 187
    .restart local v0    # "item1":Lse/volvocars/acu/weather/settings/Location;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLocationData:Ljava/util/List;

    iget v3, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/Location;

    .line 189
    .restart local v1    # "item2":Lse/volvocars/acu/weather/settings/Location;
    iget v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    invoke-virtual {p0, v0, v2}, Lse/volvocars/acu/weather/settings/LocationAdapter;->insert(Lse/volvocars/acu/weather/settings/Location;I)V

    .line 190
    invoke-virtual {p0, v1, p1}, Lse/volvocars/acu/weather/settings/LocationAdapter;->insert(Lse/volvocars/acu/weather/settings/Location;I)V

    goto :goto_0
.end method

.method public removeLocation(Lse/volvocars/acu/weather/settings/Location;)V
    .locals 4
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    .line 236
    if-nez p1, :cond_1

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/weather/settings/Location;

    .line 240
    .local v0, "data":Lse/volvocars/acu/weather/settings/Location;
    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/Location;->getArea()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getArea()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/Location;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 241
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 242
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->notifyDataSetChanged()V

    .line 243
    const-string v2, "LocationAdapter"

    const-string v3, "Location found and removed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 220
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mContext:Landroid/content/Context;

    .line 221
    return-void
.end method

.method public setLayoutResourceId(I)V
    .locals 0
    .param p1, "mLayoutResourceId"    # I

    .prologue
    .line 228
    iput p1, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLayoutResourceId:I

    .line 229
    return-void
.end method

.method public setLimit(I)V
    .locals 3
    .param p1, "limit"    # I

    .prologue
    .line 118
    iput p1, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mLimit:I

    .line 119
    :goto_0
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 120
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-super {p0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/weather/settings/Location;

    .line 121
    .local v0, "last":Lse/volvocars/acu/weather/settings/Location;
    invoke-super {p0, v0}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    goto :goto_0

    .line 123
    .end local v0    # "last":Lse/volvocars/acu/weather/settings/Location;
    :cond_0
    return-void
.end method

.method public startDrag(I)V
    .locals 3
    .param p1, "newIndex"    # I

    .prologue
    .line 200
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 201
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Index too small or too large! Index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_1
    iput p1, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    .line 204
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->notifyDataSetChanged()V

    .line 206
    return-void
.end method

.method public stopDrag()V
    .locals 1

    .prologue
    .line 209
    const/4 v0, -0x1

    iput v0, p0, Lse/volvocars/acu/weather/settings/LocationAdapter;->mEmptyIndex:I

    .line 210
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->notifyDataSetChanged()V

    .line 212
    return-void
.end method
