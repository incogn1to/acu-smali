.class public final Lse/volvocars/acu/weather/settings/LocationFlingController;
.super Ljava/lang/Object;
.source "LocationFlingController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private mDownX:I

.field private mOnLocationFlingListener:Lse/volvocars/acu/weather/settings/OnLocationFlingListener;

.field private mSwipeMargin:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "swipeMargin"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lse/volvocars/acu/weather/settings/LocationFlingController;->mSwipeMargin:I

    .line 22
    return-void
.end method

.method private onSwipe(I)Z
    .locals 2
    .param p1, "delta"    # I

    .prologue
    const/4 v1, 0x1

    .line 45
    iget v0, p0, Lse/volvocars/acu/weather/settings/LocationFlingController;->mSwipeMargin:I

    if-le p1, v0, :cond_0

    .line 47
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationFlingController;->mOnLocationFlingListener:Lse/volvocars/acu/weather/settings/OnLocationFlingListener;

    invoke-interface {v0, p1}, Lse/volvocars/acu/weather/settings/OnLocationFlingListener;->onFling(I)V

    move v0, v1

    .line 54
    :goto_0
    return v0

    .line 49
    :cond_0
    iget v0, p0, Lse/volvocars/acu/weather/settings/LocationFlingController;->mSwipeMargin:I

    neg-int v0, v0

    if-ge p1, v0, :cond_1

    .line 51
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationFlingController;->mOnLocationFlingListener:Lse/volvocars/acu/weather/settings/OnLocationFlingListener;

    invoke-interface {v0, p1}, Lse/volvocars/acu/weather/settings/OnLocationFlingListener;->onFling(I)V

    move v0, v1

    .line 52
    goto :goto_0

    :cond_1
    move v0, v1

    .line 54
    goto :goto_0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 31
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    move v1, v2

    .line 39
    :goto_0
    return v1

    .line 33
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lse/volvocars/acu/weather/settings/LocationFlingController;->mDownX:I

    move v1, v2

    .line 34
    goto :goto_0

    .line 36
    :pswitch_1
    iget v1, p0, Lse/volvocars/acu/weather/settings/LocationFlingController;->mDownX:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    sub-int v0, v1, v2

    .line 37
    .local v0, "dx":I
    invoke-direct {p0, v0}, Lse/volvocars/acu/weather/settings/LocationFlingController;->onSwipe(I)Z

    move-result v1

    goto :goto_0

    .line 31
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOnLocationFlingListener(Lse/volvocars/acu/weather/settings/OnLocationFlingListener;)V
    .locals 0
    .param p1, "listener"    # Lse/volvocars/acu/weather/settings/OnLocationFlingListener;

    .prologue
    .line 25
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationFlingController;->mOnLocationFlingListener:Lse/volvocars/acu/weather/settings/OnLocationFlingListener;

    .line 26
    return-void
.end method
