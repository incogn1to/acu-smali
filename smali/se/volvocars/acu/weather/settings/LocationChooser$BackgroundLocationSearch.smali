.class Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;
.super Landroid/os/AsyncTask;
.source "LocationChooser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/LocationChooser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackgroundLocationSearch"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lse/volvocars/acu/weather/settings/Location;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/LocationChooser;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/weather/settings/LocationChooser;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/weather/settings/LocationChooser;Lse/volvocars/acu/weather/settings/LocationChooser$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;
    .param p2, "x1"    # Lse/volvocars/acu/weather/settings/LocationChooser$1;

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;-><init>(Lse/volvocars/acu/weather/settings/LocationChooser;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 130
    check-cast p1, [Ljava/lang/CharSequence;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->doInBackground([Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 6
    .param p1, "params"    # [Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    const/4 v4, 0x0

    aget-object v3, p1, v4

    .line 136
    .local v3, "s":Ljava/lang/CharSequence;
    const/4 v1, 0x0

    .line 137
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/settings/Location;>;"
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_0

    .line 140
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    const-string v5, "location"

    invoke-virtual {v4, v5}, Lse/volvocars/acu/weather/settings/LocationChooser;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    .line 141
    .local v2, "manager":Landroid/location/LocationManager;
    const-string v4, "gps"

    invoke-virtual {v2, v4}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 143
    .local v0, "currentLocation":Landroid/location/Location;
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mProvider:Lse/volvocars/acu/weather/WeatherProvider;
    invoke-static {v4}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$700(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/WeatherProvider;

    move-result-object v4

    invoke-interface {v4, v3, v0}, Lse/volvocars/acu/weather/WeatherProvider;->searchLocation(Ljava/lang/CharSequence;Landroid/location/Location;)Ljava/util/List;

    move-result-object v1

    .line 145
    .end local v0    # "currentLocation":Landroid/location/Location;
    .end local v2    # "manager":Landroid/location/LocationManager;
    :cond_0
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 146
    const/4 v4, 0x0

    .line 151
    :goto_0
    return-object v4

    :cond_1
    move-object v4, v1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 130
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 156
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/settings/Location;>;"
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 157
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 158
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$300(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->clear()V

    .line 160
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->noMatchTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$400(Lse/volvocars/acu/weather/settings/LocationChooser;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    :cond_1
    :goto_0
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$300(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->notifyDataSetChanged()V

    .line 169
    return-void

    .line 164
    :cond_2
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->noMatchTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$400(Lse/volvocars/acu/weather/settings/LocationChooser;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$300(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->clear()V

    .line 166
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$300(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchListAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->addList(Ljava/util/List;)V

    goto :goto_0
.end method
