.class public Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;
.super Landroid/widget/ArrayAdapter;
.source "LocationGalleryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$1;,
        Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;,
        Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;,
        Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;,
        Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final DAY_FIRST_QUARTER:I = 0x6

.field private static final DAY_FOURTH_QUARTER:I = 0x18

.field private static final DAY_SECOND_QUARTER:I = 0xc

.field private static final DAY_THIRD_QUARTER:I = 0x12

.field public static final LAYOUT_SEVEN_DAYS:I = 0x1

.field public static final LAYOUT_TWENTY_FOUR_HOUR:I = 0x0

.field private static final ONE_HOUR:I = 0x36ee80

.field private static final ONE_HOUR_IN_SEC:I = 0xe10

.field private static final ONE_SECOND:I = 0x3e8


# instance fields
.field private mActiveLayout:I

.field private mContext:Landroid/content/Context;

.field private mForecastViews24H:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mForecastViews7D:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mInflator:Landroid/view/LayoutInflater;

.field private mLocationData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation
.end field

.field private mTemperatureUnit:I

.field private mWindRepresentation:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mLocationData:Ljava/util/List;

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mActiveLayout:I

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mLocationData:Ljava/util/List;

    .line 58
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mInflator:Landroid/view/LayoutInflater;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews7D:Ljava/util/Map;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews24H:Ljava/util/Map;

    .line 62
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mContext:Landroid/content/Context;

    .line 63
    return-void
.end method

.method static synthetic access$200(Ljava/util/Date;Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/util/Date;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-static {p0, p1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getFormattedDateString(Ljava/util/Date;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getUnitStringFromConstant(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private clearAdapter()V
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 421
    return-void
.end method

.method private static fill24HItem(Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;ILse/volvocars/acu/weather/WeatherProvider;Lse/volvocars/acu/weather/settings/Location;Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;ILandroid/content/Context;I)V
    .locals 9
    .param p0, "hourItem"    # Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;
    .param p1, "hourBlock"    # I
    .param p2, "weatherprovider"    # Lse/volvocars/acu/weather/WeatherProvider;
    .param p3, "location"    # Lse/volvocars/acu/weather/settings/Location;
    .param p4, "windRepresentation"    # Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;
    .param p5, "temperatureUnit"    # I
    .param p6, "context"    # Landroid/content/Context;
    .param p7, "currentDate"    # I

    .prologue
    .line 151
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 152
    .local v0, "now":J
    const-wide/32 v2, 0x1499700

    .line 154
    .local v2, "sixhours":J
    new-instance v3, Ljava/util/Date;

    const-wide/32 v4, 0x1499700

    int-to-long v6, p1

    mul-long/2addr v4, v6

    add-long/2addr v4, v0

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 155
    .end local v2    # "sixhours":J
    .local v3, "from":Ljava/util/Date;
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    .line 157
    .local v8, "to":Ljava/util/Date;
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/util/Date;->setMinutes(I)V

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/util/Date;->setSeconds(I)V

    .line 159
    invoke-virtual {v3}, Ljava/util/Date;->getHours()I

    move-result v2

    const/4 v4, 0x6

    if-ge v2, v4, :cond_2

    .line 160
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/util/Date;->setHours(I)V

    .line 168
    :cond_0
    :goto_0
    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/32 v6, 0x1499700

    add-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    sub-long/2addr v4, v6

    invoke-virtual {v8, v4, v5}, Ljava/util/Date;->setTime(J)V

    .line 171
    if-nez p1, :cond_1

    .line 172
    invoke-virtual {v3, v0, v1}, Ljava/util/Date;->setTime(J)V

    .line 175
    :cond_1
    new-instance v0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;

    .end local v0    # "now":J
    const/4 v7, 0x0

    move-object v1, p4

    move-object v2, p6

    move/from16 v4, p7

    move-object v5, p0

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;-><init>(Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;Landroid/content/Context;Ljava/util/Date;ILse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;ILse/volvocars/acu/weather/settings/LocationGalleryAdapter$1;)V

    .line 178
    .local v0, "callback":Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;
    invoke-interface {p2, p3, v3, v8, v0}, Lse/volvocars/acu/weather/WeatherProvider;->runWeatherSearch(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;)V

    .line 180
    return-void

    .line 161
    .local v0, "now":J
    :cond_2
    invoke-virtual {v3}, Ljava/util/Date;->getHours()I

    move-result v2

    const/16 v4, 0xc

    if-ge v2, v4, :cond_3

    .line 162
    const/4 v2, 0x6

    invoke-virtual {v3, v2}, Ljava/util/Date;->setHours(I)V

    goto :goto_0

    .line 163
    :cond_3
    invoke-virtual {v3}, Ljava/util/Date;->getHours()I

    move-result v2

    const/16 v4, 0x12

    if-ge v2, v4, :cond_4

    .line 164
    const/16 v2, 0xc

    invoke-virtual {v3, v2}, Ljava/util/Date;->setHours(I)V

    goto :goto_0

    .line 165
    :cond_4
    invoke-virtual {v3}, Ljava/util/Date;->getHours()I

    move-result v2

    const/16 v4, 0x18

    if-ge v2, v4, :cond_0

    .line 166
    const/16 v2, 0x12

    invoke-virtual {v3, v2}, Ljava/util/Date;->setHours(I)V

    goto :goto_0
.end method

.method private static fillSevenDayForecastItem(Lse/volvocars/acu/weather/settings/SevenDayForecastItem;ILse/volvocars/acu/weather/WeatherProvider;Lse/volvocars/acu/weather/settings/Location;ILandroid/content/Context;)V
    .locals 7
    .param p0, "dayItem"    # Lse/volvocars/acu/weather/settings/SevenDayForecastItem;
    .param p1, "day"    # I
    .param p2, "weatherprovider"    # Lse/volvocars/acu/weather/WeatherProvider;
    .param p3, "location"    # Lse/volvocars/acu/weather/settings/Location;
    .param p4, "temperatureUnit"    # I
    .param p5, "context"    # Landroid/content/Context;

    .prologue
    .line 111
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 112
    .local v0, "now":J
    const-wide/32 v3, 0x5265c00

    .line 114
    .local v3, "twentyfourhours":J
    new-instance v2, Ljava/util/Date;

    int-to-long v5, p1

    mul-long/2addr v3, v5

    add-long/2addr v0, v3

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 115
    .end local v0    # "now":J
    .end local v3    # "twentyfourhours":J
    .local v2, "from":Ljava/util/Date;
    new-instance p1, Ljava/util/Date;

    .end local p1    # "day":I
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-direct {p1, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 117
    .local p1, "to":Ljava/util/Date;
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/Date;->setHours(I)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/Date;->setMinutes(I)V

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/util/Date;->setSeconds(I)V

    .line 118
    const/16 v0, 0x17

    invoke-virtual {p1, v0}, Ljava/util/Date;->setHours(I)V

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Ljava/util/Date;->setMinutes(I)V

    const/16 v0, 0x3b

    invoke-virtual {p1, v0}, Ljava/util/Date;->setSeconds(I)V

    .line 120
    new-instance v0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;

    const/4 v5, 0x0

    move-object v1, p5

    move-object v3, p0

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;-><init>(Landroid/content/Context;Ljava/util/Date;Lse/volvocars/acu/weather/settings/SevenDayForecastItem;ILse/volvocars/acu/weather/settings/LocationGalleryAdapter$1;)V

    .line 121
    .local v0, "callback":Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;
    invoke-interface {p2, p3, v2, p1, v0}, Lse/volvocars/acu/weather/WeatherProvider;->runWeatherSearch(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;)V

    .line 123
    return-void
.end method

.method private static getFormattedDateString(Ljava/util/Date;Landroid/content/Context;)Ljava/lang/String;
    .locals 11
    .param p0, "date"    # Ljava/util/Date;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string v10, "/"

    const-string v9, "%tm"

    const-string v8, "%td"

    .line 128
    const-string v2, ""

    .line 129
    .local v2, "dateString":Ljava/lang/String;
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v1

    .line 130
    .local v1, "dateOrder":[C
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v4, 0x3

    if-ge v3, v4, :cond_0

    .line 131
    aget-char v4, v1, v3

    invoke-static {v4}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    .line 132
    .local v0, "c":Ljava/lang/Character;
    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v4

    const/16 v5, 0x59

    if-eq v4, v5, :cond_2

    .line 133
    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v4

    const/16 v5, 0x4d

    if-ne v4, v5, :cond_1

    .line 134
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%tm"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-static {v9, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%td"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-static {v8, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 141
    .end local v0    # "c":Ljava/lang/Character;
    :cond_0
    :goto_1
    return-object v2

    .line 136
    .restart local v0    # "c":Ljava/lang/Character;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%td"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-static {v8, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%tm"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-static {v9, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 138
    goto :goto_1

    .line 130
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private getUnitStringFromConstant(I)Ljava/lang/String;
    .locals 2
    .param p1, "constant"    # I

    .prologue
    .line 483
    packed-switch p1, :pswitch_data_0

    .line 499
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 485
    :pswitch_0
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 487
    :pswitch_1
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 489
    :pswitch_2
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 491
    :pswitch_3
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 493
    :pswitch_4
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 495
    :pswitch_5
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 497
    :pswitch_6
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 483
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private prepareSevenDaysForecastLayout(ILandroid/view/View;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 308
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v9

    .line 309
    .local v9, "now":J
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;

    .line 310
    .local v11, "vp":Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;
    if-nez v11, :cond_0

    .line 311
    new-instance v11, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;

    .end local v11    # "vp":Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;
    const/4 v1, 0x0

    invoke-direct {v11, v1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;-><init>(Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$1;)V

    .line 312
    .restart local v11    # "vp":Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->setTag(Ljava/lang/Object;)V

    .line 313
    const-wide/16 v5, 0x0

    invoke-virtual {v11, v5, v6}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->setLastPreparedTime(J)V

    .line 317
    :cond_0
    invoke-virtual {v11}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->getLastPreparedTime()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    sub-long v7, v9, v7

    cmp-long v1, v5, v7

    if-lez v1, :cond_1

    .line 347
    .end local p1    # "position":I
    :goto_0
    return-object p2

    .line 321
    .restart local p1    # "position":I
    :cond_1
    invoke-virtual {v11, v9, v10}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->setLastPreparedTime(J)V

    .line 324
    invoke-virtual {v11}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    .line 325
    const/4 v1, 0x7

    new-array v7, v1, [Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    const/4 v2, 0x0

    const v1, 0x7f070047

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    aput-object v1, v7, v2

    const/4 v2, 0x1

    const v1, 0x7f070048

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    aput-object v1, v7, v2

    const/4 v2, 0x2

    const v1, 0x7f070049

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    aput-object v1, v7, v2

    const/4 v2, 0x3

    const v1, 0x7f07004a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    aput-object v1, v7, v2

    const/4 v2, 0x4

    const v1, 0x7f07004b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    aput-object v1, v7, v2

    const/4 v2, 0x5

    const v1, 0x7f07004c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    aput-object v1, v7, v2

    const/4 v2, 0x6

    const v1, 0x7f07004d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    aput-object v1, v7, v2

    .line 334
    .local v7, "dayItems":[Lse/volvocars/acu/weather/settings/SevenDayForecastItem;
    invoke-virtual {v11, v7}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->setTag(Ljava/lang/Object;)V

    .line 337
    .end local v7    # "dayItems":[Lse/volvocars/acu/weather/settings/SevenDayForecastItem;
    :cond_2
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lse/volvocars/acu/weather/WeatherProviders;->getDefaultProvider(Landroid/content/Context;)Lse/volvocars/acu/weather/WeatherProvider;

    move-result-object v3

    .line 338
    .local v3, "weatherprovider":Lse/volvocars/acu/weather/WeatherProvider;
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lse/volvocars/acu/weather/settings/Location;

    .line 339
    .local v4, "location":Lse/volvocars/acu/weather/settings/Location;
    invoke-virtual {v11}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->getTag()Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "position":I
    check-cast p1, [Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    move-object v0, p1

    check-cast v0, [Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    move-object v8, v0

    .line 341
    .local v8, "items":[Lse/volvocars/acu/weather/settings/SevenDayForecastItem;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/4 v1, 0x7

    if-ge v2, v1, :cond_3

    .line 342
    aget-object v1, v8, v2

    iget v5, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mTemperatureUnit:I

    iget-object v6, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mContext:Landroid/content/Context;

    invoke-static/range {v1 .. v6}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->fillSevenDayForecastItem(Lse/volvocars/acu/weather/settings/SevenDayForecastItem;ILse/volvocars/acu/weather/WeatherProvider;Lse/volvocars/acu/weather/settings/Location;ILandroid/content/Context;)V

    .line 341
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 345
    :cond_3
    invoke-virtual {p2, v11}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private prepareTwentyFourHourLayout(ILandroid/view/View;)Landroid/view/View;
    .locals 15
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 354
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    .line 355
    .local v12, "now":J
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;

    .line 356
    .local v14, "vp":Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;
    if-nez v14, :cond_0

    .line 357
    new-instance v14, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;

    .end local v14    # "vp":Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;
    const/4 v2, 0x0

    invoke-direct {v14, v2}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;-><init>(Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$1;)V

    .line 358
    .restart local v14    # "vp":Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;
    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->setTag(Ljava/lang/Object;)V

    .line 359
    const-wide/16 v6, 0x0

    invoke-virtual {v14, v6, v7}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->setLastPreparedTime(J)V

    .line 362
    :cond_0
    invoke-virtual {v14}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->getLastPreparedTime()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    sub-long v8, v12, v8

    cmp-long v2, v6, v8

    if-lez v2, :cond_1

    .line 390
    .end local p1    # "position":I
    :goto_0
    return-object p2

    .line 366
    .restart local p1    # "position":I
    :cond_1
    invoke-virtual {v14, v12, v13}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->setLastPreparedTime(J)V

    .line 368
    invoke-virtual {v14}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    .line 369
    const/4 v2, 0x4

    new-array v10, v2, [Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;

    const/4 v3, 0x0

    const v2, 0x7f07004f

    move-object/from16 v0, p2

    move v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;

    aput-object v2, v10, v3

    const/4 v3, 0x1

    const v2, 0x7f070050

    move-object/from16 v0, p2

    move v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;

    aput-object v2, v10, v3

    const/4 v3, 0x2

    const v2, 0x7f070051

    move-object/from16 v0, p2

    move v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;

    aput-object v2, v10, v3

    const/4 v3, 0x3

    const v2, 0x7f070052

    move-object/from16 v0, p2

    move v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;

    aput-object v2, v10, v3

    .line 375
    .local v10, "hourItems":[Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;
    invoke-virtual {v14, v10}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->setTag(Ljava/lang/Object;)V

    .line 378
    .end local v10    # "hourItems":[Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;
    :cond_2
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lse/volvocars/acu/weather/WeatherProviders;->getDefaultProvider(Landroid/content/Context;)Lse/volvocars/acu/weather/WeatherProvider;

    move-result-object v4

    .line 379
    .local v4, "weatherprovider":Lse/volvocars/acu/weather/WeatherProvider;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mLocationData:Ljava/util/List;

    move-object v0, v2

    move/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lse/volvocars/acu/weather/settings/Location;

    .line 381
    .local v5, "location":Lse/volvocars/acu/weather/settings/Location;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getDate()I

    move-result v9

    .line 382
    .local v9, "currentDate":I
    invoke-virtual {v14}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->getTag()Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "position":I
    check-cast p1, [Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;

    move-object/from16 v0, p1

    check-cast v0, [Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;

    move-object v11, v0

    .line 384
    .local v11, "items":[Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    const/4 v2, 0x4

    if-ge v3, v2, :cond_3

    .line 385
    aget-object v2, v11, v3

    iget-object v6, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mWindRepresentation:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;

    iget v7, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mTemperatureUnit:I

    iget-object v8, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mContext:Landroid/content/Context;

    invoke-static/range {v2 .. v9}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->fill24HItem(Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;ILse/volvocars/acu/weather/WeatherProvider;Lse/volvocars/acu/weather/settings/Location;Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;ILandroid/content/Context;I)V

    .line 384
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 388
    :cond_3
    move-object/from16 v0, p2

    move-object v1, v14

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public flip()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 465
    iget v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mActiveLayout:I

    if-ne v0, v1, :cond_0

    .line 466
    const/4 v0, 0x0

    iput v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mActiveLayout:I

    .line 471
    :goto_0
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->notifyDataSetChanged()V

    .line 472
    return-void

    .line 468
    :cond_0
    iput v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mActiveLayout:I

    goto :goto_0
.end method

.method public getActiveLayout()I
    .locals 1

    .prologue
    .line 475
    iget v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mActiveLayout:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getLocation(I)Lse/volvocars/acu/weather/settings/Location;
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 434
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "this":Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;
    check-cast p0, Lse/volvocars/acu/weather/settings/Location;

    return-object p0
.end method

.method public getLocationData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 427
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mLocationData:Ljava/util/List;

    return-object v0
.end method

.method public getPosition(Lse/volvocars/acu/weather/settings/Location;)I
    .locals 3
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    .line 442
    const/4 v1, 0x0

    .line 443
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 444
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Lse/volvocars/acu/weather/settings/Location;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 445
    move v1, v0

    .line 449
    :cond_0
    return v1

    .line 443
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 72
    const/4 v0, 0x0

    .line 74
    .local v0, "view":Landroid/view/View;
    iget v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mActiveLayout:I

    packed-switch v1, :pswitch_data_0

    .line 102
    :goto_0
    return-object v0

    .line 77
    :pswitch_0
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews7D:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews7D:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 84
    .restart local v0    # "view":Landroid/view/View;
    :goto_1
    invoke-direct {p0, p1, v0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->prepareSevenDaysForecastLayout(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 86
    goto :goto_0

    .line 80
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mInflator:Landroid/view/LayoutInflater;

    const v2, 0x7f030016

    invoke-virtual {v1, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/LinearLayout;

    .line 81
    .restart local v0    # "view":Landroid/view/View;
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews7D:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 90
    :pswitch_1
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews24H:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews24H:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/view/View;

    .line 97
    .restart local v0    # "view":Landroid/view/View;
    :goto_2
    invoke-direct {p0, p1, v0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->prepareTwentyFourHourLayout(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 93
    :cond_1
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mInflator:Landroid/view/LayoutInflater;

    const v2, 0x7f030017

    invoke-virtual {v1, v2, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .end local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/LinearLayout;

    .line 94
    .restart local v0    # "view":Landroid/view/View;
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews24H:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setLocationList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 407
    .local p1, "locations":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/settings/Location;>;"
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->clearAdapter()V

    .line 409
    if-nez p1, :cond_1

    .line 416
    :cond_0
    return-void

    .line 413
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/Location;

    .line 414
    .local v1, "item":Lse/volvocars/acu/weather/settings/Location;
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getLocationData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setTemperatureUnit(I)V
    .locals 1
    .param p1, "tempUnit"    # I

    .prologue
    .line 518
    iput p1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mTemperatureUnit:I

    .line 519
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews7D:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 520
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews24H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 521
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->notifyDataSetChanged()V

    .line 522
    return-void
.end method

.method public setWindUnit(I)V
    .locals 1
    .param p1, "windUnit"    # I

    .prologue
    .line 507
    new-instance v0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;

    invoke-direct {v0, p0, p1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;-><init>(Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;I)V

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mWindRepresentation:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;

    .line 508
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews7D:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 509
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mForecastViews24H:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 510
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->notifyDataSetChanged()V

    .line 511
    return-void
.end method

.method public swap(Lse/volvocars/acu/weather/settings/Location;I)V
    .locals 1
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;
    .param p2, "i"    # I

    .prologue
    .line 457
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->mLocationData:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 458
    return-void
.end method
