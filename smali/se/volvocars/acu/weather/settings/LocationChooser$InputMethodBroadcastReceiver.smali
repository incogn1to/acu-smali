.class Lse/volvocars/acu/weather/settings/LocationChooser$InputMethodBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocationChooser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/LocationChooser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InputMethodBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/LocationChooser;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/weather/settings/LocationChooser;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationChooser$InputMethodBroadcastReceiver;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/weather/settings/LocationChooser;Lse/volvocars/acu/weather/settings/LocationChooser$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;
    .param p2, "x1"    # Lse/volvocars/acu/weather/settings/LocationChooser$1;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/LocationChooser$InputMethodBroadcastReceiver;-><init>(Lse/volvocars/acu/weather/settings/LocationChooser;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 63
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$InputMethodBroadcastReceiver;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "default_input_method"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.inputmethod.pinyin/.PinyinIME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$InputMethodBroadcastReceiver;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->editor:Lse/volvocars/acu/weather/settings/SearchEditText;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$100(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchEditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/SearchEditText;->setImeOptions(I)V

    .line 65
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$InputMethodBroadcastReceiver;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->editor:Lse/volvocars/acu/weather/settings/SearchEditText;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$100(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchEditText;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/SearchEditText;->setImeOptions(I)V

    .line 71
    :goto_0
    return-void

    .line 69
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$InputMethodBroadcastReceiver;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->editor:Lse/volvocars/acu/weather/settings/SearchEditText;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$100(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchEditText;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/SearchEditText;->setImeOptions(I)V

    goto :goto_0
.end method
