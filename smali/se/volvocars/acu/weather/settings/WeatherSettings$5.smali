.class Lse/volvocars/acu/weather/settings/WeatherSettings$5;
.super Ljava/lang/Object;
.source "WeatherSettings.java"

# interfaces
.implements Lse/volvocars/acu/weather/settings/UnitChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/weather/settings/WeatherSettings;->onPrepareOptionsMenu(Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;


# direct methods
.method constructor <init>(Lse/volvocars/acu/weather/settings/WeatherSettings;)V
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$5;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSmhiTargetChanged(I)V
    .locals 3
    .param p1, "smhiTarget"    # I

    .prologue
    .line 448
    const-string v0, "WeatherSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSmhiTargetChanged -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    return-void
.end method

.method public onTempUnitChanged(I)V
    .locals 4
    .param p1, "tempUnit"    # I

    .prologue
    .line 439
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$5;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    # getter for: Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;
    invoke-static {v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->access$000(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-result-object v1

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->setTemperatureUnit(I)V

    .line 441
    const-string v1, "WeatherSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTempUnitChanged -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    const/4 v1, 0x5

    if-ne p1, v1, :cond_0

    sget-object v1, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    move-object v0, v1

    .line 443
    .local v0, "unit":Lse/volvocars/acu/weather/Temperature$Unit;
    :goto_0
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$5;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    # getter for: Lse/volvocars/acu/weather/settings/WeatherSettings;->mWeatherSettingsTitleView:Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;
    invoke-static {v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->access$300(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->setFormat(Lse/volvocars/acu/weather/Temperature$Unit;)V

    .line 444
    return-void

    .line 442
    .end local v0    # "unit":Lse/volvocars/acu/weather/Temperature$Unit;
    :cond_0
    sget-object v1, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    move-object v0, v1

    goto :goto_0
.end method

.method public onWindUnitChanged(I)V
    .locals 1
    .param p1, "windUnit"    # I

    .prologue
    .line 434
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$5;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    # getter for: Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->access$000(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->setWindUnit(I)V

    .line 435
    return-void
.end method
