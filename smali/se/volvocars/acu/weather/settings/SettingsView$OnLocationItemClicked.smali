.class Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemClicked;
.super Ljava/lang/Object;
.source "SettingsView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/SettingsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnLocationItemClicked"
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/SettingsView;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/weather/settings/SettingsView;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemClicked;->this$0:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/weather/settings/SettingsView;Lse/volvocars/acu/weather/settings/SettingsView$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/weather/settings/SettingsView;
    .param p2, "x1"    # Lse/volvocars/acu/weather/settings/SettingsView$1;

    .prologue
    .line 284
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemClicked;-><init>(Lse/volvocars/acu/weather/settings/SettingsView;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 290
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemClicked;->this$0:Lse/volvocars/acu/weather/settings/SettingsView;

    # getter for: Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDelete:Z
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/SettingsView;->access$200(Lse/volvocars/acu/weather/settings/SettingsView;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 291
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemClicked;->this$0:Lse/volvocars/acu/weather/settings/SettingsView;

    # getter for: Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/SettingsView;->access$300(Lse/volvocars/acu/weather/settings/SettingsView;)Lse/volvocars/acu/weather/settings/LocationAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/weather/settings/Location;

    .line 292
    .local v0, "location":Lse/volvocars/acu/weather/settings/Location;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemClicked;->this$0:Lse/volvocars/acu/weather/settings/SettingsView;

    # getter for: Lse/volvocars/acu/weather/settings/SettingsView;->mSettings:Landroid/content/SharedPreferences;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/SettingsView;->access$400(Lse/volvocars/acu/weather/settings/SettingsView;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 293
    .local v1, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "SelectedLocation"

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/Location;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 294
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 296
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemClicked;->this$0:Lse/volvocars/acu/weather/settings/SettingsView;

    # getter for: Lse/volvocars/acu/weather/settings/SettingsView;->mLocationChosenListener:Lse/volvocars/acu/weather/settings/LocationChoosenListener;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/SettingsView;->access$500(Lse/volvocars/acu/weather/settings/SettingsView;)Lse/volvocars/acu/weather/settings/LocationChoosenListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 297
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemClicked;->this$0:Lse/volvocars/acu/weather/settings/SettingsView;

    # getter for: Lse/volvocars/acu/weather/settings/SettingsView;->mLocationChosenListener:Lse/volvocars/acu/weather/settings/LocationChoosenListener;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/SettingsView;->access$500(Lse/volvocars/acu/weather/settings/SettingsView;)Lse/volvocars/acu/weather/settings/LocationChoosenListener;

    move-result-object v2

    invoke-interface {v2, v0}, Lse/volvocars/acu/weather/settings/LocationChoosenListener;->locationChoosen(Lse/volvocars/acu/weather/settings/Location;)V

    .line 300
    .end local v0    # "location":Lse/volvocars/acu/weather/settings/Location;
    .end local v1    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method
