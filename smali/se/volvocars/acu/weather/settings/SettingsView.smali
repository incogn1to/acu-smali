.class public Lse/volvocars/acu/weather/settings/SettingsView;
.super Landroid/widget/LinearLayout;
.source "SettingsView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/settings/SettingsView$1;,
        Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemSelected;,
        Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemClicked;
    }
.end annotation


# static fields
.field private static final ALPHA_FULL:I = 0xff

.field private static final ALPHA_FULL_LIST:I = 0x32

.field public static final EXTRA_CURRENT_LATITUDE:Ljava/lang/String; = "currentPositionLat"

.field public static final EXTRA_CURRENT_LONGITUDE:Ljava/lang/String; = "currentPositionLong"

.field private static final HITBOXMARGIN:I = 0xa

.field private static final ICON_HEIGTH:I = 0x32

.field private static final ICON_WIDTH:I = 0x32

.field private static final LOCATION_ADAPTER_LIMIT:I = 0xa

.field private static final TAG:Ljava/lang/String; = "WeatherSettings"


# instance fields
.field private mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

.field private mHitboxDeleteBtn:Landroid/graphics/Rect;

.field private mHitboxEditBtn:Landroid/graphics/Rect;

.field private mLocationChosenListener:Lse/volvocars/acu/weather/settings/LocationChoosenListener;

.field private mLocationInScreen:[I

.field private mPendingDelete:Z

.field private mPendingDeleteView:Landroid/view/ViewGroup;

.field private mSettings:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    iput-boolean v1, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDelete:Z

    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mLocationInScreen:[I

    .line 57
    const-string v0, "weatherPrefs"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mSettings:Landroid/content/SharedPreferences;

    .line 58
    return-void

    .line 44
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$200(Lse/volvocars/acu/weather/settings/SettingsView;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/SettingsView;

    .prologue
    .line 31
    iget-boolean v0, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDelete:Z

    return v0
.end method

.method static synthetic access$300(Lse/volvocars/acu/weather/settings/SettingsView;)Lse/volvocars/acu/weather/settings/LocationAdapter;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/SettingsView;

    .prologue
    .line 31
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lse/volvocars/acu/weather/settings/SettingsView;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/SettingsView;

    .prologue
    .line 31
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mSettings:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$500(Lse/volvocars/acu/weather/settings/SettingsView;)Lse/volvocars/acu/weather/settings/LocationChoosenListener;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/SettingsView;

    .prologue
    .line 31
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mLocationChosenListener:Lse/volvocars/acu/weather/settings/LocationChoosenListener;

    return-object v0
.end method

.method public static decodeLocationString(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p0, "pack"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 330
    if-nez p0, :cond_0

    .line 331
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 344
    :goto_0
    return-object v4

    .line 333
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 335
    .local v3, "locations":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/settings/Location;>;"
    const-string v4, ":"

    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 337
    .local v0, "data":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v4, v0

    if-ge v1, v4, :cond_2

    .line 338
    aget-object v4, v0, v1

    invoke-static {v4}, Lse/volvocars/acu/weather/settings/Location;->fromString(Ljava/lang/String;)Lse/volvocars/acu/weather/settings/Location;

    move-result-object v2

    .line 339
    .local v2, "item":Lse/volvocars/acu/weather/settings/Location;
    if-eqz v2, :cond_1

    .line 340
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v2    # "item":Lse/volvocars/acu/weather/settings/Location;
    :cond_2
    move-object v4, v3

    .line 344
    goto :goto_0
.end method

.method private loadPrefs()V
    .locals 4

    .prologue
    .line 168
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mSettings:Landroid/content/SharedPreferences;

    const-string v2, "locations"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 169
    .local v0, "storedLocations":Ljava/lang/String;
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    invoke-static {v0}, Lse/volvocars/acu/weather/settings/SettingsView;->decodeLocationString(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/LocationAdapter;->addList(Ljava/util/List;)V

    .line 171
    return-void
.end method

.method private setTextAndTypeface()V
    .locals 4

    .prologue
    .line 87
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "fonts/VolvoSanProLig.otf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 88
    .local v1, "typeface":Landroid/graphics/Typeface;
    const v2, 0x7f070044

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/SettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 89
    .local v0, "listHeader":Landroid/widget/TextView;
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 90
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    return-void
.end method

.method private updateAddLocationView()V
    .locals 5

    .prologue
    .line 178
    const v3, 0x7f070044

    invoke-virtual {p0, v3}, Lse/volvocars/acu/weather/settings/SettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 179
    .local v2, "text":Landroid/widget/TextView;
    const v3, 0x7f070045

    invoke-virtual {p0, v3}, Lse/volvocars/acu/weather/settings/SettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 180
    .local v0, "icon":Landroid/widget/ImageView;
    const v3, 0x7f070042

    invoke-virtual {p0, v3}, Lse/volvocars/acu/weather/settings/SettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 182
    .local v1, "layout":Landroid/widget/FrameLayout;
    iget-object v3, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    invoke-virtual {v3}, Lse/volvocars/acu/weather/settings/LocationAdapter;->listFull()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 183
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/16 v4, 0x32

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 184
    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 185
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060010

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 186
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    .line 194
    :goto_0
    return-void

    .line 189
    :cond_0
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/16 v4, 0xff

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 190
    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 191
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f06000d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 192
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setFocusable(Z)V

    goto :goto_0
.end method


# virtual methods
.method public addLocationToList(Lse/volvocars/acu/weather/settings/Location;)V
    .locals 3
    .param p1, "item"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    .line 201
    if-eqz p1, :cond_0

    .line 202
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lse/volvocars/acu/weather/settings/LocationAdapter;->insert(Lse/volvocars/acu/weather/settings/Location;I)V

    .line 205
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationAdapter;->notifyDataSetChanged()V

    .line 207
    const v1, 0x7f070043

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/SettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/weather/settings/TouchListView;

    .line 208
    .local v0, "locationList":Lse/volvocars/acu/weather/settings/TouchListView;
    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/TouchListView;->setSelectionAfterHeaderView()V

    .line 210
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->storePrefs()V

    .line 211
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->updateAddLocationView()V

    .line 212
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    .line 131
    iget-boolean v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDelete:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mHitboxDeleteBtn:Landroid/graphics/Rect;

    if-eqz v2, :cond_1

    .line 132
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 133
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 135
    .local v1, "y":F
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mHitboxDeleteBtn:Landroid/graphics/Rect;

    float-to-int v3, v0

    float-to-int v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mHitboxEditBtn:Landroid/graphics/Rect;

    float-to-int v3, v0

    float-to-int v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 136
    invoke-virtual {p0, v5}, Lse/volvocars/acu/weather/settings/SettingsView;->setPendingDelete(Z)V

    .line 137
    const/4 v2, 0x1

    .line 148
    .end local v0    # "x":F
    .end local v1    # "y":F
    :goto_0
    return v2

    .line 140
    .restart local v0    # "x":F
    .restart local v1    # "y":F
    :cond_0
    const-string v2, "WeatherSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Must have pressed delete: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mHitboxDeleteBtn:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " or edit: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mHitboxEditBtn:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->toShortString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", x: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", y: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    .end local v0    # "x":F
    .end local v1    # "y":F
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_0

    .line 146
    :cond_1
    invoke-virtual {p0, v5}, Lse/volvocars/acu/weather/settings/SettingsView;->setPendingDelete(Z)V

    goto :goto_1
.end method

.method getPendingDelete()Z
    .locals 1

    .prologue
    .line 278
    iget-boolean v0, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDelete:Z

    return v0
.end method

.method public isListFull()Z
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationAdapter;->listFull()Z

    move-result v0

    return v0
.end method

.method public onClickDelete(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 110
    iget-boolean v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDelete:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDeleteView:Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    .line 111
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 112
    .local v0, "group":Landroid/view/ViewGroup;
    const v2, 0x7f07001a

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/ListItemTextView;

    .line 113
    .local v1, "listItem":Lse/volvocars/acu/weather/settings/ListItemTextView;
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/SettingsView;->setPendingDelete(Z)V

    .line 114
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/ListItemTextView;->getLocation()Lse/volvocars/acu/weather/settings/Location;

    move-result-object v3

    invoke-virtual {v2, v3}, Lse/volvocars/acu/weather/settings/LocationAdapter;->removeLocation(Lse/volvocars/acu/weather/settings/Location;)V

    .line 115
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/LocationAdapter;->notifyDataSetChanged()V

    .line 116
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->updateAddLocationView()V

    .line 117
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->storePrefs()V

    .line 118
    iput-object v4, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDeleteView:Landroid/view/ViewGroup;

    .line 119
    iput-object v4, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mHitboxDeleteBtn:Landroid/graphics/Rect;

    .line 120
    iput-object v4, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mHitboxEditBtn:Landroid/graphics/Rect;

    .line 122
    .end local v0    # "group":Landroid/view/ViewGroup;
    .end local v1    # "listItem":Lse/volvocars/acu/weather/settings/ListItemTextView;
    :cond_0
    return-void
.end method

.method public onClickEdit(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 97
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDeleteView:Landroid/view/ViewGroup;

    .line 98
    iget-boolean v0, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDelete:Z

    if-eqz v0, :cond_0

    .line 99
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/SettingsView;->setPendingDelete(Z)V

    .line 103
    :goto_0
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/SettingsView;->setPendingDelete(Z)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 64
    .local v0, "locationData":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/settings/Location;>;"
    new-instance v2, Lse/volvocars/acu/weather/settings/LocationAdapter;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f030006

    invoke-direct {v2, v3, v4, v0}, Lse/volvocars/acu/weather/settings/LocationAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    .line 65
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lse/volvocars/acu/weather/settings/LocationAdapter;->setLimit(I)V

    .line 66
    const v2, 0x7f070043

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/SettingsView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/TouchListView;

    .line 67
    .local v1, "locationList":Lse/volvocars/acu/weather/settings/TouchListView;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/TouchListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 68
    new-instance v2, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemClicked;

    invoke-direct {v2, p0, v5}, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemClicked;-><init>(Lse/volvocars/acu/weather/settings/SettingsView;Lse/volvocars/acu/weather/settings/SettingsView$1;)V

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/TouchListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 69
    new-instance v2, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemSelected;

    invoke-direct {v2, p0, v5}, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemSelected;-><init>(Lse/volvocars/acu/weather/settings/SettingsView;Lse/volvocars/acu/weather/settings/SettingsView$1;)V

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/TouchListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 70
    invoke-virtual {v1, p0}, Lse/volvocars/acu/weather/settings/TouchListView;->setWeatherSettings(Lse/volvocars/acu/weather/settings/SettingsView;)V

    .line 72
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->setTextAndTypeface()V

    .line 73
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->loadPrefs()V

    .line 75
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/SettingsView;->updateAddLocationView()V

    .line 76
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/LocationAdapter;->notifyDataSetChanged()V

    .line 77
    return-void
.end method

.method public setLocationChoosenListener(Lse/volvocars/acu/weather/settings/LocationChoosenListener;)V
    .locals 0
    .param p1, "listener"    # Lse/volvocars/acu/weather/settings/LocationChoosenListener;

    .prologue
    .line 80
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mLocationChosenListener:Lse/volvocars/acu/weather/settings/LocationChoosenListener;

    .line 81
    return-void
.end method

.method setPendingDelete(Z)V
    .locals 12
    .param p1, "mode"    # Z

    .prologue
    .line 218
    iput-boolean p1, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDelete:Z

    .line 219
    iget-object v7, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDeleteView:Landroid/view/ViewGroup;

    if-eqz v7, :cond_0

    .line 222
    iget-object v7, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDeleteView:Landroid/view/ViewGroup;

    const v8, 0x7f07001b

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 223
    .local v1, "editBtn":Landroid/widget/ImageView;
    iget-object v7, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDeleteView:Landroid/view/ViewGroup;

    const v8, 0x7f070019

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 224
    .local v0, "deleteBtn":Landroid/widget/ImageView;
    iget-object v7, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDeleteView:Landroid/view/ViewGroup;

    const v8, 0x7f07001a

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lse/volvocars/acu/weather/settings/ListItemTextView;

    .line 226
    .local v5, "locationTxt":Lse/volvocars/acu/weather/settings/ListItemTextView;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    if-eqz v5, :cond_0

    .line 227
    iget-boolean v7, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDelete:Z

    if-eqz v7, :cond_1

    .line 228
    const v7, 0x7f020095

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 229
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 231
    invoke-virtual {v5}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setWidthShort()V

    .line 233
    const/16 v6, 0x32

    .line 234
    .local v6, "width":I
    const/16 v2, 0x32

    .line 236
    .local v2, "height":I
    iget-object v7, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mLocationInScreen:[I

    invoke-virtual {p0, v7}, Lse/volvocars/acu/weather/settings/SettingsView;->getLocationOnScreen([I)V

    .line 238
    const/4 v7, 0x2

    new-array v3, v7, [I

    fill-array-data v3, :array_0

    .line 239
    .local v3, "hitboxPositionDelete":[I
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->getLocationOnScreen([I)V

    .line 240
    const/4 v7, 0x0

    const/4 v8, 0x0

    aget v8, v3, v8

    iget-object v9, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mLocationInScreen:[I

    const/4 v10, 0x0

    aget v9, v9, v10

    sub-int/2addr v8, v9

    aput v8, v3, v7

    .line 241
    const/4 v7, 0x1

    const/4 v8, 0x1

    aget v8, v3, v8

    iget-object v9, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mLocationInScreen:[I

    const/4 v10, 0x1

    aget v9, v9, v10

    sub-int/2addr v8, v9

    aput v8, v3, v7

    .line 242
    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    aget v8, v3, v8

    const/16 v9, 0xa

    sub-int/2addr v8, v9

    const/4 v9, 0x1

    aget v9, v3, v9

    const/16 v10, 0xa

    sub-int/2addr v9, v10

    const/4 v10, 0x0

    aget v10, v3, v10

    add-int/2addr v10, v6

    add-int/lit8 v10, v10, 0xa

    const/4 v11, 0x1

    aget v11, v3, v11

    add-int/2addr v11, v2

    add-int/lit8 v11, v11, 0xa

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v7, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mHitboxDeleteBtn:Landroid/graphics/Rect;

    .line 244
    const/4 v7, 0x2

    new-array v4, v7, [I

    fill-array-data v4, :array_1

    .line 245
    .local v4, "hitboxPositionEdit":[I
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->getLocationOnScreen([I)V

    .line 246
    const/4 v7, 0x0

    const/4 v8, 0x0

    aget v8, v4, v8

    iget-object v9, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mLocationInScreen:[I

    const/4 v10, 0x0

    aget v9, v9, v10

    sub-int/2addr v8, v9

    aput v8, v4, v7

    .line 247
    const/4 v7, 0x1

    const/4 v8, 0x1

    aget v8, v4, v8

    iget-object v9, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mLocationInScreen:[I

    const/4 v10, 0x1

    aget v9, v9, v10

    sub-int/2addr v8, v9

    aput v8, v4, v7

    .line 248
    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    aget v8, v4, v8

    const/16 v9, 0xa

    sub-int/2addr v8, v9

    const/4 v9, 0x1

    aget v9, v4, v9

    const/16 v10, 0xa

    sub-int/2addr v9, v10

    const/4 v10, 0x0

    aget v10, v4, v10

    add-int/2addr v10, v6

    add-int/lit8 v10, v10, 0xa

    const/4 v11, 0x1

    aget v11, v4, v11

    add-int/2addr v11, v2

    add-int/lit8 v11, v11, 0xa

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v7, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mHitboxEditBtn:Landroid/graphics/Rect;

    .line 260
    .end local v2    # "height":I
    .end local v3    # "hitboxPositionDelete":[I
    .end local v4    # "hitboxPositionEdit":[I
    .end local v6    # "width":I
    :goto_0
    invoke-virtual {v5}, Lse/volvocars/acu/weather/settings/ListItemTextView;->getLocation()Lse/volvocars/acu/weather/settings/Location;

    move-result-object v7

    invoke-virtual {v5, v7}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setLocation(Lse/volvocars/acu/weather/settings/Location;)V

    .line 261
    invoke-virtual {v5}, Lse/volvocars/acu/weather/settings/ListItemTextView;->trimAndUpdate()V

    .line 263
    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 264
    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 265
    invoke-virtual {v5}, Lse/volvocars/acu/weather/settings/ListItemTextView;->invalidate()V

    .line 266
    iget-object v7, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDeleteView:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->invalidate()V

    .line 275
    .end local v0    # "deleteBtn":Landroid/widget/ImageView;
    .end local v1    # "editBtn":Landroid/widget/ImageView;
    .end local v5    # "locationTxt":Lse/volvocars/acu/weather/settings/ListItemTextView;
    :cond_0
    return-void

    .line 252
    .restart local v0    # "deleteBtn":Landroid/widget/ImageView;
    .restart local v1    # "editBtn":Landroid/widget/ImageView;
    .restart local v5    # "locationTxt":Lse/volvocars/acu/weather/settings/ListItemTextView;
    :cond_1
    const v7, 0x7f020098

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 253
    const/16 v7, 0x8

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 254
    invoke-virtual {v5}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setWidthLong()V

    .line 256
    const/4 v7, 0x0

    iput-object v7, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mHitboxDeleteBtn:Landroid/graphics/Rect;

    .line 257
    const/4 v7, 0x0

    iput-object v7, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mHitboxEditBtn:Landroid/graphics/Rect;

    goto :goto_0

    .line 238
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 244
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method storePrefs()V
    .locals 3

    .prologue
    .line 157
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mSettings:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 158
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "locations"

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SettingsView;->mAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/LocationAdapter;->getLocationString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 159
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 161
    return-void
.end method
