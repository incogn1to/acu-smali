.class public Lse/volvocars/acu/weather/settings/SearchListAdapter;
.super Lse/volvocars/acu/weather/settings/LocationAdapter;
.source "SearchListAdapter.java"


# instance fields
.field private mSearchString:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p3, "data":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/settings/Location;>;"
    invoke-direct {p0, p1, p2, p3}, Lse/volvocars/acu/weather/settings/LocationAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 17
    const-string v0, " "

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/SearchListAdapter;->mSearchString:Ljava/lang/CharSequence;

    .line 21
    invoke-virtual {p0, p1}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->setContext(Landroid/content/Context;)V

    .line 22
    invoke-virtual {p0, p2}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->setLayoutResourceId(I)V

    .line 23
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    .line 28
    move-object v3, p2

    .line 29
    .local v3, "row":Landroid/view/View;
    const/4 v0, 0x0

    .line 31
    .local v0, "holder":Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;
    if-nez v3, :cond_0

    .line 32
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 33
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->getLayoutResourceId()I

    move-result v4

    invoke-virtual {v1, v4, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 34
    new-instance v0, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;

    .end local v0    # "holder":Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;
    invoke-direct {v0}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;-><init>()V

    .line 35
    .restart local v0    # "holder":Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;
    const v4, 0x7f07001a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lse/volvocars/acu/weather/settings/ListItemTextView;

    invoke-virtual {v0, v4}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->setLocationView(Lse/volvocars/acu/weather/settings/ListItemTextView;)V

    .line 36
    invoke-virtual {v3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 38
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "holder":Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;
    check-cast v0, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;

    .line 39
    .restart local v0    # "holder":Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->getLocationData()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/weather/settings/Location;

    .line 41
    .local v2, "locationItem":Lse/volvocars/acu/weather/settings/Location;
    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/SearchListAdapter;->mSearchString:Ljava/lang/CharSequence;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 42
    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->getLocationView()Lse/volvocars/acu/weather/settings/ListItemTextView;

    move-result-object v4

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lse/volvocars/acu/weather/settings/SearchListAdapter;->mSearchString:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setCityText(Ljava/lang/String;I)V

    .line 47
    :goto_0
    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getArea()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/SearchListAdapter;->mSearchString:Ljava/lang/CharSequence;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 48
    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->getLocationView()Lse/volvocars/acu/weather/settings/ListItemTextView;

    move-result-object v4

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getArea()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lse/volvocars/acu/weather/settings/SearchListAdapter;->mSearchString:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setAreaText(Ljava/lang/String;I)V

    .line 53
    :goto_1
    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/SearchListAdapter;->mSearchString:Ljava/lang/CharSequence;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 54
    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->getLocationView()Lse/volvocars/acu/weather/settings/ListItemTextView;

    move-result-object v4

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getCountry()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lse/volvocars/acu/weather/settings/SearchListAdapter;->mSearchString:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setCountryText(Ljava/lang/String;I)V

    .line 58
    :goto_2
    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->getLocationView()Lse/volvocars/acu/weather/settings/ListItemTextView;

    move-result-object v4

    invoke-virtual {v4}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setWidthLong()V

    .line 59
    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->getLocationView()Lse/volvocars/acu/weather/settings/ListItemTextView;

    move-result-object v4

    invoke-virtual {v4}, Lse/volvocars/acu/weather/settings/ListItemTextView;->trimAndUpdate()V

    .line 61
    return-object v3

    .line 44
    :cond_1
    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->getLocationView()Lse/volvocars/acu/weather/settings/ListItemTextView;

    move-result-object v4

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v7}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setCityText(Ljava/lang/String;I)V

    goto :goto_0

    .line 50
    :cond_2
    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->getLocationView()Lse/volvocars/acu/weather/settings/ListItemTextView;

    move-result-object v4

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getArea()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v7}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setAreaText(Ljava/lang/String;I)V

    goto :goto_1

    .line 56
    :cond_3
    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationAdapter$LocationHolder;->getLocationView()Lse/volvocars/acu/weather/settings/ListItemTextView;

    move-result-object v4

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getCountry()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v7}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setCountryText(Ljava/lang/String;I)V

    goto :goto_2
.end method

.method public setSearchString(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "seq"    # Ljava/lang/CharSequence;

    .prologue
    .line 65
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/SearchListAdapter;->mSearchString:Ljava/lang/CharSequence;

    .line 66
    return-void
.end method
