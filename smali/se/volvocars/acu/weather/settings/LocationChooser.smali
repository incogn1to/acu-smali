.class public Lse/volvocars/acu/weather/settings/LocationChooser;
.super Landroid/app/Activity;
.source "LocationChooser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;,
        Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;,
        Lse/volvocars/acu/weather/settings/LocationChooser$InternetConnectionBroadcastReceiver;,
        Lse/volvocars/acu/weather/settings/LocationChooser$InputMethodBroadcastReceiver;
    }
.end annotation


# static fields
.field protected static final ADD_LOCATION_RESULT_KEY:Ljava/lang/String; = "location"

.field protected static final INPUT_METHOD_PINYIN:Ljava/lang/String; = "com.android.inputmethod.pinyin/.PinyinIME"

.field static final RESULT_STOPPED:I = 0xa

.field protected static final TAG:Ljava/lang/String; = "LocationChooser"


# instance fields
.field private editor:Lse/volvocars/acu/weather/settings/SearchEditText;

.field private inputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

.field private internetConnectionBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private listener:Landroid/widget/AdapterView$OnItemClickListener;

.field private locationList:Landroid/widget/ListView;

.field private mBackgroundSearch:Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;

.field private mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;

.field private mProvider:Lse/volvocars/acu/weather/WeatherProvider;

.field private mWatcher:Landroid/text/TextWatcher;

.field private noInternetTextView:Landroid/widget/TextView;

.field private noMatchTextView:Landroid/widget/TextView;

.field private softKeyboardHidden:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 46
    iput-object v1, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->mProvider:Lse/volvocars/acu/weather/WeatherProvider;

    .line 48
    new-instance v0, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;

    invoke-direct {v0, p0, v1}, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;-><init>(Lse/volvocars/acu/weather/settings/LocationChooser;Lse/volvocars/acu/weather/settings/LocationChooser$1;)V

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->mWatcher:Landroid/text/TextWatcher;

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->softKeyboardHidden:Z

    .line 179
    new-instance v0, Lse/volvocars/acu/weather/settings/LocationChooser$1;

    invoke-direct {v0, p0}, Lse/volvocars/acu/weather/settings/LocationChooser$1;-><init>(Lse/volvocars/acu/weather/settings/LocationChooser;)V

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$100(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchEditText;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;

    .prologue
    .line 40
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->editor:Lse/volvocars/acu/weather/settings/SearchEditText;

    return-object v0
.end method

.method static synthetic access$200(Lse/volvocars/acu/weather/settings/LocationChooser;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;

    .prologue
    .line 40
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->noInternetTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchListAdapter;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;

    .prologue
    .line 40
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lse/volvocars/acu/weather/settings/LocationChooser;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;

    .prologue
    .line 40
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->noMatchTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;

    .prologue
    .line 40
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->mBackgroundSearch:Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;

    return-object v0
.end method

.method static synthetic access$502(Lse/volvocars/acu/weather/settings/LocationChooser;Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;)Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;
    .param p1, "x1"    # Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;

    .prologue
    .line 40
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->mBackgroundSearch:Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;

    return-object p1
.end method

.method static synthetic access$700(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/WeatherProvider;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;

    .prologue
    .line 40
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->mProvider:Lse/volvocars/acu/weather/WeatherProvider;

    return-object v0
.end method

.method static synthetic access$802(Lse/volvocars/acu/weather/settings/LocationChooser;Z)Z
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    iput-boolean p1, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->softKeyboardHidden:Z

    return p1
.end method

.method private hideSoftInput()V
    .locals 4

    .prologue
    .line 313
    const/4 v2, 0x1

    iput-boolean v2, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->softKeyboardHidden:Z

    .line 314
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 315
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 316
    .local v0, "currentFocus":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 317
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 319
    :cond_0
    return-void
.end method

.method private hideSoftKeyboardIfOutsideHitbox(Landroid/view/MotionEvent;Landroid/view/View;FF)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "x"    # F
    .param p4, "y"    # F

    .prologue
    const/4 v5, 0x1

    .line 301
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 304
    .local v0, "hitbox":Landroid/graphics/Rect;
    iget-boolean v1, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->softKeyboardHidden:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v5, :cond_0

    float-to-int v1, p3

    float-to-int v2, p4

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 305
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->hideSoftInput()V

    .line 306
    const-string v1, "LocationChooser"

    const-string v2, "Hiding soft keyboard"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v5

    .line 309
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private showSoftKeyboardIfInsideHitbox(Landroid/view/MotionEvent;FFLandroid/view/View;)V
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "v"    # Landroid/view/View;

    .prologue
    .line 293
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p4}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p4}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p4}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p4}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 294
    .local v0, "hitbox":Landroid/graphics/Rect;
    iget-boolean v1, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->softKeyboardHidden:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    float-to-int v1, p2

    float-to-int v2, p3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    const/4 v1, 0x0

    iput-boolean v1, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->softKeyboardHidden:Z

    .line 298
    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    .line 270
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    .line 272
    .local v2, "view":Landroid/view/View;
    const/4 v5, 0x2

    new-array v0, v5, [I

    .line 273
    .local v0, "screenCoords":[I
    invoke-virtual {v2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 274
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    const/4 v6, 0x0

    aget v6, v0, v6

    int-to-float v6, v6

    sub-float v3, v5, v6

    .line 275
    .local v3, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    aget v6, v0, v7

    int-to-float v6, v6

    sub-float v4, v5, v6

    .line 277
    .local v4, "y":F
    instance-of v5, v2, Lse/volvocars/acu/weather/settings/SearchEditText;

    if-eqz v5, :cond_0

    invoke-direct {p0, p1, v2, v3, v4}, Lse/volvocars/acu/weather/settings/LocationChooser;->hideSoftKeyboardIfOutsideHitbox(Landroid/view/MotionEvent;Landroid/view/View;FF)Z

    move-result v5

    if-eqz v5, :cond_0

    move v5, v7

    .line 289
    :goto_0
    return v5

    .line 281
    :cond_0
    iget-boolean v5, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->softKeyboardHidden:Z

    if-eqz v5, :cond_1

    .line 282
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 283
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 285
    .local v1, "v":Landroid/view/View;
    instance-of v5, v1, Lse/volvocars/acu/weather/settings/SearchEditText;

    if-eqz v5, :cond_1

    .line 286
    invoke-direct {p0, p1, v3, v4, v1}, Lse/volvocars/acu/weather/settings/LocationChooser;->showSoftKeyboardIfInsideHitbox(Landroid/view/MotionEvent;FFLandroid/view/View;)V

    .end local v1    # "v":Landroid/view/View;
    :cond_1
    move v5, v7

    .line 289
    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 195
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 198
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lse/volvocars/acu/weather/WeatherProviders;->getDefaultProvider(Landroid/content/Context;)Lse/volvocars/acu/weather/WeatherProvider;

    move-result-object v4

    iput-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->mProvider:Lse/volvocars/acu/weather/WeatherProvider;

    .line 199
    const v4, 0x7f030013

    invoke-virtual {p0, v4}, Lse/volvocars/acu/weather/settings/LocationChooser;->setContentView(I)V

    .line 200
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    const-string v5, "fonts/VolvoSanProLig.otf"

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 201
    .local v3, "typeface":Landroid/graphics/Typeface;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 209
    .local v2, "locationData":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/weather/settings/Location;>;"
    new-instance v4, Lse/volvocars/acu/weather/settings/SearchListAdapter;

    const v5, 0x7f03000a

    invoke-direct {v4, p0, v5, v2}, Lse/volvocars/acu/weather/settings/SearchListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;

    .line 211
    const v4, 0x7f07003d

    invoke-virtual {p0, v4}, Lse/volvocars/acu/weather/settings/LocationChooser;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lse/volvocars/acu/weather/settings/SearchEditText;

    iput-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->editor:Lse/volvocars/acu/weather/settings/SearchEditText;

    .line 213
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->editor:Lse/volvocars/acu/weather/settings/SearchEditText;

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->mWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v4, v5}, Lse/volvocars/acu/weather/settings/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 215
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->editor:Lse/volvocars/acu/weather/settings/SearchEditText;

    new-instance v5, Lse/volvocars/acu/weather/settings/LocationChooser$2;

    invoke-direct {v5, p0}, Lse/volvocars/acu/weather/settings/LocationChooser$2;-><init>(Lse/volvocars/acu/weather/settings/LocationChooser;)V

    invoke-virtual {v4, v5}, Lse/volvocars/acu/weather/settings/SearchEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 228
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->editor:Lse/volvocars/acu/weather/settings/SearchEditText;

    invoke-virtual {v4, v3}, Lse/volvocars/acu/weather/settings/SearchEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 231
    new-instance v4, Lse/volvocars/acu/weather/settings/LocationChooser$InputMethodBroadcastReceiver;

    invoke-direct {v4, p0, v7}, Lse/volvocars/acu/weather/settings/LocationChooser$InputMethodBroadcastReceiver;-><init>(Lse/volvocars/acu/weather/settings/LocationChooser;Lse/volvocars/acu/weather/settings/LocationChooser$1;)V

    iput-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->inputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 233
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "default_input_method"

    invoke-static {v4, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.android.inputmethod.pinyin/.PinyinIME"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 234
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->editor:Lse/volvocars/acu/weather/settings/SearchEditText;

    const/high16 v5, 0x10000000

    invoke-virtual {v4, v5}, Lse/volvocars/acu/weather/settings/SearchEditText;->setImeOptions(I)V

    .line 237
    :cond_0
    const v4, 0x7f070040

    invoke-virtual {p0, v4}, Lse/volvocars/acu/weather/settings/LocationChooser;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->locationList:Landroid/widget/ListView;

    .line 238
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->locationList:Landroid/widget/ListView;

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 239
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->locationList:Landroid/widget/ListView;

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 246
    const v4, 0x7f07003c

    invoke-virtual {p0, v4}, Lse/volvocars/acu/weather/settings/LocationChooser;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/weather/settings/LinearLayoutFrame;

    .line 247
    .local v0, "frame":Lse/volvocars/acu/weather/settings/LinearLayoutFrame;
    invoke-virtual {v0, p0}, Lse/volvocars/acu/weather/settings/LinearLayoutFrame;->setActivity(Lse/volvocars/acu/weather/settings/LocationChooser;)V

    .line 250
    const v4, 0x7f07003e

    invoke-virtual {p0, v4}, Lse/volvocars/acu/weather/settings/LocationChooser;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->noMatchTextView:Landroid/widget/TextView;

    .line 251
    const v4, 0x7f07003f

    invoke-virtual {p0, v4}, Lse/volvocars/acu/weather/settings/LocationChooser;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->noInternetTextView:Landroid/widget/TextView;

    .line 252
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->noMatchTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 253
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->noInternetTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 256
    new-instance v4, Lse/volvocars/acu/weather/settings/LocationChooser$InternetConnectionBroadcastReceiver;

    invoke-direct {v4, p0, v7}, Lse/volvocars/acu/weather/settings/LocationChooser$InternetConnectionBroadcastReceiver;-><init>(Lse/volvocars/acu/weather/settings/LocationChooser;Lse/volvocars/acu/weather/settings/LocationChooser$1;)V

    iput-object v4, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->internetConnectionBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 258
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "input_method"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 259
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    const/4 v4, 0x2

    invoke-virtual {v1, v4, v6}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 260
    iput-boolean v6, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->softKeyboardHidden:Z

    .line 261
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 324
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->hideSoftInput()V

    .line 325
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 326
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 357
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    .line 358
    const/4 v0, 0x1

    .line 361
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->inputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 341
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->internetConnectionBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 342
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 343
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->hideSoftInput()V

    .line 345
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->setResult(I)V

    .line 346
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationChooser;->finish()V

    .line 347
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 330
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 331
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->inputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.INPUT_METHOD_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lse/volvocars/acu/weather/settings/LocationChooser;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 332
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->internetConnectionBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lse/volvocars/acu/weather/settings/LocationChooser;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 333
    return-void
.end method

.method public setIsKeyboardHidden(Z)V
    .locals 0
    .param p1, "hidden"    # Z

    .prologue
    .line 173
    iput-boolean p1, p0, Lse/volvocars/acu/weather/settings/LocationChooser;->softKeyboardHidden:Z

    .line 174
    return-void
.end method
