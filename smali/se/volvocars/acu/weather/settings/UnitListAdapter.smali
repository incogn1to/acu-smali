.class public Lse/volvocars/acu/weather/settings/UnitListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "UnitListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final GROUP_TEMPERATURE:I = 0x1

.field public static final GROUP_WIND:I = 0x0

.field public static final UNIT_TEMP_CELCIUS:I = 0x5

.field public static final UNIT_TEMP_FAHRENHEIT:I = 0x6

.field public static final UNIT_WIND_BEAUFORT:I = 0x4

.field public static final UNIT_WIND_KNOTS:I = 0x0

.field public static final UNIT_WIND_KPH:I = 0x3

.field public static final UNIT_WIND_MPH:I = 0x2

.field public static final UNIT_WIND_MPS:I = 0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTempUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

.field private mViews:[Landroid/view/View;

.field private mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutResourceId"    # I

    .prologue
    .line 46
    invoke-direct/range {p0 .. p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 21
    const/4 v15, 0x0

    move-object v0, v15

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mViews:[Landroid/view/View;

    .line 47
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mContext:Landroid/content/Context;

    .line 50
    new-instance v15, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    const/16 v16, 0x0

    move-object v0, v15

    move-object/from16 v1, p0

    move/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;-><init>(Lse/volvocars/acu/weather/settings/UnitListAdapter;I)V

    move-object v0, v15

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    .line 52
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mContext:Landroid/content/Context;

    move-object v15, v0

    invoke-static {v15}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 53
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const v15, 0x7f030010

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v0, v7

    move v1, v15

    move-object/from16 v2, v16

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    .line 54
    .local v13, "v":Landroid/view/View;
    move-object v0, v13

    check-cast v0, Lse/volvocars/acu/weather/settings/UnitItemView;

    move-object v8, v0

    .line 55
    .local v8, "knots":Lse/volvocars/acu/weather/settings/UnitItemView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mContext:Landroid/content/Context;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f090007

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setText(Ljava/lang/String;)V

    .line 56
    const/4 v15, 0x0

    invoke-virtual {v8, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setUnitKey(I)V

    .line 57
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    move-object v15, v0

    invoke-virtual {v15, v8}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->addView(Lse/volvocars/acu/weather/settings/UnitItemView;)V

    .line 58
    const v15, 0x7f030010

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v0, v7

    move v1, v15

    move-object/from16 v2, v16

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    .line 59
    move-object v0, v13

    check-cast v0, Lse/volvocars/acu/weather/settings/UnitItemView;

    move-object v11, v0

    .line 60
    .local v11, "mps":Lse/volvocars/acu/weather/settings/UnitItemView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mContext:Landroid/content/Context;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f090008

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v11, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setText(Ljava/lang/String;)V

    .line 61
    const/4 v15, 0x1

    invoke-virtual {v11, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setUnitKey(I)V

    .line 62
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    move-object v15, v0

    invoke-virtual {v15, v11}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->addView(Lse/volvocars/acu/weather/settings/UnitItemView;)V

    .line 63
    const v15, 0x7f030010

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v0, v7

    move v1, v15

    move-object/from16 v2, v16

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    .line 64
    move-object v0, v13

    check-cast v0, Lse/volvocars/acu/weather/settings/UnitItemView;

    move-object v10, v0

    .line 65
    .local v10, "mph":Lse/volvocars/acu/weather/settings/UnitItemView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mContext:Landroid/content/Context;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f090009

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setText(Ljava/lang/String;)V

    .line 66
    const/4 v15, 0x2

    invoke-virtual {v10, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setUnitKey(I)V

    .line 67
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    move-object v15, v0

    invoke-virtual {v15, v10}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->addView(Lse/volvocars/acu/weather/settings/UnitItemView;)V

    .line 68
    const v15, 0x7f030010

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v0, v7

    move v1, v15

    move-object/from16 v2, v16

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    .line 69
    move-object v0, v13

    check-cast v0, Lse/volvocars/acu/weather/settings/UnitItemView;

    move-object v9, v0

    .line 70
    .local v9, "kph":Lse/volvocars/acu/weather/settings/UnitItemView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mContext:Landroid/content/Context;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f09000a

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setText(Ljava/lang/String;)V

    .line 71
    const/4 v15, 0x3

    invoke-virtual {v9, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setUnitKey(I)V

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    move-object v15, v0

    invoke-virtual {v15, v9}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->addView(Lse/volvocars/acu/weather/settings/UnitItemView;)V

    .line 73
    const v15, 0x7f030010

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v0, v7

    move v1, v15

    move-object/from16 v2, v16

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    .line 74
    move-object v0, v13

    check-cast v0, Lse/volvocars/acu/weather/settings/UnitItemView;

    move-object v4, v0

    .line 75
    .local v4, "beaufort":Lse/volvocars/acu/weather/settings/UnitItemView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mContext:Landroid/content/Context;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f09000b

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setText(Ljava/lang/String;)V

    .line 76
    const/4 v15, 0x4

    invoke-virtual {v4, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setUnitKey(I)V

    .line 77
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    move-object v15, v0

    invoke-virtual {v15, v4}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->addView(Lse/volvocars/acu/weather/settings/UnitItemView;)V

    .line 80
    new-instance v15, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    const/16 v16, 0x1

    move-object v0, v15

    move-object/from16 v1, p0

    move/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;-><init>(Lse/volvocars/acu/weather/settings/UnitListAdapter;I)V

    move-object v0, v15

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mTempUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    .line 82
    const v15, 0x7f030010

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v0, v7

    move v1, v15

    move-object/from16 v2, v16

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    .line 83
    move-object v0, v13

    check-cast v0, Lse/volvocars/acu/weather/settings/UnitItemView;

    move-object v5, v0

    .line 84
    .local v5, "celcius":Lse/volvocars/acu/weather/settings/UnitItemView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mContext:Landroid/content/Context;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f09000d

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setText(Ljava/lang/String;)V

    .line 85
    const/4 v15, 0x5

    invoke-virtual {v5, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setUnitKey(I)V

    .line 86
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mTempUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    move-object v15, v0

    invoke-virtual {v15, v5}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->addView(Lse/volvocars/acu/weather/settings/UnitItemView;)V

    .line 87
    const v15, 0x7f030010

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v0, v7

    move v1, v15

    move-object/from16 v2, v16

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    .line 88
    move-object v0, v13

    check-cast v0, Lse/volvocars/acu/weather/settings/UnitItemView;

    move-object v6, v0

    .line 89
    .local v6, "fahrenheit":Lse/volvocars/acu/weather/settings/UnitItemView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mContext:Landroid/content/Context;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f09000e

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setText(Ljava/lang/String;)V

    .line 90
    const/4 v15, 0x6

    invoke-virtual {v6, v15}, Lse/volvocars/acu/weather/settings/UnitItemView;->setUnitKey(I)V

    .line 91
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mTempUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    move-object v15, v0

    invoke-virtual {v15, v6}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->addView(Lse/volvocars/acu/weather/settings/UnitItemView;)V

    .line 94
    const v15, 0x7f030012

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v0, v7

    move v1, v15

    move-object/from16 v2, v16

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    .line 95
    move-object v0, v13

    check-cast v0, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;

    move-object v14, v0

    .line 96
    .local v14, "windSeparator":Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mContext:Landroid/content/Context;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f09000f

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->setText(Ljava/lang/String;)V

    .line 98
    const v15, 0x7f030012

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v0, v7

    move v1, v15

    move-object/from16 v2, v16

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    .line 99
    move-object v0, v13

    check-cast v0, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;

    move-object v12, v0

    .line 100
    .local v12, "temperatureSeparator":Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mContext:Landroid/content/Context;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f090010

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->setText(Ljava/lang/String;)V

    .line 104
    const/16 v15, 0x9

    new-array v15, v15, [Landroid/view/View;

    const/16 v16, 0x0

    aput-object v14, v15, v16

    const/16 v16, 0x1

    aput-object v8, v15, v16

    const/16 v16, 0x2

    aput-object v11, v15, v16

    const/16 v16, 0x3

    aput-object v10, v15, v16

    const/16 v16, 0x4

    aput-object v9, v15, v16

    const/16 v16, 0x5

    aput-object v4, v15, v16

    const/16 v16, 0x6

    aput-object v12, v15, v16

    const/16 v16, 0x7

    aput-object v5, v15, v16

    const/16 v16, 0x8

    aput-object v6, v15, v16

    move-object v0, v15

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mViews:[Landroid/view/View;

    .line 116
    return-void
.end method


# virtual methods
.method public clickItem(ILandroid/view/View;)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 125
    instance-of v2, p2, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;

    if-eqz v2, :cond_0

    .line 137
    :goto_0
    return-void

    .line 131
    :cond_0
    move-object v0, p2

    check-cast v0, Lse/volvocars/acu/weather/settings/UnitItemView;

    move-object v1, v0

    .line 132
    .local v1, "uiv":Lse/volvocars/acu/weather/settings/UnitItemView;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    invoke-virtual {v2, v1}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->isMember(Lse/volvocars/acu/weather/settings/UnitItemView;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 133
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    invoke-virtual {v2, v1}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->check(Lse/volvocars/acu/weather/settings/UnitItemView;)V

    goto :goto_0

    .line 135
    :cond_1
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mTempUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    invoke-virtual {v2, v1}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->check(Lse/volvocars/acu/weather/settings/UnitItemView;)V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mViews:[Landroid/view/View;

    array-length v0, v0

    return v0
.end method

.method public getGroup(I)Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;
    .locals 1
    .param p1, "groupId"    # I

    .prologue
    .line 186
    if-nez p1, :cond_0

    .line 187
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    .line 191
    :goto_0
    return-object v0

    .line 188
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 189
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mTempUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    goto :goto_0

    .line 191
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 177
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mViews:[Landroid/view/View;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 162
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mViews:[Landroid/view/View;

    aget-object v0, v1, p1

    .line 163
    .local v0, "v":Landroid/view/View;
    instance-of v1, v0, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;

    if-eqz v1, :cond_0

    .line 164
    const/4 v1, 0x0

    .line 166
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setActiveTempUnit(I)V
    .locals 2
    .param p1, "tempUnitKey"    # I

    .prologue
    .line 151
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mTempUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mTempUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    invoke-virtual {v1, p1}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->getViewFromKey(I)Lse/volvocars/acu/weather/settings/UnitItemView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->check(Lse/volvocars/acu/weather/settings/UnitItemView;)V

    .line 152
    return-void
.end method

.method public setActiveWindUnit(I)V
    .locals 2
    .param p1, "windUnitKey"    # I

    .prologue
    .line 143
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter;->mWindUnitGroup:Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    invoke-virtual {v1, p1}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->getViewFromKey(I)Lse/volvocars/acu/weather/settings/UnitItemView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->check(Lse/volvocars/acu/weather/settings/UnitItemView;)V

    .line 144
    return-void
.end method
