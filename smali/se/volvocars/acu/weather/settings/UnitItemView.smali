.class public Lse/volvocars/acu/weather/settings/UnitItemView;
.super Landroid/widget/RelativeLayout;
.source "UnitItemView.java"


# static fields
.field public static final STATE_OFF:I = 0x0

.field public static final STATE_ON:I = 0x1


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field private mRadioButtonOff:Landroid/graphics/drawable/Drawable;

.field private mRadioButtonOn:Landroid/graphics/drawable/Drawable;

.field private mTextView:Landroid/widget/TextView;

.field private mUnitKey:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method


# virtual methods
.method public getUnitKey()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mUnitKey:I

    return v0
.end method

.method public isTurnedOn()Z
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mRadioButtonOn:Landroid/graphics/drawable/Drawable;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 80
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/UnitItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "fonts/VolvoSanProLig.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 81
    .local v0, "typeface":Landroid/graphics/Typeface;
    const v1, 0x7f070008

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/UnitItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mTextView:Landroid/widget/TextView;

    .line 82
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 84
    const v1, 0x7f070009

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/UnitItemView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mImageView:Landroid/widget/ImageView;

    .line 87
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/UnitItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mRadioButtonOn:Landroid/graphics/drawable/Drawable;

    .line 88
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/UnitItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mRadioButtonOff:Landroid/graphics/drawable/Drawable;

    .line 89
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mImageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mRadioButtonOn:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 90
    return-void
.end method

.method public setState(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mImageView:Landroid/widget/ImageView;

    if-nez p1, :cond_0

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mRadioButtonOff:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 54
    return-void

    .line 53
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mRadioButtonOn:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 45
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    return-void
.end method

.method public setUnitKey(I)V
    .locals 0
    .param p1, "unitKey"    # I

    .prologue
    .line 68
    iput p1, p0, Lse/volvocars/acu/weather/settings/UnitItemView;->mUnitKey:I

    .line 69
    return-void
.end method
