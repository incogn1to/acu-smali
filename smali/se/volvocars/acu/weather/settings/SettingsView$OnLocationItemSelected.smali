.class Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemSelected;
.super Ljava/lang/Object;
.source "SettingsView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/SettingsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnLocationItemSelected"
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/SettingsView;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/weather/settings/SettingsView;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemSelected;->this$0:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/weather/settings/SettingsView;Lse/volvocars/acu/weather/settings/SettingsView$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/weather/settings/SettingsView;
    .param p2, "x1"    # Lse/volvocars/acu/weather/settings/SettingsView$1;

    .prologue
    .line 308
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemSelected;-><init>(Lse/volvocars/acu/weather/settings/SettingsView;)V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 315
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemSelected;->this$0:Lse/volvocars/acu/weather/settings/SettingsView;

    # getter for: Lse/volvocars/acu/weather/settings/SettingsView;->mPendingDelete:Z
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/SettingsView;->access$200(Lse/volvocars/acu/weather/settings/SettingsView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/SettingsView$OnLocationItemSelected;->this$0:Lse/volvocars/acu/weather/settings/SettingsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/SettingsView;->setPendingDelete(Z)V

    .line 318
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 321
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
