.class Lse/volvocars/acu/weather/settings/LocationChooser$InternetConnectionBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LocationChooser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/LocationChooser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternetConnectionBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/LocationChooser;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/weather/settings/LocationChooser;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationChooser$InternetConnectionBroadcastReceiver;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/weather/settings/LocationChooser;Lse/volvocars/acu/weather/settings/LocationChooser$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;
    .param p2, "x1"    # Lse/volvocars/acu/weather/settings/LocationChooser$1;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/LocationChooser$InternetConnectionBroadcastReceiver;-><init>(Lse/volvocars/acu/weather/settings/LocationChooser;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v4, 0x8

    .line 77
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationChooser$InternetConnectionBroadcastReceiver;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/LocationChooser;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 79
    .local v1, "connectivityManager":Landroid/net/ConnectivityManager;
    if-eqz v1, :cond_0

    .line 80
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 82
    .local v0, "activeNetInfo":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationChooser$InternetConnectionBroadcastReceiver;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->noInternetTextView:Landroid/widget/TextView;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$200(Lse/volvocars/acu/weather/settings/LocationChooser;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    .end local v0    # "activeNetInfo":Landroid/net/NetworkInfo;
    :cond_0
    :goto_0
    return-void

    .line 86
    .restart local v0    # "activeNetInfo":Landroid/net/NetworkInfo;
    :cond_1
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationChooser$InternetConnectionBroadcastReceiver;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$300(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->clear()V

    .line 87
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationChooser$InternetConnectionBroadcastReceiver;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->noMatchTextView:Landroid/widget/TextView;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$400(Lse/volvocars/acu/weather/settings/LocationChooser;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 88
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationChooser$InternetConnectionBroadcastReceiver;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->noInternetTextView:Landroid/widget/TextView;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$200(Lse/volvocars/acu/weather/settings/LocationChooser;)Landroid/widget/TextView;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
