.class public Lse/volvocars/acu/weather/settings/UnitDialog;
.super Landroid/app/Dialog;
.source "UnitDialog.java"


# static fields
.field public static final KEY_TEMP_UNIT:Ljava/lang/String; = "temp_unit"

.field public static final KEY_WIND_UNIT:Ljava/lang/String; = "wind_unit"


# instance fields
.field private mActiveTempUnit:I

.field private mActiveWindUnit:I

.field private mAdapter:Lse/volvocars/acu/weather/settings/UnitListAdapter;

.field private mCancelButton:Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field private mListView:Landroid/widget/ListView;

.field private mOkButton:Landroid/widget/Button;

.field private mSharedPreferences:Landroid/content/SharedPreferences;

.field private mUnitChangedListener:Lse/volvocars/acu/weather/settings/UnitChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/content/SharedPreferences;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I
    .param p3, "settings"    # Landroid/content/SharedPreferences;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 40
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mContext:Landroid/content/Context;

    .line 41
    iput-object p3, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lse/volvocars/acu/weather/settings/UnitDialog;)I
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/UnitDialog;

    .prologue
    .line 23
    iget v0, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mActiveWindUnit:I

    return v0
.end method

.method static synthetic access$002(Lse/volvocars/acu/weather/settings/UnitDialog;I)I
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/UnitDialog;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mActiveWindUnit:I

    return p1
.end method

.method static synthetic access$100(Lse/volvocars/acu/weather/settings/UnitDialog;I)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/UnitDialog;
    .param p1, "x1"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/UnitDialog;->setWindUnit(I)V

    return-void
.end method

.method static synthetic access$200(Lse/volvocars/acu/weather/settings/UnitDialog;)I
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/UnitDialog;

    .prologue
    .line 23
    iget v0, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mActiveTempUnit:I

    return v0
.end method

.method static synthetic access$202(Lse/volvocars/acu/weather/settings/UnitDialog;I)I
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/UnitDialog;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mActiveTempUnit:I

    return p1
.end method

.method static synthetic access$300(Lse/volvocars/acu/weather/settings/UnitDialog;I)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/UnitDialog;
    .param p1, "x1"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/UnitDialog;->setTempUnit(I)V

    return-void
.end method

.method static synthetic access$400(Lse/volvocars/acu/weather/settings/UnitDialog;)Lse/volvocars/acu/weather/settings/UnitListAdapter;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/UnitDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mAdapter:Lse/volvocars/acu/weather/settings/UnitListAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lse/volvocars/acu/weather/settings/UnitDialog;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/UnitDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method private setTempUnit(I)V
    .locals 2
    .param p1, "tempUnitKey"    # I

    .prologue
    .line 145
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 146
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "temp_unit"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 147
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 149
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mUnitChangedListener:Lse/volvocars/acu/weather/settings/UnitChangedListener;

    invoke-interface {v1, p1}, Lse/volvocars/acu/weather/settings/UnitChangedListener;->onTempUnitChanged(I)V

    .line 150
    return-void
.end method

.method private setWindUnit(I)V
    .locals 2
    .param p1, "windUnitKey"    # I

    .prologue
    .line 130
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 131
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "wind_unit"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 132
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 134
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mUnitChangedListener:Lse/volvocars/acu/weather/settings/UnitChangedListener;

    invoke-interface {v1, p1}, Lse/volvocars/acu/weather/settings/UnitChangedListener;->onWindUnitChanged(I)V

    .line 135
    return-void
.end method


# virtual methods
.method protected getTempUnit()I
    .locals 3

    .prologue
    .line 154
    const/4 v0, 0x5

    .line 155
    .local v0, "tempUnitKey":I
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "temp_unit"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method protected getWindUnit()I
    .locals 3

    .prologue
    .line 139
    const/4 v0, 0x1

    .line 140
    .local v0, "windUnitKey":I
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "wind_unit"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v2, 0x7f030011

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/UnitDialog;->setContentView(I)V

    .line 51
    new-instance v2, Lse/volvocars/acu/weather/settings/UnitListAdapter;

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mContext:Landroid/content/Context;

    const v4, 0x7f030010

    invoke-direct {v2, v3, v4}, Lse/volvocars/acu/weather/settings/UnitListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mAdapter:Lse/volvocars/acu/weather/settings/UnitListAdapter;

    .line 54
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/UnitDialog;->getWindUnit()I

    move-result v2

    iput v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mActiveWindUnit:I

    .line 55
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mAdapter:Lse/volvocars/acu/weather/settings/UnitListAdapter;

    iget v3, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mActiveWindUnit:I

    invoke-virtual {v2, v3}, Lse/volvocars/acu/weather/settings/UnitListAdapter;->setActiveWindUnit(I)V

    .line 57
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/UnitDialog;->getTempUnit()I

    move-result v2

    iput v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mActiveTempUnit:I

    .line 58
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mAdapter:Lse/volvocars/acu/weather/settings/UnitListAdapter;

    iget v3, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mActiveTempUnit:I

    invoke-virtual {v2, v3}, Lse/volvocars/acu/weather/settings/UnitListAdapter;->setActiveTempUnit(I)V

    .line 61
    const v2, 0x7f07000d

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/UnitDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mListView:Landroid/widget/ListView;

    .line 62
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mAdapter:Lse/volvocars/acu/weather/settings/UnitListAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mListView:Landroid/widget/ListView;

    const v3, 0x7f020002

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelector(I)V

    .line 66
    const v2, 0x7f07000b

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/UnitDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 67
    .local v0, "tv":Landroid/widget/TextView;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "fonts/VolvoSanProLig.otf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 68
    .local v1, "typeface":Landroid/graphics/Typeface;
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 72
    const v2, 0x7f07000e

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/UnitDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mOkButton:Landroid/widget/Button;

    .line 73
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 74
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mOkButton:Landroid/widget/Button;

    new-instance v3, Lse/volvocars/acu/weather/settings/UnitDialog$1;

    invoke-direct {v3, p0}, Lse/volvocars/acu/weather/settings/UnitDialog$1;-><init>(Lse/volvocars/acu/weather/settings/UnitDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    const v2, 0x7f070010

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/UnitDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mCancelButton:Landroid/widget/Button;

    .line 86
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 87
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mCancelButton:Landroid/widget/Button;

    new-instance v3, Lse/volvocars/acu/weather/settings/UnitDialog$2;

    invoke-direct {v3, p0}, Lse/volvocars/acu/weather/settings/UnitDialog$2;-><init>(Lse/volvocars/acu/weather/settings/UnitDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mListView:Landroid/widget/ListView;

    new-instance v3, Lse/volvocars/acu/weather/settings/UnitDialog$3;

    invoke-direct {v3, p0}, Lse/volvocars/acu/weather/settings/UnitDialog$3;-><init>(Lse/volvocars/acu/weather/settings/UnitDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 118
    return-void
.end method

.method public setUnitChangedListener(Lse/volvocars/acu/weather/settings/UnitChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lse/volvocars/acu/weather/settings/UnitChangedListener;

    .prologue
    .line 125
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/UnitDialog;->mUnitChangedListener:Lse/volvocars/acu/weather/settings/UnitChangedListener;

    .line 126
    return-void
.end method
