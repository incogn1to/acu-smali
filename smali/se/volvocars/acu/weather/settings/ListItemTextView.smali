.class public Lse/volvocars/acu/weather/settings/ListItemTextView;
.super Landroid/view/View;
.source "ListItemTextView.java"


# static fields
.field private static final DIVIDER_HEIGHT:I = 0x1e

.field private static final DIVIDER_MARGIN:I = 0xf

.field private static final ELLIPSIZE_STYLE:Ljava/lang/String; = "..."

.field private static final FONTSIZE_LARGE:I = 0x26

.field private static final FONTSIZE_SMALL:I = 0x1e

.field private static final LONG_MARGIN:I = 0x1e

.field private static final OFFSET_Y:I = 0x26

.field private static final PERCENT_FACTOR:F = 100.0f

.field private static final SEPERATOR_AREA:Ljava/lang/String; = ", "

.field private static final SEPERATOR_CITY:Ljava/lang/String; = " | "

.field private static final SHORT_MARGIN:I = 0xa

.field private static final WEIGHT_PERCENT_AREA:I = 0x19

.field private static final WEIGHT_PERCENT_CITY:I = 0x32

.field private static final WEIGHT_PERCENT_COUNTRY:I = 0x19

.field private static final WIDTH_LONG:I = 0x276

.field private static final WIDTH_SHORT:I = 0x244

.field private static final sHeight:I = 0x32


# instance fields
.field private mAreaMatch:I

.field private mAreaText:Ljava/lang/String;

.field private mCityMatch:I

.field private mCityText:Ljava/lang/String;

.field private final mColorFaded:I

.field private final mColorMatch:I

.field private final mColorWhite:I

.field private mCountryMatch:I

.field private mCountryText:Ljava/lang/String;

.field private mLocation:Lse/volvocars/acu/weather/settings/Location;

.field private final mPaintCity:Landroid/graphics/Paint;

.field private final mPaintOther:Landroid/graphics/Paint;

.field private final mTypeface:Landroid/graphics/Typeface;

.field private mWidth:I

.field private mWidthArea:I

.field private mWidthCity:I

.field private mWidthCountry:I

.field private mWidthLimitArea:I

.field private mWidthLimitCity:I

.field private mWidthLimitCountry:I

.field private mWidthTotal:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const-string v1, ""

    .line 73
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const-string v0, ""

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    .line 49
    const/16 v0, 0x26c

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidth:I

    .line 55
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/ListItemTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mColorWhite:I

    .line 56
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/ListItemTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mColorFaded:I

    .line 57
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/ListItemTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060018

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mColorMatch:I

    .line 74
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/ListItemTextView;->calculateWidths()V

    .line 75
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts/VolvoSanProLig.otf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mTypeface:Landroid/graphics/Typeface;

    .line 76
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mColorWhite:I

    const/16 v1, 0x26

    invoke-direct {p0, v0, v1}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setupPaint(II)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    .line 77
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mColorFaded:I

    const/16 v1, 0x1e

    invoke-direct {p0, v0, v1}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setupPaint(II)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    .line 78
    return-void
.end method

.method private calcWidth(Landroid/graphics/Paint;Ljava/lang/String;)I
    .locals 1
    .param p1, "paint"    # Landroid/graphics/Paint;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 169
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method private calculateWidths()V
    .locals 3

    .prologue
    const/high16 v2, 0x3e800000    # 0.25f

    .line 357
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidth:I

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitCity:I

    .line 358
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidth:I

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitArea:I

    .line 359
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidth:I

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitCountry:I

    .line 360
    return-void
.end method

.method private setupPaint(II)Landroid/graphics/Paint;
    .locals 2
    .param p1, "color"    # I
    .param p2, "size"    # I

    .prologue
    .line 81
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 82
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 84
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 85
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 86
    return-object v0
.end method


# virtual methods
.method public getAreaMatch()I
    .locals 1

    .prologue
    .line 229
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaMatch:I

    return v0
.end method

.method public getAreaText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    return-object v0
.end method

.method public getCityMatch()I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityMatch:I

    return v0
.end method

.method public getCityText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    return-object v0
.end method

.method public getCountryMatch()I
    .locals 1

    .prologue
    .line 233
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryMatch:I

    return v0
.end method

.method public getCountryText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Lse/volvocars/acu/weather/settings/Location;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x42200000    # 40.0f

    const/high16 v2, 0x41200000    # 10.0f

    const/4 v3, 0x0

    const/4 v10, 0x0

    const/high16 v9, 0x42180000    # 38.0f

    .line 100
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 102
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityMatch:I

    if-lez v0, :cond_3

    .line 104
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityMatch:I

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 105
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityMatch:I

    .line 108
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityMatch:I

    invoke-virtual {v0, v10, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 109
    .local v6, "matchString":Ljava/lang/String;
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityMatch:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 111
    .local v7, "restString":Ljava/lang/String;
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mColorMatch:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v3, v9, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 113
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mColorWhite:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 115
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0, v9, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 117
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v8, v0

    .line 118
    .local v8, "xOffset":I
    add-int/lit8 v0, v8, 0xf

    int-to-float v1, v0

    add-int/lit8 v0, v8, 0xf

    int-to-float v3, v0

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 129
    .end local v6    # "matchString":Ljava/lang/String;
    .end local v7    # "restString":Ljava/lang/String;
    :goto_0
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaMatch:I

    if-lez v0, :cond_5

    .line 131
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaMatch:I

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 132
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaMatch:I

    .line 135
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaMatch:I

    invoke-virtual {v0, v10, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 136
    .restart local v6    # "matchString":Ljava/lang/String;
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaMatch:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 138
    .restart local v7    # "restString":Ljava/lang/String;
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mColorMatch:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 139
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    int-to-float v0, v0

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v0, v9, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 140
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mColorFaded:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    const-string v1, ", "

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    int-to-float v1, v1

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v9, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 147
    .end local v6    # "matchString":Ljava/lang/String;
    .end local v7    # "restString":Ljava/lang/String;
    :goto_2
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryMatch:I

    if-lez v0, :cond_7

    .line 149
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryMatch:I

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 150
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryMatch:I

    .line 153
    :cond_2
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryMatch:I

    invoke-virtual {v0, v10, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 154
    .restart local v6    # "matchString":Ljava/lang/String;
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryMatch:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 156
    .restart local v7    # "restString":Ljava/lang/String;
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mColorMatch:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 157
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthArea:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v0, v9, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 158
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mColorFaded:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 159
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthArea:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0, v9, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 163
    .end local v6    # "matchString":Ljava/lang/String;
    .end local v7    # "restString":Ljava/lang/String;
    :goto_3
    return-void

    .line 122
    .end local v8    # "xOffset":I
    :cond_3
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v9, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 124
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v8, v0

    .line 125
    .restart local v8    # "xOffset":I
    add-int/lit8 v0, v8, 0xf

    int-to-float v1, v0

    add-int/lit8 v0, v8, 0xf

    int-to-float v3, v0

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 141
    .restart local v6    # "matchString":Ljava/lang/String;
    .restart local v7    # "restString":Ljava/lang/String;
    :cond_4
    const-string v1, ""

    goto/16 :goto_1

    .line 143
    .end local v6    # "matchString":Ljava/lang/String;
    .end local v7    # "restString":Ljava/lang/String;
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_6

    const-string v1, ", "

    :goto_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    int-to-float v1, v1

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v9, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    :cond_6
    const-string v1, ""

    goto :goto_4

    .line 161
    :cond_7
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    iget v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    iget v2, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthArea:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v9, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_3
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 92
    iget v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidth:I

    const/16 v1, 0x32

    invoke-virtual {p0, v0, v1}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setMeasuredDimension(II)V

    .line 93
    return-void
.end method

.method public setAreaText(Ljava/lang/String;I)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "match"    # I

    .prologue
    .line 188
    if-nez p1, :cond_0

    .line 189
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input string cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    .line 192
    iput p2, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaMatch:I

    .line 193
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 194
    const/4 v0, 0x0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthArea:I

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lse/volvocars/acu/weather/settings/ListItemTextView;->calcWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthArea:I

    goto :goto_0
.end method

.method public setCityText(Ljava/lang/String;I)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "match"    # I

    .prologue
    .line 176
    if-nez p1, :cond_0

    .line 177
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input string cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    .line 180
    iput p2, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityMatch:I

    .line 181
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lse/volvocars/acu/weather/settings/ListItemTextView;->calcWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    .line 182
    return-void
.end method

.method public setCountryText(Ljava/lang/String;I)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "match"    # I

    .prologue
    .line 204
    if-nez p1, :cond_0

    .line 205
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input string cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    .line 208
    iput p2, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryMatch:I

    .line 209
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lse/volvocars/acu/weather/settings/ListItemTextView;->calcWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCountry:I

    .line 210
    return-void
.end method

.method public setLocation(Lse/volvocars/acu/weather/settings/Location;)V
    .locals 2
    .param p1, "locationItem"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    const/4 v1, 0x0

    .line 325
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    .line 327
    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setCityText(Ljava/lang/String;I)V

    .line 328
    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getArea()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setAreaText(Ljava/lang/String;I)V

    .line 329
    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lse/volvocars/acu/weather/settings/ListItemTextView;->setCountryText(Ljava/lang/String;I)V

    .line 331
    return-void
.end method

.method public setWidthLong()V
    .locals 1

    .prologue
    .line 349
    const/16 v0, 0x276

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidth:I

    .line 350
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/ListItemTextView;->calculateWidths()V

    .line 351
    return-void
.end method

.method public setWidthShort()V
    .locals 1

    .prologue
    .line 341
    const/16 v0, 0x244

    iput v0, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidth:I

    .line 342
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/ListItemTextView;->calculateWidths()V

    .line 343
    return-void
.end method

.method public trimAndUpdate()V
    .locals 13

    .prologue
    .line 240
    iget v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    iget v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthArea:I

    add-int/2addr v8, v9

    iget v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCountry:I

    add-int/2addr v8, v9

    iput v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthTotal:I

    .line 242
    iget v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthTotal:I

    iget v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidth:I

    if-le v8, v9, :cond_5

    .line 244
    iget-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    const-string v9, "..."

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    float-to-int v8, v8

    add-int/lit8 v5, v8, 0x1e

    .line 245
    .local v5, "trimMarginBig":I
    iget-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    const-string v9, "..."

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    float-to-int v8, v8

    add-int/lit8 v6, v8, 0xa

    .line 247
    .local v6, "trimMarginSmall":I
    iget v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthTotal:I

    iget v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidth:I

    sub-int v4, v8, v9

    .line 249
    .local v4, "totalOvershoot":I
    iget v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthArea:I

    iget v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitArea:I

    if-le v8, v9, :cond_1

    .line 250
    iget v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthArea:I

    iget v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitArea:I

    sub-int v0, v8, v9

    .line 251
    .local v0, "areaOvershoot":I
    const/4 v3, 0x0

    .line 253
    .local v3, "maxChars":I
    if-le v4, v0, :cond_6

    .line 255
    iget-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    iget v11, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitArea:I

    sub-int/2addr v11, v6

    int-to-float v11, v11

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v3

    .line 256
    sub-int/2addr v4, v0

    .line 264
    :goto_0
    iget-object v7, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    .line 266
    .local v7, "trimmedString":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v3, v8, :cond_0

    .line 267
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    invoke-virtual {v7, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    .line 270
    :cond_0
    iget-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lse/volvocars/acu/weather/settings/ListItemTextView;->calcWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthArea:I

    .line 274
    .end local v0    # "areaOvershoot":I
    .end local v3    # "maxChars":I
    .end local v7    # "trimmedString":Ljava/lang/String;
    :cond_1
    iget v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    iget v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitCity:I

    if-le v8, v9, :cond_3

    if-lez v4, :cond_3

    .line 275
    iget v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    iget v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitCity:I

    sub-int v1, v8, v9

    .line 276
    .local v1, "cityOvershoot":I
    const/4 v3, 0x0

    .line 278
    .restart local v3    # "maxChars":I
    if-le v4, v1, :cond_7

    .line 280
    iget-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " | "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    iget v11, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitCity:I

    sub-int/2addr v11, v5

    int-to-float v11, v11

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v3

    .line 281
    sub-int/2addr v4, v1

    .line 289
    :goto_1
    iget-object v7, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    .line 291
    .restart local v7    # "trimmedString":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v3, v8, :cond_2

    .line 292
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    invoke-virtual {v7, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    .line 295
    :cond_2
    iget-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " | "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v8, v9}, Lse/volvocars/acu/weather/settings/ListItemTextView;->calcWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    .line 298
    .end local v1    # "cityOvershoot":I
    .end local v3    # "maxChars":I
    .end local v7    # "trimmedString":Ljava/lang/String;
    :cond_3
    iget v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCountry:I

    iget v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitCountry:I

    if-le v8, v9, :cond_5

    if-lez v4, :cond_5

    .line 299
    iget v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCountry:I

    iget v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitCountry:I

    sub-int v2, v8, v9

    .line 300
    .local v2, "countryOvershoot":I
    const/4 v3, 0x0

    .line 302
    .restart local v3    # "maxChars":I
    if-le v4, v2, :cond_8

    .line 304
    iget-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    iget-object v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    const/4 v10, 0x1

    iget v11, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthLimitCountry:I

    sub-int/2addr v11, v6

    int-to-float v11, v11

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v3

    .line 311
    :goto_2
    iget-object v7, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    .line 313
    .restart local v7    # "trimmedString":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v3, v8, :cond_4

    .line 314
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    invoke-virtual {v7, v9, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    .line 317
    :cond_4
    iget-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    iget-object v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    invoke-direct {p0, v8, v9}, Lse/volvocars/acu/weather/settings/ListItemTextView;->calcWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCountry:I

    .line 322
    .end local v2    # "countryOvershoot":I
    .end local v3    # "maxChars":I
    .end local v4    # "totalOvershoot":I
    .end local v5    # "trimMarginBig":I
    .end local v6    # "trimMarginSmall":I
    .end local v7    # "trimmedString":Ljava/lang/String;
    :cond_5
    return-void

    .line 260
    .restart local v0    # "areaOvershoot":I
    .restart local v3    # "maxChars":I
    .restart local v4    # "totalOvershoot":I
    .restart local v5    # "trimMarginBig":I
    .restart local v6    # "trimMarginSmall":I
    :cond_6
    iget-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mAreaText:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    iget v11, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthArea:I

    sub-int/2addr v11, v4

    sub-int/2addr v11, v6

    int-to-float v11, v11

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v8

    const/4 v9, 0x1

    sub-int v3, v8, v9

    .line 261
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 285
    .end local v0    # "areaOvershoot":I
    .restart local v1    # "cityOvershoot":I
    :cond_7
    iget-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintCity:Landroid/graphics/Paint;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCityText:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " | "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    iget v11, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCity:I

    sub-int/2addr v11, v4

    sub-int/2addr v11, v5

    int-to-float v11, v11

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v3

    .line 286
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 308
    .end local v1    # "cityOvershoot":I
    .restart local v2    # "countryOvershoot":I
    :cond_8
    iget-object v8, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mPaintOther:Landroid/graphics/Paint;

    iget-object v9, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mCountryText:Ljava/lang/String;

    const/4 v10, 0x1

    iget v11, p0, Lse/volvocars/acu/weather/settings/ListItemTextView;->mWidthCountry:I

    sub-int/2addr v11, v4

    sub-int/2addr v11, v6

    int-to-float v11, v11

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v3

    goto/16 :goto_2
.end method
