.class public Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;
.super Ljava/lang/Object;
.source "UnitListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/UnitListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Group"
.end annotation


# instance fields
.field private mCurrentlySelected:Lse/volvocars/acu/weather/settings/UnitItemView;

.field private mGroupId:I

.field private mViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/UnitItemView;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lse/volvocars/acu/weather/settings/UnitListAdapter;


# direct methods
.method constructor <init>(Lse/volvocars/acu/weather/settings/UnitListAdapter;I)V
    .locals 1
    .param p2, "groupId"    # I

    .prologue
    const/4 v0, 0x0

    .line 203
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->this$0:Lse/volvocars/acu/weather/settings/UnitListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    iput-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mViews:Ljava/util/List;

    .line 201
    iput-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mCurrentlySelected:Lse/volvocars/acu/weather/settings/UnitItemView;

    .line 204
    iput p2, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mGroupId:I

    .line 205
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mViews:Ljava/util/List;

    .line 206
    return-void
.end method


# virtual methods
.method public addView(Lse/volvocars/acu/weather/settings/UnitItemView;)V
    .locals 1
    .param p1, "view"    # Lse/volvocars/acu/weather/settings/UnitItemView;

    .prologue
    .line 219
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mViews:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    return-void
.end method

.method public check(Lse/volvocars/acu/weather/settings/UnitItemView;)V
    .locals 3
    .param p1, "view"    # Lse/volvocars/acu/weather/settings/UnitItemView;

    .prologue
    .line 226
    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->isChecked(Lse/volvocars/acu/weather/settings/UnitItemView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 231
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mViews:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 232
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mViews:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/UnitItemView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/UnitItemView;->setState(I)V

    .line 231
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 235
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lse/volvocars/acu/weather/settings/UnitItemView;->setState(I)V

    .line 236
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mCurrentlySelected:Lse/volvocars/acu/weather/settings/UnitItemView;

    goto :goto_0
.end method

.method public getCurrentlySelected()Lse/volvocars/acu/weather/settings/UnitItemView;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mCurrentlySelected:Lse/volvocars/acu/weather/settings/UnitItemView;

    return-object v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mGroupId:I

    return v0
.end method

.method public getViewFromKey(I)Lse/volvocars/acu/weather/settings/UnitItemView;
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 270
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mViews:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 271
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mViews:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/settings/UnitItemView;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/UnitItemView;->getUnitKey()I

    move-result v1

    if-ne v1, p1, :cond_0

    .line 273
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mViews:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "this":Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;
    check-cast p0, Lse/volvocars/acu/weather/settings/UnitItemView;

    move-object v1, p0

    .line 276
    :goto_1
    return-object v1

    .line 270
    .restart local p0    # "this":Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 276
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isChecked(Lse/volvocars/acu/weather/settings/UnitItemView;)Z
    .locals 1
    .param p1, "view"    # Lse/volvocars/acu/weather/settings/UnitItemView;

    .prologue
    .line 251
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mCurrentlySelected:Lse/volvocars/acu/weather/settings/UnitItemView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isMember(Lse/volvocars/acu/weather/settings/UnitItemView;)Z
    .locals 2
    .param p1, "view"    # Lse/volvocars/acu/weather/settings/UnitItemView;

    .prologue
    .line 258
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mViews:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 259
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->mViews:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 260
    const/4 v1, 0x1

    .line 263
    :goto_1
    return v1

    .line 258
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 263
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
