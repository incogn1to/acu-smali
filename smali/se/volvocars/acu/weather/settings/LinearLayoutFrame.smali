.class public Lse/volvocars/acu/weather/settings/LinearLayoutFrame;
.super Landroid/widget/LinearLayout;
.source "LinearLayoutFrame.java"


# instance fields
.field private mActivity:Lse/volvocars/acu/weather/settings/LocationChooser;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method


# virtual methods
.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 24
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 25
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LinearLayoutFrame;->mActivity:Lse/volvocars/acu/weather/settings/LocationChooser;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/LocationChooser;->setIsKeyboardHidden(Z)V

    .line 26
    const-string v0, "LinearLayoutFrame"

    const-string v1, "Soft keyboard hidden true on back key."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public setActivity(Lse/volvocars/acu/weather/settings/LocationChooser;)V
    .locals 0
    .param p1, "activity"    # Lse/volvocars/acu/weather/settings/LocationChooser;

    .prologue
    .line 19
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LinearLayoutFrame;->mActivity:Lse/volvocars/acu/weather/settings/LocationChooser;

    .line 20
    return-void
.end method
