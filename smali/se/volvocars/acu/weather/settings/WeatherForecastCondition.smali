.class public final enum Lse/volvocars/acu/weather/settings/WeatherForecastCondition;
.super Ljava/lang/Enum;
.source "WeatherForecastCondition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/settings/WeatherForecastCondition$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lse/volvocars/acu/weather/settings/WeatherForecastCondition;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum Blizzard:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum Cloudy:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum Fog:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum FreezingFog:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum FreezingRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum HeavyRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum LightFreezingRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum LightRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum LightRainAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum LightSleet:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum LightSnow:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum LightSnowAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum LightSnowShowers:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum Mist:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum Overcast:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum PartlyCloudy:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum Rain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum RaindAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum Sleet:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum Snow:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum SnowAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum SnowShowers:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum Sun:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum Sunny:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

.field public static final enum Thunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;


# instance fields
.field private final mDescription:I

.field private final mIcon24Hours:I

.field private final mIcon7Days:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const v14, 0x7f02000d

    const v4, 0x7f020074

    const v5, 0x7f020075

    const v13, 0x7f020063

    const v12, 0x7f020062

    .line 18
    new-instance v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v1, "Blizzard"

    const/4 v2, 0x0

    const v3, 0x7f090027

    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Blizzard:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 19
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "Cloudy"

    const/4 v8, 0x1

    const v9, 0x7f09002a

    const v10, 0x7f020008

    const v11, 0x7f020009

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Cloudy:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 20
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "Fog"

    const/4 v8, 0x2

    const v9, 0x7f090026

    const v11, 0x7f02000e

    move v10, v14

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Fog:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 21
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "FreezingFog"

    const/4 v8, 0x3

    const v9, 0x7f090025

    const v11, 0x7f02000e

    move v10, v14

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->FreezingFog:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 22
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "FreezingRain"

    const/4 v8, 0x4

    const v9, 0x7f090023

    move v10, v12

    move v11, v13

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->FreezingRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 23
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "HeavyRain"

    const/4 v8, 0x5

    const v9, 0x7f09001c

    move v10, v12

    move v11, v13

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->HeavyRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 24
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "LightFreezingRain"

    const/4 v8, 0x6

    const v9, 0x7f090024

    move v10, v12

    move v11, v13

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightFreezingRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 25
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "LightRain"

    const/4 v8, 0x7

    const v9, 0x7f09001e

    move v10, v12

    move v11, v13

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 26
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "LightRainAndThunder"

    const/16 v8, 0x8

    const v9, 0x7f090018

    const v10, 0x7f020080

    const v11, 0x7f020081

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightRainAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 27
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "LightSleet"

    const/16 v8, 0x9

    const v9, 0x7f090020

    const v10, 0x7f02006e

    const v11, 0x7f02006f

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightSleet:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 28
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "LightSnow"

    const/16 v8, 0xa

    const v9, 0x7f09001b

    const v10, 0x7f020071

    const v11, 0x7f020072

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightSnow:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 29
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "LightSnowAndThunder"

    const/16 v8, 0xb

    const v9, 0x7f090016

    const v10, 0x7f020071

    const v11, 0x7f020072

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightSnowAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 30
    new-instance v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v1, "LightSnowShowers"

    const/16 v2, 0xc

    const v3, 0x7f090022

    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightSnowShowers:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 31
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "Mist"

    const/16 v8, 0xd

    const v9, 0x7f090028

    const v11, 0x7f02000e

    move v10, v14

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Mist:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 32
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "Overcast"

    const/16 v8, 0xe

    const v9, 0x7f090029

    const v10, 0x7f020054

    const v11, 0x7f020055

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Overcast:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 33
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "PartlyCloudy"

    const/16 v8, 0xf

    const v9, 0x7f09002b

    const v10, 0x7f020008

    const v11, 0x7f020009

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->PartlyCloudy:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 34
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "Rain"

    const/16 v8, 0x10

    const v9, 0x7f09001d

    move v10, v12

    move v11, v13

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Rain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 35
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "RaindAndThunder"

    const/16 v8, 0x11

    const v9, 0x7f090017

    const v10, 0x7f020080

    const v11, 0x7f020081

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->RaindAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 36
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "Sleet"

    const/16 v8, 0x12

    const v9, 0x7f09001f

    const v10, 0x7f02006e

    const v11, 0x7f02006f

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Sleet:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 37
    new-instance v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v7, "Snow"

    const/16 v8, 0x13

    const v9, 0x7f09001a

    const v10, 0x7f020071

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v6, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Snow:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 38
    new-instance v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v1, "SnowAndThunder"

    const/16 v2, 0x14

    const v3, 0x7f090015

    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->SnowAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 39
    new-instance v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v1, "SnowShowers"

    const/16 v2, 0x15

    const v3, 0x7f090021

    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->SnowShowers:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 40
    new-instance v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v1, "Sun"

    const/16 v2, 0x16

    const v3, 0x7f09002c

    const v4, 0x7f020077

    const v5, 0x7f020078

    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Sun:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 41
    new-instance v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v1, "Sunny"

    const/16 v2, 0x17

    const v3, 0x7f09002d

    const v4, 0x7f020057

    const v5, 0x7f020058

    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Sunny:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 42
    new-instance v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const-string v1, "Thunder"

    const/16 v2, 0x18

    const v3, 0x7f090019

    const v4, 0x7f02007d

    const v5, 0x7f02007e

    invoke-direct/range {v0 .. v5}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Thunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    .line 16
    const/16 v0, 0x19

    new-array v0, v0, [Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    const/4 v1, 0x0

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Blizzard:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Cloudy:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Fog:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->FreezingFog:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->FreezingRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->HeavyRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightFreezingRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightRainAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightSleet:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightSnow:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightSnowAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightSnowShowers:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Mist:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Overcast:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->PartlyCloudy:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Rain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->RaindAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Sleet:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Snow:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->SnowAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->SnowShowers:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Sun:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Sunny:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Thunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    aput-object v2, v0, v1

    sput-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->$VALUES:[Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .param p3, "description"    # I
    .param p4, "icon7Days"    # I
    .param p5, "icon24Hours"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 50
    iput p3, p0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->mDescription:I

    .line 51
    iput p4, p0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->mIcon7Days:I

    .line 52
    iput p5, p0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->mIcon24Hours:I

    .line 53
    return-void
.end method

.method public static fromWeatherCondition(Lse/volvocars/acu/weather/WeatherCondition;)Lse/volvocars/acu/weather/settings/WeatherForecastCondition;
    .locals 2
    .param p0, "c"    # Lse/volvocars/acu/weather/WeatherCondition;

    .prologue
    .line 81
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition$1;->$SwitchMap$se$volvocars$acu$weather$WeatherCondition:[I

    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherCondition;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 99
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Sun:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    :goto_0
    return-object v0

    .line 82
    :pswitch_0
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Cloudy:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 83
    :pswitch_1
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Fog:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 84
    :pswitch_2
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->HeavyRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 85
    :pswitch_3
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightRain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 86
    :pswitch_4
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightRainAndThunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 87
    :pswitch_5
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightSleet:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 88
    :pswitch_6
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->LightSnow:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 89
    :pswitch_7
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Overcast:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 90
    :pswitch_8
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->PartlyCloudy:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 91
    :pswitch_9
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Rain:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 92
    :pswitch_a
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Sleet:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 93
    :pswitch_b
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Snow:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 94
    :pswitch_c
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->SnowShowers:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 95
    :pswitch_d
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Sun:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 96
    :pswitch_e
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Sunny:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 97
    :pswitch_f
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->Thunder:Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lse/volvocars/acu/weather/settings/WeatherForecastCondition;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    .end local p0    # "name":Ljava/lang/String;
    check-cast p0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    return-object p0
.end method

.method public static values()[Lse/volvocars/acu/weather/settings/WeatherForecastCondition;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->$VALUES:[Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    invoke-virtual {v0}, [Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    return-object v0
.end method


# virtual methods
.method public getDescriptionId()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->mDescription:I

    return v0
.end method

.method public getDrawable24HoursId()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->mIcon24Hours:I

    return v0
.end method

.method public getDrawable7DaysId()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->mIcon7Days:I

    return v0
.end method
