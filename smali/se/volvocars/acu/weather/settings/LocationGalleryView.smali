.class public Lse/volvocars/acu/weather/settings/LocationGalleryView;
.super Landroid/widget/Gallery;
.source "LocationGalleryView.java"

# interfaces
.implements Landroid/location/LocationListener;


# static fields
.field private static final COORDINATE_MINIMUM_DISTANCE_IN_M:I = 0x7530

.field private static final MAX_SPEED:F = 1200.0f

.field private static final TAG:Ljava/lang/String; = "LocationGalleryView"


# instance fields
.field private mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

.field private mLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation
.end field

.field private mOnGalleryItemChangedListener:Lse/volvocars/acu/weather/settings/OnGalleryItemChangedListener;

.field private mPosition:I

.field private mSettings:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/Gallery;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const-string v0, "weatherPrefs"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mSettings:Landroid/content/SharedPreferences;

    .line 47
    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->setCallbackDuringFling(Z)V

    .line 49
    new-instance v0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    const v1, 0x7f030016

    invoke-direct {v0, p1, v1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    .line 50
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 67
    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 70
    return-void
.end method

.method static synthetic access$000(Lse/volvocars/acu/weather/settings/LocationGalleryView;)Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/LocationGalleryView;

    .prologue
    .line 25
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    return-object v0
.end method

.method private loadPrefs()V
    .locals 15

    .prologue
    .line 77
    iget-object v11, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mSettings:Landroid/content/SharedPreferences;

    const-string v12, "locations"

    const/4 v13, 0x0

    invoke-interface {v11, v12, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 78
    .local v10, "storedLocations":Ljava/lang/String;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    .line 79
    iget-object v11, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    invoke-static {v10}, Lse/volvocars/acu/weather/settings/SettingsView;->decodeLocationString(Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 81
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "location"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/location/LocationManager;

    .line 82
    .local v6, "manager":Landroid/location/LocationManager;
    const-string v11, "gps"

    invoke-virtual {v6, v11}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v3

    .line 85
    .local v3, "location":Landroid/location/Location;
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getContext()Landroid/content/Context;

    move-result-object v11

    const-string v12, "weatherPrefs"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 86
    .local v9, "settings":Landroid/content/SharedPreferences;
    const-string v11, "CurrentGpsPosition"

    const-string v12, ""

    invoke-interface {v9, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 87
    .local v7, "selectedGpsLocation":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_0

    const-string v11, ","

    invoke-virtual {v7, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 88
    const/4 v11, 0x0

    const/16 v12, 0x2c

    invoke-virtual {v7, v12}, Ljava/lang/String;->indexOf(I)I

    move-result v12

    invoke-virtual {v7, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 89
    .local v0, "lat":D
    const/16 v11, 0x2c

    invoke-virtual {v7, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v7, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 90
    .local v4, "lon":D
    new-instance v2, Lse/volvocars/acu/weather/settings/Location;

    const-string v11, "Current Location"

    const-string v12, ""

    const-string v13, ""

    new-instance v14, Lse/volvocars/acu/weather/Coordinate;

    invoke-direct {v14, v0, v1, v4, v5}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    invoke-direct {v2, v11, v12, v13, v14}, Lse/volvocars/acu/weather/settings/Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lse/volvocars/acu/weather/Coordinate;)V

    .line 91
    .local v2, "loc":Lse/volvocars/acu/weather/settings/Location;
    iget-object v11, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    const/4 v12, 0x0

    invoke-interface {v11, v12, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 97
    .end local v0    # "lat":D
    .end local v2    # "loc":Lse/volvocars/acu/weather/settings/Location;
    .end local v4    # "lon":D
    :goto_0
    iget-object v11, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mSettings:Landroid/content/SharedPreferences;

    const-string v12, "SelectedLocation"

    const-string v13, ""

    invoke-interface {v11, v12, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 99
    .local v8, "selectedLocation":Ljava/lang/String;
    const-string v11, "LocationGalleryView"

    const-string v12, "updating gallery adapter, be aware.. be very aware"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v11, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    iget-object v12, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    invoke-virtual {v11, v12}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->setLocationList(Ljava/util/List;)V

    .line 101
    iget-object v11, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    invoke-virtual {v11}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->notifyDataSetChanged()V

    .line 103
    invoke-virtual {p0, v8}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->setSelectedLocation(Ljava/lang/String;)V

    .line 105
    return-void

    .line 94
    .end local v8    # "selectedLocation":Ljava/lang/String;
    :cond_0
    iget-object v11, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    const/4 v12, 0x0

    sget-object v13, Lse/volvocars/acu/weather/WeatherWidget;->CURRENT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

    invoke-interface {v11, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/SpinnerAdapter;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    return-object v0
.end method

.method public getAdapterItemCount()I
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getLocationPosition()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mPosition:I

    return v0
.end method

.method protected notifyOnGalleryItemChanged(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 221
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mOnGalleryItemChangedListener:Lse/volvocars/acu/weather/settings/OnGalleryItemChangedListener;

    invoke-interface {v0, p1}, Lse/volvocars/acu/weather/settings/OnGalleryItemChangedListener;->onGalleryItemChanged(I)V

    .line 222
    const-string v0, "LocationGalleryView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Notifying gallery position changed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 182
    invoke-super {p0}, Landroid/widget/Gallery;->onFinishInflate()V

    .line 200
    new-instance v0, Lse/volvocars/acu/weather/settings/LocationGalleryView$1;

    invoke-direct {v0, p0}, Lse/volvocars/acu/weather/settings/LocationGalleryView$1;-><init>(Lse/volvocars/acu/weather/settings/LocationGalleryView;)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 218
    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/high16 v2, 0x44960000    # 1200.0f

    .line 165
    move v0, p3

    .line 166
    .local v0, "newVelocityX":F
    cmpl-float v1, p3, v2

    if-lez v1, :cond_1

    .line 168
    const/high16 v0, 0x44960000    # 1200.0f

    .line 174
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, v0, p4}, Landroid/widget/Gallery;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v1

    return v1

    .line 170
    :cond_1
    cmpg-float v1, p3, v2

    if-gez v1, :cond_0

    .line 172
    const/high16 v0, -0x3b6a0000    # -1200.0f

    goto :goto_0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 13
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    .line 243
    if-eqz p1, :cond_3

    .line 245
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v9

    const-wide v11, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->round(D)J

    move-result-wide v9

    long-to-double v9, v9

    const-wide v11, 0x412e848000000000L    # 1000000.0

    div-double v4, v9, v11

    .line 246
    .local v4, "lat":D
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v9

    const-wide v11, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->round(D)J

    move-result-wide v9

    long-to-double v9, v9

    const-wide v11, 0x412e848000000000L    # 1000000.0

    div-double v6, v9, v11

    .line 248
    .local v6, "lon":D
    const-string v9, "LocationGalleryView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Location changed: provider="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " long="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " lat="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    new-instance v0, Lse/volvocars/acu/weather/settings/Location;

    const-string v9, ""

    const-string v10, ""

    const-string v11, ""

    new-instance v12, Lse/volvocars/acu/weather/Coordinate;

    invoke-direct {v12, v4, v5, v6, v7}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    invoke-direct {v0, v9, v10, v11, v12}, Lse/volvocars/acu/weather/settings/Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lse/volvocars/acu/weather/Coordinate;)V

    .line 252
    .local v0, "currentLocation":Lse/volvocars/acu/weather/settings/Location;
    iget-object v9, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getLocation(I)Lse/volvocars/acu/weather/settings/Location;

    move-result-object v9

    invoke-virtual {v9}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v8

    .line 253
    .local v8, "previousLocation":Lse/volvocars/acu/weather/Coordinate;
    const-wide/16 v1, 0x0

    .line 254
    .local v1, "distance":D
    if-eqz v8, :cond_0

    .line 255
    new-instance v3, Landroid/location/Location;

    const-string v9, "gps"

    invoke-direct {v3, v9}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 256
    .local v3, "l":Landroid/location/Location;
    invoke-virtual {v8}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v9

    invoke-virtual {v3, v9, v10}, Landroid/location/Location;->setLatitude(D)V

    .line 257
    invoke-virtual {v8}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v9

    invoke-virtual {v3, v9, v10}, Landroid/location/Location;->setLongitude(D)V

    .line 258
    invoke-virtual {v3, p1}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v9

    float-to-double v1, v9

    .line 259
    const-string v9, "LocationGalleryView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Distance from previous location: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "m"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    .end local v3    # "l":Landroid/location/Location;
    :cond_0
    const-wide/16 v9, 0x0

    cmpl-double v9, v1, v9

    if-eqz v9, :cond_1

    const-wide v9, 0x40dd4c0000000000L    # 30000.0

    cmpl-double v9, v1, v9

    if-lez v9, :cond_2

    .line 264
    :cond_1
    iget-object v9, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    const/4 v10, 0x0

    invoke-virtual {v9, v0, v10}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->swap(Lse/volvocars/acu/weather/settings/Location;I)V

    .line 265
    iget-object v9, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    const/4 v10, 0x0

    invoke-interface {v9, v10, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 266
    const-string v9, "LocationGalleryView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Current location set to: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_2
    iget-object v9, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    invoke-virtual {v9}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->notifyDataSetChanged()V

    .line 271
    .end local v0    # "currentLocation":Lse/volvocars/acu/weather/settings/Location;
    .end local v1    # "distance":D
    .end local v4    # "lat":D
    .end local v6    # "lon":D
    .end local v8    # "previousLocation":Lse/volvocars/acu/weather/Coordinate;
    :cond_3
    return-void
.end method

.method public onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 293
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 294
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 295
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "weatherPrefs"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 296
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 297
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "CurrentGpsPosition"

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "this":Lse/volvocars/acu/weather/settings/LocationGalleryView;
    check-cast p0, Lse/volvocars/acu/weather/settings/Location;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v3

    invoke-virtual {v3}, Lse/volvocars/acu/weather/Coordinate;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 298
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 300
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "settings":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 3
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 276
    const-string v0, "LocationGalleryView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provider <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "> was disabled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 3
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 281
    const-string v0, "LocationGalleryView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provider <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "> was enabled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 287
    const-string v0, "LocationGalleryView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provider <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "> status changed. status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    return-void
.end method

.method public saveSelectedLocation()V
    .locals 4

    .prologue
    .line 126
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getLocation(I)Lse/volvocars/acu/weather/settings/Location;

    move-result-object v0

    .line 127
    .local v0, "location":Lse/volvocars/acu/weather/settings/Location;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mSettings:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 128
    .local v1, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "SelectedLocation"

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/Location;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 129
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 130
    return-void
.end method

.method public setOnGalleryItemChangedListener(Lse/volvocars/acu/weather/settings/OnGalleryItemChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lse/volvocars/acu/weather/settings/OnGalleryItemChangedListener;

    .prologue
    .line 227
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mOnGalleryItemChangedListener:Lse/volvocars/acu/weather/settings/OnGalleryItemChangedListener;

    .line 228
    return-void
.end method

.method public setSelectedLocation(Ljava/lang/String;)V
    .locals 5
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 113
    const/4 v1, 0x0

    .line 114
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 115
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/weather/settings/Location;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 116
    move v1, v0

    .line 120
    :cond_0
    const-string v2, "LocationGalleryView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Location set to index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->setSelection(I)V

    .line 122
    iput v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mPosition:I

    .line 123
    return-void

    .line 114
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setSelection(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    const/4 v5, 0x0

    const-string v6, "gps"

    .line 134
    const-string v2, "LocationGalleryView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Setting selection: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    invoke-virtual {v3}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getCount()I

    move-result v3

    if-le v2, v3, :cond_0

    .line 154
    :goto_0
    return-void

    .line 138
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Gallery;->setSelection(I)V

    .line 140
    if-nez p1, :cond_1

    .line 141
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/weather/settings/Location;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v2

    sget-object v3, Lse/volvocars/acu/weather/Coordinate;->UNDEF:Lse/volvocars/acu/weather/Coordinate;

    if-eq v2, v3, :cond_2

    .line 142
    new-instance v0, Landroid/location/Location;

    const-string v2, "gps"

    invoke-direct {v0, v6}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 143
    .local v0, "loc":Landroid/location/Location;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/weather/settings/Location;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v2

    invoke-virtual {v2}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 144
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView;->mLocations:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/weather/settings/Location;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v2

    invoke-virtual {v2}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 145
    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->onLocationChanged(Landroid/location/Location;)V

    .line 152
    .end local v0    # "loc":Landroid/location/Location;
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->saveSelectedLocation()V

    goto :goto_0

    .line 148
    :cond_2
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "location"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    .line 149
    .local v1, "manager":Landroid/location/LocationManager;
    const-string v2, "gps"

    invoke-virtual {v1, v6}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v2

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_1
.end method

.method public updateAdapter()V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->loadPrefs()V

    .line 233
    return-void
.end method
