.class Lse/volvocars/acu/weather/settings/LocationGalleryView$1;
.super Ljava/lang/Object;
.source "LocationGalleryView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/weather/settings/LocationGalleryView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/LocationGalleryView;


# direct methods
.method constructor <init>(Lse/volvocars/acu/weather/settings/LocationGalleryView;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView$1;->this$0:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 205
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 213
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 207
    :pswitch_0
    const-string v0, "LocationGalleryView"

    const-string v1, "On touch up, flipping weather forecast."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView$1;->this$0:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    # getter for: Lse/volvocars/acu/weather/settings/LocationGalleryView;->mGalleryAdapter:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->access$000(Lse/volvocars/acu/weather/settings/LocationGalleryView;)Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->flip()V

    .line 209
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryView$1;->this$0:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->invalidate()V

    goto :goto_0

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
