.class Lse/volvocars/acu/weather/settings/WeatherSettings$2;
.super Ljava/lang/Object;
.source "WeatherSettings.java"

# interfaces
.implements Lse/volvocars/acu/weather/settings/OnLocationFlingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/weather/settings/WeatherSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;


# direct methods
.method constructor <init>(Lse/volvocars/acu/weather/settings/WeatherSettings;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$2;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(I)V
    .locals 3
    .param p1, "fling"    # I

    .prologue
    const/4 v2, 0x1

    .line 115
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$2;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    # getter for: Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;
    invoke-static {v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->access$000(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-result-object v1

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getSelectedItemPosition()I

    move-result v0

    .line 116
    .local v0, "pos":I
    if-gez p1, :cond_1

    const/4 v1, -0x1

    :goto_0
    add-int/2addr v0, v1

    .line 117
    if-gez v0, :cond_2

    .line 118
    const/4 v0, 0x0

    .line 123
    :cond_0
    :goto_1
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$2;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    # getter for: Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;
    invoke-static {v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->access$000(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->setSelection(I)V

    .line 124
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$2;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    # invokes: Lse/volvocars/acu/weather/settings/WeatherSettings;->setTabButtonAndTitleToLocationAtPosition(I)V
    invoke-static {v1, v0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->access$100(Lse/volvocars/acu/weather/settings/WeatherSettings;I)V

    .line 126
    return-void

    :cond_1
    move v1, v2

    .line 116
    goto :goto_0

    .line 119
    :cond_2
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$2;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    # getter for: Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;
    invoke-static {v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->access$000(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-result-object v1

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 120
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$2;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    # getter for: Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;
    invoke-static {v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->access$000(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-result-object v1

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getCount()I

    move-result v1

    sub-int v0, v1, v2

    goto :goto_1
.end method
