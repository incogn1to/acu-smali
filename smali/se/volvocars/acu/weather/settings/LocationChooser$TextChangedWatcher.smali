.class final Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;
.super Ljava/lang/Object;
.source "LocationChooser.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/LocationChooser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TextChangedWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/LocationChooser;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/weather/settings/LocationChooser;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/weather/settings/LocationChooser;Lse/volvocars/acu/weather/settings/LocationChooser$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/weather/settings/LocationChooser;
    .param p2, "x1"    # Lse/volvocars/acu/weather/settings/LocationChooser$1;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;-><init>(Lse/volvocars/acu/weather/settings/LocationChooser;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 114
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 111
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 97
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mBackgroundSearch:Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$500(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mBackgroundSearch:Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$500(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;

    move-result-object v0

    invoke-virtual {v0, v4}, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->cancel(Z)Z

    .line 100
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    new-instance v1, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;-><init>(Lse/volvocars/acu/weather/settings/LocationChooser;Lse/volvocars/acu/weather/settings/LocationChooser$1;)V

    # setter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mBackgroundSearch:Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;
    invoke-static {v0, v1}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$502(Lse/volvocars/acu/weather/settings/LocationChooser;Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;)Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;

    .line 101
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$300(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchListAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->setSearchString(Ljava/lang/CharSequence;)V

    .line 102
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v5, :cond_1

    .line 103
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mBackgroundSearch:Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$500(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/CharSequence;

    aput-object p1, v1, v4

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/LocationChooser$BackgroundLocationSearch;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 108
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->noMatchTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$400(Lse/volvocars/acu/weather/settings/LocationChooser;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationChooser$TextChangedWatcher;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$300(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->clear()V

    goto :goto_0
.end method
