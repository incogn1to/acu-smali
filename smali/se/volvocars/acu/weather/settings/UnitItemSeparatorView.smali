.class public Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;
.super Landroid/widget/LinearLayout;
.source "UnitItemSeparatorView.java"


# instance fields
.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 36
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "fonts/VolvoSanProLig.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 37
    .local v0, "typeface":Landroid/graphics/Typeface;
    const v1, 0x7f07003b

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->mTextView:Landroid/widget/TextView;

    .line 38
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 40
    invoke-virtual {p0, v4}, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    invoke-virtual {p0, v4}, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 42
    invoke-virtual {p0, v3}, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->setLongClickable(Z)V

    .line 43
    invoke-virtual {p0, v3}, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->setFocusable(Z)V

    .line 44
    invoke-virtual {p0, v3}, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->setClickable(Z)V

    .line 45
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 29
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/UnitItemSeparatorView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 30
    return-void
.end method
