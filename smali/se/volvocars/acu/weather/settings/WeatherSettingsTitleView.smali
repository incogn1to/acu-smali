.class public Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;
.super Landroid/widget/LinearLayout;
.source "WeatherSettingsTitleView.java"


# static fields
.field public static final FORMAT_CELCIUS:Lse/volvocars/acu/weather/Temperature$Unit;

.field public static final FORMAT_DEFAULT:Lse/volvocars/acu/weather/Temperature$Unit;

.field public static final FORMAT_FAHRENHEIT:Lse/volvocars/acu/weather/Temperature$Unit;

.field private static final TAG:Ljava/lang/String; = "WeatherSettings"


# instance fields
.field private mCurrentFormat:Lse/volvocars/acu/weather/Temperature$Unit;

.field private mSettings:Landroid/content/SharedPreferences;

.field private mUnitChangedListener:Lse/volvocars/acu/weather/settings/UnitChangedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    sput-object v0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->FORMAT_CELCIUS:Lse/volvocars/acu/weather/Temperature$Unit;

    .line 26
    sget-object v0, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    sput-object v0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->FORMAT_FAHRENHEIT:Lse/volvocars/acu/weather/Temperature$Unit;

    .line 27
    sget-object v0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->FORMAT_CELCIUS:Lse/volvocars/acu/weather/Temperature$Unit;

    sput-object v0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->FORMAT_DEFAULT:Lse/volvocars/acu/weather/Temperature$Unit;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const-string v0, "weatherPrefs"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mSettings:Landroid/content/SharedPreferences;

    .line 36
    return-void
.end method

.method private loadPrefs()V
    .locals 3

    .prologue
    .line 88
    const/4 v0, 0x5

    .line 89
    .local v0, "tempUnitKey":I
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mSettings:Landroid/content/SharedPreferences;

    const-string v2, "temp_unit"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 90
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 91
    sget-object v1, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->setFormat(Lse/volvocars/acu/weather/Temperature$Unit;)V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 93
    sget-object v1, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->setFormat(Lse/volvocars/acu/weather/Temperature$Unit;)V

    goto :goto_0
.end method

.method private setTextAndTypeface()V
    .locals 4

    .prologue
    .line 137
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "fonts/VolvoSanProLig.otf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 139
    .local v1, "typeface":Landroid/graphics/Typeface;
    const v2, 0x7f07002f

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 140
    .local v0, "settingsTitle":Landroid/widget/TextView;
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 141
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09002f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->setTextAndTypeface()V

    .line 41
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->loadPrefs()V

    .line 42
    const v2, 0x7f070030

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 43
    .local v0, "celsius":Landroid/view/View;
    new-instance v2, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView$1;

    invoke-direct {v2, p0}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView$1;-><init>(Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    const v2, 0x7f070031

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 53
    .local v1, "fahrenheit":Landroid/view/View;
    new-instance v2, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView$2;

    invoke-direct {v2, p0}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView$2;-><init>(Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-void
.end method

.method public setFormat(Lse/volvocars/acu/weather/Temperature$Unit;)V
    .locals 5
    .param p1, "format"    # Lse/volvocars/acu/weather/Temperature$Unit;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 103
    const v2, 0x7f070030

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 104
    .local v0, "formatCelcius":Landroid/widget/ImageView;
    const v2, 0x7f070031

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 106
    .local v1, "formatFahrenheit":Landroid/widget/ImageView;
    sget-object v2, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    if-ne p1, v2, :cond_2

    .line 107
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 108
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 109
    sget-object v2, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    iput-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mCurrentFormat:Lse/volvocars/acu/weather/Temperature$Unit;

    .line 110
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mUnitChangedListener:Lse/volvocars/acu/weather/settings/UnitChangedListener;

    if-eqz v2, :cond_0

    .line 111
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mUnitChangedListener:Lse/volvocars/acu/weather/settings/UnitChangedListener;

    const/4 v3, 0x5

    invoke-interface {v2, v3}, Lse/volvocars/acu/weather/settings/UnitChangedListener;->onTempUnitChanged(I)V

    .line 128
    :cond_0
    :goto_0
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mCurrentFormat:Lse/volvocars/acu/weather/Temperature$Unit;

    if-eqz v2, :cond_1

    .line 129
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->storePrefs()V

    .line 131
    :cond_1
    return-void

    .line 114
    :cond_2
    sget-object v2, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    if-ne p1, v2, :cond_3

    .line 115
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 116
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 117
    sget-object v2, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    iput-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mCurrentFormat:Lse/volvocars/acu/weather/Temperature$Unit;

    .line 119
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mUnitChangedListener:Lse/volvocars/acu/weather/settings/UnitChangedListener;

    if-eqz v2, :cond_0

    .line 120
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mUnitChangedListener:Lse/volvocars/acu/weather/settings/UnitChangedListener;

    const/4 v3, 0x6

    invoke-interface {v2, v3}, Lse/volvocars/acu/weather/settings/UnitChangedListener;->onTempUnitChanged(I)V

    goto :goto_0

    .line 123
    :cond_3
    const-string v2, "WeatherSettings"

    const-string v3, "setFormat: illegal temperature format as input. Setting default format"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    const/4 v2, 0x0

    iput-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mCurrentFormat:Lse/volvocars/acu/weather/Temperature$Unit;

    .line 125
    sget-object v2, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->FORMAT_DEFAULT:Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->setFormat(Lse/volvocars/acu/weather/Temperature$Unit;)V

    goto :goto_0
.end method

.method public setUnitChangedListener(Lse/volvocars/acu/weather/settings/UnitChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lse/volvocars/acu/weather/settings/UnitChangedListener;

    .prologue
    .line 146
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mUnitChangedListener:Lse/volvocars/acu/weather/settings/UnitChangedListener;

    .line 147
    return-void
.end method

.method storePrefs()V
    .locals 4

    .prologue
    const-string v3, "temp_unit"

    .line 68
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mSettings:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 72
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mCurrentFormat:Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/Temperature$Unit;->getUnitLetter()C

    move-result v1

    sget-object v2, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/Temperature$Unit;->getUnitLetter()C

    move-result v2

    if-ne v1, v2, :cond_1

    .line 73
    const-string v1, "temp_unit"

    const/4 v1, 0x5

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 74
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->mCurrentFormat:Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/Temperature$Unit;->getUnitLetter()C

    move-result v1

    sget-object v2, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/Temperature$Unit;->getUnitLetter()C

    move-result v2

    if-ne v1, v2, :cond_0

    .line 77
    const-string v1, "temp_unit"

    const/4 v1, 0x6

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 78
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
