.class Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;
.super Ljava/lang/Object;
.source "LocationGalleryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewPack"
.end annotation


# instance fields
.field private mLastPrepared:J

.field private mTag:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$1;

    .prologue
    .line 282
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;-><init>()V

    return-void
.end method


# virtual methods
.method public getLastPreparedTime()J
    .locals 2

    .prologue
    .line 295
    iget-wide v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->mLastPrepared:J

    return-wide v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->mTag:Ljava/lang/Object;

    return-object v0
.end method

.method public setLastPreparedTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 299
    iput-wide p1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->mLastPrepared:J

    .line 300
    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 0
    .param p1, "t"    # Ljava/lang/Object;

    .prologue
    .line 291
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$ViewPack;->mTag:Ljava/lang/Object;

    .line 292
    return-void
.end method
