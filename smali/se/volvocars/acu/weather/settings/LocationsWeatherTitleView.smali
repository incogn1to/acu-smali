.class public Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;
.super Landroid/widget/LinearLayout;
.source "LocationsWeatherTitleView.java"


# instance fields
.field private mArea:Landroid/widget/TextView;

.field private mCountry:Landroid/widget/TextView;

.field private final mCurrentLocationString:Ljava/lang/String;

.field private mLocation:Landroid/widget/TextView;

.field private mSplitLine:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mCurrentLocationString:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 30
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "fonts/VolvoSanProLig.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 31
    .local v0, "typeface":Landroid/graphics/Typeface;
    const v1, 0x7f07002b

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mLocation:Landroid/widget/TextView;

    .line 32
    const v1, 0x7f07002e

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mCountry:Landroid/widget/TextView;

    .line 33
    const v1, 0x7f07002d

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mArea:Landroid/widget/TextView;

    .line 34
    const v1, 0x7f07002c

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mSplitLine:Landroid/view/View;

    .line 35
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mLocation:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 36
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mCountry:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 37
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mArea:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 38
    return-void
.end method

.method public setCurrentLocation(Lse/volvocars/acu/weather/settings/Location;Z)V
    .locals 6
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;
    .param p2, "currentLocation"    # Z

    .prologue
    const/4 v5, -0x2

    const-string v4, ""

    .line 41
    sget-object v2, Lse/volvocars/acu/weather/WeatherWidget;->CURRENT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

    if-ne p1, v2, :cond_0

    .line 42
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090031

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "nodata":Ljava/lang/String;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mLocation:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mCountry:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mArea:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    .end local v0    # "nodata":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mCountry:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mArea:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 58
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 59
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mLocation:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 61
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mSplitLine:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 69
    :goto_1
    return-void

    .line 46
    .end local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    const-string v2, ""

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 47
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mLocation:Landroid/widget/TextView;

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mCountry:Landroid/widget/TextView;

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mArea:Landroid/widget/TextView;

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getArea()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 52
    :cond_1
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mLocation:Landroid/widget/TextView;

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mCurrentLocationString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mCountry:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mArea:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 63
    :cond_2
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 64
    .restart local v1    # "params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mLocation:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->mSplitLine:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
