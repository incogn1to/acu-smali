.class Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;
.super Ljava/lang/Object;
.source "LocationGalleryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WindRepresentation"
.end annotation


# instance fields
.field private mWindSpeedUnit:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

.field private mWindUnitString:Ljava/lang/String;

.field final synthetic this$0:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;I)V
    .locals 1
    .param p2, "windUnit"    # I

    .prologue
    .line 532
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;->this$0:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 534
    packed-switch p2, :pswitch_data_0

    .line 553
    :goto_0
    # invokes: Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getUnitStringFromConstant(I)Ljava/lang/String;
    invoke-static {p1, p2}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->access$400(Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;->mWindUnitString:Ljava/lang/String;

    .line 554
    return-void

    .line 536
    :pswitch_0
    sget-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->mps:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;->mWindSpeedUnit:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    goto :goto_0

    .line 539
    :pswitch_1
    sget-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->knots:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;->mWindSpeedUnit:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    goto :goto_0

    .line 542
    :pswitch_2
    sget-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->kmph:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;->mWindSpeedUnit:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    goto :goto_0

    .line 545
    :pswitch_3
    sget-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->mph:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;->mWindSpeedUnit:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    goto :goto_0

    .line 548
    :pswitch_4
    sget-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->beaufort:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;->mWindSpeedUnit:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    goto :goto_0

    .line 534
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getWindSpeed(Lse/volvocars/acu/weather/Wind;)I
    .locals 2
    .param p1, "wind"    # Lse/volvocars/acu/weather/Wind;

    .prologue
    .line 561
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;->mWindSpeedUnit:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    invoke-virtual {p1, v0}, Lse/volvocars/acu/weather/Wind;->getSpeed(Lse/volvocars/acu/weather/Wind$WindSpeedUnit;)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public getWindUnitString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;->mWindUnitString:Ljava/lang/String;

    return-object v0
.end method
