.class final Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;
.super Ljava/lang/Object;
.source "LocationGalleryAdapter.java"

# interfaces
.implements Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Forecast24hReceiver"
.end annotation


# instance fields
.field private final mCtxt:Landroid/content/Context;

.field private final mCurrentDate:I

.field private final mFrom:Ljava/util/Date;

.field private final mHourItem:Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;

.field private final mTempUnit:I

.field private final mWindRepr:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;Landroid/content/Context;Ljava/util/Date;ILse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;I)V
    .locals 0
    .param p1, "windRepresentation"    # Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "from"    # Ljava/util/Date;
    .param p4, "currentDate"    # I
    .param p5, "hourItem"    # Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;
    .param p6, "temperatureUnit"    # I

    .prologue
    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mWindRepr:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;

    .line 226
    iput-object p2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mCtxt:Landroid/content/Context;

    .line 227
    iput-object p3, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mFrom:Ljava/util/Date;

    .line 228
    iput p4, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mCurrentDate:I

    .line 229
    iput-object p5, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mHourItem:Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;

    .line 230
    iput p6, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mTempUnit:I

    .line 231
    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;Landroid/content/Context;Ljava/util/Date;ILse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;ILse/volvocars/acu/weather/settings/LocationGalleryAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;
    .param p2, "x1"    # Landroid/content/Context;
    .param p3, "x2"    # Ljava/util/Date;
    .param p4, "x3"    # I
    .param p5, "x4"    # Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;
    .param p6, "x5"    # I
    .param p7, "x6"    # Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$1;

    .prologue
    .line 214
    invoke-direct/range {p0 .. p6}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;-><init>(Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;Landroid/content/Context;Ljava/util/Date;ILse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;I)V

    return-void
.end method


# virtual methods
.method public onWeatherReceive(Lse/volvocars/acu/weather/CurrentWeather;)V
    .locals 15
    .param p1, "forecast"    # Lse/volvocars/acu/weather/CurrentWeather;

    .prologue
    .line 237
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mCtxt:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090037

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 238
    .local v2, "temp":Ljava/lang/String;
    const/4 v3, -0x1

    .line 239
    .local v3, "windResourceId":I
    const/4 v5, -0x1

    .line 240
    .local v5, "iconResourceId":I
    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    .line 241
    .local v10, "windSpeed":D
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mCtxt:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 242
    .local v9, "windDirection":Ljava/lang/String;
    const-string v12, ""

    .line 245
    .local v12, "windUnit":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 246
    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/weather/CurrentWeather;->getTemperature()Lse/volvocars/acu/weather/Temperature;

    move-result-object v0

    iget v1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mTempUnit:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .end local v2    # "temp":Ljava/lang/String;
    sget-object v1, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    :goto_0
    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/Temperature;->toString(Lse/volvocars/acu/weather/Temperature$Unit;)Ljava/lang/String;

    move-result-object v2

    .line 247
    .restart local v2    # "temp":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/weather/CurrentWeather;->getWind()Lse/volvocars/acu/weather/Wind;

    move-result-object v8

    .line 248
    .local v8, "wind":Lse/volvocars/acu/weather/Wind;
    invoke-virtual {v8}, Lse/volvocars/acu/weather/Wind;->getWindDirectionResource()I

    move-result v3

    .line 249
    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/weather/CurrentWeather;->getForecastCondition()Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->getDrawable24HoursId()I

    move-result v5

    .line 250
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mWindRepr:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;->getWindUnitString()Ljava/lang/String;

    move-result-object v12

    .line 251
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mWindRepr:Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;

    invoke-virtual {v0, v8}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$WindRepresentation;->getWindSpeed(Lse/volvocars/acu/weather/Wind;)I

    move-result v0

    int-to-double v10, v0

    .line 252
    invoke-virtual {v8}, Lse/volvocars/acu/weather/Wind;->getDirection()Lse/volvocars/acu/weather/Wind$WindDirection;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/Wind$WindDirection;->name()Ljava/lang/String;

    move-result-object v9

    .line 256
    .end local v8    # "wind":Lse/volvocars/acu/weather/Wind;
    :cond_0
    const-string v0, "%tR"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v6, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mFrom:Ljava/util/Date;

    aput-object v6, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 257
    .local v1, "time":Ljava/lang/String;
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mCtxt:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "%tI"

    .end local v1    # "time":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mFrom:Ljava/util/Date;

    aput-object v7, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%tM"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mFrom:Ljava/util/Date;

    aput-object v7, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 261
    .restart local v1    # "time":Ljava/lang/String;
    :cond_1
    const-string v0, "%Tp"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mFrom:Ljava/util/Date;

    aput-object v7, v4, v6

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 264
    .local v7, "ampm":Ljava/lang/String;
    const-wide/high16 v13, -0x4010000000000000L    # -1.0

    cmpl-double v0, v10, v13

    if-eqz v0, :cond_3

    const-wide/high16 v13, -0x4000000000000000L    # -2.0

    cmpl-double v0, v10, v13

    if-eqz v0, :cond_3

    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    double-to-int v4, v10

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 274
    .local v4, "windString":Ljava/lang/String;
    :goto_1
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mHourItem:Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;

    iget v6, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mCurrentDate:I

    iget-object v8, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mFrom:Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getDate()I

    move-result v8

    if-eq v6, v8, :cond_5

    const/4 v6, 0x1

    :goto_2
    invoke-virtual/range {v0 .. v7}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->setData(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IZLjava/lang/String;)V

    .line 275
    return-void

    .line 246
    .end local v1    # "time":Ljava/lang/String;
    .end local v2    # "temp":Ljava/lang/String;
    .end local v4    # "windString":Ljava/lang/String;
    .end local v7    # "ampm":Ljava/lang/String;
    :cond_2
    sget-object v1, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    goto/16 :goto_0

    .line 267
    .restart local v1    # "time":Ljava/lang/String;
    .restart local v2    # "temp":Ljava/lang/String;
    .restart local v7    # "ampm":Ljava/lang/String;
    :cond_3
    const-wide/high16 v13, -0x4000000000000000L    # -2.0

    cmpl-double v0, v10, v13

    if-nez v0, :cond_4

    .line 268
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast24hReceiver;->mCtxt:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090037

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .end local v3    # "windResourceId":I
    move-result-object v4

    .line 269
    .restart local v4    # "windString":Ljava/lang/String;
    const/4 v3, -0x1

    .restart local v3    # "windResourceId":I
    goto :goto_1

    .line 272
    .end local v4    # "windString":Ljava/lang/String;
    :cond_4
    move-object v4, v9

    .restart local v4    # "windString":Ljava/lang/String;
    goto :goto_1

    .line 274
    :cond_5
    const/4 v6, 0x0

    goto :goto_2
.end method
