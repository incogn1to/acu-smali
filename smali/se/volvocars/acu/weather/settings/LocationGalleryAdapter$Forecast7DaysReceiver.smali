.class final Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;
.super Ljava/lang/Object;
.source "LocationGalleryAdapter.java"

# interfaces
.implements Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Forecast7DaysReceiver"
.end annotation


# instance fields
.field private mCtxt:Landroid/content/Context;

.field private mDayItem:Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

.field private mFrom:Ljava/util/Date;

.field private mTempUnit:I


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/util/Date;Lse/volvocars/acu/weather/settings/SevenDayForecastItem;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "from"    # Ljava/util/Date;
    .param p3, "dayItem"    # Lse/volvocars/acu/weather/settings/SevenDayForecastItem;
    .param p4, "temperatureUnit"    # I

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;->mCtxt:Landroid/content/Context;

    .line 191
    iput-object p2, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;->mFrom:Ljava/util/Date;

    .line 192
    iput-object p3, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;->mDayItem:Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    .line 193
    iput p4, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;->mTempUnit:I

    .line 194
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Ljava/util/Date;Lse/volvocars/acu/weather/settings/SevenDayForecastItem;ILse/volvocars/acu/weather/settings/LocationGalleryAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Ljava/util/Date;
    .param p3, "x2"    # Lse/volvocars/acu/weather/settings/SevenDayForecastItem;
    .param p4, "x3"    # I
    .param p5, "x4"    # Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$1;

    .prologue
    .line 182
    invoke-direct {p0, p1, p2, p3, p4}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;-><init>(Landroid/content/Context;Ljava/util/Date;Lse/volvocars/acu/weather/settings/SevenDayForecastItem;I)V

    return-void
.end method


# virtual methods
.method public onWeatherReceive(Lse/volvocars/acu/weather/CurrentWeather;)V
    .locals 10
    .param p1, "forecast"    # Lse/volvocars/acu/weather/CurrentWeather;

    .prologue
    const/4 v8, 0x5

    .line 198
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;->mCtxt:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f090037

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 199
    .local v3, "min":Ljava/lang/String;
    move-object v4, v3

    .line 200
    .local v4, "max":Ljava/lang/String;
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 202
    .local v6, "drawableId":Ljava/lang/Integer;
    if-eqz p1, :cond_0

    .line 203
    invoke-virtual {p1}, Lse/volvocars/acu/weather/CurrentWeather;->minTemp()Lse/volvocars/acu/weather/Temperature;

    move-result-object v0

    iget v5, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;->mTempUnit:I

    if-ne v5, v8, :cond_1

    sget-object v5, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    :goto_0
    invoke-virtual {v0, v5}, Lse/volvocars/acu/weather/Temperature;->toString(Lse/volvocars/acu/weather/Temperature$Unit;)Ljava/lang/String;

    move-result-object v3

    .line 204
    invoke-virtual {p1}, Lse/volvocars/acu/weather/CurrentWeather;->maxTemp()Lse/volvocars/acu/weather/Temperature;

    move-result-object v0

    iget v5, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;->mTempUnit:I

    if-ne v5, v8, :cond_2

    sget-object v5, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    :goto_1
    invoke-virtual {v0, v5}, Lse/volvocars/acu/weather/Temperature;->toString(Lse/volvocars/acu/weather/Temperature$Unit;)Ljava/lang/String;

    move-result-object v4

    .line 205
    invoke-virtual {p1}, Lse/volvocars/acu/weather/CurrentWeather;->getForecastCondition()Lse/volvocars/acu/weather/settings/WeatherForecastCondition;

    move-result-object v7

    .line 206
    .local v7, "wc":Lse/volvocars/acu/weather/settings/WeatherForecastCondition;
    invoke-virtual {v7}, Lse/volvocars/acu/weather/settings/WeatherForecastCondition;->getDrawable7DaysId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 208
    .end local v7    # "wc":Lse/volvocars/acu/weather/settings/WeatherForecastCondition;
    :cond_0
    const-string v0, "%Ta"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;->mFrom:Ljava/util/Date;

    aput-object v9, v5, v8

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 209
    .local v1, "weekDay":Ljava/lang/String;
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;->mFrom:Ljava/util/Date;

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;->mCtxt:Landroid/content/Context;

    # invokes: Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getFormattedDateString(Ljava/util/Date;Landroid/content/Context;)Ljava/lang/String;
    invoke-static {v0, v5}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->access$200(Ljava/util/Date;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 210
    .local v2, "theDate":Ljava/lang/String;
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter$Forecast7DaysReceiver;->mDayItem:Lse/volvocars/acu/weather/settings/SevenDayForecastItem;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->setData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 211
    return-void

    .line 203
    .end local v1    # "weekDay":Ljava/lang/String;
    .end local v2    # "theDate":Ljava/lang/String;
    :cond_1
    sget-object v5, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    goto :goto_0

    .line 204
    :cond_2
    sget-object v5, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    goto :goto_1
.end method
