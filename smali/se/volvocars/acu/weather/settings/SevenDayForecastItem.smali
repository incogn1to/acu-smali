.class public Lse/volvocars/acu/weather/settings/SevenDayForecastItem;
.super Landroid/widget/LinearLayout;
.source "SevenDayForecastItem.java"


# static fields
.field private static final MAX_TEXT_SIZE_DP:I = 0x1c


# instance fields
.field private mDate:Landroid/widget/TextView;

.field private mDay:Landroid/widget/TextView;

.field private mMax:Landroid/widget/TextView;

.field private mMaxWidth:I

.field private mMin:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 33
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03000b

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 34
    .local v1, "item":Landroid/view/View;
    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->addView(Landroid/view/View;)V

    .line 35
    return-void
.end method

.method private resizeText(Ljava/lang/String;)V
    .locals 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/high16 v3, 0x41e00000    # 28.0f

    .line 85
    const/16 v0, 0x1c

    .line 87
    .local v0, "currentSizeDp":I
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 88
    .local v1, "mPaint":Landroid/graphics/Paint;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMax:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 91
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMax:Landroid/widget/TextView;

    invoke-virtual {v2, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 92
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 94
    iget v2, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMaxWidth:I

    if-lez v2, :cond_0

    .line 96
    :goto_0
    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    iget v3, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMaxWidth:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 98
    add-int/lit8 v0, v0, -0x1

    .line 100
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMax:Landroid/widget/TextView;

    int-to-float v3, v0

    invoke-virtual {v2, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 101
    int-to-float v2, v0

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    goto :goto_0

    .line 104
    :cond_0
    return-void
.end method

.method private setBackgroundFromIconId(I)V
    .locals 2
    .param p1, "iconId"    # I

    .prologue
    .line 51
    const/4 v0, 0x0

    .line 52
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 53
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 55
    :cond_0
    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 56
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 62
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "fonts/VolvoSanProLig.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 64
    .local v0, "typeface":Landroid/graphics/Typeface;
    const v1, 0x7f070027

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mDay:Landroid/widget/TextView;

    .line 65
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mDay:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 67
    const v1, 0x7f070028

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mDate:Landroid/widget/TextView;

    .line 68
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mDate:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 70
    const v1, 0x7f07002a

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMin:Landroid/widget/TextView;

    .line 71
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMin:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 73
    const v1, 0x7f070029

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMax:Landroid/widget/TextView;

    .line 74
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMax:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 75
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 80
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMaxWidth:I

    .line 81
    return-void
.end method

.method public setData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "day"    # Ljava/lang/String;
    .param p2, "date"    # Ljava/lang/String;
    .param p3, "min"    # Ljava/lang/String;
    .param p4, "max"    # Ljava/lang/String;
    .param p5, "iconId"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mDay:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMax:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->mMin:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    invoke-direct {p0, p4}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->resizeText(Ljava/lang/String;)V

    .line 46
    invoke-direct {p0, p5}, Lse/volvocars/acu/weather/settings/SevenDayForecastItem;->setBackgroundFromIconId(I)V

    .line 47
    return-void
.end method
