.class public final Lse/volvocars/acu/weather/settings/Location;
.super Ljava/lang/Object;
.source "Location.java"


# instance fields
.field private final mArea:Ljava/lang/String;

.field private final mCity:Ljava/lang/String;

.field private final mCoordinate:Lse/volvocars/acu/weather/Coordinate;

.field private final mCountry:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lse/volvocars/acu/weather/Coordinate;)V
    .locals 2
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "area"    # Ljava/lang/String;
    .param p3, "country"    # Ljava/lang/String;
    .param p4, "coordinate"    # Lse/volvocars/acu/weather/Coordinate;

    .prologue
    const-string v1, ""

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    if-nez p1, :cond_0

    const-string v0, ""

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lse/volvocars/acu/weather/settings/Location;->mCity:Ljava/lang/String;

    .line 23
    if-nez p2, :cond_1

    const-string v0, ""

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lse/volvocars/acu/weather/settings/Location;->mArea:Ljava/lang/String;

    .line 24
    if-nez p3, :cond_2

    const-string v0, ""

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lse/volvocars/acu/weather/settings/Location;->mCountry:Ljava/lang/String;

    .line 25
    if-nez p4, :cond_3

    sget-object v0, Lse/volvocars/acu/weather/Coordinate;->UNDEF:Lse/volvocars/acu/weather/Coordinate;

    :goto_3
    iput-object v0, p0, Lse/volvocars/acu/weather/settings/Location;->mCoordinate:Lse/volvocars/acu/weather/Coordinate;

    .line 26
    return-void

    :cond_0
    move-object v0, p1

    .line 22
    goto :goto_0

    :cond_1
    move-object v0, p2

    .line 23
    goto :goto_1

    :cond_2
    move-object v0, p3

    .line 24
    goto :goto_2

    :cond_3
    move-object v0, p4

    .line 25
    goto :goto_3
.end method

.method public static fromString(Ljava/lang/String;)Lse/volvocars/acu/weather/settings/Location;
    .locals 13
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x0

    .line 56
    if-nez p0, :cond_0

    move-object v9, v12

    .line 73
    :goto_0
    return-object v9

    .line 59
    :cond_0
    const-string v9, ","

    const/4 v10, -0x4

    invoke-virtual {p0, v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "a":[Ljava/lang/String;
    array-length v9, v0

    const/4 v10, 0x5

    if-ge v9, v10, :cond_1

    move-object v9, v12

    .line 61
    goto :goto_0

    .line 63
    :cond_1
    const/4 v9, 0x0

    aget-object v8, v0, v9

    .line 64
    .local v8, "name":Ljava/lang/String;
    const/4 v9, 0x1

    aget-object v1, v0, v9

    .line 65
    .local v1, "area":Ljava/lang/String;
    const/4 v9, 0x2

    aget-object v3, v0, v9

    .line 67
    .local v3, "country":Ljava/lang/String;
    const/4 v9, 0x3

    :try_start_0
    aget-object v9, v0, v9

    invoke-static {v9}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 68
    .local v4, "lat":D
    const/4 v9, 0x4

    aget-object v9, v0, v9

    invoke-static {v9}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 69
    .local v6, "lon":D
    new-instance v9, Lse/volvocars/acu/weather/settings/Location;

    new-instance v10, Lse/volvocars/acu/weather/Coordinate;

    invoke-direct {v10, v4, v5, v6, v7}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    invoke-direct {v9, v8, v1, v3, v10}, Lse/volvocars/acu/weather/settings/Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lse/volvocars/acu/weather/Coordinate;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 70
    .end local v4    # "lat":D
    .end local v6    # "lon":D
    :catch_0
    move-exception v9

    move-object v2, v9

    .line 71
    .local v2, "cause":Ljava/lang/NumberFormatException;
    const-string v9, "Location"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Could not parse string: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v9, v12

    .line 73
    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 14
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/Location;->clone()Lse/volvocars/acu/weather/settings/Location;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lse/volvocars/acu/weather/settings/Location;
    .locals 9

    .prologue
    .line 100
    new-instance v0, Lse/volvocars/acu/weather/settings/Location;

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/Location;->mCity:Ljava/lang/String;

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/Location;->mArea:Ljava/lang/String;

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/Location;->mCountry:Ljava/lang/String;

    new-instance v4, Lse/volvocars/acu/weather/Coordinate;

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/Location;->mCoordinate:Lse/volvocars/acu/weather/Coordinate;

    invoke-virtual {v5}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v5

    iget-object v7, p0, Lse/volvocars/acu/weather/settings/Location;->mCoordinate:Lse/volvocars/acu/weather/Coordinate;

    invoke-virtual {v7}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v7

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/settings/Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lse/volvocars/acu/weather/Coordinate;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 78
    if-ne p0, p1, :cond_0

    move v2, v5

    .line 85
    :goto_0
    return v2

    .line 81
    :cond_0
    instance-of v2, p1, Lse/volvocars/acu/weather/settings/Location;

    if-nez v2, :cond_1

    move v2, v4

    .line 82
    goto :goto_0

    .line 84
    :cond_1
    move-object v0, p1

    check-cast v0, Lse/volvocars/acu/weather/settings/Location;

    move-object v1, v0

    .line 85
    .local v1, "o":Lse/volvocars/acu/weather/settings/Location;
    iget-object v2, v1, Lse/volvocars/acu/weather/settings/Location;->mArea:Ljava/lang/String;

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/Location;->mArea:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Lse/volvocars/acu/weather/settings/Location;->mCity:Ljava/lang/String;

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/Location;->mCity:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Lse/volvocars/acu/weather/settings/Location;->mCountry:Ljava/lang/String;

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/Location;->mCountry:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Lse/volvocars/acu/weather/settings/Location;->mCoordinate:Lse/volvocars/acu/weather/Coordinate;

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/Location;->mCoordinate:Lse/volvocars/acu/weather/Coordinate;

    invoke-virtual {v2, v3}, Lse/volvocars/acu/weather/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v5

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_0
.end method

.method public getArea()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/Location;->mArea:Ljava/lang/String;

    return-object v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/Location;->mCity:Ljava/lang/String;

    return-object v0
.end method

.method public getCoordinate()Lse/volvocars/acu/weather/Coordinate;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/Location;->mCoordinate:Lse/volvocars/acu/weather/Coordinate;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/Location;->mCountry:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/Location;->mArea:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/Location;->mCity:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/Location;->mCountry:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/Location;->mCoordinate:Lse/volvocars/acu/weather/Coordinate;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/Coordinate;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const-string v3, ","

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/Location;->mCity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/Location;->mArea:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/Location;->mCountry:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/Location;->mCoordinate:Lse/volvocars/acu/weather/Coordinate;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/Location;->mCoordinate:Lse/volvocars/acu/weather/Coordinate;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 52
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
