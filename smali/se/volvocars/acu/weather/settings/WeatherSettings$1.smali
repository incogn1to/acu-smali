.class Lse/volvocars/acu/weather/settings/WeatherSettings$1;
.super Ljava/lang/Object;
.source "WeatherSettings.java"

# interfaces
.implements Lse/volvocars/acu/weather/settings/UnitChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/weather/settings/WeatherSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;


# direct methods
.method constructor <init>(Lse/volvocars/acu/weather/settings/WeatherSettings;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$1;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSmhiTargetChanged(I)V
    .locals 0
    .param p1, "smhiTarget"    # I

    .prologue
    .line 105
    return-void
.end method

.method public onTempUnitChanged(I)V
    .locals 1
    .param p1, "tempUnit"    # I

    .prologue
    .line 98
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$1;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    # getter for: Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->access$000(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-result-object v0

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->setTemperatureUnit(I)V

    .line 99
    return-void
.end method

.method public onWindUnitChanged(I)V
    .locals 0
    .param p1, "windUnit"    # I

    .prologue
    .line 94
    return-void
.end method
