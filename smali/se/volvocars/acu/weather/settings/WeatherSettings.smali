.class public Lse/volvocars/acu/weather/settings/WeatherSettings;
.super Landroid/app/Activity;
.source "WeatherSettings.java"

# interfaces
.implements Lse/volvocars/acu/weather/settings/LocationChoosenListener;


# static fields
.field private static final ADD_LOCATION_REQUEST_CODE:I = 0x1

.field private static final ADD_LOCATION_RESULT_KEY:Ljava/lang/String; = "location"

.field public static final EXTRA_CURRENT_LATITUDE:Ljava/lang/String; = "currentPositionLat"

.field public static final EXTRA_CURRENT_LONGITUDE:Ljava/lang/String; = "currentPositionLong"

.field private static final GPS_MINIMUM_DISTANCE_IN_M:I = 0x1388

.field private static final GPS_MINIMUM_INTERVAL_IN_MS:I = 0x493e0

.field public static final INVALID_TAB:I = -0x1

.field public static final PREFERENCE_STRING:Ljava/lang/String; = "se.volvocars.acu.apps"

.field public static final TAB_MY_LOCATIONS:I = 0x0

.field public static final TAB_SETTINGS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "WeatherSettings"

.field private static final TOAST_MAX_WIDTH:I = 0x190

.field private static mStartedFromWidget:Z


# instance fields
.field private mCurrentPosition:Lse/volvocars/acu/weather/Coordinate;

.field private mCurrentTab:I

.field private mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

.field private mLocationManager:Landroid/location/LocationManager;

.field private mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

.field private mMyLocationsTitleView:Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;

.field private mSettingsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

.field private mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

.field private mSmhiLogoView:Landroid/view/View;

.field private mToast:Landroid/widget/Toast;

.field private mToastTextView:Landroid/widget/TextView;

.field private mUnitDialog:Lse/volvocars/acu/weather/settings/UnitDialog;

.field private mWaitingForResult:Z

.field private mWeatherSettingsTitleView:Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    sput-boolean v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mStartedFromWidget:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mWaitingForResult:Z

    return-void
.end method

.method static synthetic access$000(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/LocationGalleryView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/WeatherSettings;

    .prologue
    .line 32
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    return-object v0
.end method

.method static synthetic access$100(Lse/volvocars/acu/weather/settings/WeatherSettings;I)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/WeatherSettings;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->setTabButtonAndTitleToLocationAtPosition(I)V

    return-void
.end method

.method static synthetic access$200(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/SettingsView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/WeatherSettings;

    .prologue
    .line 32
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    return-object v0
.end method

.method static synthetic access$300(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/WeatherSettings;

    .prologue
    .line 32
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mWeatherSettingsTitleView:Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;

    return-object v0
.end method

.method private setTabButtonAndTitleToLocationAtPosition(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 239
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    invoke-virtual {v1, p1}, Lse/volvocars/acu/weather/settings/TabButton;->setPosition(I)V

    .line 240
    const-string v1, "WeatherSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gallery changed to position -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getLocation(I)Lse/volvocars/acu/weather/settings/Location;

    move-result-object v0

    .line 243
    .local v0, "current":Lse/volvocars/acu/weather/settings/Location;
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mMyLocationsTitleView:Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;

    if-nez p1, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v1, v0, v2}, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->setCurrentLocation(Lse/volvocars/acu/weather/settings/Location;Z)V

    .line 244
    return-void

    .line 243
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static startFromWidget(Lse/volvocars/acu/weather/WeatherWidget;)V
    .locals 5
    .param p0, "widget"    # Lse/volvocars/acu/weather/WeatherWidget;

    .prologue
    .line 492
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 493
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lse/volvocars/acu/weather/settings/WeatherSettings;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 494
    const/high16 v2, 0x800000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 495
    const-string v2, "statusbarVisible"

    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->isStatusBarVisible()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 496
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getLastKnownGpsPosition()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v1

    .line 497
    .local v1, "position":Lse/volvocars/acu/weather/Coordinate;
    if-eqz v1, :cond_0

    .line 498
    const-string v2, "currentPositionLat"

    invoke-virtual {v1}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 499
    const-string v2, "currentPositionLong"

    invoke-virtual {v1}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 501
    :cond_0
    const/4 v2, 0x1

    sput-boolean v2, Lse/volvocars/acu/weather/settings/WeatherSettings;->mStartedFromWidget:Z

    .line 502
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 503
    return-void
.end method

.method private startGpsUpdates()V
    .locals 6

    .prologue
    .line 230
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationManager:Landroid/location/LocationManager;

    .line 231
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/32 v2, 0x493e0

    const v4, 0x459c4000    # 5000.0f

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    .line 232
    return-void
.end method

.method private stopGpsUpdates()V
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationManager:Landroid/location/LocationManager;

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 236
    return-void
.end method

.method private updateLocationGalleryAdapter()V
    .locals 2

    .prologue
    .line 414
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->updateAdapter()V

    .line 415
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapterItemCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/TabButton;->setListSize(I)V

    .line 416
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 282
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 283
    .local v0, "settingsViewRect":Landroid/graphics/Rect;
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-virtual {v1, v0}, Lse/volvocars/acu/weather/settings/SettingsView;->getHitRect(Landroid/graphics/Rect;)V

    .line 284
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 285
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/SettingsView;->setPendingDelete(Z)V

    .line 287
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1
.end method

.method public getCurrentTab()I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mCurrentTab:I

    packed-switch v0, :pswitch_data_0

    .line 331
    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 321
    :pswitch_0
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/TabButton;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/TabButton;->isActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 322
    const/4 v0, 0x0

    goto :goto_0

    .line 326
    :pswitch_1
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/TabButton;->isActive()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/TabButton;->isActive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    const/4 v0, 0x1

    goto :goto_0

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected getDialog()Lse/volvocars/acu/weather/settings/UnitDialog;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mUnitDialog:Lse/volvocars/acu/weather/settings/UnitDialog;

    return-object v0
.end method

.method public isUnitDialogOpen()Z
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mUnitDialog:Lse/volvocars/acu/weather/settings/UnitDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mUnitDialog:Lse/volvocars/acu/weather/settings/UnitDialog;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/UnitDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public locationChoosen(Lse/volvocars/acu/weather/settings/Location;)V
    .locals 3
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    .line 403
    iget v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mCurrentTab:I

    if-eqz v1, :cond_0

    .line 404
    const-string v1, "WeatherSettings"

    const-string v2, "Going to my locations tab"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->getPosition(Lse/volvocars/acu/weather/settings/Location;)I

    move-result v0

    .line 406
    .local v0, "position":I
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v1, v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->setSelection(I)V

    .line 407
    invoke-direct {p0, v0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->setTabButtonAndTitleToLocationAtPosition(I)V

    .line 408
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->setTab(I)V

    .line 411
    .end local v0    # "position":I
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    const-string v7, "location"

    const-string v5, "WeatherSettings"

    .line 373
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 375
    packed-switch p1, :pswitch_data_0

    .line 396
    :goto_0
    iput-boolean v6, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mWaitingForResult:Z

    .line 397
    return-void

    .line 377
    :pswitch_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    .line 378
    const-string v2, "WeatherSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Location received:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "location"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "location"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 381
    .local v1, "result":Ljava/lang/String;
    invoke-static {v1}, Lse/volvocars/acu/weather/settings/Location;->fromString(Ljava/lang/String;)Lse/volvocars/acu/weather/settings/Location;

    move-result-object v0

    .line 382
    .local v0, "item":Lse/volvocars/acu/weather/settings/Location;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-virtual {v2, v0}, Lse/volvocars/acu/weather/settings/SettingsView;->addLocationToList(Lse/volvocars/acu/weather/settings/Location;)V

    .line 383
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->updateLocationGalleryAdapter()V

    .line 391
    .end local v0    # "item":Lse/volvocars/acu/weather/settings/Location;
    .end local v1    # "result":Ljava/lang/String;
    :goto_1
    sput-boolean v6, Lse/volvocars/acu/weather/settings/WeatherSettings;->mStartedFromWidget:Z

    goto :goto_0

    .line 385
    :cond_0
    const/16 v2, 0xa

    if-ne p2, v2, :cond_1

    sget-boolean v2, Lse/volvocars/acu/weather/settings/WeatherSettings;->mStartedFromWidget:Z

    if-nez v2, :cond_1

    .line 386
    const-string v2, "WeatherSettings"

    const-string v2, "Returning back to launcher."

    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->finish()V

    goto :goto_1

    .line 389
    :cond_1
    const-string v2, "WeatherSettings"

    const-string v2, "No location received, aborted"

    invoke-static {v5, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 375
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClickAddLocation(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 252
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/SettingsView;->isListFull()Z

    move-result v2

    if-nez v2, :cond_1

    .line 254
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 255
    .local v0, "intent":Landroid/content/Intent;
    const-class v2, Lse/volvocars/acu/weather/settings/LocationChooser;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 257
    const-string v2, "currentPositionLat"

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mCurrentPosition:Lse/volvocars/acu/weather/Coordinate;

    invoke-virtual {v3}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 258
    const-string v2, "currentPositionLong"

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mCurrentPosition:Lse/volvocars/acu/weather/Coordinate;

    invoke-virtual {v3}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 259
    invoke-virtual {p0, v0, v5}, Lse/volvocars/acu/weather/settings/WeatherSettings;->startActivityForResult(Landroid/content/Intent;I)V

    .line 260
    iput-boolean v5, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mWaitingForResult:Z

    .line 271
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToast:Landroid/widget/Toast;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToastTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 265
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090038

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 266
    .local v1, "tooManyLocations":Ljava/lang/String;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToastTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToast:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onClickDelete(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 339
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/weather/settings/SettingsView;->onClickDelete(Landroid/view/View;)V

    .line 344
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->updateLocationGalleryAdapter()V

    .line 346
    return-void
.end method

.method public onClickEdit(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 277
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/weather/settings/SettingsView;->onClickEdit(Landroid/view/View;)V

    .line 278
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 21
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 79
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    const v17, 0x7f030018

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->setContentView(I)V

    .line 82
    const v17, 0x7f07005a

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .end local p1    # "savedInstanceState":Landroid/os/Bundle;
    check-cast p1, Lse/volvocars/acu/weather/settings/SettingsView;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    .line 83
    const v17, 0x7f070054

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/WeatherSettings;->mWeatherSettingsTitleView:Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;

    .line 84
    const v17, 0x7f07005b

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    .line 85
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->updateAdapter()V

    .line 86
    const v17, 0x7f070055

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/WeatherSettings;->mMyLocationsTitleView:Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;

    .line 87
    const v17, 0x7f07005c

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->findViewById(I)Landroid/view/View;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSmhiLogoView:Landroid/view/View;

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/SettingsView;->setLocationChoosenListener(Lse/volvocars/acu/weather/settings/LocationChoosenListener;)V

    .line 90
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mWeatherSettingsTitleView:Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;

    move-object/from16 v17, v0

    new-instance v18, Lse/volvocars/acu/weather/settings/WeatherSettings$1;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings$1;-><init>(Lse/volvocars/acu/weather/settings/WeatherSettings;)V

    invoke-virtual/range {v17 .. v18}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->setUnitChangedListener(Lse/volvocars/acu/weather/settings/UnitChangedListener;)V

    .line 110
    new-instance v10, Lse/volvocars/acu/weather/settings/LocationFlingController;

    const/16 v17, 0xa

    move-object v0, v10

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lse/volvocars/acu/weather/settings/LocationFlingController;-><init>(I)V

    .line 111
    .local v10, "lfc":Lse/volvocars/acu/weather/settings/LocationFlingController;
    new-instance v17, Lse/volvocars/acu/weather/settings/WeatherSettings$2;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings$2;-><init>(Lse/volvocars/acu/weather/settings/WeatherSettings;)V

    move-object v0, v10

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/LocationFlingController;->setOnLocationFlingListener(Lse/volvocars/acu/weather/settings/OnLocationFlingListener;)V

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mMyLocationsTitleView:Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object v1, v10

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 131
    invoke-virtual/range {p0 .. p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->getIntent()Landroid/content/Intent;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 133
    .local v5, "extras":Landroid/os/Bundle;
    sget-object v17, Lse/volvocars/acu/weather/Coordinate;->UNDEF:Lse/volvocars/acu/weather/Coordinate;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/WeatherSettings;->mCurrentPosition:Lse/volvocars/acu/weather/Coordinate;

    .line 134
    if-eqz v5, :cond_0

    .line 135
    const-string v17, "currentPositionLat"

    const-wide/16 v18, 0x0

    move-object v0, v5

    move-object/from16 v1, v17

    move-wide/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v7

    .line 136
    .local v7, "lat":D
    const-string v17, "currentPositionLong"

    const-wide/16 v18, 0x0

    move-object v0, v5

    move-object/from16 v1, v17

    move-wide/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v11

    .line 137
    .local v11, "lon":D
    new-instance v17, Lse/volvocars/acu/weather/Coordinate;

    move-object/from16 v0, v17

    move-wide v1, v7

    move-wide v3, v11

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/WeatherSettings;->mCurrentPosition:Lse/volvocars/acu/weather/Coordinate;

    .line 146
    .end local v7    # "lat":D
    .end local v11    # "lon":D
    :cond_0
    const v17, 0x7f070058

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lse/volvocars/acu/weather/settings/TabButton;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    .line 147
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    move-object/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f090039

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lse/volvocars/acu/weather/settings/TabButton;->setTitle(Ljava/lang/String;)V

    .line 148
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapterItemCount()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Lse/volvocars/acu/weather/settings/TabButton;->setListSize(I)V

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lse/volvocars/acu/weather/settings/TabButton;->setDimmedPearls(Z)V

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lse/volvocars/acu/weather/settings/TabButton;->setActiveStatus(Z)V

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    move-object/from16 v17, v0

    new-instance v18, Lse/volvocars/acu/weather/settings/WeatherSettings$3;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings$3;-><init>(Lse/volvocars/acu/weather/settings/WeatherSettings;)V

    invoke-virtual/range {v17 .. v18}, Lse/volvocars/acu/weather/settings/TabButton;->setOnTabButtonClickListener(Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;)V

    .line 164
    const v17, 0x7f070059

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lse/volvocars/acu/weather/settings/TabButton;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    move-object/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f09003a

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lse/volvocars/acu/weather/settings/TabButton;->setTitle(Ljava/lang/String;)V

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    move-object/from16 v17, v0

    new-instance v18, Lse/volvocars/acu/weather/settings/WeatherSettings$4;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings$4;-><init>(Lse/volvocars/acu/weather/settings/WeatherSettings;)V

    invoke-virtual/range {v17 .. v18}, Lse/volvocars/acu/weather/settings/TabButton;->setOnTabButtonClickListener(Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;)V

    .line 176
    const/16 v16, 0x1

    .line 177
    .local v16, "windUnitKey":I
    const-string v17, "weatherPrefs"

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lse/volvocars/acu/weather/settings/WeatherSettings;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 178
    .local v13, "settings":Landroid/content/SharedPreferences;
    const-string v17, "wind_unit"

    move-object v0, v13

    move-object/from16 v1, v17

    move/from16 v2, v16

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v16

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->setWindUnit(I)V

    .line 180
    const/4 v14, 0x5

    .line 181
    .local v14, "tempUnitKey":I
    const-string v17, "temp_unit"

    move-object v0, v13

    move-object/from16 v1, v17

    move v2, v14

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v14

    .line 182
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getAdapter()Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;

    move-result-object v17

    move-object/from16 v0, v17

    move v1, v14

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/LocationGalleryAdapter;->setTemperatureUnit(I)V

    .line 205
    invoke-direct/range {p0 .. p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->startGpsUpdates()V

    .line 208
    invoke-virtual/range {p0 .. p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->getBaseContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 209
    .local v6, "inflater":Landroid/view/LayoutInflater;
    const v17, 0x7f03000e

    const v18, 0x7f070032

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    move-object v0, v6

    move/from16 v1, v17

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 211
    .local v9, "layout":Landroid/view/View;
    new-instance v17, Landroid/widget/Toast;

    invoke-virtual/range {p0 .. p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->getBaseContext()Landroid/content/Context;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToast:Landroid/widget/Toast;

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToast:Landroid/widget/Toast;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/Toast;->setDuration(I)V

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToast:Landroid/widget/Toast;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object v1, v9

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToast:Landroid/widget/Toast;

    move-object/from16 v17, v0

    const/16 v18, 0x11

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/widget/Toast;->setGravity(III)V

    .line 216
    const v17, 0x7f070033

    move-object v0, v9

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToastTextView:Landroid/widget/TextView;

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToastTextView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x190

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToastTextView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 219
    invoke-virtual/range {p0 .. p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->getBaseContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v17

    const-string v18, "fonts/VolvoSanProLig.otf"

    invoke-static/range {v17 .. v18}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v15

    .line 220
    .local v15, "typeface":Landroid/graphics/Typeface;
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mToastTextView:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object v1, v15

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->updateAdapter()V

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getLocationPosition()I

    move-result v17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lse/volvocars/acu/weather/settings/WeatherSettings;->setTabButtonAndTitleToLocationAtPosition(I)V

    .line 227
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 421
    const/4 v0, 0x1

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 353
    const-string v0, "WeatherSettings"

    const-string v1, "On key up!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    if-ne p1, v4, :cond_0

    iget v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mCurrentTab:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/SettingsView;->getPendingDelete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-virtual {v0, v2}, Lse/volvocars/acu/weather/settings/SettingsView;->setPendingDelete(Z)V

    move v0, v3

    .line 364
    :goto_0
    return v0

    .line 359
    :cond_0
    if-ne p1, v4, :cond_1

    .line 360
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->saveSelectedLocation()V

    .line 362
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-virtual {v0, v2}, Lse/volvocars/acu/weather/settings/SettingsView;->setPendingDelete(Z)V

    .line 364
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 471
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->stopGpsUpdates()V

    .line 472
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 474
    iget-boolean v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mWaitingForResult:Z

    if-nez v0, :cond_0

    .line 475
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->finish()V

    .line 478
    :cond_0
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->isUnitDialogOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 479
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mUnitDialog:Lse/volvocars/acu/weather/settings/UnitDialog;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/UnitDialog;->dismiss()V

    .line 482
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->onPause()V

    .line 483
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 427
    const-string v1, "weatherPrefs"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lse/volvocars/acu/weather/settings/WeatherSettings;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 428
    .local v0, "settings":Landroid/content/SharedPreferences;
    new-instance v1, Lse/volvocars/acu/weather/settings/UnitDialog;

    const/high16 v2, 0x7f0a0000

    invoke-direct {v1, p0, v2, v0}, Lse/volvocars/acu/weather/settings/UnitDialog;-><init>(Landroid/content/Context;ILandroid/content/SharedPreferences;)V

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mUnitDialog:Lse/volvocars/acu/weather/settings/UnitDialog;

    .line 430
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mUnitDialog:Lse/volvocars/acu/weather/settings/UnitDialog;

    new-instance v2, Lse/volvocars/acu/weather/settings/WeatherSettings$5;

    invoke-direct {v2, p0}, Lse/volvocars/acu/weather/settings/WeatherSettings$5;-><init>(Lse/volvocars/acu/weather/settings/WeatherSettings;)V

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/UnitDialog;->setUnitChangedListener(Lse/volvocars/acu/weather/settings/UnitChangedListener;)V

    .line 454
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mUnitDialog:Lse/volvocars/acu/weather/settings/UnitDialog;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/UnitDialog;->show()V

    .line 455
    const/4 v1, 0x1

    return v1
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 487
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->startGpsUpdates()V

    .line 488
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 489
    return-void
.end method

.method public setTab(I)V
    .locals 5
    .param p1, "tab"    # I

    .prologue
    const/4 v4, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 291
    packed-switch p1, :pswitch_data_0

    .line 315
    :goto_0
    iput p1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mCurrentTab:I

    .line 316
    return-void

    .line 293
    :pswitch_0
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-virtual {v1, v3}, Lse/volvocars/acu/weather/settings/SettingsView;->setVisibility(I)V

    .line 294
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mWeatherSettingsTitleView:Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;

    invoke-virtual {v1, v3}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->setVisibility(I)V

    .line 295
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mMyLocationsTitleView:Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->setVisibility(I)V

    .line 296
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->setVisibility(I)V

    .line 297
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSmhiLogoView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 298
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    invoke-virtual {v1, v4}, Lse/volvocars/acu/weather/settings/TabButton;->setActiveStatus(Z)V

    .line 299
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/TabButton;->setActiveStatus(Z)V

    .line 301
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->updateAdapter()V

    .line 302
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->getSelectedItemPosition()I

    move-result v0

    .line 303
    .local v0, "position":I
    invoke-direct {p0, v0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->setTabButtonAndTitleToLocationAtPosition(I)V

    goto :goto_0

    .line 306
    .end local v0    # "position":I
    :pswitch_1
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/SettingsView;->setVisibility(I)V

    .line 307
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mWeatherSettingsTitleView:Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/WeatherSettingsTitleView;->setVisibility(I)V

    .line 308
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mMyLocationsTitleView:Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;

    invoke-virtual {v1, v3}, Lse/volvocars/acu/weather/settings/LocationsWeatherTitleView;->setVisibility(I)V

    .line 309
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationGalleryView:Lse/volvocars/acu/weather/settings/LocationGalleryView;

    invoke-virtual {v1, v3}, Lse/volvocars/acu/weather/settings/LocationGalleryView;->setVisibility(I)V

    .line 310
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSmhiLogoView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 311
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mLocationsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/TabButton;->setActiveStatus(Z)V

    .line 312
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsTabButton:Lse/volvocars/acu/weather/settings/TabButton;

    invoke-virtual {v1, v4}, Lse/volvocars/acu/weather/settings/TabButton;->setActiveStatus(Z)V

    goto :goto_0

    .line 291
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
