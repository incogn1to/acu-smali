.class Lse/volvocars/acu/weather/settings/TabButton$1;
.super Ljava/lang/Object;
.source "TabButton.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/weather/settings/TabButton;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/TabButton;


# direct methods
.method constructor <init>(Lse/volvocars/acu/weather/settings/TabButton;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 115
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 116
    .local v1, "ey":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 118
    .local v0, "ex":F
    float-to-int v3, v1

    .line 119
    .local v3, "y":I
    float-to-int v2, v0

    .line 121
    .local v2, "x":I
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    # getter for: Lse/volvocars/acu/weather/settings/TabButton;->mActive:Z
    invoke-static {v5}, Lse/volvocars/acu/weather/settings/TabButton;->access$100(Lse/volvocars/acu/weather/settings/TabButton;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v6

    :goto_0
    # setter for: Lse/volvocars/acu/weather/settings/TabButton;->mState:I
    invoke-static {v4, v5}, Lse/volvocars/acu/weather/settings/TabButton;->access$002(Lse/volvocars/acu/weather/settings/TabButton;I)I

    .line 123
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 146
    :cond_0
    :goto_1
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    invoke-virtual {v4}, Lse/volvocars/acu/weather/settings/TabButton;->invalidate()V

    .line 148
    return v6

    .line 121
    :cond_1
    const/4 v5, 0x4

    goto :goto_0

    .line 125
    :pswitch_0
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    # setter for: Lse/volvocars/acu/weather/settings/TabButton;->mState:I
    invoke-static {v4, v7}, Lse/volvocars/acu/weather/settings/TabButton;->access$002(Lse/volvocars/acu/weather/settings/TabButton;I)I

    .line 126
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    const/4 v5, 0x0

    # setter for: Lse/volvocars/acu/weather/settings/TabButton;->mMovedOutsidePressArea:Z
    invoke-static {v4, v5}, Lse/volvocars/acu/weather/settings/TabButton;->access$202(Lse/volvocars/acu/weather/settings/TabButton;Z)Z

    goto :goto_1

    .line 129
    :pswitch_1
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    # getter for: Lse/volvocars/acu/weather/settings/TabButton;->mReleaseSquare:Landroid/graphics/Rect;
    invoke-static {v4}, Lse/volvocars/acu/weather/settings/TabButton;->access$300(Lse/volvocars/acu/weather/settings/TabButton;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    # getter for: Lse/volvocars/acu/weather/settings/TabButton;->mMovedOutsidePressArea:Z
    invoke-static {v4}, Lse/volvocars/acu/weather/settings/TabButton;->access$200(Lse/volvocars/acu/weather/settings/TabButton;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 130
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    # setter for: Lse/volvocars/acu/weather/settings/TabButton;->mState:I
    invoke-static {v4, v7}, Lse/volvocars/acu/weather/settings/TabButton;->access$002(Lse/volvocars/acu/weather/settings/TabButton;I)I

    goto :goto_1

    .line 132
    :cond_2
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    # setter for: Lse/volvocars/acu/weather/settings/TabButton;->mMovedOutsidePressArea:Z
    invoke-static {v4, v6}, Lse/volvocars/acu/weather/settings/TabButton;->access$202(Lse/volvocars/acu/weather/settings/TabButton;Z)Z

    goto :goto_1

    .line 138
    :pswitch_2
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    # getter for: Lse/volvocars/acu/weather/settings/TabButton;->mMovedOutsidePressArea:Z
    invoke-static {v4}, Lse/volvocars/acu/weather/settings/TabButton;->access$200(Lse/volvocars/acu/weather/settings/TabButton;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 139
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TabButton$1;->this$0:Lse/volvocars/acu/weather/settings/TabButton;

    # getter for: Lse/volvocars/acu/weather/settings/TabButton;->mOnTabButtonClickListener:Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;
    invoke-static {v4}, Lse/volvocars/acu/weather/settings/TabButton;->access$400(Lse/volvocars/acu/weather/settings/TabButton;)Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;

    move-result-object v4

    invoke-interface {v4}, Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;->onTabButtonClick()V

    goto :goto_1

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
