.class public Lse/volvocars/acu/weather/settings/TabButton;
.super Landroid/view/View;
.source "TabButton.java"


# static fields
.field private static final FONTSIZE:I = 0x19

.field private static final HEIGHT:I = 0x23

.field private static final NAV_PEARL_DISTANCE:I = 0xe

.field private static final NOT_SELECTED_OPACITY:I = 0x4c

.field private static final PADDING:I = 0x5

.field private static final PEARL_DISTANCE:I = 0xa

.field private static final PRESSED_OPACITY:I = 0xff

.field private static final SELECTED_OPACITY:I = 0xbf

.field private static final STATE_MARKED:I = 0x3

.field private static final STATE_NOT_SELECTED:I = 0x4

.field private static final STATE_PRESSED:I = 0x2

.field private static final STATE_SELECTED:I = 0x1

.field private static final TITLE_PEARLS_MARGIN:I = 0xa

.field private static final WIDTH:I = 0x170


# instance fields
.field private mActive:Z

.field private final mBackgroundMarked:Landroid/graphics/drawable/Drawable;

.field private final mBackgroundPressed:Landroid/graphics/drawable/Drawable;

.field private final mBackgroundSelected:Landroid/graphics/drawable/Drawable;

.field private final mColorDefault:I

.field private mDimmedPearls:Z

.field private mMovedOutsidePressArea:Z

.field private final mNavPearl:Landroid/graphics/drawable/Drawable;

.field private final mNavPearlHeight:I

.field private final mNavPearlSelected:Landroid/graphics/drawable/Drawable;

.field private final mNavPearlWidth:I

.field private mOnTabButtonClickListener:Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;

.field private mPaint:Landroid/graphics/Paint;

.field private final mPearl:Landroid/graphics/drawable/Drawable;

.field private mPearlCount:I

.field private final mPearlHeight:I

.field private final mPearlSelected:Landroid/graphics/drawable/Drawable;

.field private final mPearlWidth:I

.field private mReleaseSquare:Landroid/graphics/Rect;

.field private mSelectedPearlIndex:I

.field private mState:I

.field private mTitle:Ljava/lang/String;

.field private mTitleWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0x23

    const/16 v3, 0x170

    const/4 v4, 0x0

    .line 65
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const-string v1, "button"

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mTitle:Ljava/lang/String;

    .line 43
    iput v4, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlCount:I

    .line 51
    const/4 v1, 0x4

    iput v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mState:I

    .line 52
    iput-boolean v4, p0, Lse/volvocars/acu/weather/settings/TabButton;->mActive:Z

    .line 56
    new-instance v1, Landroid/graphics/Rect;

    const/16 v2, 0x28

    invoke-direct {v1, v6, v4, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mReleaseSquare:Landroid/graphics/Rect;

    .line 67
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TabButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mColorDefault:I

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02007a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mBackgroundMarked:Landroid/graphics/drawable/Drawable;

    .line 70
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mBackgroundMarked:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4, v4, v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mBackgroundPressed:Landroid/graphics/drawable/Drawable;

    .line 72
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mBackgroundPressed:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4, v4, v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02007c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mBackgroundSelected:Landroid/graphics/drawable/Drawable;

    .line 74
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mBackgroundSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4, v4, v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearl:Landroid/graphics/drawable/Drawable;

    .line 77
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlWidth:I

    .line 78
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlHeight:I

    .line 79
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearl:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlWidth:I

    iget v3, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlHeight:I

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    .line 81
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearl:Landroid/graphics/drawable/Drawable;

    .line 83
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearl:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    .line 85
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 86
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlWidth:I

    .line 87
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlHeight:I

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "fonts/VolvoSanProLig.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 92
    .local v0, "typeface":Landroid/graphics/Typeface;
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPaint:Landroid/graphics/Paint;

    .line 93
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lse/volvocars/acu/weather/settings/TabButton;->mColorDefault:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 95
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 96
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 99
    return-void
.end method

.method static synthetic access$002(Lse/volvocars/acu/weather/settings/TabButton;I)I
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TabButton;
    .param p1, "x1"    # I

    .prologue
    .line 17
    iput p1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mState:I

    return p1
.end method

.method static synthetic access$100(Lse/volvocars/acu/weather/settings/TabButton;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TabButton;

    .prologue
    .line 17
    iget-boolean v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mActive:Z

    return v0
.end method

.method static synthetic access$200(Lse/volvocars/acu/weather/settings/TabButton;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TabButton;

    .prologue
    .line 17
    iget-boolean v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mMovedOutsidePressArea:Z

    return v0
.end method

.method static synthetic access$202(Lse/volvocars/acu/weather/settings/TabButton;Z)Z
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TabButton;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mMovedOutsidePressArea:Z

    return p1
.end method

.method static synthetic access$300(Lse/volvocars/acu/weather/settings/TabButton;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TabButton;

    .prologue
    .line 17
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mReleaseSquare:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$400(Lse/volvocars/acu/weather/settings/TabButton;)Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TabButton;

    .prologue
    .line 17
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mOnTabButtonClickListener:Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;

    return-object v0
.end method

.method private getPearlsWidth()I
    .locals 2

    .prologue
    .line 216
    iget v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlCount:I

    mul-int/lit8 v0, v0, 0xa

    add-int/lit8 v0, v0, 0xe

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public isActive()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 211
    iget-boolean v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mActive:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mState:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/16 v8, 0x11

    const/4 v9, 0x1

    .line 222
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 225
    iget-boolean v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mActive:Z

    if-eqz v5, :cond_1

    const/16 v5, 0xbf

    move v1, v5

    .line 227
    .local v1, "opacity":I
    :goto_0
    iget v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mState:I

    packed-switch v5, :pswitch_data_0

    .line 245
    :goto_1
    :pswitch_0
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 248
    const/16 v5, 0xb8

    iget v6, p0, Lse/volvocars/acu/weather/settings/TabButton;->mTitleWidth:I

    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/TabButton;->getPearlsWidth()I

    move-result v7

    add-int/2addr v6, v7

    iget v7, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlCount:I

    if-lez v7, :cond_2

    const/16 v7, 0xa

    :goto_2
    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    sub-int v2, v5, v6

    .line 249
    .local v2, "xStart":I
    iget v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int v3, v8, v5

    .line 250
    .local v3, "yStart":I
    iget v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int v4, v8, v5

    .line 252
    .local v4, "yStartNav":I
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mTitle:Ljava/lang/String;

    int-to-float v6, v2

    const/high16 v7, 0x41c80000    # 25.0f

    iget-object v8, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 255
    iget v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mTitleWidth:I

    add-int/lit8 v5, v5, 0xa

    add-int/2addr v2, v5

    .line 257
    iget v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlCount:I

    if-lez v5, :cond_0

    .line 258
    iget v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mSelectedPearlIndex:I

    if-nez v5, :cond_3

    iget-boolean v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mActive:Z

    if-eqz v5, :cond_3

    iget-boolean v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mDimmedPearls:Z

    if-nez v5, :cond_3

    .line 259
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    iget v6, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlWidth:I

    add-int/2addr v6, v2

    iget v7, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlHeight:I

    add-int/2addr v7, v4

    invoke-virtual {v5, v2, v4, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 260
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 266
    :cond_0
    :goto_3
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_4
    iget v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlCount:I

    if-ge v0, v5, :cond_5

    .line 269
    iget v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mSelectedPearlIndex:I

    if-ne v5, v0, :cond_4

    iget-boolean v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mActive:Z

    if-eqz v5, :cond_4

    iget-boolean v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mDimmedPearls:Z

    if-nez v5, :cond_4

    .line 270
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    add-int/lit8 v6, v2, 0xe

    sub-int v7, v0, v9

    mul-int/lit8 v7, v7, 0xa

    add-int/2addr v6, v7

    add-int/lit8 v7, v2, 0xe

    iget v8, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlWidth:I

    add-int/2addr v7, v8

    sub-int v8, v0, v9

    mul-int/lit8 v8, v8, 0xa

    add-int/2addr v7, v8

    iget v8, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlHeight:I

    add-int/2addr v8, v3

    invoke-virtual {v5, v6, v3, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 271
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 266
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 225
    .end local v0    # "i":I
    .end local v1    # "opacity":I
    .end local v2    # "xStart":I
    .end local v3    # "yStart":I
    .end local v4    # "yStartNav":I
    :cond_1
    const/16 v5, 0x4c

    move v1, v5

    goto/16 :goto_0

    .line 229
    .restart local v1    # "opacity":I
    :pswitch_1
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mBackgroundSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 234
    :pswitch_2
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mBackgroundPressed:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 235
    const/16 v1, 0xff

    .line 236
    goto/16 :goto_1

    .line 238
    :pswitch_3
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mBackgroundMarked:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 248
    :cond_2
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 262
    .restart local v2    # "xStart":I
    .restart local v3    # "yStart":I
    .restart local v4    # "yStartNav":I
    :cond_3
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearl:Landroid/graphics/drawable/Drawable;

    iget v6, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlWidth:I

    add-int/2addr v6, v2

    iget v7, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearlHeight:I

    add-int/2addr v7, v4

    invoke-virtual {v5, v2, v4, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 263
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_3

    .line 274
    .restart local v0    # "i":I
    :cond_4
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearl:Landroid/graphics/drawable/Drawable;

    add-int/lit8 v6, v2, 0xe

    sub-int v7, v0, v9

    mul-int/lit8 v7, v7, 0xa

    add-int/2addr v6, v7

    add-int/lit8 v7, v2, 0xe

    iget v8, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlWidth:I

    add-int/2addr v7, v8

    sub-int v8, v0, v9

    mul-int/lit8 v8, v8, 0xa

    add-int/2addr v7, v8

    iget v8, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlHeight:I

    add-int/2addr v8, v3

    invoke-virtual {v5, v6, v3, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 275
    iget-object v5, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_5

    .line 279
    :cond_5
    return-void

    .line 227
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Landroid/view/View;->onFinishInflate()V

    .line 110
    new-instance v0, Lse/volvocars/acu/weather/settings/TabButton$1;

    invoke-direct {v0, p0}, Lse/volvocars/acu/weather/settings/TabButton$1;-><init>(Lse/volvocars/acu/weather/settings/TabButton;)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/TabButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 153
    new-instance v0, Lse/volvocars/acu/weather/settings/TabButton$2;

    invoke-direct {v0, p0}, Lse/volvocars/acu/weather/settings/TabButton$2;-><init>(Lse/volvocars/acu/weather/settings/TabButton;)V

    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/TabButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 294
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 296
    if-eqz p1, :cond_0

    .line 297
    const/4 v0, 0x3

    iput v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mState:I

    .line 301
    :goto_0
    return-void

    .line 299
    :cond_0
    iget-boolean v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mActive:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mState:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 167
    const/16 v0, 0x170

    const/16 v1, 0x23

    invoke-virtual {p0, v0, v1}, Lse/volvocars/acu/weather/settings/TabButton;->setMeasuredDimension(II)V

    .line 168
    return-void
.end method

.method public setActiveStatus(Z)V
    .locals 1
    .param p1, "active"    # Z

    .prologue
    .line 205
    iput-boolean p1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mActive:Z

    .line 206
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mState:I

    .line 207
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TabButton;->invalidate()V

    .line 208
    return-void

    .line 206
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setDimmedPearls(Z)V
    .locals 0
    .param p1, "dimmed"    # Z

    .prologue
    .line 286
    iput-boolean p1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mDimmedPearls:Z

    .line 287
    return-void
.end method

.method public setListSize(I)V
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 183
    iput p1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlCount:I

    .line 184
    const/4 v0, 0x0

    iput v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mSelectedPearlIndex:I

    .line 185
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TabButton;->invalidate()V

    .line 186
    return-void
.end method

.method public setOnTabButtonClickListener(Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;)V
    .locals 0
    .param p1, "listener"    # Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;

    .prologue
    .line 307
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mOnTabButtonClickListener:Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;

    .line 308
    return-void
.end method

.method public setPosition(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 192
    if-ltz p1, :cond_0

    iget v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPearlCount:I

    if-ge p1, v0, :cond_0

    .line 193
    iput p1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mSelectedPearlIndex:I

    .line 194
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TabButton;->invalidate()V

    .line 197
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 175
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/TabButton;->mTitle:Ljava/lang/String;

    .line 176
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lse/volvocars/acu/weather/settings/TabButton;->mTitleWidth:I

    .line 177
    return-void
.end method
