.class Lse/volvocars/acu/weather/settings/UnitDialog$3;
.super Ljava/lang/Object;
.source "UnitDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/weather/settings/UnitDialog;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/UnitDialog;


# direct methods
.method constructor <init>(Lse/volvocars/acu/weather/settings/UnitDialog;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/UnitDialog$3;->this$0:Lse/volvocars/acu/weather/settings/UnitDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    instance-of v2, p2, Lse/volvocars/acu/weather/settings/UnitItemView;

    if-eqz v2, :cond_1

    .line 101
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog$3;->this$0:Lse/volvocars/acu/weather/settings/UnitDialog;

    # getter for: Lse/volvocars/acu/weather/settings/UnitDialog;->mAdapter:Lse/volvocars/acu/weather/settings/UnitListAdapter;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/UnitDialog;->access$400(Lse/volvocars/acu/weather/settings/UnitDialog;)Lse/volvocars/acu/weather/settings/UnitListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3, p2}, Lse/volvocars/acu/weather/settings/UnitListAdapter;->clickItem(ILandroid/view/View;)V

    .line 102
    move-object v0, p2

    check-cast v0, Lse/volvocars/acu/weather/settings/UnitItemView;

    move-object v1, v0

    .line 106
    .local v1, "uiv":Lse/volvocars/acu/weather/settings/UnitItemView;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog$3;->this$0:Lse/volvocars/acu/weather/settings/UnitDialog;

    # getter for: Lse/volvocars/acu/weather/settings/UnitDialog;->mAdapter:Lse/volvocars/acu/weather/settings/UnitListAdapter;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/UnitDialog;->access$400(Lse/volvocars/acu/weather/settings/UnitDialog;)Lse/volvocars/acu/weather/settings/UnitListAdapter;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lse/volvocars/acu/weather/settings/UnitListAdapter;->getGroup(I)Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    move-result-object v2

    invoke-virtual {v2, v1}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->isMember(Lse/volvocars/acu/weather/settings/UnitItemView;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 107
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog$3;->this$0:Lse/volvocars/acu/weather/settings/UnitDialog;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/UnitItemView;->getUnitKey()I

    move-result v3

    # setter for: Lse/volvocars/acu/weather/settings/UnitDialog;->mActiveWindUnit:I
    invoke-static {v2, v3}, Lse/volvocars/acu/weather/settings/UnitDialog;->access$002(Lse/volvocars/acu/weather/settings/UnitDialog;I)I

    .line 113
    :cond_0
    :goto_0
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog$3;->this$0:Lse/volvocars/acu/weather/settings/UnitDialog;

    # getter for: Lse/volvocars/acu/weather/settings/UnitDialog;->mListView:Landroid/widget/ListView;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/UnitDialog;->access$500(Lse/volvocars/acu/weather/settings/UnitDialog;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->invalidate()V

    .line 115
    .end local v1    # "uiv":Lse/volvocars/acu/weather/settings/UnitItemView;
    :cond_1
    return-void

    .line 109
    .restart local v1    # "uiv":Lse/volvocars/acu/weather/settings/UnitItemView;
    :cond_2
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog$3;->this$0:Lse/volvocars/acu/weather/settings/UnitDialog;

    # getter for: Lse/volvocars/acu/weather/settings/UnitDialog;->mAdapter:Lse/volvocars/acu/weather/settings/UnitListAdapter;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/UnitDialog;->access$400(Lse/volvocars/acu/weather/settings/UnitDialog;)Lse/volvocars/acu/weather/settings/UnitListAdapter;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lse/volvocars/acu/weather/settings/UnitListAdapter;->getGroup(I)Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;

    move-result-object v2

    invoke-virtual {v2, v1}, Lse/volvocars/acu/weather/settings/UnitListAdapter$Group;->isMember(Lse/volvocars/acu/weather/settings/UnitItemView;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/UnitDialog$3;->this$0:Lse/volvocars/acu/weather/settings/UnitDialog;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/settings/UnitItemView;->getUnitKey()I

    move-result v3

    # setter for: Lse/volvocars/acu/weather/settings/UnitDialog;->mActiveTempUnit:I
    invoke-static {v2, v3}, Lse/volvocars/acu/weather/settings/UnitDialog;->access$202(Lse/volvocars/acu/weather/settings/UnitDialog;I)I

    goto :goto_0
.end method
