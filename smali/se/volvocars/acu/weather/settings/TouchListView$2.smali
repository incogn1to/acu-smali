.class Lse/volvocars/acu/weather/settings/TouchListView$2;
.super Ljava/lang/Object;
.source "TouchListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/weather/settings/TouchListView;->dragItem(Landroid/view/MotionEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/TouchListView;

.field final synthetic val$itemnum:I


# direct methods
.method constructor <init>(Lse/volvocars/acu/weather/settings/TouchListView;I)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/TouchListView$2;->this$0:Lse/volvocars/acu/weather/settings/TouchListView;

    iput p2, p0, Lse/volvocars/acu/weather/settings/TouchListView$2;->val$itemnum:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const-string v3, "TouchListView"

    .line 376
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView$2;->this$0:Lse/volvocars/acu/weather/settings/TouchListView;

    # getter for: Lse/volvocars/acu/weather/settings/TouchListView;->mLocationAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;
    invoke-static {v1}, Lse/volvocars/acu/weather/settings/TouchListView;->access$500(Lse/volvocars/acu/weather/settings/TouchListView;)Lse/volvocars/acu/weather/settings/LocationAdapter;

    move-result-object v1

    iget v2, p0, Lse/volvocars/acu/weather/settings/TouchListView$2;->val$itemnum:I

    invoke-virtual {v1, v2}, Lse/volvocars/acu/weather/settings/LocationAdapter;->newCurrentDragIndex(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 382
    :goto_0
    return-void

    .line 377
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 378
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "TouchListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t swap indexes as item is illegal: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 379
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    move-object v0, v1

    .line 380
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v1, "TouchListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t swap indexes as index is out of bounds: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
