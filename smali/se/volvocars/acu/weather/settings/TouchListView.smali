.class public Lse/volvocars/acu/weather/settings/TouchListView;
.super Landroid/widget/ListView;
.source "TouchListView.java"


# static fields
.field private static final LONGPRESS_DOWNTIME:I = 0x1f4

.field private static final SWAP_ITEM_ANIM_DURATION:J = 0x64L

.field private static final TAG:Ljava/lang/String; = "TouchListView"


# instance fields
.field private dragndropBackgroundColor:I

.field private grabberId:I

.field private mCoordOffset:I

.field private mDragBitmap:Landroid/graphics/Bitmap;

.field private mDragPoint:I

.field private mDragPos:I

.field private mDragView:Landroid/widget/ImageView;

.field private mHeight:I

.field private mHitbox:Landroid/graphics/Rect;

.field private mHitboxPosition:[I

.field private mItemHeightNormal:I

.field private mLastPos:I

.field private mLastY:I

.field private mLocationAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

.field private mLowerBound:I

.field private mMotionEvent:Landroid/view/MotionEvent;

.field private mMovedOutsideItem:Z

.field private mStartedDrag:Z

.field private mTempRect:Landroid/graphics/Rect;

.field private final mTouchSlop:I

.field private mUpperBound:I

.field private mWeatherSettings:Lse/volvocars/acu/weather/settings/SettingsView;

.field private mWindowManager:Landroid/view/WindowManager;

.field private mWindowParams:Landroid/view/WindowManager$LayoutParams;

.field private triggerDetach:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lse/volvocars/acu/weather/settings/TouchListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 106
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mTempRect:Landroid/graphics/Rect;

    .line 69
    iput v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mItemHeightNormal:I

    .line 70
    iput v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->grabberId:I

    .line 71
    iput v3, p0, Lse/volvocars/acu/weather/settings/TouchListView;->dragndropBackgroundColor:I

    .line 73
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v3, v3, v2, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHitbox:Landroid/graphics/Rect;

    .line 74
    new-array v1, v5, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHitboxPosition:[I

    .line 75
    iput-boolean v3, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mMovedOutsideItem:Z

    .line 76
    iput-boolean v3, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mStartedDrag:Z

    .line 85
    new-instance v1, Lse/volvocars/acu/weather/settings/TouchListView$1;

    invoke-direct {v1, p0}, Lse/volvocars/acu/weather/settings/TouchListView$1;-><init>(Lse/volvocars/acu/weather/settings/TouchListView;)V

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->triggerDetach:Ljava/lang/Runnable;

    .line 108
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mTouchSlop:I

    .line 110
    if-eqz p2, :cond_0

    .line 111
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lse/volvocars/acu/R$styleable;->TouchListView:[I

    invoke-virtual {v1, p2, v2, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 116
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mItemHeightNormal:I

    .line 117
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->grabberId:I

    .line 118
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->dragndropBackgroundColor:I

    .line 120
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 122
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_0
    return-void

    .line 74
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lse/volvocars/acu/weather/settings/TouchListView;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TouchListView;

    .prologue
    .line 53
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mMotionEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$100(Lse/volvocars/acu/weather/settings/TouchListView;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TouchListView;

    .prologue
    .line 53
    iget-boolean v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mMovedOutsideItem:Z

    return v0
.end method

.method static synthetic access$200(Lse/volvocars/acu/weather/settings/TouchListView;)Z
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TouchListView;

    .prologue
    .line 53
    iget-boolean v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mStartedDrag:Z

    return v0
.end method

.method static synthetic access$300(Lse/volvocars/acu/weather/settings/TouchListView;Landroid/view/MotionEvent;)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TouchListView;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/TouchListView;->initDrag(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic access$400(Lse/volvocars/acu/weather/settings/TouchListView;Landroid/view/MotionEvent;)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TouchListView;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/TouchListView;->dragItem(Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic access$500(Lse/volvocars/acu/weather/settings/TouchListView;)Lse/volvocars/acu/weather/settings/LocationAdapter;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/settings/TouchListView;

    .prologue
    .line 53
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLocationAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    return-object v0
.end method

.method private adjustScrollBounds(I)V
    .locals 1
    .param p1, "y"    # I

    .prologue
    .line 165
    iget v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHeight:I

    div-int/lit8 v0, v0, 0x3

    if-lt p1, v0, :cond_0

    .line 166
    iget v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHeight:I

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mUpperBound:I

    .line 168
    :cond_0
    iget v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHeight:I

    mul-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    if-gt p1, v0, :cond_1

    .line 169
    iget v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHeight:I

    mul-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLowerBound:I

    .line 171
    :cond_1
    return-void
.end method

.method private dragItem(Landroid/view/MotionEvent;)V
    .locals 13
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 318
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v8, v9

    .line 319
    .local v8, "y":I
    if-gez v8, :cond_1

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    invoke-direct {p0, v8}, Lse/volvocars/acu/weather/settings/TouchListView;->dragView(I)V

    .line 323
    invoke-direct {p0, v8}, Lse/volvocars/acu/weather/settings/TouchListView;->getItemForPosition(I)I

    move-result v3

    .line 324
    .local v3, "itemnum":I
    if-ltz v3, :cond_0

    .line 325
    const/4 v6, 0x0

    .line 326
    .local v6, "speed":I
    invoke-direct {p0, v8}, Lse/volvocars/acu/weather/settings/TouchListView;->adjustScrollBounds(I)V

    .line 327
    iget v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLowerBound:I

    if-le v8, v9, :cond_6

    .line 329
    iget v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHeight:I

    iget v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLowerBound:I

    add-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    if-le v8, v9, :cond_5

    const/16 v9, 0x10

    move v6, v9

    .line 334
    :cond_2
    :goto_1
    if-eqz v6, :cond_4

    .line 335
    const/4 v9, 0x0

    iget v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHeight:I

    div-int/lit8 v10, v10, 0x2

    invoke-virtual {p0, v9, v10}, Lse/volvocars/acu/weather/settings/TouchListView;->pointToPosition(II)I

    move-result v5

    .line 336
    .local v5, "ref":I
    const/4 v9, -0x1

    if-ne v5, v9, :cond_3

    .line 338
    const/4 v9, 0x0

    iget v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHeight:I

    div-int/lit8 v10, v10, 0x2

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getDividerHeight()I

    move-result v11

    add-int/2addr v10, v11

    add-int/lit8 v10, v10, 0x40

    invoke-virtual {p0, v9, v10}, Lse/volvocars/acu/weather/settings/TouchListView;->pointToPosition(II)I

    move-result v5

    .line 340
    :cond_3
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getFirstVisiblePosition()I

    move-result v9

    sub-int v9, v5, v9

    invoke-virtual {p0, v9}, Lse/volvocars/acu/weather/settings/TouchListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 341
    .local v7, "v":Landroid/view/View;
    if-eqz v7, :cond_4

    .line 342
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v4

    .line 343
    .local v4, "pos":I
    sub-int v9, v4, v6

    invoke-virtual {p0, v5, v9}, Lse/volvocars/acu/weather/settings/TouchListView;->setSelectionFromTop(II)V

    .line 346
    .end local v4    # "pos":I
    .end local v5    # "ref":I
    .end local v7    # "v":Landroid/view/View;
    :cond_4
    iget v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLastPos:I

    if-ne v3, v9, :cond_8

    iget v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLastY:I

    sub-int/2addr v9, v8

    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v9

    const/4 v10, 0x5

    if-ge v9, v10, :cond_8

    .line 347
    const-string v9, "TouchListView"

    const-string v10, "Not switching as too close to last move."

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 329
    :cond_5
    const/4 v9, 0x4

    move v6, v9

    goto :goto_1

    .line 330
    :cond_6
    iget v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mUpperBound:I

    if-ge v8, v9, :cond_2

    .line 332
    iget v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mUpperBound:I

    div-int/lit8 v9, v9, 0x2

    if-ge v8, v9, :cond_7

    const/16 v9, -0x10

    move v6, v9

    :goto_2
    goto :goto_1

    :cond_7
    const/4 v9, -0x4

    move v6, v9

    goto :goto_2

    .line 349
    :cond_8
    iget v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPos:I

    if-eq v9, v3, :cond_0

    .line 350
    iget-object v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLocationAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    if-eqz v9, :cond_a

    .line 351
    const-string v9, "TouchListView"

    const-string v10, "Swaping!"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getFirstVisiblePosition()I

    move-result v9

    sub-int v2, v3, v9

    .line 354
    .local v2, "itemPosition":I
    iget v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPos:I

    if-le v9, v3, :cond_b

    .line 355
    const v1, 0x7f040001

    .line 361
    .local v1, "animId":I
    :goto_3
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 363
    .local v0, "anim":Landroid/view/animation/Animation;
    const-wide/16 v9, 0x64

    invoke-virtual {v0, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 366
    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/TouchListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_9

    .line 367
    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/TouchListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 371
    :cond_9
    new-instance v9, Landroid/os/Handler;

    invoke-direct {v9}, Landroid/os/Handler;-><init>()V

    new-instance v10, Lse/volvocars/acu/weather/settings/TouchListView$2;

    invoke-direct {v10, p0, v3}, Lse/volvocars/acu/weather/settings/TouchListView$2;-><init>(Lse/volvocars/acu/weather/settings/TouchListView;I)V

    const-wide/16 v11, 0x64

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 391
    .end local v0    # "anim":Landroid/view/animation/Animation;
    .end local v1    # "animId":I
    .end local v2    # "itemPosition":I
    :cond_a
    iget v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPos:I

    iput v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLastPos:I

    .line 392
    iput v8, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLastY:I

    .line 393
    iput v3, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPos:I

    goto/16 :goto_0

    .line 358
    .restart local v2    # "itemPosition":I
    :cond_b
    const/high16 v1, 0x7f040000

    .restart local v1    # "animId":I
    goto :goto_3
.end method

.method private dragView(I)V
    .locals 3
    .param p1, "y"    # I

    .prologue
    .line 427
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPoint:I

    sub-int v1, p1, v1

    iget v2, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mCoordOffset:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 428
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragView:Landroid/widget/ImageView;

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 429
    return-void
.end method

.method private getItemForPosition(I)I
    .locals 4
    .param p1, "y"    # I

    .prologue
    .line 156
    iget v2, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPoint:I

    sub-int v2, p1, v2

    iget v3, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mItemHeightNormal:I

    div-int/lit8 v3, v3, 0x2

    add-int v0, v2, v3

    .line 157
    .local v0, "adjustedy":I
    const/4 v2, 0x0

    invoke-direct {p0, v2, v0}, Lse/volvocars/acu/weather/settings/TouchListView;->myPointToPosition(II)I

    move-result v1

    .line 158
    .local v1, "pos":I
    if-gez v0, :cond_0

    .line 159
    const/4 v1, 0x0

    .line 161
    :cond_0
    return v1
.end method

.method private initDrag(Landroid/view/MotionEvent;)V
    .locals 13
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 256
    const/4 v10, 0x0

    iput-boolean v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mStartedDrag:Z

    .line 257
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    float-to-int v8, v10

    .line 258
    .local v8, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    float-to-int v9, v10

    .line 259
    .local v9, "y":I
    invoke-virtual {p0, v8, v9}, Lse/volvocars/acu/weather/settings/TouchListView;->pointToPosition(II)I

    move-result v10

    iput v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPos:I

    .line 260
    iget v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPos:I

    iput v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLastPos:I

    .line 261
    iput v9, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLastY:I

    .line 262
    iget v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPos:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_1

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    iget v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPos:I

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getFirstVisiblePosition()I

    move-result v11

    sub-int/2addr v10, v11

    invoke-virtual {p0, v10}, Lse/volvocars/acu/weather/settings/TouchListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 268
    .local v4, "item":Landroid/view/View;
    invoke-virtual {p0, v4}, Lse/volvocars/acu/weather/settings/TouchListView;->isDraggableRow(Landroid/view/View;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 269
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v10

    sub-int v10, v9, v10

    iput v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPoint:I

    .line 270
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v10

    float-to-int v10, v10

    sub-int/2addr v10, v9

    iput v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mCoordOffset:I

    .line 271
    iget v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->grabberId:I

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 272
    .local v1, "dragger":Landroid/view/View;
    iget-object v6, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mTempRect:Landroid/graphics/Rect;

    .line 275
    .local v6, "r":Landroid/graphics/Rect;
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v10

    iput v10, v6, Landroid/graphics/Rect;->left:I

    .line 276
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v10

    iput v10, v6, Landroid/graphics/Rect;->right:I

    .line 277
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v10

    iput v10, v6, Landroid/graphics/Rect;->top:I

    .line 278
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v10

    iput v10, v6, Landroid/graphics/Rect;->bottom:I

    .line 280
    iget v10, v6, Landroid/graphics/Rect;->left:I

    if-ge v10, v8, :cond_3

    iget v10, v6, Landroid/graphics/Rect;->right:I

    if-ge v8, v10, :cond_3

    .line 281
    iget-object v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLocationAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    if-eqz v10, :cond_2

    .line 283
    :try_start_0
    iget-object v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLocationAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    iget v11, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPos:I

    invoke-virtual {v10, v11}, Lse/volvocars/acu/weather/settings/LocationAdapter;->startDrag(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :cond_2
    :goto_1
    const v10, 0x106000d

    invoke-virtual {p0, v10}, Lse/volvocars/acu/weather/settings/TouchListView;->setSelector(I)V

    .line 289
    const v10, 0x7f020038

    invoke-virtual {v4, v10}, Landroid/view/View;->setBackgroundResource(I)V

    .line 291
    const v10, 0x7f07001b

    invoke-virtual {v4, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 292
    .local v3, "icon":Landroid/widget/ImageView;
    const v10, 0x7f02003a

    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 294
    const/4 v10, 0x1

    invoke-virtual {v4, v10}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 297
    invoke-virtual {v4}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-static {v10}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 298
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 300
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 302
    .local v5, "listBounds":Landroid/graphics/Rect;
    const/4 v10, 0x0

    invoke-virtual {p0, v5, v10}, Lse/volvocars/acu/weather/settings/TouchListView;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    .line 304
    iget v10, v5, Landroid/graphics/Rect;->left:I

    invoke-direct {p0, v0, v10, v9}, Lse/volvocars/acu/weather/settings/TouchListView;->startDragging(Landroid/graphics/Bitmap;II)V

    .line 305
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getHeight()I

    move-result v10

    iput v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHeight:I

    .line 306
    iget v7, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mTouchSlop:I

    .line 307
    .local v7, "touchSlop":I
    sub-int v10, v9, v7

    iget v11, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHeight:I

    div-int/lit8 v11, v11, 0x3

    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v10

    iput v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mUpperBound:I

    .line 308
    add-int v10, v9, v7

    iget v11, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHeight:I

    div-int/lit8 v11, v11, 0x3

    mul-int/lit8 v11, v11, 0x2

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v10

    iput v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLowerBound:I

    .line 309
    const/4 v10, 0x1

    iput-boolean v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mStartedDrag:Z

    goto/16 :goto_0

    .line 284
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v3    # "icon":Landroid/widget/ImageView;
    .end local v5    # "listBounds":Landroid/graphics/Rect;
    .end local v7    # "touchSlop":I
    :catch_0
    move-exception v10

    move-object v2, v10

    .line 285
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    const-string v10, "TouchListView"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Couldn\'t start drag with index: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 313
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :cond_3
    const/4 v10, 0x0

    iput-object v10, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragView:Landroid/widget/ImageView;

    goto/16 :goto_0
.end method

.method private initItemSelection(II)V
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 238
    invoke-virtual {p0, p1, p2}, Lse/volvocars/acu/weather/settings/TouchListView;->pointToPosition(II)I

    move-result v1

    .line 239
    .local v1, "itemnum":I
    if-ltz v1, :cond_0

    .line 240
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int v2, v1, v2

    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/TouchListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 241
    .local v0, "item":Landroid/view/View;
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHitboxPosition:[I

    invoke-virtual {v0, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 242
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHitbox:Landroid/graphics/Rect;

    .line 243
    const/4 v2, 0x0

    iput-boolean v2, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mMovedOutsideItem:Z

    .line 247
    .end local v0    # "item":Landroid/view/View;
    :goto_0
    return-void

    .line 245
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHitbox:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method private myPointToPosition(II)I
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 143
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mTempRect:Landroid/graphics/Rect;

    .line 144
    .local v2, "frame":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getChildCount()I

    move-result v1

    .line 145
    .local v1, "count":I
    const/4 v4, 0x1

    sub-int v3, v1, v4

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_1

    .line 146
    invoke-virtual {p0, v3}, Lse/volvocars/acu/weather/settings/TouchListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 147
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 148
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 149
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getFirstVisiblePosition()I

    move-result v4

    add-int/2addr v4, v3

    .line 152
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v4

    .line 145
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 152
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v4, -0x1

    goto :goto_1
.end method

.method private startDragging(Landroid/graphics/Bitmap;II)V
    .locals 5
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v4, -0x2

    .line 400
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->stopDragging()V

    .line 402
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    .line 403
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x33

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 404
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput p2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 405
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragPoint:I

    sub-int v2, p3, v2

    iget v3, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mCoordOffset:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 407
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 408
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 409
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x198

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 413
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v2, -0x3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 414
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v2, 0x0

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 416
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 417
    .local v0, "v":Landroid/widget/ImageView;
    iget v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->dragndropBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 418
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 419
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragBitmap:Landroid/graphics/Bitmap;

    .line 421
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowManager:Landroid/view/WindowManager;

    .line 422
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v0, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 423
    iput-object v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragView:Landroid/widget/ImageView;

    .line 424
    return-void
.end method

.method private stopDragging()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 432
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 433
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 434
    .local v0, "wm":Landroid/view/WindowManager;
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 435
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 436
    iput-object v3, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragView:Landroid/widget/ImageView;

    .line 438
    .end local v0    # "wm":Landroid/view/WindowManager;
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 439
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 440
    iput-object v3, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragBitmap:Landroid/graphics/Bitmap;

    .line 442
    :cond_1
    return-void
.end method


# virtual methods
.method public final addHeaderView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 131
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Headers are not supported with TouchListView"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "data"    # Ljava/lang/Object;
    .param p3, "isSelectable"    # Z

    .prologue
    .line 126
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Headers are not supported with TouchListView"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected isDraggableRow(Landroid/view/View;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 135
    iget v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->grabberId:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isDragging()Z
    .locals 1

    .prologue
    .line 250
    iget-boolean v0, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mStartedDrag:Z

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v9, 0x1f4

    const/4 v8, 0x1

    .line 176
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    long-to-int v0, v4

    .line 178
    .local v0, "downTime":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 179
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 182
    .local v3, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 234
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    :goto_1
    return v4

    .line 184
    :pswitch_0
    invoke-direct {p0, v2, v3}, Lse/volvocars/acu/weather/settings/TouchListView;->initItemSelection(II)V

    .line 186
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mMotionEvent:Landroid/view/MotionEvent;

    .line 187
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->triggerDetach:Ljava/lang/Runnable;

    const-wide/16 v5, 0x1f4

    invoke-virtual {p0, v4, v5, v6}, Lse/volvocars/acu/weather/settings/TouchListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 194
    :pswitch_1
    const/4 v4, 0x0

    iput-object v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mMotionEvent:Landroid/view/MotionEvent;

    .line 195
    iget-boolean v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mStartedDrag:Z

    if-eqz v4, :cond_0

    .line 196
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mTempRect:Landroid/graphics/Rect;

    .line 197
    .local v1, "r":Landroid/graphics/Rect;
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 199
    invoke-direct {p0}, Lse/volvocars/acu/weather/settings/TouchListView;->stopDragging()V

    .line 201
    const/4 v4, 0x0

    iput-boolean v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mStartedDrag:Z

    .line 202
    const v4, 0x7f020039

    invoke-virtual {p0, v4}, Lse/volvocars/acu/weather/settings/TouchListView;->setSelector(I)V

    .line 203
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLocationAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    if-eqz v4, :cond_1

    .line 204
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLocationAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    invoke-virtual {v4}, Lse/volvocars/acu/weather/settings/LocationAdapter;->stopDrag()V

    .line 207
    :cond_1
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWeatherSettings:Lse/volvocars/acu/weather/settings/SettingsView;

    if-eqz v4, :cond_0

    .line 208
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWeatherSettings:Lse/volvocars/acu/weather/settings/SettingsView;

    invoke-virtual {v4}, Lse/volvocars/acu/weather/settings/SettingsView;->storePrefs()V

    goto :goto_0

    .line 215
    .end local v1    # "r":Landroid/graphics/Rect;
    :pswitch_2
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHitbox:Landroid/graphics/Rect;

    if-eqz v4, :cond_0

    .line 217
    iget-object v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mHitbox:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mMovedOutsideItem:Z

    if-nez v4, :cond_2

    if-ge v0, v9, :cond_2

    .line 218
    iput-boolean v8, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mMovedOutsideItem:Z

    .line 221
    :cond_2
    if-le v0, v9, :cond_0

    iget-boolean v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mMovedOutsideItem:Z

    if-nez v4, :cond_0

    .line 223
    iget-boolean v4, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mStartedDrag:Z

    if-nez v4, :cond_3

    .line 224
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/TouchListView;->initDrag(Landroid/view/MotionEvent;)V

    :goto_2
    move v4, v8

    .line 228
    goto :goto_1

    .line 226
    :cond_3
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/settings/TouchListView;->dragItem(Landroid/view/MotionEvent;)V

    goto :goto_2

    .line 182
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 53
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lse/volvocars/acu/weather/settings/TouchListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 446
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 448
    instance-of v0, p1, Lse/volvocars/acu/weather/settings/LocationAdapter;

    if-eqz v0, :cond_0

    .line 449
    check-cast p1, Lse/volvocars/acu/weather/settings/LocationAdapter;

    .end local p1    # "adapter":Landroid/widget/ListAdapter;
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mLocationAdapter:Lse/volvocars/acu/weather/settings/LocationAdapter;

    .line 452
    :cond_0
    return-void
.end method

.method public setWeatherSettings(Lse/volvocars/acu/weather/settings/SettingsView;)V
    .locals 0
    .param p1, "object"    # Lse/volvocars/acu/weather/settings/SettingsView;

    .prologue
    .line 455
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/TouchListView;->mWeatherSettings:Lse/volvocars/acu/weather/settings/SettingsView;

    .line 456
    return-void
.end method
