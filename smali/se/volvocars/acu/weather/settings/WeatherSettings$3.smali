.class Lse/volvocars/acu/weather/settings/WeatherSettings$3;
.super Ljava/lang/Object;
.source "WeatherSettings.java"

# interfaces
.implements Lse/volvocars/acu/weather/settings/OnTabButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lse/volvocars/acu/weather/settings/WeatherSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;


# direct methods
.method constructor <init>(Lse/volvocars/acu/weather/settings/WeatherSettings;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$3;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTabButtonClick()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 155
    const-string v0, "WeatherSettings"

    const-string v1, "clicked on my locations tab"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$3;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    invoke-virtual {v0, v2}, Lse/volvocars/acu/weather/settings/WeatherSettings;->setTab(I)V

    .line 157
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/WeatherSettings$3;->this$0:Lse/volvocars/acu/weather/settings/WeatherSettings;

    # getter for: Lse/volvocars/acu/weather/settings/WeatherSettings;->mSettingsView:Lse/volvocars/acu/weather/settings/SettingsView;
    invoke-static {v0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->access$200(Lse/volvocars/acu/weather/settings/WeatherSettings;)Lse/volvocars/acu/weather/settings/SettingsView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lse/volvocars/acu/weather/settings/SettingsView;->setPendingDelete(Z)V

    .line 159
    return-void
.end method
