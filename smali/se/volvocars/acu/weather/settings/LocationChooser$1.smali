.class Lse/volvocars/acu/weather/settings/LocationChooser$1;
.super Ljava/lang/Object;
.source "LocationChooser.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/settings/LocationChooser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/settings/LocationChooser;


# direct methods
.method constructor <init>(Lse/volvocars/acu/weather/settings/LocationChooser;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lse/volvocars/acu/weather/settings/LocationChooser$1;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 184
    .local v1, "result":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 185
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "location"

    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationChooser$1;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    # getter for: Lse/volvocars/acu/weather/settings/LocationChooser;->mLocationAdapter:Lse/volvocars/acu/weather/settings/SearchListAdapter;
    invoke-static {v2}, Lse/volvocars/acu/weather/settings/LocationChooser;->access$300(Lse/volvocars/acu/weather/settings/LocationChooser;)Lse/volvocars/acu/weather/settings/SearchListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Lse/volvocars/acu/weather/settings/SearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lse/volvocars/acu/weather/settings/Location;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 188
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationChooser$1;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Lse/volvocars/acu/weather/settings/LocationChooser;->setResult(ILandroid/content/Intent;)V

    .line 189
    iget-object v2, p0, Lse/volvocars/acu/weather/settings/LocationChooser$1;->this$0:Lse/volvocars/acu/weather/settings/LocationChooser;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/LocationChooser;->finish()V

    .line 190
    return-void
.end method
