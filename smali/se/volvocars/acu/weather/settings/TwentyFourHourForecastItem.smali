.class public Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;
.super Landroid/widget/LinearLayout;
.source "TwentyFourHourForecastItem.java"


# instance fields
.field private mCurrent:Z

.field private mDay:Landroid/widget/TextView;

.field private mTemperature:Landroid/widget/TextView;

.field private mTime:Landroid/widget/TextView;

.field private mTimeAMPM:Landroid/widget/TextView;

.field private mWindArrow:Landroid/widget/ImageView;

.field private mWindSpeed:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x0

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    iput-boolean v5, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mCurrent:Z

    .line 36
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 37
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03000f

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 38
    .local v2, "item":Landroid/view/View;
    invoke-virtual {p0, v2}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->addView(Landroid/view/View;)V

    .line 41
    if-eqz p2, :cond_0

    .line 42
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lse/volvocars/acu/R$styleable;->TwentyFourHourForecastItem:[I

    invoke-virtual {v3, p2, v4, v5, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 43
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mCurrent:Z

    .line 44
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 47
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_0
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 109
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "fonts/VolvoSanProLig.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 112
    .local v0, "typeface":Landroid/graphics/Typeface;
    const v1, 0x7f070035

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTime:Landroid/widget/TextView;

    .line 113
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTime:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 115
    const v1, 0x7f070036

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTimeAMPM:Landroid/widget/TextView;

    .line 116
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTimeAMPM:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 118
    const v1, 0x7f070037

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTemperature:Landroid/widget/TextView;

    .line 119
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTemperature:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 121
    const v1, 0x7f070038

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mWindArrow:Landroid/widget/ImageView;

    .line 123
    const v1, 0x7f070039

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mWindSpeed:Landroid/widget/TextView;

    .line 124
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mWindSpeed:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 126
    const v1, 0x7f070027

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mDay:Landroid/widget/TextView;

    .line 127
    iget-object v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mDay:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 129
    iget-boolean v1, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mCurrent:Z

    if-eqz v1, :cond_0

    .line 130
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->setCurrent(Z)V

    .line 133
    :cond_0
    return-void
.end method

.method public setCurrent(Z)V
    .locals 3
    .param p1, "current"    # Z

    .prologue
    .line 93
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 94
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 95
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTemperature:Landroid/widget/TextView;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 96
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mWindSpeed:Landroid/widget/TextView;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 97
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mDay:Landroid/widget/TextView;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 104
    :goto_0
    return-void

    .line 99
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTime:Landroid/widget/TextView;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 100
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTemperature:Landroid/widget/TextView;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 101
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mWindSpeed:Landroid/widget/TextView;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 102
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mDay:Landroid/widget/TextView;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setData(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IZLjava/lang/String;)V
    .locals 5
    .param p1, "time"    # Ljava/lang/String;
    .param p2, "temperature"    # Ljava/lang/String;
    .param p3, "windResId"    # I
    .param p4, "windSpeed"    # Ljava/lang/String;
    .param p5, "iconResId"    # I
    .param p6, "tomorrow"    # Z
    .param p7, "ampm"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 51
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTime:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTemperature:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mWindSpeed:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mDay:Landroid/widget/TextView;

    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p6, :cond_1

    const v2, 0x7f090036

    :goto_0
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTimeAMPM:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTime:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    :goto_1
    if-eq p3, v4, :cond_3

    .line 70
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mWindArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, p3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 71
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTemperature:Landroid/widget/TextView;

    const/16 v1, 0x2d

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 72
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mWindSpeed:Landroid/widget/TextView;

    const/16 v1, 0x96

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 82
    :goto_2
    if-eq p5, v4, :cond_0

    .line 83
    invoke-virtual {p0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 86
    :cond_0
    return-void

    .line 54
    :cond_1
    const v2, 0x7f090035

    goto :goto_0

    .line 61
    :cond_2
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTimeAMPM:Landroid/widget/TextView;

    invoke-virtual {v0, p7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTimeAMPM:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTime:Landroid/widget/TextView;

    const-string v1, "^0"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 77
    :cond_3
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mTemperature:Landroid/widget/TextView;

    const/16 v1, 0x26

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 78
    iget-object v0, p0, Lse/volvocars/acu/weather/settings/TwentyFourHourForecastItem;->mWindSpeed:Landroid/widget/TextView;

    const/16 v1, 0xa0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_2
.end method
