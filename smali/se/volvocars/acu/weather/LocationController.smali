.class public final Lse/volvocars/acu/weather/LocationController;
.super Ljava/lang/Object;
.source "LocationController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private mDownX:I

.field private final mLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPearls:Lse/volvocars/acu/weather/PearlView;

.field private mSwipeMargin:I

.field private final mWidget:Lse/volvocars/acu/weather/WeatherWidget;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/weather/WeatherWidget;Lse/volvocars/acu/weather/PearlView;I)V
    .locals 2
    .param p1, "widget"    # Lse/volvocars/acu/weather/WeatherWidget;
    .param p2, "pearls"    # Lse/volvocars/acu/weather/PearlView;
    .param p3, "swipeMargin"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    if-nez p1, :cond_0

    .line 30
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parameter widget cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_0
    if-nez p2, :cond_1

    .line 33
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parameter pearls cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_1
    iput-object p1, p0, Lse/volvocars/acu/weather/LocationController;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    .line 36
    iput-object p2, p0, Lse/volvocars/acu/weather/LocationController;->mPearls:Lse/volvocars/acu/weather/PearlView;

    .line 37
    iget-object v0, p0, Lse/volvocars/acu/weather/LocationController;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lse/volvocars/acu/weather/LocationController;->loadLocations(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/LocationController;->mLocations:Ljava/util/List;

    .line 38
    iget-object v0, p0, Lse/volvocars/acu/weather/LocationController;->mPearls:Lse/volvocars/acu/weather/PearlView;

    iget-object v1, p0, Lse/volvocars/acu/weather/LocationController;->mLocations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/PearlView;->setNumberOfPearls(I)V

    .line 39
    invoke-direct {p0}, Lse/volvocars/acu/weather/LocationController;->updateWidgetLocation()V

    .line 40
    iput p3, p0, Lse/volvocars/acu/weather/LocationController;->mSwipeMargin:I

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lse/volvocars/acu/weather/LocationController;->mDownX:I

    .line 42
    return-void
.end method

.method private loadLocations(Landroid/content/Context;)Ljava/util/List;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    const-string v7, "weatherPrefs"

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 46
    .local v5, "preferences":Landroid/content/SharedPreferences;
    const-string v7, "locations"

    const/4 v8, 0x0

    invoke-interface {v5, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 47
    .local v6, "storedLocations":Ljava/lang/String;
    if-eqz v6, :cond_0

    const-string v7, ""

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 48
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    .line 54
    :goto_0
    return-object v7

    .line 50
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v4, "locations":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 52
    .local v3, "location":Ljava/lang/String;
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v3    # "location":Ljava/lang/String;
    :cond_2
    move-object v7, v4

    .line 54
    goto :goto_0
.end method

.method private onSwipe(I)Z
    .locals 2
    .param p1, "delta"    # I

    .prologue
    const/4 v1, 0x1

    .line 72
    iget v0, p0, Lse/volvocars/acu/weather/LocationController;->mSwipeMargin:I

    if-le p1, v0, :cond_0

    .line 73
    iget-object v0, p0, Lse/volvocars/acu/weather/LocationController;->mPearls:Lse/volvocars/acu/weather/PearlView;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/PearlView;->goOneRight()V

    .line 74
    invoke-direct {p0}, Lse/volvocars/acu/weather/LocationController;->updateWidgetLocation()V

    move v0, v1

    .line 81
    :goto_0
    return v0

    .line 76
    :cond_0
    iget v0, p0, Lse/volvocars/acu/weather/LocationController;->mSwipeMargin:I

    neg-int v0, v0

    if-ge p1, v0, :cond_1

    .line 77
    iget-object v0, p0, Lse/volvocars/acu/weather/LocationController;->mPearls:Lse/volvocars/acu/weather/PearlView;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/PearlView;->goOneLeft()V

    .line 78
    invoke-direct {p0}, Lse/volvocars/acu/weather/LocationController;->updateWidgetLocation()V

    move v0, v1

    .line 79
    goto :goto_0

    :cond_1
    move v0, v1

    .line 81
    goto :goto_0
.end method

.method private updateWidgetLocation()V
    .locals 4

    .prologue
    .line 85
    iget-object v2, p0, Lse/volvocars/acu/weather/LocationController;->mPearls:Lse/volvocars/acu/weather/PearlView;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/PearlView;->getSelectedPearl()I

    move-result v0

    .line 86
    .local v0, "index":I
    iget-object v2, p0, Lse/volvocars/acu/weather/LocationController;->mLocations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v0, v2, :cond_0

    if-gez v0, :cond_1

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    if-nez v0, :cond_2

    .line 90
    iget-object v2, p0, Lse/volvocars/acu/weather/LocationController;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    sget-object v3, Lse/volvocars/acu/weather/WeatherWidget;->CURRENT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

    invoke-virtual {v2, v3}, Lse/volvocars/acu/weather/WeatherWidget;->setLocation(Lse/volvocars/acu/weather/settings/Location;)V

    .line 95
    :goto_1
    iget-object v2, p0, Lse/volvocars/acu/weather/LocationController;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/WeatherWidget;->updateWeather()V

    goto :goto_0

    .line 92
    :cond_2
    iget-object v2, p0, Lse/volvocars/acu/weather/LocationController;->mLocations:Ljava/util/List;

    const/4 v3, 0x1

    sub-int v3, v0, v3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 93
    .local v1, "location":Ljava/lang/String;
    iget-object v2, p0, Lse/volvocars/acu/weather/LocationController;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-static {v1}, Lse/volvocars/acu/weather/settings/Location;->fromString(Ljava/lang/String;)Lse/volvocars/acu/weather/settings/Location;

    move-result-object v3

    invoke-virtual {v2, v3}, Lse/volvocars/acu/weather/WeatherWidget;->setLocation(Lse/volvocars/acu/weather/settings/Location;)V

    goto :goto_1
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 59
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    move v1, v2

    .line 67
    :goto_0
    return v1

    .line 61
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lse/volvocars/acu/weather/LocationController;->mDownX:I

    move v1, v2

    .line 62
    goto :goto_0

    .line 64
    :pswitch_1
    iget v1, p0, Lse/volvocars/acu/weather/LocationController;->mDownX:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    sub-int v0, v1, v2

    .line 65
    .local v0, "dx":I
    invoke-direct {p0, v0}, Lse/volvocars/acu/weather/LocationController;->onSwipe(I)Z

    move-result v1

    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public select(Ljava/lang/String;)V
    .locals 3
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 102
    const/4 v1, 0x0

    .line 103
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lse/volvocars/acu/weather/LocationController;->mLocations:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 104
    iget-object v2, p0, Lse/volvocars/acu/weather/LocationController;->mLocations:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105
    add-int/lit8 v1, v0, 0x1

    .line 109
    :cond_0
    iget-object v2, p0, Lse/volvocars/acu/weather/LocationController;->mPearls:Lse/volvocars/acu/weather/PearlView;

    invoke-virtual {v2, v1}, Lse/volvocars/acu/weather/PearlView;->setSelectedPearl(I)V

    .line 110
    invoke-direct {p0}, Lse/volvocars/acu/weather/LocationController;->updateWidgetLocation()V

    .line 111
    return-void

    .line 103
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
