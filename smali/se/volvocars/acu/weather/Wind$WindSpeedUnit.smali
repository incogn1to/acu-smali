.class public final enum Lse/volvocars/acu/weather/Wind$WindSpeedUnit;
.super Ljava/lang/Enum;
.source "Wind.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/Wind;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WindSpeedUnit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lse/volvocars/acu/weather/Wind$WindSpeedUnit;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

.field public static final enum beaufort:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

.field public static final enum ftps:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

.field public static final enum kmph:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

.field public static final enum knots:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

.field public static final enum mph:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

.field public static final enum mps:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;


# instance fields
.field private mConversionFactor:D


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 18
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    const-string v1, "kmph"

    const-wide v2, 0x400ccccccccccccdL    # 3.6

    invoke-direct {v0, v1, v5, v2, v3}, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;-><init>(Ljava/lang/String;ID)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->kmph:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    .line 19
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    const-string v1, "mph"

    const-wide v2, 0x4001e53eda8648daL    # 2.23693629

    invoke-direct {v0, v1, v6, v2, v3}, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;-><init>(Ljava/lang/String;ID)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->mph:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    .line 20
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    const-string v1, "knots"

    const-wide v2, 0x3fff19fcae10f4f0L    # 1.94384449

    invoke-direct {v0, v1, v7, v2, v3}, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;-><init>(Ljava/lang/String;ID)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->knots:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    .line 21
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    const-string v1, "mps"

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {v0, v1, v8, v2, v3}, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;-><init>(Ljava/lang/String;ID)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->mps:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    .line 22
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    const-string v1, "ftps"

    const-wide v2, 0x400a3f290abb44e5L    # 3.28084

    invoke-direct {v0, v1, v9, v2, v3}, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;-><init>(Ljava/lang/String;ID)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->ftps:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    .line 23
    new-instance v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    const-string v1, "beaufort"

    const/4 v2, 0x5

    const-wide/16 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;-><init>(Ljava/lang/String;ID)V

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->beaufort:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    .line 17
    const/4 v0, 0x6

    new-array v0, v0, [Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    sget-object v1, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->kmph:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    aput-object v1, v0, v5

    sget-object v1, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->mph:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    aput-object v1, v0, v6

    sget-object v1, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->knots:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    aput-object v1, v0, v7

    sget-object v1, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->mps:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    aput-object v1, v0, v8

    sget-object v1, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->ftps:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->beaufort:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    aput-object v2, v0, v1

    sput-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->$VALUES:[Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ID)V
    .locals 0
    .param p3, "conversionFactor"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput-wide p3, p0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->mConversionFactor:D

    .line 29
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lse/volvocars/acu/weather/Wind$WindSpeedUnit;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    .end local p0    # "name":Ljava/lang/String;
    check-cast p0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    return-object p0
.end method

.method public static values()[Lse/volvocars/acu/weather/Wind$WindSpeedUnit;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->$VALUES:[Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    invoke-virtual {v0}, [Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    return-object v0
.end method


# virtual methods
.method protected convertFromMps(D)D
    .locals 2
    .param p1, "speedInMps"    # D

    .prologue
    .line 36
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->mConversionFactor:D

    mul-double/2addr v0, p1

    return-wide v0
.end method

.method protected convertToMps(D)D
    .locals 2
    .param p1, "speedInUnit"    # D

    .prologue
    .line 32
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->mConversionFactor:D

    div-double v0, p1, v0

    return-wide v0
.end method
