.class public final Lse/volvocars/acu/weather/PearlView;
.super Landroid/view/View;
.source "PearlView.java"


# static fields
.field private static final FADE_ALPHA:I = 0x59

.field private static final FULL_ALPHA:I = 0xff

.field private static final PEARL_DISTANCE:I = 0x4


# instance fields
.field private final mLock:Ljava/lang/Object;

.field private mNPearls:I

.field private final mNavPearl:Landroid/graphics/drawable/Drawable;

.field private final mNavPearlSelected:Landroid/graphics/drawable/Drawable;

.field private final mPearl:Landroid/graphics/drawable/Drawable;

.field private final mPearlSelected:Landroid/graphics/drawable/Drawable;

.field private mSelected:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mLock:Ljava/lang/Object;

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mPearl:Landroid/graphics/drawable/Drawable;

    .line 36
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mPearl:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lse/volvocars/acu/weather/PearlView;->mPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lse/volvocars/acu/weather/PearlView;->mPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02005b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    .line 38
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lse/volvocars/acu/weather/PearlView;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lse/volvocars/acu/weather/PearlView;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearl:Landroid/graphics/drawable/Drawable;

    .line 40
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearl:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    .line 42
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 43
    return-void
.end method


# virtual methods
.method public getNumberOfPearls()I
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 60
    :try_start_0
    iget v1, p0, Lse/volvocars/acu/weather/PearlView;->mNPearls:I

    monitor-exit v0

    return v1

    .line 61
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getSelectedPearl()I
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 99
    :try_start_0
    iget v1, p0, Lse/volvocars/acu/weather/PearlView;->mSelected:I

    monitor-exit v0

    return v1

    .line 100
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public goOneLeft()V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 81
    :try_start_0
    iget v1, p0, Lse/volvocars/acu/weather/PearlView;->mSelected:I

    if-lez v1, :cond_0

    .line 82
    iget v1, p0, Lse/volvocars/acu/weather/PearlView;->mSelected:I

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    iput v1, p0, Lse/volvocars/acu/weather/PearlView;->mSelected:I

    .line 83
    invoke-virtual {p0}, Lse/volvocars/acu/weather/PearlView;->invalidate()V

    .line 85
    :cond_0
    monitor-exit v0

    .line 86
    return-void

    .line 85
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public goOneRight()V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 69
    :try_start_0
    iget v1, p0, Lse/volvocars/acu/weather/PearlView;->mSelected:I

    iget v2, p0, Lse/volvocars/acu/weather/PearlView;->mNPearls:I

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    if-ge v1, v2, :cond_0

    .line 70
    iget v1, p0, Lse/volvocars/acu/weather/PearlView;->mSelected:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lse/volvocars/acu/weather/PearlView;->mSelected:I

    .line 71
    invoke-virtual {p0}, Lse/volvocars/acu/weather/PearlView;->invalidate()V

    .line 73
    :cond_0
    monitor-exit v0

    .line 74
    return-void

    .line 73
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 119
    iget-object v6, p0, Lse/volvocars/acu/weather/PearlView;->mPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    add-int/lit8 v5, v6, 0x4

    .line 120
    .local v5, "width":I
    iget-object v6, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    add-int/lit8 v0, v6, 0x4

    .line 121
    .local v0, "gpsWidth":I
    iget-object v6, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    iget-object v7, p0, Lse/volvocars/acu/weather/PearlView;->mPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    sub-int v1, v6, v7

    .line 125
    .local v1, "heightDiff":I
    iget-object v6, p0, Lse/volvocars/acu/weather/PearlView;->mLock:Ljava/lang/Object;

    monitor-enter v6

    .line 126
    :try_start_0
    iget v3, p0, Lse/volvocars/acu/weather/PearlView;->mNPearls:I

    .line 127
    .local v3, "nPearls":I
    iget v4, p0, Lse/volvocars/acu/weather/PearlView;->mSelected:I

    .line 128
    .local v4, "selected":I
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    if-lez v3, :cond_0

    .line 130
    if-nez v4, :cond_1

    .line 131
    iget-object v6, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 135
    :goto_0
    int-to-float v6, v0

    div-int/lit8 v7, v1, 0x2

    int-to-float v7, v7

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 137
    :cond_0
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_3

    .line 138
    if-ne v2, v4, :cond_2

    .line 139
    iget-object v6, p0, Lse/volvocars/acu/weather/PearlView;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 143
    :goto_2
    int-to-float v6, v5

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    .line 137
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 128
    .end local v2    # "i":I
    .end local v3    # "nPearls":I
    .end local v4    # "selected":I
    :catchall_0
    move-exception v7

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    .line 133
    .restart local v3    # "nPearls":I
    .restart local v4    # "selected":I
    :cond_1
    iget-object v6, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 141
    .restart local v2    # "i":I
    :cond_2
    iget-object v6, p0, Lse/volvocars/acu/weather/PearlView;->mPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 145
    :cond_3
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v6, 0x1

    .line 105
    iget-object v5, p0, Lse/volvocars/acu/weather/PearlView;->mPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 106
    .local v3, "pearlWidth":I
    iget-object v5, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 107
    .local v1, "gpsWidth":I
    iget-object v5, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 108
    .local v0, "gpsHeight":I
    invoke-virtual {p0}, Lse/volvocars/acu/weather/PearlView;->getNumberOfPearls()I

    move-result v2

    .line 109
    .local v2, "nPearls":I
    if-gtz v2, :cond_0

    .line 110
    const/4 v5, 0x0

    invoke-virtual {p0, v5, v0}, Lse/volvocars/acu/weather/PearlView;->setMeasuredDimension(II)V

    .line 115
    :goto_0
    return-void

    .line 112
    :cond_0
    sub-int v5, v2, v6

    mul-int/2addr v5, v3

    add-int/2addr v5, v1

    sub-int v6, v2, v6

    mul-int/lit8 v6, v6, 0x4

    add-int v4, v5, v6

    .line 113
    .local v4, "width":I
    invoke-virtual {p0, v4, v0}, Lse/volvocars/acu/weather/PearlView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setIdleMode(Z)V
    .locals 3
    .param p1, "idle"    # Z

    .prologue
    const/16 v2, 0xff

    const/16 v1, 0x59

    .line 148
    if-eqz p1, :cond_0

    .line 149
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 150
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 151
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 152
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 160
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 156
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 157
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearlSelected:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 158
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mNavPearl:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method

.method public setNumberOfPearls(I)V
    .locals 4
    .param p1, "nPearls"    # I

    .prologue
    .line 46
    if-gez p1, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Number of pearls cannot be less than 0. <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 50
    :try_start_0
    iput p1, p0, Lse/volvocars/acu/weather/PearlView;->mNPearls:I

    .line 51
    iget v1, p0, Lse/volvocars/acu/weather/PearlView;->mSelected:I

    iget v2, p0, Lse/volvocars/acu/weather/PearlView;->mNPearls:I

    if-lt v1, v2, :cond_1

    .line 52
    const/4 v1, 0x0

    iget v2, p0, Lse/volvocars/acu/weather/PearlView;->mNPearls:I

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lse/volvocars/acu/weather/PearlView;->mSelected:I

    .line 54
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    invoke-virtual {p0}, Lse/volvocars/acu/weather/PearlView;->requestLayout()V

    .line 56
    return-void

    .line 54
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public setSelectedPearl(I)V
    .locals 4
    .param p1, "selected"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lse/volvocars/acu/weather/PearlView;->mLock:Ljava/lang/Object;

    monitor-enter v0

    .line 90
    if-ltz p1, :cond_0

    :try_start_0
    iget v1, p0, Lse/volvocars/acu/weather/PearlView;->mNPearls:I

    if-lt p1, v1, :cond_1

    .line 91
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Selected pearl <"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "> is out of range [0, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lse/volvocars/acu/weather/PearlView;->mNPearls:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 94
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 93
    :cond_1
    :try_start_1
    iput p1, p0, Lse/volvocars/acu/weather/PearlView;->mSelected:I

    .line 94
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    return-void
.end method
