.class Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;
.super Ljava/lang/Object;
.source "WeatherForecast.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/WeatherForecast;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ForecastHolder"
.end annotation


# instance fields
.field private mTemperature:D

.field private mTime:J

.field private mWeather:I

.field private mWinddirection:I

.field private mWindspeed:D

.field final synthetic this$0:Lse/volvocars/acu/weather/WeatherForecast;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/weather/WeatherForecast;JDIID)V
    .locals 0
    .param p2, "time"    # J
    .param p4, "temperature"    # D
    .param p6, "weather"    # I
    .param p7, "direction"    # I
    .param p8, "speed"    # D

    .prologue
    .line 14
    iput-object p1, p0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;->this$0:Lse/volvocars/acu/weather/WeatherForecast;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-wide p2, p0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;->mTime:J

    .line 16
    iput-wide p4, p0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;->mTemperature:D

    .line 17
    iput p6, p0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;->mWeather:I

    .line 18
    iput p7, p0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;->mWinddirection:I

    .line 19
    iput-wide p8, p0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;->mWindspeed:D

    .line 20
    return-void
.end method


# virtual methods
.method public getTemperature()D
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;->mTemperature:D

    return-wide v0
.end method

.method public getValidTime()J
    .locals 2

    .prologue
    .line 23
    iget-wide v0, p0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;->mTime:J

    return-wide v0
.end method

.method public getWeather()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;->mWeather:I

    return v0
.end method

.method public getWindDirection()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;->mWinddirection:I

    return v0
.end method

.method public getWindSpeed()D
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lse/volvocars/acu/weather/WeatherForecast$ForecastHolder;->mWindspeed:D

    return-wide v0
.end method
