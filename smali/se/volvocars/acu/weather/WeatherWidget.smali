.class public final Lse/volvocars/acu/weather/WeatherWidget;
.super Landroid/widget/FrameLayout;
.source "WeatherWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/WeatherWidget$WeatherReciever;
    }
.end annotation


# static fields
.field public static final CURRENT_GPS_POSITION:Ljava/lang/String; = "CurrentGpsPosition"

.field public static final CURRENT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

.field private static final FADE_ALPHA:I = 0x59

.field private static final FULL_ALPHA:I = 0xff

.field public static final KEY_FORMAT:Ljava/lang/String; = "format"

.field public static final KEY_LOCATIONS:Ljava/lang/String; = "locations"

.field public static final PREFERENCE_SELECTED_LOCATION:Ljava/lang/String; = "SelectedLocation"

.field public static final PREFERENCE_WEATHER:Ljava/lang/String; = "weatherPrefs"

.field private static final TAG:Ljava/lang/String; = "WeatherWidget"


# instance fields
.field private mConditionSymbol:Landroid/widget/ImageView;

.field private mConditionText:Landroid/widget/TextView;

.field private mContainer:Landroid/view/View;

.field private final mCurrentLocationString:Ljava/lang/String;

.field private mDividerView:Landroid/widget/ImageView;

.field private final mHandler:Landroid/os/Handler;

.field private mIdle:Z

.field private mLastKnownPosition:Lse/volvocars/acu/weather/Coordinate;

.field private mLocation:Lse/volvocars/acu/weather/settings/Location;

.field private final mLocationLock:Ljava/lang/Object;

.field private mLocationView:Landroid/widget/TextView;

.field private mPearlView:Lse/volvocars/acu/weather/PearlView;

.field private mStatusbarVisible:Z

.field private mTemperature:Landroid/widget/TextView;

.field private mTemperatureFormat:Lse/volvocars/acu/weather/Temperature$Unit;

.field private final mTextFadeGrayColor:I

.field private final mTextFadeWhiteColor:I

.field private final mTextGrayColor:I

.field private final mTextGrayGlowColor:I

.field private final mTextWhiteColor:I

.field private final mTextWhiteGlowColor:I

.field private final mTouchListener:Lse/volvocars/acu/weather/WeatherWidgetTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-string v3, ""

    .line 41
    new-instance v0, Lse/volvocars/acu/weather/settings/Location;

    const-string v1, "Current Location"

    const-string v2, ""

    const-string v2, ""

    sget-object v2, Lse/volvocars/acu/weather/Coordinate;->UNDEF:Lse/volvocars/acu/weather/Coordinate;

    invoke-direct {v0, v1, v3, v3, v2}, Lse/volvocars/acu/weather/settings/Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lse/volvocars/acu/weather/Coordinate;)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherWidget;->CURRENT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationLock:Ljava/lang/Object;

    .line 79
    new-instance v0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;

    invoke-direct {v0, p0}, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;-><init>(Lse/volvocars/acu/weather/WeatherWidget;)V

    iput-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTouchListener:Lse/volvocars/acu/weather/WeatherWidgetTouchListener;

    .line 86
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mHandler:Landroid/os/Handler;

    .line 87
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextFadeWhiteColor:I

    .line 88
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextWhiteColor:I

    .line 89
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextWhiteGlowColor:I

    .line 90
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060016

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextFadeGrayColor:I

    .line 91
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextGrayColor:I

    .line 92
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060017

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextGrayGlowColor:I

    .line 93
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mCurrentLocationString:Ljava/lang/String;

    .line 94
    return-void
.end method

.method static synthetic access$000(Lse/volvocars/acu/weather/WeatherWidget;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/weather/WeatherWidget;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/WeatherWidget;->storeSelectedLocation(Ljava/lang/String;)V

    return-void
.end method

.method private oneOrMoreViewsNotInitiated()Z
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionSymbol:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setLocationViewText(Lse/volvocars/acu/weather/settings/Location;)V
    .locals 2
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    const-string v1, ""

    .line 236
    if-nez p1, :cond_0

    .line 237
    const-string v0, "WeatherWidget"

    const-string v1, "Location is null when setting location text view."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :goto_0
    return-void

    .line 240
    :cond_0
    const-string v0, ""

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 241
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 242
    :cond_1
    const-string v0, ""

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getArea()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 243
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getArea()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 245
    :cond_2
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mCurrentLocationString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private storeSelectedLocation(Ljava/lang/String;)V
    .locals 5
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 351
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "weatherPrefs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 352
    .local v1, "settings":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 353
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "SelectedLocation"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 354
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 355
    return-void
.end method


# virtual methods
.method public getLastKnownGpsPosition()Lse/volvocars/acu/weather/Coordinate;
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationLock:Ljava/lang/Object;

    monitor-enter v0

    .line 294
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLastKnownPosition:Lse/volvocars/acu/weather/Coordinate;

    monitor-exit v0

    return-object v1

    .line 295
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getLocation()Lse/volvocars/acu/weather/settings/Location;
    .locals 6

    .prologue
    const-string v0, ""

    .line 304
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationLock:Ljava/lang/Object;

    monitor-enter v0

    .line 305
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    sget-object v2, Lse/volvocars/acu/weather/WeatherWidget;->CURRENT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

    if-ne v1, v2, :cond_1

    .line 306
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLastKnownPosition:Lse/volvocars/acu/weather/Coordinate;

    if-nez v1, :cond_0

    .line 307
    const/4 v1, 0x0

    monitor-exit v0

    move-object v0, v1

    .line 312
    :goto_0
    return-object v0

    .line 310
    :cond_0
    new-instance v1, Lse/volvocars/acu/weather/settings/Location;

    const-string v2, ""

    const-string v3, ""

    const-string v4, ""

    iget-object v5, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLastKnownPosition:Lse/volvocars/acu/weather/Coordinate;

    invoke-direct {v1, v2, v3, v4, v5}, Lse/volvocars/acu/weather/settings/Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lse/volvocars/acu/weather/Coordinate;)V

    monitor-exit v0

    move-object v0, v1

    goto :goto_0

    .line 312
    :cond_1
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    monitor-exit v0

    move-object v0, v1

    goto :goto_0

    .line 313
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isInIdleMode()Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mIdle:Z

    return v0
.end method

.method public isInPressedState()Z
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextWhiteGlowColor:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextWhiteGlowColor:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextGrayGlowColor:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStatusBarVisible()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mStatusbarVisible:Z

    return v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 98
    const v1, 0x7f07005d

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mContainer:Landroid/view/View;

    .line 99
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "fonts/VolvoSanProLig.otf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 100
    .local v0, "typeface":Landroid/graphics/Typeface;
    const v1, 0x7f07005f

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mDividerView:Landroid/widget/ImageView;

    .line 101
    const v1, 0x7f070037

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    .line 102
    const v1, 0x7f070060

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    .line 103
    const v1, 0x7f070061

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    .line 104
    const v1, 0x7f07005e

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionSymbol:Landroid/widget/ImageView;

    .line 105
    const v1, 0x7f070062

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/PearlView;

    iput-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mPearlView:Lse/volvocars/acu/weather/PearlView;

    .line 107
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 108
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 109
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 111
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTouchListener:Lse/volvocars/acu/weather/WeatherWidgetTouchListener;

    invoke-virtual {p0, v1}, Lse/volvocars/acu/weather/WeatherWidget;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 113
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->reloadSettings()V

    .line 114
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->updateWeather()V

    .line 115
    return-void
.end method

.method public reloadSettings()V
    .locals 8

    .prologue
    .line 336
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "weatherPrefs"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 338
    .local v3, "settings":Landroid/content/SharedPreferences;
    const/4 v4, 0x5

    .line 339
    .local v4, "temp":I
    const-string v5, "temp_unit"

    invoke-interface {v3, v5, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 340
    const/4 v5, 0x5

    if-ne v4, v5, :cond_0

    sget-object v5, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    :goto_0
    iput-object v5, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperatureFormat:Lse/volvocars/acu/weather/Temperature$Unit;

    .line 343
    const v5, 0x7f070062

    invoke-virtual {p0, v5}, Lse/volvocars/acu/weather/WeatherWidget;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/PearlView;

    .line 344
    .local v1, "pearls":Lse/volvocars/acu/weather/PearlView;
    new-instance v0, Lse/volvocars/acu/weather/LocationController;

    const/16 v5, 0x20

    invoke-direct {v0, p0, v1, v5}, Lse/volvocars/acu/weather/LocationController;-><init>(Lse/volvocars/acu/weather/WeatherWidget;Lse/volvocars/acu/weather/PearlView;I)V

    .line 345
    .local v0, "locationController":Lse/volvocars/acu/weather/LocationController;
    const-string v5, "SelectedLocation"

    const-string v6, ""

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 346
    .local v2, "selectedLocation":Ljava/lang/String;
    invoke-virtual {v0, v2}, Lse/volvocars/acu/weather/LocationController;->select(Ljava/lang/String;)V

    .line 347
    iget-object v5, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTouchListener:Lse/volvocars/acu/weather/WeatherWidgetTouchListener;

    invoke-virtual {v5, v0}, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->setLocationController(Lse/volvocars/acu/weather/LocationController;)V

    .line 348
    return-void

    .line 340
    .end local v0    # "locationController":Lse/volvocars/acu/weather/LocationController;
    .end local v1    # "pearls":Lse/volvocars/acu/weather/PearlView;
    .end local v2    # "selectedLocation":Ljava/lang/String;
    :cond_0
    sget-object v5, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    goto :goto_0
.end method

.method public setCurrentGpsPosition(Lse/volvocars/acu/weather/Coordinate;)V
    .locals 9
    .param p1, "position"    # Lse/volvocars/acu/weather/Coordinate;

    .prologue
    const-wide v7, 0x412e848000000000L    # 1000000.0

    .line 279
    iget-object v4, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationLock:Ljava/lang/Object;

    monitor-enter v4

    .line 280
    :try_start_0
    invoke-virtual {p1}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v5

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-double v5, v5

    div-double v0, v5, v7

    .line 281
    .local v0, "lat":D
    invoke-virtual {p1}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v5

    mul-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-double v5, v5

    div-double v2, v5, v7

    .line 282
    .local v2, "lon":D
    new-instance v5, Lse/volvocars/acu/weather/Coordinate;

    invoke-direct {v5, v0, v1, v2, v3}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    iput-object v5, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLastKnownPosition:Lse/volvocars/acu/weather/Coordinate;

    .line 283
    const-string v5, "WeatherWidget"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Current GPS position set to: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    monitor-exit v4

    .line 285
    return-void

    .line 284
    .end local v0    # "lat":D
    .end local v2    # "lon":D
    :catchall_0
    move-exception v5

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public setIdleMode(Z)V
    .locals 3
    .param p1, "idle"    # Z

    .prologue
    const/16 v2, 0xff

    const/16 v1, 0x59

    .line 126
    if-eqz p1, :cond_0

    .line 127
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionSymbol:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 128
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mDividerView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 129
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextFadeWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 130
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextFadeWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 131
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextFadeGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 139
    :goto_0
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mPearlView:Lse/volvocars/acu/weather/PearlView;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/weather/PearlView;->setIdleMode(Z)V

    .line 140
    iput-boolean p1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mIdle:Z

    .line 141
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionSymbol:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 134
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mDividerView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 135
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 136
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 137
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setLocation(Lse/volvocars/acu/weather/settings/Location;)V
    .locals 3
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    .line 256
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationLock:Ljava/lang/Object;

    monitor-enter v0

    .line 257
    :try_start_0
    iput-object p1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    .line 260
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mHandler:Landroid/os/Handler;

    new-instance v2, Lse/volvocars/acu/weather/WeatherWidget$1;

    invoke-direct {v2, p0, p1}, Lse/volvocars/acu/weather/WeatherWidget$1;-><init>(Lse/volvocars/acu/weather/WeatherWidget;Lse/volvocars/acu/weather/settings/Location;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 270
    monitor-exit v0

    .line 271
    return-void

    .line 270
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setPressedState(Z)V
    .locals 2
    .param p1, "pressed"    # Z

    .prologue
    .line 148
    if-eqz p1, :cond_0

    .line 149
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextWhiteGlowColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 150
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextWhiteGlowColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 151
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextGrayGlowColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 152
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mContainer:Landroid/view/View;

    const v1, 0x7f02005e

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 159
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 156
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 157
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    iget v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTextGrayColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setStatusBarVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mStatusbarVisible:Z

    .line 119
    return-void
.end method

.method public setWeatherConditionSymbolVisibility(Z)V
    .locals 2
    .param p1, "visibility"    # Z

    .prologue
    .line 361
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionSymbol:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 362
    return-void

    .line 361
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method showErrorMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 209
    if-nez p1, :cond_0

    .line 210
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Input message cannot be null!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 212
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionSymbol:Landroid/widget/ImageView;

    if-nez v1, :cond_2

    .line 213
    :cond_1
    const-string v1, "WeatherWidget"

    const-string v2, "Parts of the layout have not been inflated."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    :goto_0
    return-void

    .line 216
    :cond_2
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090031

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "nodata":Ljava/lang/String;
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionSymbol:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 218
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationLock:Ljava/lang/Object;

    monitor-enter v1

    .line 219
    :try_start_0
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getLocation()Lse/volvocars/acu/weather/settings/Location;

    move-result-object v2

    if-nez v2, :cond_3

    .line 220
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 222
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getLocation()Lse/volvocars/acu/weather/settings/Location;

    move-result-object v2

    invoke-direct {p0, v2}, Lse/volvocars/acu/weather/WeatherWidget;->setLocationViewText(Lse/volvocars/acu/weather/settings/Location;)V

    goto :goto_1

    .line 224
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method showWeatherData(Lse/volvocars/acu/weather/CurrentWeather;)V
    .locals 5
    .param p1, "weather"    # Lse/volvocars/acu/weather/CurrentWeather;

    .prologue
    .line 188
    if-nez p1, :cond_0

    .line 189
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Input weather cannot be null!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 191
    :cond_0
    invoke-direct {p0}, Lse/volvocars/acu/weather/WeatherWidget;->oneOrMoreViewsNotInitiated()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 192
    const-string v2, "WeatherWidget"

    const-string v3, "Parts of the layout have not been inflated."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    invoke-virtual {p1}, Lse/volvocars/acu/weather/CurrentWeather;->getTemperature()Lse/volvocars/acu/weather/Temperature;

    move-result-object v3

    iget-object v4, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperatureFormat:Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {v3, v4}, Lse/volvocars/acu/weather/Temperature;->toString(Lse/volvocars/acu/weather/Temperature$Unit;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    invoke-virtual {p1}, Lse/volvocars/acu/weather/CurrentWeather;->getLocation()Lse/volvocars/acu/weather/settings/Location;

    move-result-object v2

    invoke-direct {p0, v2}, Lse/volvocars/acu/weather/WeatherWidget;->setLocationViewText(Lse/volvocars/acu/weather/settings/Location;)V

    .line 197
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, Lse/volvocars/acu/weather/CurrentWeather;->getCondition()Lse/volvocars/acu/weather/WeatherCondition;

    move-result-object v3

    invoke-virtual {v3}, Lse/volvocars/acu/weather/WeatherCondition;->getDescriptionId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "description":Ljava/lang/String;
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, Lse/volvocars/acu/weather/CurrentWeather;->getCondition()Lse/volvocars/acu/weather/WeatherCondition;

    move-result-object v3

    invoke-virtual {v3}, Lse/volvocars/acu/weather/WeatherCondition;->getDrawableId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 200
    .local v1, "symbol":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionSymbol:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method showWeatherDataLoading(Lse/volvocars/acu/weather/settings/Location;)V
    .locals 4
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    const-string v3, ""

    .line 169
    invoke-direct {p0}, Lse/volvocars/acu/weather/WeatherWidget;->oneOrMoreViewsNotInitiated()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    const-string v1, "WeatherWidget"

    const-string v2, "Parts of the layout have not been inflated."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :goto_0
    return-void

    .line 173
    :cond_0
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090031

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "nodata":Ljava/lang/String;
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationLock:Ljava/lang/Object;

    monitor-enter v1

    .line 175
    :try_start_0
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getLocation()Lse/volvocars/acu/weather/settings/Location;

    move-result-object v2

    if-nez v2, :cond_1

    .line 176
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mTemperature:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionSymbol:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 184
    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget;->mConditionText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 178
    :cond_1
    :try_start_1
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidget;->mLocationView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/WeatherWidget;->setLocationViewText(Lse/volvocars/acu/weather/settings/Location;)V

    goto :goto_1

    .line 181
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public updateWeather()V
    .locals 8

    .prologue
    .line 327
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 328
    .local v1, "now":Ljava/util/Date;
    new-instance v2, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/32 v6, 0x36ee80

    sub-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 329
    .local v2, "oneHourAgo":Ljava/util/Date;
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lse/volvocars/acu/weather/WeatherProviders;->getDefaultProvider(Landroid/content/Context;)Lse/volvocars/acu/weather/WeatherProvider;

    move-result-object v3

    .line 330
    .local v3, "provider":Lse/volvocars/acu/weather/WeatherProvider;
    invoke-virtual {p0}, Lse/volvocars/acu/weather/WeatherWidget;->getLocation()Lse/volvocars/acu/weather/settings/Location;

    move-result-object v0

    .line 331
    .local v0, "location":Lse/volvocars/acu/weather/settings/Location;
    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/WeatherWidget;->showWeatherDataLoading(Lse/volvocars/acu/weather/settings/Location;)V

    .line 332
    new-instance v4, Lse/volvocars/acu/weather/WeatherWidget$WeatherReciever;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lse/volvocars/acu/weather/WeatherWidget$WeatherReciever;-><init>(Lse/volvocars/acu/weather/WeatherWidget;Lse/volvocars/acu/weather/WeatherWidget$1;)V

    invoke-interface {v3, v0, v2, v1, v4}, Lse/volvocars/acu/weather/WeatherProvider;->runWeatherSearch(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;)V

    .line 333
    return-void
.end method
