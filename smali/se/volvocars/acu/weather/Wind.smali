.class public Lse/volvocars/acu/weather/Wind;
.super Ljava/lang/Object;
.source "Wind.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/Wind$1;,
        Lse/volvocars/acu/weather/Wind$WindDirection;,
        Lse/volvocars/acu/weather/Wind$WindSpeedUnit;
    }
.end annotation


# instance fields
.field private mWindDir:Lse/volvocars/acu/weather/Wind$WindDirection;

.field private mWindSpeedMPS:D


# direct methods
.method public constructor <init>(DLse/volvocars/acu/weather/Wind$WindSpeedUnit;I)V
    .locals 2
    .param p1, "speed"    # D
    .param p3, "windSpeedUnit"    # Lse/volvocars/acu/weather/Wind$WindSpeedUnit;
    .param p4, "cardinalDegrees"    # I

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    invoke-virtual {p3, p1, p2}, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->convertToMps(D)D

    move-result-wide v0

    iput-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    .line 177
    invoke-direct {p0, p4}, Lse/volvocars/acu/weather/Wind;->directionFromCaridnalDegrees(I)Lse/volvocars/acu/weather/Wind$WindDirection;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/Wind;->mWindDir:Lse/volvocars/acu/weather/Wind$WindDirection;

    .line 178
    return-void
.end method

.method private convertMpsToBeaufort()D
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4020000000000000L    # 8.0

    .line 141
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    const-wide v2, 0x3fd3333333333333L    # 0.3

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 142
    const-wide/16 v0, 0x0

    .line 167
    :goto_0
    return-wide v0

    .line 143
    :cond_0
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    const-wide v2, 0x3ff999999999999aL    # 1.6

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 144
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto :goto_0

    .line 145
    :cond_1
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    const-wide v2, 0x400b333333333333L    # 3.4

    cmpg-double v0, v0, v2

    if-gez v0, :cond_2

    .line 146
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    goto :goto_0

    .line 147
    :cond_2
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    const-wide/high16 v2, 0x4016000000000000L    # 5.5

    cmpg-double v0, v0, v2

    if-gez v0, :cond_3

    .line 148
    const-wide/high16 v0, 0x4008000000000000L    # 3.0

    goto :goto_0

    .line 149
    :cond_3
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    cmpg-double v0, v0, v4

    if-gez v0, :cond_4

    .line 150
    const-wide/high16 v0, 0x4010000000000000L    # 4.0

    goto :goto_0

    .line 151
    :cond_4
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    const-wide v2, 0x402599999999999aL    # 10.8

    cmpg-double v0, v0, v2

    if-gez v0, :cond_5

    .line 152
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    goto :goto_0

    .line 153
    :cond_5
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    const-wide v2, 0x402bcccccccccccdL    # 13.9

    cmpg-double v0, v0, v2

    if-gez v0, :cond_6

    .line 154
    const-wide/high16 v0, 0x4018000000000000L    # 6.0

    goto :goto_0

    .line 155
    :cond_6
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    const-wide v2, 0x4031333333333333L    # 17.2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_7

    .line 156
    const-wide/high16 v0, 0x401c000000000000L    # 7.0

    goto :goto_0

    .line 157
    :cond_7
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    const-wide v2, 0x4034cccccccccccdL    # 20.8

    cmpg-double v0, v0, v2

    if-gez v0, :cond_8

    move-wide v0, v4

    .line 158
    goto :goto_0

    .line 159
    :cond_8
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    const-wide v2, 0x4038800000000000L    # 24.5

    cmpg-double v0, v0, v2

    if-gez v0, :cond_9

    .line 160
    const-wide/high16 v0, 0x4022000000000000L    # 9.0

    goto :goto_0

    .line 161
    :cond_9
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    const-wide v2, 0x403c800000000000L    # 28.5

    cmpg-double v0, v0, v2

    if-gez v0, :cond_a

    .line 162
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    goto/16 :goto_0

    .line 163
    :cond_a
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    const-wide v2, 0x404059999999999aL    # 32.7

    cmpg-double v0, v0, v2

    if-gez v0, :cond_b

    .line 164
    const-wide/high16 v0, 0x4026000000000000L    # 11.0

    goto/16 :goto_0

    .line 167
    :cond_b
    const-wide/high16 v0, 0x4028000000000000L    # 12.0

    goto/16 :goto_0
.end method

.method private directionFromCaridnalDegrees(I)Lse/volvocars/acu/weather/Wind$WindDirection;
    .locals 2
    .param p1, "cardinalDegrees"    # I

    .prologue
    .line 56
    move v0, p1

    .line 57
    .local v0, "degrees":I
    :goto_0
    if-gez v0, :cond_0

    .line 58
    add-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 60
    :cond_0
    const/16 v1, 0x168

    if-lt v0, v1, :cond_1

    .line 61
    rem-int/lit16 v0, v0, 0x168

    .line 63
    :cond_1
    const/16 v1, 0x17

    if-lt v0, v1, :cond_2

    const/16 v1, 0x152

    if-lt v0, v1, :cond_3

    .line 64
    :cond_2
    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->N:Lse/volvocars/acu/weather/Wind$WindDirection;

    .line 78
    :goto_1
    return-object v1

    .line 65
    :cond_3
    const/16 v1, 0x44

    if-ge v0, v1, :cond_4

    .line 66
    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->NE:Lse/volvocars/acu/weather/Wind$WindDirection;

    goto :goto_1

    .line 67
    :cond_4
    const/16 v1, 0x71

    if-ge v0, v1, :cond_5

    .line 68
    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->E:Lse/volvocars/acu/weather/Wind$WindDirection;

    goto :goto_1

    .line 69
    :cond_5
    const/16 v1, 0x9e

    if-ge v0, v1, :cond_6

    .line 70
    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->SE:Lse/volvocars/acu/weather/Wind$WindDirection;

    goto :goto_1

    .line 71
    :cond_6
    const/16 v1, 0xcb

    if-ge v0, v1, :cond_7

    .line 72
    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->S:Lse/volvocars/acu/weather/Wind$WindDirection;

    goto :goto_1

    .line 73
    :cond_7
    const/16 v1, 0xf8

    if-ge v0, v1, :cond_8

    .line 74
    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->SW:Lse/volvocars/acu/weather/Wind$WindDirection;

    goto :goto_1

    .line 75
    :cond_8
    const/16 v1, 0x125

    if-ge v0, v1, :cond_9

    .line 76
    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->W:Lse/volvocars/acu/weather/Wind$WindDirection;

    goto :goto_1

    .line 78
    :cond_9
    sget-object v1, Lse/volvocars/acu/weather/Wind$WindDirection;->NW:Lse/volvocars/acu/weather/Wind$WindDirection;

    goto :goto_1
.end method


# virtual methods
.method public getDirection()Lse/volvocars/acu/weather/Wind$WindDirection;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lse/volvocars/acu/weather/Wind;->mWindDir:Lse/volvocars/acu/weather/Wind$WindDirection;

    return-object v0
.end method

.method public getSpeed(Lse/volvocars/acu/weather/Wind$WindSpeedUnit;)D
    .locals 4
    .param p1, "unit"    # Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    .prologue
    const-wide/high16 v2, -0x4000000000000000L    # -2.0

    .line 188
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    move-wide v0, v2

    .line 195
    :goto_0
    return-wide v0

    .line 192
    :cond_0
    sget-object v0, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->beaufort:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    if-ne p1, v0, :cond_1

    .line 193
    invoke-direct {p0}, Lse/volvocars/acu/weather/Wind;->convertMpsToBeaufort()D

    move-result-wide v0

    goto :goto_0

    .line 195
    :cond_1
    iget-wide v0, p0, Lse/volvocars/acu/weather/Wind;->mWindSpeedMPS:D

    invoke-virtual {p1, v0, v1}, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->convertFromMps(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public getWindDirectionResource()I
    .locals 2

    .prologue
    .line 87
    sget-object v0, Lse/volvocars/acu/weather/Wind$1;->$SwitchMap$se$volvocars$acu$weather$Wind$WindDirection:[I

    iget-object v1, p0, Lse/volvocars/acu/weather/Wind;->mWindDir:Lse/volvocars/acu/weather/Wind$WindDirection;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/Wind$WindDirection;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 105
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 89
    :pswitch_0
    const v0, 0x7f0200a1

    goto :goto_0

    .line 91
    :pswitch_1
    const v0, 0x7f0200a3

    goto :goto_0

    .line 93
    :pswitch_2
    const v0, 0x7f02009f

    goto :goto_0

    .line 95
    :pswitch_3
    const v0, 0x7f0200a9

    goto :goto_0

    .line 97
    :pswitch_4
    const v0, 0x7f0200a7

    goto :goto_0

    .line 99
    :pswitch_5
    const v0, 0x7f0200ab

    goto :goto_0

    .line 101
    :pswitch_6
    const v0, 0x7f0200ad

    goto :goto_0

    .line 103
    :pswitch_7
    const v0, 0x7f0200a5

    goto :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public getWindDirectionResourceHighlighted()I
    .locals 2

    .prologue
    .line 114
    sget-object v0, Lse/volvocars/acu/weather/Wind$1;->$SwitchMap$se$volvocars$acu$weather$Wind$WindDirection:[I

    iget-object v1, p0, Lse/volvocars/acu/weather/Wind;->mWindDir:Lse/volvocars/acu/weather/Wind$WindDirection;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/Wind$WindDirection;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 132
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 116
    :pswitch_0
    const v0, 0x7f0200a0

    goto :goto_0

    .line 118
    :pswitch_1
    const v0, 0x7f0200a2

    goto :goto_0

    .line 120
    :pswitch_2
    const v0, 0x7f02009e

    goto :goto_0

    .line 122
    :pswitch_3
    const v0, 0x7f0200a8

    goto :goto_0

    .line 124
    :pswitch_4
    const v0, 0x7f0200a6

    goto :goto_0

    .line 126
    :pswitch_5
    const v0, 0x7f0200aa

    goto :goto_0

    .line 128
    :pswitch_6
    const v0, 0x7f0200ac

    goto :goto_0

    .line 130
    :pswitch_7
    const v0, 0x7f0200a4

    goto :goto_0

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
