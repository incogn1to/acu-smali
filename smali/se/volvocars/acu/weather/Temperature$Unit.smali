.class public final enum Lse/volvocars/acu/weather/Temperature$Unit;
.super Ljava/lang/Enum;
.source "Temperature.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/Temperature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Unit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lse/volvocars/acu/weather/Temperature$Unit;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lse/volvocars/acu/weather/Temperature$Unit;

.field public static final enum Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

.field public static final enum Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;


# instance fields
.field private final mLetter:C


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lse/volvocars/acu/weather/Temperature$Unit;

    const-string v1, "Celcius"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v3, v2}, Lse/volvocars/acu/weather/Temperature$Unit;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    new-instance v0, Lse/volvocars/acu/weather/Temperature$Unit;

    const-string v1, "Fahrenheit"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v4, v2}, Lse/volvocars/acu/weather/Temperature$Unit;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    .line 17
    const/4 v0, 0x2

    new-array v0, v0, [Lse/volvocars/acu/weather/Temperature$Unit;

    sget-object v1, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    aput-object v1, v0, v3

    sget-object v1, Lse/volvocars/acu/weather/Temperature$Unit;->Fahrenheit:Lse/volvocars/acu/weather/Temperature$Unit;

    aput-object v1, v0, v4

    sput-object v0, Lse/volvocars/acu/weather/Temperature$Unit;->$VALUES:[Lse/volvocars/acu/weather/Temperature$Unit;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IC)V
    .locals 0
    .param p3, "letter"    # C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput-char p3, p0, Lse/volvocars/acu/weather/Temperature$Unit;->mLetter:C

    .line 25
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lse/volvocars/acu/weather/Temperature$Unit;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    .end local p0    # "name":Ljava/lang/String;
    check-cast p0, Lse/volvocars/acu/weather/Temperature$Unit;

    return-object p0
.end method

.method public static values()[Lse/volvocars/acu/weather/Temperature$Unit;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lse/volvocars/acu/weather/Temperature$Unit;->$VALUES:[Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {v0}, [Lse/volvocars/acu/weather/Temperature$Unit;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lse/volvocars/acu/weather/Temperature$Unit;

    return-object v0
.end method


# virtual methods
.method public getUnitLetter()C
    .locals 1

    .prologue
    .line 31
    iget-char v0, p0, Lse/volvocars/acu/weather/Temperature$Unit;->mLetter:C

    return v0
.end method
