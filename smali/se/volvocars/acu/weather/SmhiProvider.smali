.class final Lse/volvocars/acu/weather/SmhiProvider;
.super Ljava/lang/Object;
.source "SmhiProvider.java"

# interfaces
.implements Lse/volvocars/acu/weather/WeatherProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;,
        Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;,
        Lse/volvocars/acu/weather/SmhiProvider$KeyEncoder;
    }
.end annotation


# static fields
.field private static final ANDSIGN:Ljava/lang/String; = "&"

.field private static final BASE_URI_LIVE:Ljava/lang/String; = "/ws/products/"

.field private static final CUSTOMER_ID:Ljava/lang/String; = "1"

.field public static final DEFAULT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

.field private static final EQUALSIGN:Ljava/lang/String; = "="

.field private static final FORECAST_LENGTH_7DAYS:Ljava/lang/String; = "&Length=168"

.field private static final FORECAST_PARAMS:Ljava/lang/String; = "&Values=DN,SY,T,WD,WS"

.field private static final FORECAST_URI_LIVE:Ljava/lang/String; = "weatherdata?"

.field private static final KEY:Ljava/lang/String; = "Br9aT752?r"

.field private static final LOCATION_URI_LIVE:Ljava/lang/String; = "location?"

.field public static final NAME:Ljava/lang/String; = "SMHI"

.field private static final ONE_HOUR:I = 0x36ee80

.field private static final PROVIDER_HOST_LIVE:Ljava/lang/String; = "wsmedia.smhi.se"

.field private static final TAG:Ljava/lang/String; = "SmhiProvider"

.field private static final UUID:Ljava/lang/String; = ""

.field private static mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mKeyEncoder:Lse/volvocars/acu/weather/SmhiProvider$KeyEncoder;

.field private mLocation:Lse/volvocars/acu/weather/settings/Location;

.field private final mLocationLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 132
    new-instance v0, Lse/volvocars/acu/weather/settings/Location;

    const-string v1, "Amagerbro"

    const-string v2, "K\ufffd\ufffdbenhavn"

    const-string v3, "Denmark"

    new-instance v4, Lse/volvocars/acu/weather/Coordinate;

    const-wide v5, 0x404bd5551d68c693L    # 55.66666

    const-wide v7, 0x40292aaa3ad18d26L    # 12.58333

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/settings/Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lse/volvocars/acu/weather/Coordinate;)V

    sput-object v0, Lse/volvocars/acu/weather/SmhiProvider;->DEFAULT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lse/volvocars/acu/weather/SmhiProvider$KeyEncoder;

    const-string v1, "Br9aT752?r"

    invoke-direct {v0, v1}, Lse/volvocars/acu/weather/SmhiProvider$KeyEncoder;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider;->mKeyEncoder:Lse/volvocars/acu/weather/SmhiProvider$KeyEncoder;

    .line 135
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider;->mLocationLock:Ljava/lang/Object;

    .line 572
    return-void
.end method

.method static synthetic access$000(Lse/volvocars/acu/weather/SmhiProvider;Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;)Lse/volvocars/acu/weather/CurrentWeather;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/weather/SmhiProvider;
    .param p1, "x1"    # Lse/volvocars/acu/weather/settings/Location;
    .param p2, "x2"    # Ljava/util/Date;
    .param p3, "x3"    # Ljava/util/Date;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lse/volvocars/acu/weather/SmhiProvider;->getWeatherForecast(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;)Lse/volvocars/acu/weather/CurrentWeather;

    move-result-object v0

    return-object v0
.end method

.method private constructRequestUri(Lse/volvocars/acu/weather/settings/Location;)Ljava/lang/String;
    .locals 3
    .param p1, "loc"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, "/ws/products/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "weatherdata?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/SmhiProvider;->getLocationString(Lse/volvocars/acu/weather/settings/Location;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    const-string v1, "&Length=168"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&Values=DN,SY,T,WD,WS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private fillDatabase(Lse/volvocars/acu/weather/settings/Location;Lse/volvocars/acu/weather/db/WeatherDatabase;)V
    .locals 32
    .param p1, "loc"    # Lse/volvocars/acu/weather/settings/Location;
    .param p2, "db"    # Lse/volvocars/acu/weather/db/WeatherDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 350
    invoke-direct/range {p0 .. p1}, Lse/volvocars/acu/weather/SmhiProvider;->requestCurrentWeather(Lse/volvocars/acu/weather/settings/Location;)Lorg/json/JSONObject;

    move-result-object v29

    .line 368
    .local v29, "value":Lorg/json/JSONObject;
    move-object/from16 v22, v29

    .line 369
    .local v22, "jsValue":Lorg/json/JSONObject;
    const-string v5, "data"

    move-object/from16 v0, v22

    move-object v1, v5

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v15

    .line 371
    .local v15, "data":Lorg/json/JSONArray;
    new-instance v17, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyy-MM-dd HH:mm:ss"

    move-object/from16 v0, v17

    move-object v1, v5

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 372
    .local v17, "format":Ljava/text/SimpleDateFormat;
    const-string v5, "issuedate"

    move-object/from16 v0, v22

    move-object v1, v5

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    move-object v1, v5

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v20

    .line 373
    .local v20, "issuedate":J
    const-string v5, "nextupdate"

    move-object/from16 v0, v22

    move-object v1, v5

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    move-object v1, v5

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v23

    .line 375
    .local v23, "nextupdate":J
    const/16 v18, 0x0

    .line 376
    .local v18, "i":I
    const-wide/16 v25, 0x0

    .local v25, "offset":J
    move/from16 v19, v18

    .line 381
    .end local v18    # "i":I
    .local v19, "i":I
    :goto_0
    add-int/lit8 v18, v19, 0x1

    .end local v19    # "i":I
    .restart local v18    # "i":I
    :try_start_0
    move-object v0, v15

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v13

    .line 382
    .local v13, "currentWeather":Lorg/json/JSONArray;
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 384
    .local v14, "cv":Landroid/content/ContentValues;
    const/4 v5, 0x0

    invoke-virtual {v13, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    move-object v1, v5

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v27

    .line 391
    .local v27, "validdate":J
    const/4 v5, 0x1

    move/from16 v0, v18

    move v1, v5

    if-ne v0, v1, :cond_0

    .line 392
    new-instance v16, Ljava/util/Date;

    invoke-direct/range {v16 .. v16}, Ljava/util/Date;-><init>()V

    .line 393
    .local v16, "date":Ljava/util/Date;
    new-instance v4, Ljava/util/Date;

    invoke-virtual/range {v16 .. v16}, Ljava/util/Date;->getYear()I

    move-result v5

    invoke-virtual/range {v16 .. v16}, Ljava/util/Date;->getMonth()I

    move-result v6

    invoke-virtual/range {v16 .. v16}, Ljava/util/Date;->getDate()I

    move-result v7

    invoke-virtual/range {v16 .. v16}, Ljava/util/Date;->getHours()I

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v4 .. v10}, Ljava/util/Date;-><init>(IIIIII)V

    .line 394
    .local v4, "d":Ljava/util/Date;
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    .line 397
    .local v11, "currentHour":J
    const/4 v5, 0x0

    invoke-virtual {v13, v5}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    move-object v1, v5

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    const-wide/32 v7, 0x36ee80

    sub-long/2addr v5, v7

    sub-long v25, v5, v11

    .line 400
    sub-long v23, v23, v25

    .line 404
    .end local v4    # "d":Ljava/util/Date;
    .end local v11    # "currentHour":J
    .end local v16    # "date":Ljava/util/Date;
    :cond_0
    sub-long v27, v27, v25

    .line 406
    const-string v5, "issuedate"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 407
    const-string v5, "nextupdate"

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 408
    const-string v5, "longitude"

    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v6

    invoke-virtual {v6}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 409
    const-string v5, "latitude"

    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v6

    invoke-virtual {v6}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 410
    const-string v5, "validfordate"

    invoke-static/range {v27 .. v28}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 411
    const-string v5, "day_night"

    const/4 v6, 0x1

    invoke-virtual {v13, v6}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    const-string v5, "weather_symbol"

    const/4 v6, 0x2

    invoke-virtual {v13, v6}, Lorg/json/JSONArray;->optInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 413
    const-string v5, "temp"

    const/4 v6, 0x3

    invoke-virtual {v13, v6}, Lorg/json/JSONArray;->optDouble(I)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 414
    const-string v5, "wind_dir"

    const/4 v6, 0x4

    invoke-virtual {v13, v6}, Lorg/json/JSONArray;->optInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 415
    const-string v5, "wind_speed"

    const/4 v6, 0x5

    invoke-virtual {v13, v6}, Lorg/json/JSONArray;->optDouble(I)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    invoke-virtual {v14, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 424
    const-string v31, "longitude=? AND latitude=? AND validfordate=? "

    .line 428
    .local v31, "whereClause":Ljava/lang/String;
    const/4 v5, 0x3

    move v0, v5

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v30, v0

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v6

    invoke-virtual {v6}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v30, v5

    const/4 v5, 0x1

    invoke-virtual/range {p1 .. p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v6

    invoke-virtual {v6}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v30, v5

    const/4 v5, 0x2

    invoke-static/range {v27 .. v28}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v30, v5

    .line 434
    .local v30, "whereArgs":[Ljava/lang/String;
    move-object/from16 v0, p2

    move-object v1, v14

    move-object/from16 v2, v31

    move-object/from16 v3, v30

    invoke-virtual {v0, v1, v2, v3}, Lse/volvocars/acu/weather/db/WeatherDatabase;->update(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x1

    if-ge v5, v6, :cond_1

    .line 435
    move-object/from16 v0, p2

    move-object v1, v14

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/db/WeatherDatabase;->addValues(Landroid/content/ContentValues;)J
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move/from16 v19, v18

    .line 437
    .end local v18    # "i":I
    .restart local v19    # "i":I
    goto/16 :goto_0

    .line 438
    .end local v13    # "currentWeather":Lorg/json/JSONArray;
    .end local v14    # "cv":Landroid/content/ContentValues;
    .end local v19    # "i":I
    .end local v27    # "validdate":J
    .end local v30    # "whereArgs":[Ljava/lang/String;
    .end local v31    # "whereClause":Ljava/lang/String;
    .restart local v18    # "i":I
    :catch_0
    move-exception v5

    .line 442
    invoke-virtual/range {p2 .. p2}, Lse/volvocars/acu/weather/db/WeatherDatabase;->clearExpiredData()I

    .line 443
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Lse/volvocars/acu/weather/db/WeatherDatabase;->clearOldData(Lse/volvocars/acu/weather/settings/Location;J)I

    .line 445
    return-void
.end method

.method private getLocationString(Lse/volvocars/acu/weather/settings/Location;)Ljava/lang/String;
    .locals 4
    .param p1, "loc"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    const-string v0, "long="

    const-string v0, "lat="

    const-string v0, "&"

    .line 189
    iget-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider;->mLocationLock:Ljava/lang/Object;

    monitor-enter v0

    .line 190
    if-eqz p1, :cond_0

    .line 191
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v2

    invoke-virtual {v2}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "long="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v2

    invoke-virtual {v2}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    monitor-exit v0

    move-object v0, v1

    .line 200
    :goto_0
    return-object v0

    .line 196
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/weather/SmhiProvider;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    if-nez v1, :cond_1

    .line 197
    sget-object v1, Lse/volvocars/acu/weather/SmhiProvider;->DEFAULT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

    iput-object v1, p0, Lse/volvocars/acu/weather/SmhiProvider;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    .line 200
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lat="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lse/volvocars/acu/weather/SmhiProvider;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v2

    invoke-virtual {v2}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "long="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lse/volvocars/acu/weather/SmhiProvider;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    invoke-virtual {v2}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v2

    invoke-virtual {v2}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    monitor-exit v0

    move-object v0, v1

    goto :goto_0

    .line 202
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getWeatherForecast(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;)Lse/volvocars/acu/weather/CurrentWeather;
    .locals 6
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;
    .param p2, "from"    # Ljava/util/Date;
    .param p3, "to"    # Ljava/util/Date;

    .prologue
    const-string v3, "SmhiProvider"

    .line 533
    if-eqz p1, :cond_0

    sget-object v3, Lse/volvocars/acu/weather/Coordinate;->UNDEF:Lse/volvocars/acu/weather/Coordinate;

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v4

    invoke-virtual {v3, v4}, Lse/volvocars/acu/weather/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 534
    :cond_0
    const/4 v3, 0x0

    .line 549
    :goto_0
    return-object v3

    .line 536
    :cond_1
    new-instance v0, Lse/volvocars/acu/weather/db/WeatherDatabase;

    iget-object v3, p0, Lse/volvocars/acu/weather/SmhiProvider;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lse/volvocars/acu/weather/db/WeatherDatabase;-><init>(Landroid/content/Context;)V

    .line 537
    .local v0, "db":Lse/volvocars/acu/weather/db/WeatherDatabase;
    const/4 v2, 0x0

    .line 539
    .local v2, "retVal":Lse/volvocars/acu/weather/CurrentWeather;
    :try_start_0
    invoke-direct {p0, v0, p1, p2, p3}, Lse/volvocars/acu/weather/SmhiProvider;->getWeatherForecastAux(Lse/volvocars/acu/weather/db/WeatherDatabase;Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;)Lse/volvocars/acu/weather/CurrentWeather;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 547
    invoke-virtual {v0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->close()V

    :goto_1
    move-object v3, v2

    .line 549
    goto :goto_0

    .line 540
    :catch_0
    move-exception v1

    .line 541
    .local v1, "e":Lorg/json/JSONException;
    :try_start_1
    const-string v3, "SmhiProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "JSONException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 547
    invoke-virtual {v0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->close()V

    goto :goto_1

    .line 542
    .end local v1    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v1

    .line 543
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    const-string v3, "SmhiProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 547
    invoke-virtual {v0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->close()V

    goto :goto_1

    .line 544
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 545
    .local v1, "e":Ljava/text/ParseException;
    :try_start_3
    const-string v3, "SmhiProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ParseException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 547
    invoke-virtual {v0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->close()V

    goto :goto_1

    .end local v1    # "e":Ljava/text/ParseException;
    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Lse/volvocars/acu/weather/db/WeatherDatabase;->close()V

    throw v3
.end method

.method private getWeatherForecastAux(Lse/volvocars/acu/weather/db/WeatherDatabase;Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;)Lse/volvocars/acu/weather/CurrentWeather;
    .locals 32
    .param p1, "db"    # Lse/volvocars/acu/weather/db/WeatherDatabase;
    .param p2, "location"    # Lse/volvocars/acu/weather/settings/Location;
    .param p3, "from"    # Ljava/util/Date;
    .param p4, "to"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/io/IOException;,
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 451
    invoke-virtual/range {p1 .. p4}, Lse/volvocars/acu/weather/db/WeatherDatabase;->findWeather(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;)Landroid/database/Cursor;

    move-result-object v13

    .line 452
    .local v13, "cur":Landroid/database/Cursor;
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 454
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 456
    .local v14, "currentTime":J
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-gtz v6, :cond_1

    .line 457
    invoke-virtual/range {p1 .. p2}, Lse/volvocars/acu/weather/db/WeatherDatabase;->getNextUpdateForLocation(Lse/volvocars/acu/weather/settings/Location;)J

    move-result-wide v30

    cmp-long v6, v30, v14

    if-gez v6, :cond_0

    .line 458
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lse/volvocars/acu/weather/SmhiProvider;->fillDatabase(Lse/volvocars/acu/weather/settings/Location;Lse/volvocars/acu/weather/db/WeatherDatabase;)V

    .line 459
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 460
    invoke-virtual/range {p1 .. p4}, Lse/volvocars/acu/weather/db/WeatherDatabase;->findWeather(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;)Landroid/database/Cursor;

    move-result-object v13

    .line 461
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 463
    :cond_0
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-gtz v6, :cond_2

    .line 464
    const-string v6, "SmhiProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No weather data in database and no weahter fetched from weather provider: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lse/volvocars/acu/weather/settings/Location;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    const/4 v6, 0x0

    .line 529
    :goto_0
    return-object v6

    .line 469
    :cond_1
    invoke-virtual/range {p1 .. p2}, Lse/volvocars/acu/weather/db/WeatherDatabase;->getNextUpdateForLocation(Lse/volvocars/acu/weather/settings/Location;)J

    move-result-wide v30

    cmp-long v6, v30, v14

    if-gez v6, :cond_2

    .line 470
    const-string v6, "SmhiProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Weather data to old, fetch new forecast for location: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lse/volvocars/acu/weather/settings/Location;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lse/volvocars/acu/weather/SmhiProvider;->fillDatabase(Lse/volvocars/acu/weather/settings/Location;Lse/volvocars/acu/weather/db/WeatherDatabase;)V

    .line 472
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 473
    invoke-virtual/range {p1 .. p4}, Lse/volvocars/acu/weather/db/WeatherDatabase;->findWeather(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;)Landroid/database/Cursor;

    move-result-object v13

    .line 474
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    .line 478
    :cond_2
    const-string v6, "temp"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 479
    .local v23, "tempI":I
    const-string v6, "wind_speed"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 480
    .local v29, "windsI":I
    const-string v6, "wind_dir"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 481
    .local v28, "winddI":I
    const-string v6, "weather_symbol"

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 482
    .local v12, "condI":I
    const/16 v16, 0x0

    .line 483
    .local v16, "i":I
    const-wide/16 v17, 0x0

    .line 484
    .local v17, "maxTemp":D
    const-wide/16 v19, 0x0

    .line 485
    .local v19, "minTemp":D
    const-wide/16 v26, 0x0

    .line 486
    .local v26, "windSpeed":D
    const/16 v25, 0x0

    .line 488
    .local v25, "windDir":I
    :goto_1
    invoke-interface {v13}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v6

    if-nez v6, :cond_7

    .line 489
    move-object v0, v13

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v21

    .line 490
    .local v21, "temp":D
    invoke-interface {v13}, Landroid/database/Cursor;->isFirst()Z

    move-result v6

    if-nez v6, :cond_3

    cmpl-double v6, v21, v17

    if-lez v6, :cond_4

    .line 491
    :cond_3
    move-wide/from16 v17, v21

    .line 493
    :cond_4
    invoke-interface {v13}, Landroid/database/Cursor;->isFirst()Z

    move-result v6

    if-nez v6, :cond_5

    cmpg-double v6, v21, v19

    if-gez v6, :cond_6

    .line 494
    :cond_5
    move-wide/from16 v19, v21

    .line 496
    :cond_6
    move-object v0, v13

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    add-int v25, v25, v6

    .line 499
    move-object v0, v13

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v30

    add-double v26, v26, v30

    .line 502
    add-int/lit8 v16, v16, 0x1

    .line 503
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 506
    .end local v21    # "temp":D
    :cond_7
    if-eqz v16, :cond_9

    .line 507
    div-int v25, v25, v16

    .line 508
    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v30, v0

    div-double v26, v26, v30

    .line 515
    :goto_2
    div-int/lit8 v6, v16, 0x2

    invoke-interface {v13, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 517
    sget-object v6, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    move-object v0, v6

    move-wide/from16 v1, v17

    invoke-static {v0, v1, v2}, Lse/volvocars/acu/weather/Temperature;->from(Lse/volvocars/acu/weather/Temperature$Unit;D)Lse/volvocars/acu/weather/Temperature;

    move-result-object v8

    .line 518
    .local v8, "tMax":Lse/volvocars/acu/weather/Temperature;
    sget-object v6, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    move-object v0, v6

    move-wide/from16 v1, v19

    invoke-static {v0, v1, v2}, Lse/volvocars/acu/weather/Temperature;->from(Lse/volvocars/acu/weather/Temperature$Unit;D)Lse/volvocars/acu/weather/Temperature;

    move-result-object v7

    .line 519
    .local v7, "tMin":Lse/volvocars/acu/weather/Temperature;
    new-instance v10, Lse/volvocars/acu/weather/Wind;

    sget-object v6, Lse/volvocars/acu/weather/Wind$WindSpeedUnit;->mps:Lse/volvocars/acu/weather/Wind$WindSpeedUnit;

    move-object v0, v10

    move-wide/from16 v1, v26

    move-object v3, v6

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/Wind;-><init>(DLse/volvocars/acu/weather/Wind$WindSpeedUnit;I)V

    .line 520
    .local v10, "wind":Lse/volvocars/acu/weather/Wind;
    invoke-interface {v13, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 521
    .local v11, "code":I
    invoke-static {v11}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->fromCode(I)Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    move-result-object v24

    .line 522
    .local v24, "wc":Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;
    if-nez v24, :cond_8

    .line 524
    sget-object v24, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W001:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 526
    :cond_8
    invoke-virtual/range {v24 .. v24}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->getCondition()Lse/volvocars/acu/weather/WeatherCondition;

    move-result-object v9

    .line 527
    .local v9, "cond":Lse/volvocars/acu/weather/WeatherCondition;
    new-instance v5, Lse/volvocars/acu/weather/CurrentWeather;

    move-object/from16 v6, p2

    invoke-direct/range {v5 .. v10}, Lse/volvocars/acu/weather/CurrentWeather;-><init>(Lse/volvocars/acu/weather/settings/Location;Lse/volvocars/acu/weather/Temperature;Lse/volvocars/acu/weather/Temperature;Lse/volvocars/acu/weather/WeatherCondition;Lse/volvocars/acu/weather/Wind;)V

    .line 528
    .local v5, "cw":Lse/volvocars/acu/weather/CurrentWeather;
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    move-object v6, v5

    .line 529
    goto/16 :goto_0

    .line 512
    .end local v5    # "cw":Lse/volvocars/acu/weather/CurrentWeather;
    .end local v7    # "tMin":Lse/volvocars/acu/weather/Temperature;
    .end local v8    # "tMax":Lse/volvocars/acu/weather/Temperature;
    .end local v9    # "cond":Lse/volvocars/acu/weather/WeatherCondition;
    .end local v10    # "wind":Lse/volvocars/acu/weather/Wind;
    .end local v11    # "code":I
    .end local v24    # "wc":Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;
    :cond_9
    const-wide/high16 v26, -0x4000000000000000L    # -2.0

    goto :goto_2
.end method

.method private requestCurrentWeather(Lse/volvocars/acu/weather/settings/Location;)Lorg/json/JSONObject;
    .locals 2
    .param p1, "loc"    # Lse/volvocars/acu/weather/settings/Location;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/SmhiProvider;->constructRequestUri(Lse/volvocars/acu/weather/settings/Location;)Ljava/lang/String;

    move-result-object v1

    .line 168
    .local v1, "uri":Ljava/lang/String;
    invoke-direct {p0, v1}, Lse/volvocars/acu/weather/SmhiProvider;->sendRequest(Ljava/lang/CharSequence;)Lorg/json/JSONObject;

    move-result-object v0

    .line 170
    .local v0, "jsValue":Lorg/json/JSONObject;
    return-object v0
.end method

.method private sendRequest(Ljava/lang/CharSequence;)Lorg/json/JSONObject;
    .locals 7
    .param p1, "uri"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 262
    const/4 v2, 0x0

    .line 263
    .local v2, "tokener":Lorg/json/JSONTokener;
    const-string v4, "wsmedia.smhi.se"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lse/volvocars/acu/weather/SmhiProvider;->mKeyEncoder:Lse/volvocars/acu/weather/SmhiProvider$KeyEncoder;

    invoke-virtual {v6}, Lse/volvocars/acu/weather/SmhiProvider$KeyEncoder;->getEncodedKeyAsHeader()Ljava/util/Map;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lse/volvocars/acu/weather/Utils;->sendHttpGetRequest(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Ljava/io/InputStream;

    move-result-object v1

    .line 264
    .local v1, "response":Ljava/io/InputStream;
    invoke-static {v1}, Lse/volvocars/acu/weather/Utils;->inputStreamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .line 266
    .local v0, "json":Ljava/lang/String;
    new-instance v2, Lorg/json/JSONTokener;

    .end local v2    # "tokener":Lorg/json/JSONTokener;
    invoke-direct {v2, v0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 267
    .restart local v2    # "tokener":Lorg/json/JSONTokener;
    invoke-virtual {v2}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v3

    .line 268
    .local v3, "value":Ljava/lang/Object;
    instance-of v4, v3, Lorg/json/JSONObject;

    if-nez v4, :cond_0

    .line 269
    new-instance v4, Lorg/json/JSONException;

    const-string v5, "Unexpected contents, not a JSON object."

    invoke-direct {v4, v5}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 271
    :cond_0
    check-cast v3, Lorg/json/JSONObject;

    .end local v3    # "value":Ljava/lang/Object;
    return-object v3
.end method


# virtual methods
.method public getLocation()Lse/volvocars/acu/weather/settings/Location;
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider;->mLocationLock:Ljava/lang/Object;

    monitor-enter v0

    .line 150
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/weather/SmhiProvider;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    monitor-exit v0

    return-object v1

    .line 151
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    const-string v0, "SMHI"

    return-object v0
.end method

.method public runWeatherSearch(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;)V
    .locals 3
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;
    .param p2, "from"    # Ljava/util/Date;
    .param p3, "to"    # Ljava/util/Date;
    .param p4, "callback"    # Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;

    .prologue
    const/4 v2, 0x0

    .line 554
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v0

    sget-object v1, Lse/volvocars/acu/weather/Coordinate;->UNDEF:Lse/volvocars/acu/weather/Coordinate;

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 555
    :cond_0
    invoke-interface {p4, v2}, Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;->onWeatherReceive(Lse/volvocars/acu/weather/CurrentWeather;)V

    .line 570
    :cond_1
    :goto_0
    return-void

    .line 559
    :cond_2
    sget-object v0, Lse/volvocars/acu/weather/SmhiProvider;->mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    if-nez v0, :cond_3

    .line 560
    new-instance v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    invoke-direct {v0, p0, v2}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;-><init>(Lse/volvocars/acu/weather/SmhiProvider;Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;)V

    sput-object v0, Lse/volvocars/acu/weather/SmhiProvider;->mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    .line 561
    sget-object v0, Lse/volvocars/acu/weather/SmhiProvider;->mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    invoke-virtual {v0, p4, p2, p3, p1}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->addJob(Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;Ljava/util/Date;Ljava/util/Date;Lse/volvocars/acu/weather/settings/Location;)V

    .line 562
    sget-object v0, Lse/volvocars/acu/weather/SmhiProvider;->mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->start()V

    goto :goto_0

    .line 564
    :cond_3
    sget-object v0, Lse/volvocars/acu/weather/SmhiProvider;->mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    invoke-virtual {v0, p4, p2, p3, p1}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->addJob(Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;Ljava/util/Date;Ljava/util/Date;Lse/volvocars/acu/weather/settings/Location;)V

    .line 565
    sget-object v0, Lse/volvocars/acu/weather/SmhiProvider;->mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 566
    new-instance v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    sget-object v1, Lse/volvocars/acu/weather/SmhiProvider;->mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    invoke-direct {v0, p0, v1}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;-><init>(Lse/volvocars/acu/weather/SmhiProvider;Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;)V

    sput-object v0, Lse/volvocars/acu/weather/SmhiProvider;->mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    .line 567
    sget-object v0, Lse/volvocars/acu/weather/SmhiProvider;->mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->start()V

    goto :goto_0
.end method

.method public searchLocation(Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 1
    .param p1, "location"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 339
    const/4 v0, 0x0

    return-object v0
.end method

.method public searchLocation(Ljava/lang/CharSequence;Landroid/location/Location;)Ljava/util/List;
    .locals 20
    .param p1, "location"    # Ljava/lang/CharSequence;
    .param p2, "currentLocation"    # Landroid/location/Location;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Landroid/location/Location;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 276
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 277
    .local v10, "locLs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lse/volvocars/acu/weather/settings/Location;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lse/volvocars/acu/weather/SmhiProvider;->mContext:Landroid/content/Context;

    move-object v12, v0

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090054

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 278
    .local v11, "smhiLangCode":Ljava/lang/String;
    invoke-static {}, Lse/volvocars/acu/AcuActivity;->getAcuContext()Landroid/content/Context;

    move-result-object v3

    .line 279
    .local v3, "context":Landroid/content/Context;
    if-eqz v3, :cond_0

    .line 280
    const v12, 0x7f090054

    invoke-virtual {v3, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 283
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 291
    .local v2, "builder":Ljava/lang/StringBuilder;
    const-string v12, "/ws/products/"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "location?"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    if-eqz p2, :cond_1

    .line 295
    const-string v12, "lat"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {p2 .. p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "&"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "long"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {p2 .. p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "&"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    :cond_1
    const-string v12, "query"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object v0, v12

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "&"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    const-string v12, "lang"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "&"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const-string v12, "limit"

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/16 v13, 0x1e

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 315
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    move-object v1, v12

    invoke-direct {v0, v1}, Lse/volvocars/acu/weather/SmhiProvider;->sendRequest(Ljava/lang/CharSequence;)Lorg/json/JSONObject;

    move-result-object v8

    .line 316
    .local v8, "jsValue":Lorg/json/JSONObject;
    const-string v12, "data"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 318
    .local v4, "data":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v12

    if-ge v6, v12, :cond_2

    .line 319
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/json/JSONArray;

    .line 320
    .local v7, "item":Lorg/json/JSONArray;
    new-instance v9, Lse/volvocars/acu/weather/settings/Location;

    const/4 v12, 0x0

    invoke-virtual {v7, v12}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v7, v13}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x3

    invoke-virtual {v7, v14}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v14

    new-instance v15, Lse/volvocars/acu/weather/Coordinate;

    const/16 v16, 0x4

    move-object v0, v7

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v16

    const/16 v18, 0x5

    move-object v0, v7

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v18

    invoke-direct/range {v15 .. v19}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    invoke-direct {v9, v12, v13, v14, v15}, Lse/volvocars/acu/weather/settings/Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lse/volvocars/acu/weather/Coordinate;)V

    .line 323
    .local v9, "l":Lse/volvocars/acu/weather/settings/Location;
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 318
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .end local v7    # "item":Lorg/json/JSONArray;
    .end local v9    # "l":Lse/volvocars/acu/weather/settings/Location;
    :cond_2
    move-object v12, v10

    .line 334
    .end local v4    # "data":Lorg/json/JSONArray;
    .end local v6    # "i":I
    .end local v8    # "jsValue":Lorg/json/JSONObject;
    :goto_1
    return-object v12

    .line 328
    :catch_0
    move-exception v12

    move-object v5, v12

    .line 329
    .local v5, "e":Ljava/io/IOException;
    const-string v12, "SmhiProvider"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Can\'t find location, IOException: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    .end local v5    # "e":Ljava/io/IOException;
    :goto_2
    const/4 v12, 0x0

    goto :goto_1

    .line 330
    :catch_1
    move-exception v12

    move-object v5, v12

    .line 331
    .local v5, "e":Lorg/json/JSONException;
    const-string v12, "SmhiProvider"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Can\'t find location, JSONException: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public searchLocation(Lse/volvocars/acu/weather/Coordinate;)Ljava/util/List;
    .locals 1
    .param p1, "coordinate"    # Lse/volvocars/acu/weather/Coordinate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lse/volvocars/acu/weather/Coordinate;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 344
    const/4 v0, 0x0

    return-object v0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 690
    iput-object p1, p0, Lse/volvocars/acu/weather/SmhiProvider;->mContext:Landroid/content/Context;

    .line 691
    return-void
.end method

.method public setLocation(DD)V
    .locals 3
    .param p1, "lat"    # D
    .param p3, "lon"    # D

    .prologue
    const-string v2, ""

    .line 162
    new-instance v0, Lse/volvocars/acu/weather/settings/Location;

    const-string v1, ""

    const-string v1, ""

    const-string v1, ""

    new-instance v1, Lse/volvocars/acu/weather/Coordinate;

    invoke-direct {v1, p1, p2, p3, p4}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    invoke-direct {v0, v2, v2, v2, v1}, Lse/volvocars/acu/weather/settings/Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lse/volvocars/acu/weather/Coordinate;)V

    iput-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    .line 163
    return-void
.end method

.method public setLocation(Lse/volvocars/acu/weather/settings/Location;)V
    .locals 0
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    .line 156
    iput-object p1, p0, Lse/volvocars/acu/weather/SmhiProvider;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    .line 158
    return-void
.end method

.method public waitForSearchTofinish()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 694
    sget-object v0, Lse/volvocars/acu/weather/SmhiProvider;->mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    if-eqz v0, :cond_0

    .line 695
    sget-object v0, Lse/volvocars/acu/weather/SmhiProvider;->mBgwf:Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    invoke-virtual {v0}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->join()V

    .line 698
    :cond_0
    return-void
.end method
