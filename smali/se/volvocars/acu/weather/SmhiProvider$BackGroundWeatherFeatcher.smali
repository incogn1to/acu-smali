.class Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;
.super Ljava/lang/Thread;
.source "SmhiProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/SmhiProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackGroundWeatherFeatcher"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$CallbackRunner;,
        Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;
    }
.end annotation


# static fields
.field private static final WEATHER_SEARCH_STACK_DEPTH:I = 0xa


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mIsRunning:Z

.field private mQueue:Ljava/util/concurrent/LinkedBlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lse/volvocars/acu/weather/SmhiProvider;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/weather/SmhiProvider;Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;)V
    .locals 1
    .param p2, "mBgwf"    # Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;

    .prologue
    .line 592
    iput-object p1, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->this$0:Lse/volvocars/acu/weather/SmhiProvider;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 583
    const/4 v0, 0x1

    iput-boolean v0, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mIsRunning:Z

    .line 593
    if-eqz p2, :cond_0

    .line 594
    invoke-direct {p2}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->getArgumentQueue()Ljava/util/concurrent/LinkedBlockingDeque;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 595
    invoke-direct {p2}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mHandler:Landroid/os/Handler;

    .line 597
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 598
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mHandler:Landroid/os/Handler;

    .line 601
    :cond_1
    return-void
.end method

.method private getArgumentQueue()Ljava/util/concurrent/LinkedBlockingDeque;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;",
            ">;"
        }
    .end annotation

    .prologue
    .line 612
    iget-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    return-object v0
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public addJob(Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;Ljava/util/Date;Ljava/util/Date;Lse/volvocars/acu/weather/settings/Location;)V
    .locals 6
    .param p1, "callback"    # Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;
    .param p2, "from"    # Ljava/util/Date;
    .param p3, "to"    # Ljava/util/Date;
    .param p4, "location"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    .line 624
    new-instance v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;

    invoke-direct {v0, p0}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;-><init>(Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;)V

    .line 625
    .local v0, "arg":Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;
    iput-object p1, v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;->callback:Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;

    .line 626
    iput-object p2, v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;->from:Ljava/util/Date;

    .line 627
    iput-object p3, v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;->to:Ljava/util/Date;

    .line 628
    iput-object p4, v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;->location:Lse/volvocars/acu/weather/settings/Location;

    .line 629
    iget-object v2, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    if-nez v2, :cond_0

    .line 630
    new-instance v2, Ljava/util/concurrent/LinkedBlockingDeque;

    const/16 v3, 0xa

    invoke-direct {v2, v3}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>(I)V

    iput-object v2, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 632
    :cond_0
    :goto_0
    iget-object v2, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/LinkedBlockingDeque;->offerFirst(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 633
    iget-object v2, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v2}, Ljava/util/concurrent/LinkedBlockingDeque;->removeLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;

    .line 634
    .local v1, "l":Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;
    iget-object v2, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mHandler:Landroid/os/Handler;

    new-instance v3, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$CallbackRunner;

    iget-object v4, v1, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;->callback:Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v5}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$CallbackRunner;-><init>(Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;Lse/volvocars/acu/weather/CurrentWeather;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 636
    .end local v1    # "l":Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;
    :cond_1
    return-void
.end method

.method isRunning()Z
    .locals 1

    .prologue
    .line 608
    iget-boolean v0, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mIsRunning:Z

    return v0
.end method

.method public run()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const-string v10, "SmhiProvider"

    .line 656
    const/4 v0, 0x0

    .line 657
    .local v0, "arg":Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;
    iget-object v6, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    if-eqz v6, :cond_0

    .line 658
    iget-object v6, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v6}, Ljava/util/concurrent/LinkedBlockingDeque;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v6, 0x1

    :goto_0
    iput-boolean v6, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mIsRunning:Z

    .line 660
    :cond_0
    :goto_1
    iget-boolean v6, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mIsRunning:Z

    if-eqz v6, :cond_4

    .line 663
    :try_start_0
    iget-object v6, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    const-wide/16 v7, 0x2

    sget-object v9, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v7, v8, v9}, Ljava/util/concurrent/LinkedBlockingDeque;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "arg":Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;
    check-cast v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 667
    .restart local v0    # "arg":Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;
    :goto_2
    if-eqz v0, :cond_3

    .line 668
    iget-object v4, v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;->location:Lse/volvocars/acu/weather/settings/Location;

    .line 669
    .local v4, "location":Lse/volvocars/acu/weather/settings/Location;
    iget-object v3, v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;->from:Ljava/util/Date;

    .line 670
    .local v3, "from":Ljava/util/Date;
    iget-object v5, v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;->to:Ljava/util/Date;

    .line 671
    .local v5, "to":Ljava/util/Date;
    const-string v6, "SmhiProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Running weather search for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lse/volvocars/acu/weather/settings/Location;->getCity()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", from: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", to: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", job queue size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-virtual {v7}, Ljava/util/concurrent/LinkedBlockingDeque;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v10, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    const-string v6, "SmhiProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " Long: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v7

    invoke-virtual {v7}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " Lat: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lse/volvocars/acu/weather/settings/Location;->getCoordinate()Lse/volvocars/acu/weather/Coordinate;

    move-result-object v7

    invoke-virtual {v7}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v10, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    iget-object v6, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->this$0:Lse/volvocars/acu/weather/SmhiProvider;

    # invokes: Lse/volvocars/acu/weather/SmhiProvider;->getWeatherForecast(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;)Lse/volvocars/acu/weather/CurrentWeather;
    invoke-static {v6, v4, v3, v5}, Lse/volvocars/acu/weather/SmhiProvider;->access$000(Lse/volvocars/acu/weather/SmhiProvider;Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;)Lse/volvocars/acu/weather/CurrentWeather;

    move-result-object v2

    .line 674
    .local v2, "forecast":Lse/volvocars/acu/weather/CurrentWeather;
    if-nez v2, :cond_1

    .line 675
    const-string v6, "SmhiProvider"

    const-string v6, "Warning: Received an empty (null) forecast."

    invoke-static {v10, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    :cond_1
    iget-object v6, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mHandler:Landroid/os/Handler;

    new-instance v7, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$CallbackRunner;

    iget-object v8, v0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;->callback:Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;

    invoke-direct {v7, p0, v8, v2}, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$CallbackRunner;-><init>(Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;Lse/volvocars/acu/weather/CurrentWeather;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .end local v2    # "forecast":Lse/volvocars/acu/weather/CurrentWeather;
    .end local v3    # "from":Ljava/util/Date;
    .end local v4    # "location":Lse/volvocars/acu/weather/settings/Location;
    .end local v5    # "to":Ljava/util/Date;
    :cond_2
    move v6, v11

    .line 658
    goto/16 :goto_0

    .line 664
    .end local v0    # "arg":Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;
    :catch_0
    move-exception v6

    move-object v1, v6

    .line 665
    .local v1, "e":Ljava/lang/InterruptedException;
    const/4 v0, 0x0

    .restart local v0    # "arg":Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher$Arguments;
    goto/16 :goto_2

    .line 682
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_3
    iput-boolean v11, p0, Lse/volvocars/acu/weather/SmhiProvider$BackGroundWeatherFeatcher;->mIsRunning:Z

    goto/16 :goto_1

    .line 685
    :cond_4
    return-void
.end method
