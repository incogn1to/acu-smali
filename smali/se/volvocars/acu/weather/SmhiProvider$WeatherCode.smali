.class final enum Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;
.super Ljava/lang/Enum;
.source "SmhiProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/SmhiProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "WeatherCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field private static final CODES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum W001:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W002:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W003:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W004:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W005:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W006:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W007:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W008:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W009:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W010:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W011:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W012:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W013:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W014:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

.field public static final enum W015:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;


# instance fields
.field private final mCode:I

.field private final mCondition:Lse/volvocars/acu/weather/WeatherCondition;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x5

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 213
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W001"

    const/4 v6, 0x0

    sget-object v7, Lse/volvocars/acu/weather/WeatherCondition;->Sun:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v6, v9, v7}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W001:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 214
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W002"

    sget-object v6, Lse/volvocars/acu/weather/WeatherCondition;->Sunny:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v9, v10, v6}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W002:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 215
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W003"

    sget-object v6, Lse/volvocars/acu/weather/WeatherCondition;->PartlyCloudy:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v10, v11, v6}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W003:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 216
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W004"

    sget-object v6, Lse/volvocars/acu/weather/WeatherCondition;->PartlyCloudy:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v11, v12, v6}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W004:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 217
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W005"

    sget-object v6, Lse/volvocars/acu/weather/WeatherCondition;->Cloudy:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v12, v13, v6}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W005:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 218
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W006"

    const/4 v6, 0x6

    sget-object v7, Lse/volvocars/acu/weather/WeatherCondition;->Overcast:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v13, v6, v7}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W006:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 219
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W007"

    const/4 v6, 0x6

    const/4 v7, 0x7

    sget-object v8, Lse/volvocars/acu/weather/WeatherCondition;->Fog:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W007:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 220
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W008"

    const/4 v6, 0x7

    const/16 v7, 0x8

    sget-object v8, Lse/volvocars/acu/weather/WeatherCondition;->LightRain:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W008:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 221
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W009"

    const/16 v6, 0x8

    const/16 v7, 0x9

    sget-object v8, Lse/volvocars/acu/weather/WeatherCondition;->LightRainAndThunder:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W009:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 222
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W010"

    const/16 v6, 0x9

    const/16 v7, 0xa

    sget-object v8, Lse/volvocars/acu/weather/WeatherCondition;->LightSleet:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W010:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 223
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W011"

    const/16 v6, 0xa

    const/16 v7, 0xb

    sget-object v8, Lse/volvocars/acu/weather/WeatherCondition;->SnowShowers:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W011:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 224
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W012"

    const/16 v6, 0xb

    const/16 v7, 0xc

    sget-object v8, Lse/volvocars/acu/weather/WeatherCondition;->Rain:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W012:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 225
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W013"

    const/16 v6, 0xc

    const/16 v7, 0xd

    sget-object v8, Lse/volvocars/acu/weather/WeatherCondition;->Thunder:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W013:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 226
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W014"

    const/16 v6, 0xd

    const/16 v7, 0xe

    sget-object v8, Lse/volvocars/acu/weather/WeatherCondition;->Sleet:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W014:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 227
    new-instance v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const-string v5, "W015"

    const/16 v6, 0xe

    const/16 v7, 0xf

    sget-object v8, Lse/volvocars/acu/weather/WeatherCondition;->Snow:Lse/volvocars/acu/weather/WeatherCondition;

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;-><init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W015:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 208
    const/16 v4, 0xf

    new-array v4, v4, [Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    const/4 v5, 0x0

    sget-object v6, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W001:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v6, v4, v5

    sget-object v5, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W002:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v5, v4, v9

    sget-object v5, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W003:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v5, v4, v10

    sget-object v5, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W004:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v5, v4, v11

    sget-object v5, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W005:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v5, v4, v12

    sget-object v5, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W006:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v5, v4, v13

    const/4 v5, 0x6

    sget-object v6, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W007:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W008:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W009:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W010:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    sget-object v6, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W011:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v6, v4, v5

    const/16 v5, 0xb

    sget-object v6, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W012:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v6, v4, v5

    const/16 v5, 0xc

    sget-object v6, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W013:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v6, v4, v5

    const/16 v5, 0xd

    sget-object v6, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W014:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v6, v4, v5

    const/16 v5, 0xe

    sget-object v6, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->W015:Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    aput-object v6, v4, v5

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->$VALUES:[Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    .line 235
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->CODES:Ljava/util/Map;

    .line 236
    invoke-static {}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->values()[Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    move-result-object v0

    .local v0, "arr$":[Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 237
    .local v1, "code":Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;
    sget-object v4, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->CODES:Ljava/util/Map;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->getCode()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 239
    .end local v1    # "code":Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILse/volvocars/acu/weather/WeatherCondition;)V
    .locals 0
    .param p3, "code"    # I
    .param p4, "condition"    # Lse/volvocars/acu/weather/WeatherCondition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lse/volvocars/acu/weather/WeatherCondition;",
            ")V"
        }
    .end annotation

    .prologue
    .line 245
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 246
    iput p3, p0, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->mCode:I

    .line 247
    iput-object p4, p0, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->mCondition:Lse/volvocars/acu/weather/WeatherCondition;

    .line 248
    return-void
.end method

.method public static fromCode(I)Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;
    .locals 2
    .param p0, "code"    # I

    .prologue
    .line 242
    sget-object v0, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->CODES:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    .end local p0    # "code":I
    check-cast p0, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 208
    const-class v0, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    .end local p0    # "name":Ljava/lang/String;
    check-cast p0, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    return-object p0
.end method

.method public static values()[Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;
    .locals 1

    .prologue
    .line 208
    sget-object v0, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->$VALUES:[Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    invoke-virtual {v0}, [Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->mCode:I

    return v0
.end method

.method public getCondition()Lse/volvocars/acu/weather/WeatherCondition;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lse/volvocars/acu/weather/SmhiProvider$WeatherCode;->mCondition:Lse/volvocars/acu/weather/WeatherCondition;

    return-object v0
.end method
