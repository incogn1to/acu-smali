.class final Lse/volvocars/acu/weather/WeatherWidgetTouchListener;
.super Ljava/lang/Object;
.source "WeatherWidgetTouchListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field static final SQRA:I = 0x20


# instance fields
.field private mLocationController:Lse/volvocars/acu/weather/LocationController;

.field private mMovedOutsidePressArea:Z

.field private mReleaseSquare:Landroid/graphics/Rect;

.field private final mWidget:Lse/volvocars/acu/weather/WeatherWidget;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/weather/WeatherWidget;)V
    .locals 2
    .param p1, "widget"    # Lse/volvocars/acu/weather/WeatherWidget;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mReleaseSquare:Landroid/graphics/Rect;

    .line 26
    if-nez p1, :cond_0

    .line 27
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input widget cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object p1, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    .line 30
    return-void
.end method

.method private startWeatherSettings()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-static {v0}, Lse/volvocars/acu/weather/settings/WeatherSettings;->startFromWidget(Lse/volvocars/acu/weather/WeatherWidget;)V

    .line 70
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v4, 0x20

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 35
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mLocationController:Lse/volvocars/acu/weather/LocationController;

    if-eqz v2, :cond_0

    .line 36
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mLocationController:Lse/volvocars/acu/weather/LocationController;

    invoke-virtual {v2, p1, p2}, Lse/volvocars/acu/weather/LocationController;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 38
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    .line 39
    .local v0, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 41
    .local v1, "y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v2, v8

    .line 65
    :goto_0
    return v2

    .line 43
    :pswitch_0
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-virtual {v2, v7}, Lse/volvocars/acu/weather/WeatherWidget;->setPressedState(Z)V

    .line 44
    new-instance v2, Landroid/graphics/Rect;

    sub-int v3, v0, v4

    sub-int v4, v1, v4

    add-int/lit8 v5, v0, 0x20

    add-int/lit8 v6, v1, 0x20

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mReleaseSquare:Landroid/graphics/Rect;

    .line 45
    iput-boolean v8, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mMovedOutsidePressArea:Z

    move v2, v7

    .line 46
    goto :goto_0

    .line 48
    :pswitch_1
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-virtual {v2, v8}, Lse/volvocars/acu/weather/WeatherWidget;->setPressedState(Z)V

    .line 49
    iput-boolean v7, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mMovedOutsidePressArea:Z

    move v2, v7

    .line 50
    goto :goto_0

    .line 52
    :pswitch_2
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mReleaseSquare:Landroid/graphics/Rect;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mMovedOutsidePressArea:Z

    if-nez v2, :cond_1

    .line 53
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-virtual {v2, v8}, Lse/volvocars/acu/weather/WeatherWidget;->setPressedState(Z)V

    .line 54
    iput-boolean v7, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mMovedOutsidePressArea:Z

    :cond_1
    move v2, v7

    .line 56
    goto :goto_0

    .line 58
    :pswitch_3
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mWidget:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-virtual {v2, v8}, Lse/volvocars/acu/weather/WeatherWidget;->setPressedState(Z)V

    .line 59
    iget-object v2, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mReleaseSquare:Landroid/graphics/Rect;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mMovedOutsidePressArea:Z

    if-nez v2, :cond_2

    .line 60
    invoke-direct {p0}, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->startWeatherSettings()V

    move v2, v7

    .line 61
    goto :goto_0

    :cond_2
    move v2, v7

    .line 63
    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setLocationController(Lse/volvocars/acu/weather/LocationController;)V
    .locals 0
    .param p1, "locationController"    # Lse/volvocars/acu/weather/LocationController;

    .prologue
    .line 73
    iput-object p1, p0, Lse/volvocars/acu/weather/WeatherWidgetTouchListener;->mLocationController:Lse/volvocars/acu/weather/LocationController;

    .line 74
    return-void
.end method
