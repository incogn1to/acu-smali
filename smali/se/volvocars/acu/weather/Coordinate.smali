.class public final Lse/volvocars/acu/weather/Coordinate;
.super Ljava/lang/Object;
.source "Coordinate.java"


# static fields
.field private static final DEGREE:C = '\u00b0'

.field private static final MINUTE:C = '\u2032'

.field private static final PRECISION:D = 1.0E-4

.field private static final SECOND:C = '\u2033'

.field public static final UNDEF:Lse/volvocars/acu/weather/Coordinate;

.field public static final ZERO:Lse/volvocars/acu/weather/Coordinate;


# instance fields
.field private final mLatitude:D

.field private final mLongitude:D


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const-wide v3, 0x40f86a0000000000L    # 100000.0

    const-wide/16 v1, 0x0

    .line 20
    new-instance v0, Lse/volvocars/acu/weather/Coordinate;

    invoke-direct {v0, v3, v4, v3, v4}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    sput-object v0, Lse/volvocars/acu/weather/Coordinate;->UNDEF:Lse/volvocars/acu/weather/Coordinate;

    .line 25
    new-instance v0, Lse/volvocars/acu/weather/Coordinate;

    invoke-direct {v0, v1, v2, v1, v2}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    sput-object v0, Lse/volvocars/acu/weather/Coordinate;->ZERO:Lse/volvocars/acu/weather/Coordinate;

    return-void
.end method

.method public constructor <init>(DD)V
    .locals 0
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide p1, p0, Lse/volvocars/acu/weather/Coordinate;->mLatitude:D

    .line 38
    iput-wide p3, p0, Lse/volvocars/acu/weather/Coordinate;->mLongitude:D

    .line 39
    return-void
.end method

.method private static toArcString(D)Ljava/lang/String;
    .locals 9
    .param p0, "value"    # D

    .prologue
    const-wide/high16 v7, 0x404e000000000000L    # 60.0

    .line 103
    move-wide v2, p0

    .line 104
    .local v2, "res":D
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-int v0, v5

    .line 105
    .local v0, "degrees":I
    int-to-double v5, v0

    sub-double v5, v2, v5

    mul-double v2, v5, v7

    .line 106
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-int v1, v5

    .line 107
    .local v1, "minutes":I
    int-to-double v5, v1

    sub-double v5, v2, v5

    mul-double v2, v5, v7

    .line 108
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-int v4, v5

    .line 109
    .local v4, "seconds":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\u00b0 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\u2032 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x2033

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method


# virtual methods
.method public distance(Lse/volvocars/acu/weather/Coordinate;)D
    .locals 12
    .param p1, "location"    # Lse/volvocars/acu/weather/Coordinate;

    .prologue
    const-wide v10, 0x4066800000000000L    # 180.0

    .line 54
    if-nez p1, :cond_0

    .line 55
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Input location cannot be null."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 57
    :cond_0
    iget-wide v6, p0, Lse/volvocars/acu/weather/Coordinate;->mLatitude:D

    invoke-virtual {p1}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v8

    add-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double v4, v6, v8

    .line 58
    .local v4, "meanLatitude":D
    iget-wide v6, p0, Lse/volvocars/acu/weather/Coordinate;->mLongitude:D

    invoke-virtual {p1}, Lse/volvocars/acu/weather/Coordinate;->getLongitude()D

    move-result-wide v8

    sub-double v0, v6, v8

    .line 59
    .local v0, "dx":D
    cmpl-double v6, v0, v10

    if-lez v6, :cond_1

    .line 60
    const-wide v6, 0x4076800000000000L    # 360.0

    sub-double v0, v6, v0

    .line 62
    :cond_1
    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v4

    div-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    mul-double/2addr v0, v6

    .line 63
    iget-wide v6, p0, Lse/volvocars/acu/weather/Coordinate;->mLatitude:D

    invoke-virtual {p1}, Lse/volvocars/acu/weather/Coordinate;->getLatitude()D

    move-result-wide v8

    sub-double v2, v6, v8

    .line 64
    .local v2, "dy":D
    mul-double v6, v0, v0

    mul-double v8, v2, v2

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    return-wide v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const-wide v6, 0x3f1a36e2eb1c432dL    # 1.0E-4

    .line 74
    if-ne p0, p1, :cond_0

    move v2, v9

    .line 81
    :goto_0
    return v2

    .line 77
    :cond_0
    instance-of v2, p1, Lse/volvocars/acu/weather/Coordinate;

    if-nez v2, :cond_1

    move v2, v8

    .line 78
    goto :goto_0

    .line 80
    :cond_1
    move-object v0, p1

    check-cast v0, Lse/volvocars/acu/weather/Coordinate;

    move-object v1, v0

    .line 81
    .local v1, "o":Lse/volvocars/acu/weather/Coordinate;
    iget-wide v2, p0, Lse/volvocars/acu/weather/Coordinate;->mLatitude:D

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    iget-wide v4, v1, Lse/volvocars/acu/weather/Coordinate;->mLatitude:D

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lse/volvocars/acu/weather/Coordinate;->mLongitude:D

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    iget-wide v4, v1, Lse/volvocars/acu/weather/Coordinate;->mLongitude:D

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    move v2, v9

    goto :goto_0

    :cond_2
    move v2, v8

    goto :goto_0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lse/volvocars/acu/weather/Coordinate;->mLatitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lse/volvocars/acu/weather/Coordinate;->mLongitude:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const-wide v3, 0x3f1a36e2eb1c432dL    # 1.0E-4

    .line 87
    iget-wide v0, p0, Lse/volvocars/acu/weather/Coordinate;->mLatitude:D

    div-double/2addr v0, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iget-wide v1, p0, Lse/volvocars/acu/weather/Coordinate;->mLongitude:D

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toBeautifiedString()Ljava/lang/String;
    .locals 6

    .prologue
    const-wide/16 v3, 0x0

    const-string v5, " "

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-wide v1, p0, Lse/volvocars/acu/weather/Coordinate;->mLatitude:D

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    invoke-static {v1, v2}, Lse/volvocars/acu/weather/Coordinate;->toArcString(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    const-string v1, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    iget-wide v1, p0, Lse/volvocars/acu/weather/Coordinate;->mLatitude:D

    cmpg-double v1, v1, v3

    if-gez v1, :cond_0

    const-string v1, "S"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    iget-wide v1, p0, Lse/volvocars/acu/weather/Coordinate;->mLongitude:D

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(D)D

    move-result-wide v1

    invoke-static {v1, v2}, Lse/volvocars/acu/weather/Coordinate;->toArcString(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    const-string v1, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    iget-wide v1, p0, Lse/volvocars/acu/weather/Coordinate;->mLongitude:D

    cmpg-double v1, v1, v3

    if-gez v1, :cond_1

    const-string v1, "W"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 94
    :cond_0
    const-string v1, "N"

    goto :goto_0

    .line 98
    :cond_1
    const-string v1, "E"

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, p0, Lse/volvocars/acu/weather/Coordinate;->mLatitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lse/volvocars/acu/weather/Coordinate;->mLongitude:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
