.class final Lse/volvocars/acu/weather/WeatherWidget$WeatherReciever;
.super Ljava/lang/Object;
.source "WeatherWidget.java"

# interfaces
.implements Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/weather/WeatherWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WeatherReciever"
.end annotation


# instance fields
.field final synthetic this$0:Lse/volvocars/acu/weather/WeatherWidget;


# direct methods
.method private constructor <init>(Lse/volvocars/acu/weather/WeatherWidget;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lse/volvocars/acu/weather/WeatherWidget$WeatherReciever;->this$0:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lse/volvocars/acu/weather/WeatherWidget;Lse/volvocars/acu/weather/WeatherWidget$1;)V
    .locals 0
    .param p1, "x0"    # Lse/volvocars/acu/weather/WeatherWidget;
    .param p2, "x1"    # Lse/volvocars/acu/weather/WeatherWidget$1;

    .prologue
    .line 368
    invoke-direct {p0, p1}, Lse/volvocars/acu/weather/WeatherWidget$WeatherReciever;-><init>(Lse/volvocars/acu/weather/WeatherWidget;)V

    return-void
.end method


# virtual methods
.method public onWeatherReceive(Lse/volvocars/acu/weather/CurrentWeather;)V
    .locals 3
    .param p1, "weather"    # Lse/volvocars/acu/weather/CurrentWeather;

    .prologue
    .line 373
    if-eqz p1, :cond_1

    .line 374
    invoke-virtual {p1}, Lse/volvocars/acu/weather/CurrentWeather;->getLocation()Lse/volvocars/acu/weather/settings/Location;

    move-result-object v0

    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget$WeatherReciever;->this$0:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/WeatherWidget;->getLocation()Lse/volvocars/acu/weather/settings/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/settings/Location;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    const-string v0, "WeatherWidget"

    const-string v1, "Received weather data for another location than the current one."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    :goto_0
    return-void

    .line 378
    :cond_0
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget$WeatherReciever;->this$0:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-virtual {v0, p1}, Lse/volvocars/acu/weather/WeatherWidget;->showWeatherData(Lse/volvocars/acu/weather/CurrentWeather;)V

    goto :goto_0

    .line 380
    :cond_1
    iget-object v0, p0, Lse/volvocars/acu/weather/WeatherWidget$WeatherReciever;->this$0:Lse/volvocars/acu/weather/WeatherWidget;

    iget-object v1, p0, Lse/volvocars/acu/weather/WeatherWidget$WeatherReciever;->this$0:Lse/volvocars/acu/weather/WeatherWidget;

    invoke-virtual {v1}, Lse/volvocars/acu/weather/WeatherWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    #const v2, 0x7f090033

    #invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #move-result-object v1

    #invoke-virtual {v0, v1}, Lse/volvocars/acu/weather/WeatherWidget;->showErrorMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
