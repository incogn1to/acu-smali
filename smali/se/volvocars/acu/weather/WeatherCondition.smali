.class public final enum Lse/volvocars/acu/weather/WeatherCondition;
.super Ljava/lang/Enum;
.source "WeatherCondition.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lse/volvocars/acu/weather/WeatherCondition;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum Cloudy:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum Fog:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum HeavyRain:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum LightRain:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum LightRainAndThunder:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum LightSleet:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum LightSnow:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum Overcast:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum PartlyCloudy:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum Rain:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum Sleet:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum Snow:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum SnowShowers:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum Sun:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum Sunny:Lse/volvocars/acu/weather/WeatherCondition;

.field public static final enum Thunder:Lse/volvocars/acu/weather/WeatherCondition;


# instance fields
.field private final mDescription:I

.field private final mIcon:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const v5, 0x7f020073

    const v4, 0x7f020064

    .line 14
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "PartlyCloudy"

    const v2, 0x7f09002b

    const v3, 0x7f02000a

    invoke-direct {v0, v1, v6, v2, v3}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->PartlyCloudy:Lse/volvocars/acu/weather/WeatherCondition;

    .line 15
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "Cloudy"

    const v2, 0x7f09002a

    const v3, 0x7f02000a

    invoke-direct {v0, v1, v7, v2, v3}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->Cloudy:Lse/volvocars/acu/weather/WeatherCondition;

    .line 16
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "Fog"

    const v2, 0x7f090026

    const v3, 0x7f02000f

    invoke-direct {v0, v1, v8, v2, v3}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->Fog:Lse/volvocars/acu/weather/WeatherCondition;

    .line 18
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "HeavyRain"

    const/4 v2, 0x3

    const v3, 0x7f09001c

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->HeavyRain:Lse/volvocars/acu/weather/WeatherCondition;

    .line 19
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "LightRain"

    const/4 v2, 0x4

    const v3, 0x7f09001e

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->LightRain:Lse/volvocars/acu/weather/WeatherCondition;

    .line 20
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "Rain"

    const/4 v2, 0x5

    const v3, 0x7f09001d

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->Rain:Lse/volvocars/acu/weather/WeatherCondition;

    .line 22
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "Sleet"

    const/4 v2, 0x6

    const v3, 0x7f09001f

    const v4, 0x7f020070

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->Sleet:Lse/volvocars/acu/weather/WeatherCondition;

    .line 23
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "LightSleet"

    const/4 v2, 0x7

    const v3, 0x7f090020

    const v4, 0x7f020070

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->LightSleet:Lse/volvocars/acu/weather/WeatherCondition;

    .line 25
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "Thunder"

    const/16 v2, 0x8

    const v3, 0x7f090019

    const v4, 0x7f02007f

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->Thunder:Lse/volvocars/acu/weather/WeatherCondition;

    .line 26
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "LightRainAndThunder"

    const/16 v2, 0x9

    const v3, 0x7f090018

    const v4, 0x7f020082

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->LightRainAndThunder:Lse/volvocars/acu/weather/WeatherCondition;

    .line 28
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "Snow"

    const/16 v2, 0xa

    const v3, 0x7f09001a

    invoke-direct {v0, v1, v2, v3, v5}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->Snow:Lse/volvocars/acu/weather/WeatherCondition;

    .line 29
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "LightSnow"

    const/16 v2, 0xb

    const v3, 0x7f09001b

    invoke-direct {v0, v1, v2, v3, v5}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->LightSnow:Lse/volvocars/acu/weather/WeatherCondition;

    .line 30
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "SnowShowers"

    const/16 v2, 0xc

    const v3, 0x7f090021

    invoke-direct {v0, v1, v2, v3, v5}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->SnowShowers:Lse/volvocars/acu/weather/WeatherCondition;

    .line 32
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "Sun"

    const/16 v2, 0xd

    const v3, 0x7f09002c

    const v4, 0x7f020079

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->Sun:Lse/volvocars/acu/weather/WeatherCondition;

    .line 33
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "Sunny"

    const/16 v2, 0xe

    const v3, 0x7f09002d

    const v4, 0x7f020059

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->Sunny:Lse/volvocars/acu/weather/WeatherCondition;

    .line 35
    new-instance v0, Lse/volvocars/acu/weather/WeatherCondition;

    const-string v1, "Overcast"

    const/16 v2, 0xf

    const v3, 0x7f090029

    const v4, 0x7f020056

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/WeatherCondition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->Overcast:Lse/volvocars/acu/weather/WeatherCondition;

    .line 12
    const/16 v0, 0x10

    new-array v0, v0, [Lse/volvocars/acu/weather/WeatherCondition;

    sget-object v1, Lse/volvocars/acu/weather/WeatherCondition;->PartlyCloudy:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v1, v0, v6

    sget-object v1, Lse/volvocars/acu/weather/WeatherCondition;->Cloudy:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v1, v0, v7

    sget-object v1, Lse/volvocars/acu/weather/WeatherCondition;->Fog:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v1, v0, v8

    const/4 v1, 0x3

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->HeavyRain:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->LightRain:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->Rain:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->Sleet:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->LightSleet:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->Thunder:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->LightRainAndThunder:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->Snow:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->LightSnow:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->SnowShowers:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->Sun:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->Sunny:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lse/volvocars/acu/weather/WeatherCondition;->Overcast:Lse/volvocars/acu/weather/WeatherCondition;

    aput-object v2, v0, v1

    sput-object v0, Lse/volvocars/acu/weather/WeatherCondition;->$VALUES:[Lse/volvocars/acu/weather/WeatherCondition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "description"    # I
    .param p4, "icon"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput p3, p0, Lse/volvocars/acu/weather/WeatherCondition;->mDescription:I

    .line 53
    iput p4, p0, Lse/volvocars/acu/weather/WeatherCondition;->mIcon:I

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lse/volvocars/acu/weather/WeatherCondition;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lse/volvocars/acu/weather/WeatherCondition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    .end local p0    # "name":Ljava/lang/String;
    check-cast p0, Lse/volvocars/acu/weather/WeatherCondition;

    return-object p0
.end method

.method public static values()[Lse/volvocars/acu/weather/WeatherCondition;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lse/volvocars/acu/weather/WeatherCondition;->$VALUES:[Lse/volvocars/acu/weather/WeatherCondition;

    invoke-virtual {v0}, [Lse/volvocars/acu/weather/WeatherCondition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lse/volvocars/acu/weather/WeatherCondition;

    return-object v0
.end method


# virtual methods
.method public getDescriptionId()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lse/volvocars/acu/weather/WeatherCondition;->mDescription:I

    return v0
.end method

.method public getDrawableId()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lse/volvocars/acu/weather/WeatherCondition;->mIcon:I

    return v0
.end method
