.class public final Lse/volvocars/acu/weather/Temperature;
.super Ljava/lang/Object;
.source "Temperature.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/weather/Temperature$1;,
        Lse/volvocars/acu/weather/Temperature$Unit;
    }
.end annotation


# static fields
.field private static final CONVERSION_SCALE:D = 1.8

.field public static final DEGREE:C = '\u00b0'

.field private static final OFFSET:D = 32.0


# instance fields
.field private final mTemperature:D


# direct methods
.method private constructor <init>(D)V
    .locals 0
    .param p1, "celcius"    # D

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-wide p1, p0, Lse/volvocars/acu/weather/Temperature;->mTemperature:D

    .line 43
    return-void
.end method

.method private static celciusToFahrenheit(D)D
    .locals 4
    .param p0, "temperature"    # D

    .prologue
    .line 65
    const-wide v0, 0x3ffccccccccccccdL    # 1.8

    mul-double/2addr v0, p0

    const-wide/high16 v2, 0x4040000000000000L    # 32.0

    add-double/2addr v0, v2

    return-wide v0
.end method

.method private static fahrenheitToCelcius(D)D
    .locals 4
    .param p0, "temperature"    # D

    .prologue
    .line 61
    const-wide/high16 v0, 0x4040000000000000L    # 32.0

    sub-double v0, p0, v0

    const-wide v2, 0x3ffccccccccccccdL    # 1.8

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static from(Lse/volvocars/acu/weather/Temperature$Unit;D)Lse/volvocars/acu/weather/Temperature;
    .locals 5
    .param p0, "unit"    # Lse/volvocars/acu/weather/Temperature$Unit;
    .param p1, "temperature"    # D

    .prologue
    .line 49
    sget-object v2, Lse/volvocars/acu/weather/Temperature$1;->$SwitchMap$se$volvocars$acu$weather$Temperature$Unit:[I

    invoke-virtual {p0}, Lse/volvocars/acu/weather/Temperature$Unit;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 56
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No conversion for <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 51
    :pswitch_0
    new-instance v2, Lse/volvocars/acu/weather/Temperature;

    invoke-direct {v2, p1, p2}, Lse/volvocars/acu/weather/Temperature;-><init>(D)V

    .line 54
    :goto_0
    return-object v2

    .line 53
    :pswitch_1
    invoke-static {p1, p2}, Lse/volvocars/acu/weather/Temperature;->fahrenheitToCelcius(D)D

    move-result-wide v0

    .line 54
    .local v0, "celcius":D
    new-instance v2, Lse/volvocars/acu/weather/Temperature;

    invoke-direct {v2, v0, v1}, Lse/volvocars/acu/weather/Temperature;-><init>(D)V

    goto :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public get(Lse/volvocars/acu/weather/Temperature$Unit;)D
    .locals 3
    .param p1, "unit"    # Lse/volvocars/acu/weather/Temperature$Unit;

    .prologue
    .line 72
    sget-object v0, Lse/volvocars/acu/weather/Temperature$1;->$SwitchMap$se$volvocars$acu$weather$Temperature$Unit:[I

    invoke-virtual {p1}, Lse/volvocars/acu/weather/Temperature$Unit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No conversion for <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :pswitch_0
    iget-wide v0, p0, Lse/volvocars/acu/weather/Temperature;->mTemperature:D

    .line 76
    :goto_0
    return-wide v0

    :pswitch_1
    iget-wide v0, p0, Lse/volvocars/acu/weather/Temperature;->mTemperature:D

    invoke-static {v0, v1}, Lse/volvocars/acu/weather/Temperature;->celciusToFahrenheit(D)D

    move-result-wide v0

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lse/volvocars/acu/weather/Temperature$Unit;->Celcius:Lse/volvocars/acu/weather/Temperature$Unit;

    invoke-virtual {p0, v0}, Lse/volvocars/acu/weather/Temperature;->toString(Lse/volvocars/acu/weather/Temperature$Unit;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Lse/volvocars/acu/weather/Temperature$Unit;)Ljava/lang/String;
    .locals 4
    .param p1, "unit"    # Lse/volvocars/acu/weather/Temperature$Unit;

    .prologue
    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0, p1}, Lse/volvocars/acu/weather/Temperature;->get(Lse/volvocars/acu/weather/Temperature$Unit;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    .line 93
    .local v1, "temperature":I
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 94
    const/16 v2, 0xb0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 95
    invoke-virtual {p1}, Lse/volvocars/acu/weather/Temperature$Unit;->getUnitLetter()C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 96
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
