.class final Lse/volvocars/acu/weather/DummyProvider;
.super Ljava/lang/Object;
.source "DummyProvider.java"

# interfaces
.implements Lse/volvocars/acu/weather/WeatherProvider;


# static fields
.field public static final DEFAULT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

.field public static final NAME:Ljava/lang/String; = "Dummy"


# instance fields
.field private mLocation:Lse/volvocars/acu/weather/settings/Location;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 20
    new-instance v0, Lse/volvocars/acu/weather/settings/Location;

    const-string v1, "Amagerbro"

    const-string v2, "K\ufffd\ufffdbenhavn"

    const-string v3, "Denmark"

    new-instance v4, Lse/volvocars/acu/weather/Coordinate;

    const-wide v5, 0x404bd5551d68c693L    # 55.66666

    const-wide v7, 0x40292aaa3ad18d26L    # 12.58333

    invoke-direct {v4, v5, v6, v7, v8}, Lse/volvocars/acu/weather/Coordinate;-><init>(DD)V

    invoke-direct {v0, v1, v2, v3, v4}, Lse/volvocars/acu/weather/settings/Location;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lse/volvocars/acu/weather/Coordinate;)V

    sput-object v0, Lse/volvocars/acu/weather/DummyProvider;->DEFAULT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lse/volvocars/acu/weather/DummyProvider;->DEFAULT_LOCATION:Lse/volvocars/acu/weather/settings/Location;

    iput-object v0, p0, Lse/volvocars/acu/weather/DummyProvider;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    return-void
.end method


# virtual methods
.method public getLocation()Lse/volvocars/acu/weather/settings/Location;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lse/volvocars/acu/weather/DummyProvider;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "Dummy"

    return-object v0
.end method

.method public runWeatherSearch(Lse/volvocars/acu/weather/settings/Location;Ljava/util/Date;Ljava/util/Date;Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;)V
    .locals 0
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;
    .param p2, "from"    # Ljava/util/Date;
    .param p3, "to"    # Ljava/util/Date;
    .param p4, "callback"    # Lse/volvocars/acu/weather/WeatherProvider$ForecastReceiver;

    .prologue
    .line 85
    return-void
.end method

.method public searchLocation(Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 1
    .param p1, "location"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public searchLocation(Ljava/lang/CharSequence;Landroid/location/Location;)Ljava/util/List;
    .locals 1
    .param p1, "location"    # Ljava/lang/CharSequence;
    .param p2, "currentLocation"    # Landroid/location/Location;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Landroid/location/Location;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public searchLocation(Lse/volvocars/acu/weather/Coordinate;)Ljava/util/List;
    .locals 1
    .param p1, "coordinate"    # Lse/volvocars/acu/weather/Coordinate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lse/volvocars/acu/weather/Coordinate;",
            ")",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/weather/settings/Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    return-void
.end method

.method public setLocation(DD)V
    .locals 0
    .param p1, "lat"    # D
    .param p3, "lon"    # D

    .prologue
    .line 58
    return-void
.end method

.method public setLocation(Lse/volvocars/acu/weather/settings/Location;)V
    .locals 1
    .param p1, "location"    # Lse/volvocars/acu/weather/settings/Location;

    .prologue
    .line 36
    if-nez p1, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lse/volvocars/acu/weather/DummyProvider;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    .line 41
    :goto_0
    return-void

    .line 39
    :cond_0
    iput-object p1, p0, Lse/volvocars/acu/weather/DummyProvider;->mLocation:Lse/volvocars/acu/weather/settings/Location;

    goto :goto_0
.end method
