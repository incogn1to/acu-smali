.class public Lse/volvocars/acu/AppOrderDialog;
.super Landroid/app/Dialog;
.source "AppOrderDialog.java"


# static fields
.field private static final LIST_ITEM_DYNAMIC:I = 0x1

.field private static final LIST_ITEM_STATIC:I


# instance fields
.field private mAdapter:Lse/volvocars/acu/AppOrderListAdapter;

.field private mAppsHandler:Lse/volvocars/acu/AppHandler;

.field private mContext:Landroid/content/Context;

.field private mListView:Landroid/widget/ListView;

.field private mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILse/volvocars/acu/AppHandler;Landroid/content/SharedPreferences;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I
    .param p3, "appHandler"    # Lse/volvocars/acu/AppHandler;
    .param p4, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 40
    iput-object p1, p0, Lse/volvocars/acu/AppOrderDialog;->mContext:Landroid/content/Context;

    .line 41
    iput-object p3, p0, Lse/volvocars/acu/AppOrderDialog;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    .line 42
    iput-object p4, p0, Lse/volvocars/acu/AppOrderDialog;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lse/volvocars/acu/AppOrderDialog;)Lse/volvocars/acu/AppOrderListAdapter;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/AppOrderDialog;

    .prologue
    .line 20
    iget-object v0, p0, Lse/volvocars/acu/AppOrderDialog;->mAdapter:Lse/volvocars/acu/AppOrderListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lse/volvocars/acu/AppOrderDialog;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lse/volvocars/acu/AppOrderDialog;

    .prologue
    .line 20
    iget-object v0, p0, Lse/volvocars/acu/AppOrderDialog;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$200(Lse/volvocars/acu/AppOrderDialog;Z)V
    .locals 0
    .param p0, "x0"    # Lse/volvocars/acu/AppOrderDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lse/volvocars/acu/AppOrderDialog;->setReorderApps(Z)V

    return-void
.end method

.method private getReorderApps()Z
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lse/volvocars/acu/AppOrderDialog;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "reorder_apps"

    iget-object v2, p0, Lse/volvocars/acu/AppOrderDialog;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v2}, Lse/volvocars/acu/AppHandler;->isReorderingApps()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private setReorderApps(Z)V
    .locals 2
    .param p1, "reorderApps"    # Z

    .prologue
    .line 116
    iget-object v1, p0, Lse/volvocars/acu/AppOrderDialog;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 117
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "reorder_apps"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 118
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 119
    iget-object v1, p0, Lse/volvocars/acu/AppOrderDialog;->mAppsHandler:Lse/volvocars/acu/AppHandler;

    invoke-virtual {v1}, Lse/volvocars/acu/AppHandler;->loadSettings()V

    .line 120
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const v5, 0x7f030003

    invoke-virtual {p0, v5}, Lse/volvocars/acu/AppOrderDialog;->setContentView(I)V

    .line 52
    new-instance v5, Lse/volvocars/acu/AppOrderListAdapter;

    iget-object v6, p0, Lse/volvocars/acu/AppOrderDialog;->mContext:Landroid/content/Context;

    const v7, 0x7f030002

    invoke-direct {v5, v6, v7}, Lse/volvocars/acu/AppOrderListAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lse/volvocars/acu/AppOrderDialog;->mAdapter:Lse/volvocars/acu/AppOrderListAdapter;

    .line 55
    invoke-direct {p0}, Lse/volvocars/acu/AppOrderDialog;->getReorderApps()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    move v0, v5

    .line 58
    .local v0, "activeItem":I
    :goto_0
    const v5, 0x7f07000d

    invoke-virtual {p0, v5}, Lse/volvocars/acu/AppOrderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lse/volvocars/acu/AppOrderDialog;->mListView:Landroid/widget/ListView;

    .line 59
    iget-object v5, p0, Lse/volvocars/acu/AppOrderDialog;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lse/volvocars/acu/AppOrderDialog;->mAdapter:Lse/volvocars/acu/AppOrderListAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 60
    iget-object v5, p0, Lse/volvocars/acu/AppOrderDialog;->mListView:Landroid/widget/ListView;

    const v6, 0x7f020002

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setSelector(I)V

    .line 63
    const v5, 0x7f07000b

    invoke-virtual {p0, v5}, Lse/volvocars/acu/AppOrderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 64
    .local v4, "tv":Landroid/widget/TextView;
    iget-object v5, p0, Lse/volvocars/acu/AppOrderDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    const-string v6, "fonts/VolvoSanProLig.otf"

    invoke-static {v5, v6}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 65
    .local v2, "mTypeface":Landroid/graphics/Typeface;
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 68
    const v5, 0x7f07000e

    invoke-virtual {p0, v5}, Lse/volvocars/acu/AppOrderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 69
    .local v3, "okButton":Landroid/widget/Button;
    invoke-virtual {v3, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 70
    const v5, 0x7f070010

    invoke-virtual {p0, v5}, Lse/volvocars/acu/AppOrderDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 71
    .local v1, "cancelButton":Landroid/widget/Button;
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 75
    iget-object v5, p0, Lse/volvocars/acu/AppOrderDialog;->mAdapter:Lse/volvocars/acu/AppOrderListAdapter;

    invoke-virtual {v5, v0}, Lse/volvocars/acu/AppOrderListAdapter;->setActiveItem(I)V

    .line 76
    iget-object v5, p0, Lse/volvocars/acu/AppOrderDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 81
    iget-object v5, p0, Lse/volvocars/acu/AppOrderDialog;->mListView:Landroid/widget/ListView;

    new-instance v6, Lse/volvocars/acu/AppOrderDialog$1;

    invoke-direct {v6, p0}, Lse/volvocars/acu/AppOrderDialog$1;-><init>(Lse/volvocars/acu/AppOrderDialog;)V

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 91
    new-instance v5, Lse/volvocars/acu/AppOrderDialog$2;

    invoke-direct {v5, p0, v0}, Lse/volvocars/acu/AppOrderDialog$2;-><init>(Lse/volvocars/acu/AppOrderDialog;I)V

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    new-instance v5, Lse/volvocars/acu/AppOrderDialog$3;

    invoke-direct {v5, p0}, Lse/volvocars/acu/AppOrderDialog$3;-><init>(Lse/volvocars/acu/AppOrderDialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    return-void

    .line 55
    .end local v0    # "activeItem":I
    .end local v1    # "cancelButton":Landroid/widget/Button;
    .end local v2    # "mTypeface":Landroid/graphics/Typeface;
    .end local v3    # "okButton":Landroid/widget/Button;
    .end local v4    # "tv":Landroid/widget/TextView;
    :cond_0
    const/4 v5, 0x0

    move v0, v5

    goto :goto_0
.end method
