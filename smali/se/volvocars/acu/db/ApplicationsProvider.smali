.class public final Lse/volvocars/acu/db/ApplicationsProvider;
.super Landroid/content/ContentProvider;
.source "ApplicationsProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/db/ApplicationsProvider$CursorWithDatabase;
    }
.end annotation


# static fields
.field private static final ALL_APPS:I = 0x1

.field private static final AUTHORITY_URI:Ljava/lang/String; = "com.volvocars.applications.provider"

.field private static final SINGLE_APP:I = 0x2

.field private static final TABLE_NAME:Ljava/lang/String; = "applications"

.field private static final URI_MATCHER:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-string v3, "com.volvocars.applications.provider"

    .line 26
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lse/volvocars/acu/db/ApplicationsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 27
    sget-object v0, Lse/volvocars/acu/db/ApplicationsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.volvocars.applications.provider"

    const-string v1, "applications"

    const/4 v2, 0x1

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 28
    sget-object v0, Lse/volvocars/acu/db/ApplicationsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.volvocars.applications.provider"

    const-string v1, "applications/#"

    const/4 v2, 0x2

    invoke-virtual {v0, v3, v1, v2}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 29
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 93
    return-void
.end method

.method private updateHiddenColumn(II)I
    .locals 3
    .param p1, "id"    # I
    .param p2, "hidden"    # I

    .prologue
    .line 81
    new-instance v0, Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {p0}, Lse/volvocars/acu/db/ApplicationsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lse/volvocars/acu/db/ApplicationsDatabase;-><init>(Landroid/content/Context;)V

    .line 83
    .local v0, "db":Lse/volvocars/acu/db/ApplicationsDatabase;
    int-to-long v1, p1

    :try_start_0
    invoke-virtual {v0, v1, v2, p2}, Lse/volvocars/acu/db/ApplicationsDatabase;->updateHiddenColumn(JI)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 85
    invoke-virtual {v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->close()V

    return v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->close()V

    throw v1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "arg0"    # Landroid/net/Uri;
    .param p2, "arg1"    # Ljava/lang/String;
    .param p3, "arg2"    # [Ljava/lang/String;

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 53
    sget-object v2, Lse/volvocars/acu/db/ApplicationsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 59
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "URI is not expected : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 55
    :pswitch_0
    new-instance v1, Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {p0}, Lse/volvocars/acu/db/ApplicationsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lse/volvocars/acu/db/ApplicationsDatabase;-><init>(Landroid/content/Context;)V

    .line 56
    .local v1, "db":Lse/volvocars/acu/db/ApplicationsDatabase;
    invoke-virtual {v1}, Lse/volvocars/acu/db/ApplicationsDatabase;->fetchAllNonMediaDeviceNonSettingsApplications()Landroid/database/Cursor;

    move-result-object v0

    .line 57
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v2, Lse/volvocars/acu/db/ApplicationsProvider$CursorWithDatabase;

    invoke-direct {v2, v1, v0}, Lse/volvocars/acu/db/ApplicationsProvider$CursorWithDatabase;-><init>(Lse/volvocars/acu/db/ApplicationsDatabase;Landroid/database/Cursor;)V

    return-object v2

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 66
    if-nez p2, :cond_0

    .line 67
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Input values cannot be null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 69
    :cond_0
    sget-object v2, Lse/volvocars/acu/db/ApplicationsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 75
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "URI is not expected : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 71
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 72
    .local v1, "id":I
    const-string v2, "hidden"

    invoke-virtual {p2, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 73
    .local v0, "hidden":I
    invoke-direct {p0, v1, v0}, Lse/volvocars/acu/db/ApplicationsProvider;->updateHiddenColumn(II)I

    move-result v2

    return v2

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
