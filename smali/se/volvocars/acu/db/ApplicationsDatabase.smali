.class public final Lse/volvocars/acu/db/ApplicationsDatabase;
.super Ljava/lang/Object;
.source "ApplicationsDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;,
        Lse/volvocars/acu/db/ApplicationsDatabase$Notifier;
    }
.end annotation


# static fields
.field public static final APPLICATION_HIDDEN:Ljava/lang/String; = "hidden"

.field public static final APPLICATION_ID:Ljava/lang/String; = "_id"

.field public static final APPLICATION_LAST_USED:Ljava/lang/String; = "lastUsed"

.field public static final APPLICATION_NAME:Ljava/lang/String; = "name"

.field public static final APPLICATION_PACKAGE:Ljava/lang/String; = "package"

.field private static final CONNECTIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lse/volvocars/acu/db/ApplicationsDatabase;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final CONNECTIONS_LOCK:Ljava/lang/Object;

.field private static final EXCLUSIVE_LOCK:Ljava/lang/Object;

.field private static final EXECUTOR:Ljava/util/concurrent/Executor;

.field private static final TAG:Ljava/lang/String;

.field private static mLastUsedCounter:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDatabaseLock:Ljava/lang/Object;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mDbHelper:Lse/volvocars/acu/db/ApplicationsOpenHelper;

.field private mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private mListenersLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    const-class v0, Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->TAG:Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->EXCLUSIVE_LOCK:Ljava/lang/Object;

    .line 61
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->CONNECTIONS_LOCK:Ljava/lang/Object;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->CONNECTIONS:Ljava/util/List;

    .line 63
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->EXECUTOR:Ljava/util/concurrent/Executor;

    .line 65
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->mLastUsedCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDatabaseLock:Ljava/lang/Object;

    .line 46
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mListenersLock:Ljava/lang/Object;

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mListeners:Ljava/util/List;

    .line 72
    iput-object p1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mContext:Landroid/content/Context;

    .line 73
    sget-object v1, Lse/volvocars/acu/db/ApplicationsDatabase;->CONNECTIONS_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 74
    :try_start_0
    sget-object v2, Lse/volvocars/acu/db/ApplicationsDatabase;->CONNECTIONS:Ljava/util/List;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    invoke-virtual {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->reopen()V

    .line 77
    sget-object v1, Lse/volvocars/acu/db/ApplicationsDatabase;->EXCLUSIVE_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 78
    :try_start_1
    sget-object v2, Lse/volvocars/acu/db/ApplicationsDatabase;->mLastUsedCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 79
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->retrieveNextLastUsedValueFromDb()I

    move-result v0

    .line 80
    .local v0, "next":I
    sget-object v2, Lse/volvocars/acu/db/ApplicationsDatabase;->mLastUsedCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 82
    .end local v0    # "next":I
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 83
    return-void

    .line 75
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 82
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private assertConnectionStatus()V
    .locals 2

    .prologue
    .line 228
    invoke-virtual {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Connection is closed, use open() to reopen a connection. Note that listeners still receives change updates even when the connection is closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_0
    return-void
.end method

.method private assertNotInvalidId(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 222
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal database id <"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_0
    return-void
.end method

.method private delete(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1, "whereClause"    # Ljava/lang/String;
    .param p2, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 425
    sget-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->EXCLUSIVE_LOCK:Ljava/lang/Object;

    monitor-enter v0

    .line 426
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDatabaseLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 427
    :try_start_1
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->assertConnectionStatus()V

    .line 428
    iget-object v2, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "applications"

    invoke-virtual {v2, v3, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return v2

    .line 429
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2

    .line 430
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method private fetchAllApplications()Landroid/database/Cursor;
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 261
    iget-object v10, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDatabaseLock:Ljava/lang/Object;

    monitor-enter v10

    .line 262
    :try_start_0
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->assertConnectionStatus()V

    .line 263
    new-instance v9, Ljava/lang/StringBuffer;

    const-string v0, ""

    invoke-direct {v9, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    .local v9, "query":Ljava/lang/StringBuffer;
    :try_start_1
    iget-object v0, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "applications"

    const/4 v2, 0x0

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "lastUsed DESC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v10

    .line 270
    :goto_0
    return-object v0

    .line 267
    :catch_0
    move-exception v0

    move-object v8, v0

    .line 268
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    sget-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->TAG:Ljava/lang/String;

    const-string v1, "Exception while performing query"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    monitor-exit v10

    move-object v0, v11

    goto :goto_0

    .line 271
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v9    # "query":Ljava/lang/StringBuffer;
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method private insert(Landroid/content/ContentValues;)J
    .locals 5
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 434
    sget-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->EXCLUSIVE_LOCK:Ljava/lang/Object;

    monitor-enter v0

    .line 435
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDatabaseLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 436
    :try_start_1
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->assertConnectionStatus()V

    .line 437
    iget-object v2, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "applications"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-wide v2

    .line 438
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2

    .line 439
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method

.method private nextLastUsed()I
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->mLastUsedCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method

.method private notifyListeners()V
    .locals 7

    .prologue
    .line 120
    sget-object v3, Lse/volvocars/acu/db/ApplicationsDatabase;->CONNECTIONS_LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 121
    :try_start_0
    sget-object v4, Lse/volvocars/acu/db/ApplicationsDatabase;->CONNECTIONS:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 122
    .end local p0    # "this":Lse/volvocars/acu/db/ApplicationsDatabase;
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lse/volvocars/acu/db/ApplicationsDatabase;>;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 123
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse/volvocars/acu/db/ApplicationsDatabase;

    .line 124
    .local v0, "connection":Lse/volvocars/acu/db/ApplicationsDatabase;
    if-nez v0, :cond_0

    .line 125
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 133
    .end local v0    # "connection":Lse/volvocars/acu/db/ApplicationsDatabase;
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lse/volvocars/acu/db/ApplicationsDatabase;>;>;"
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 128
    .restart local v0    # "connection":Lse/volvocars/acu/db/ApplicationsDatabase;
    .restart local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lse/volvocars/acu/db/ApplicationsDatabase;>;>;"
    :cond_0
    :try_start_1
    iget-object v4, v0, Lse/volvocars/acu/db/ApplicationsDatabase;->mListenersLock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    iget-object v5, v0, Lse/volvocars/acu/db/ApplicationsDatabase;->mListeners:Ljava/util/List;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 130
    .local v1, "copy":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;>;"
    sget-object v5, Lse/volvocars/acu/db/ApplicationsDatabase;->EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v6, Lse/volvocars/acu/db/ApplicationsDatabase$Notifier;

    invoke-direct {v6, v1}, Lse/volvocars/acu/db/ApplicationsDatabase$Notifier;-><init>(Ljava/util/List;)V

    invoke-interface {v5, v6}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 131
    monitor-exit v4

    goto :goto_0

    .end local v1    # "copy":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;>;"
    :catchall_1
    move-exception v5

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    throw v5

    .line 133
    .end local v0    # "connection":Lse/volvocars/acu/db/ApplicationsDatabase;
    :cond_1
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 134
    return-void
.end method

.method private rawQuery(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3
    .param p1, "sql"    # Ljava/lang/String;

    .prologue
    .line 452
    iget-object v0, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDatabaseLock:Ljava/lang/Object;

    monitor-enter v0

    .line 453
    :try_start_0
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->assertConnectionStatus()V

    .line 454
    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    monitor-exit v0

    return-object v1

    .line 455
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private retrieveNextLastUsedValueFromDb()I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 87
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v3, "SELECT MAX("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "lastUsed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    const-string v3, " from "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "applications"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lse/volvocars/acu/db/ApplicationsDatabase;->rawQuery(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 92
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 99
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v3, v5

    :goto_0
    return v3

    .line 95
    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 96
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 97
    .local v2, "max":I
    add-int/lit8 v3, v2, 0x1

    .line 99
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v2    # "max":I
    :catchall_0
    move-exception v3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v3
.end method

.method private update(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "whereClause"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 443
    sget-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->EXCLUSIVE_LOCK:Ljava/lang/Object;

    monitor-enter v0

    .line 444
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDatabaseLock:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 445
    :try_start_1
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->assertConnectionStatus()V

    .line 446
    iget-object v2, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "applications"

    invoke-virtual {v2, v3, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return v2

    .line 447
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2

    .line 448
    :catchall_1
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method


# virtual methods
.method public addApplication(Lse/volvocars/acu/db/StoredAppInfo;)J
    .locals 5
    .param p1, "app"    # Lse/volvocars/acu/db/StoredAppInfo;

    .prologue
    .line 211
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 212
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v3, "name"

    invoke-virtual {p1}, Lse/volvocars/acu/db/StoredAppInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v3, "package"

    invoke-virtual {p1}, Lse/volvocars/acu/db/StoredAppInfo;->getPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v3, "hidden"

    invoke-virtual {p1}, Lse/volvocars/acu/db/StoredAppInfo;->isHidden()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 215
    const-string v3, "lastUsed"

    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->nextLastUsed()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 216
    invoke-direct {p0, v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->insert(Landroid/content/ContentValues;)J

    move-result-wide v1

    .line 217
    .local v1, "id":J
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->notifyListeners()V

    .line 218
    return-wide v1
.end method

.method public addOnDatabaseChangedListener(Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;

    .prologue
    .line 108
    iget-object v0, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mListenersLock:Ljava/lang/Object;

    monitor-enter v0

    .line 109
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    monitor-exit v0

    .line 111
    return-void

    .line 110
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public close()V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDatabaseLock:Ljava/lang/Object;

    monitor-enter v0

    .line 194
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 196
    const/4 v1, 0x0

    iput-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 198
    :cond_0
    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDbHelper:Lse/volvocars/acu/db/ApplicationsOpenHelper;

    if-eqz v1, :cond_1

    .line 199
    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDbHelper:Lse/volvocars/acu/db/ApplicationsOpenHelper;

    invoke-virtual {v1}, Lse/volvocars/acu/db/ApplicationsOpenHelper;->close()V

    .line 200
    const/4 v1, 0x0

    iput-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDbHelper:Lse/volvocars/acu/db/ApplicationsOpenHelper;

    .line 202
    :cond_1
    monitor-exit v0

    .line 203
    return-void

    .line 202
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method fetchAllNonMediaDeviceNonSettingsApplications()Landroid/database/Cursor;
    .locals 7

    .prologue
    const-string v6, "%\'"

    const-string v5, " NOT LIKE \'"

    const-string v4, " AND "

    const-string v3, "package"

    .line 278
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "SELECT * FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 279
    .local v0, "query":Ljava/lang/StringBuffer;
    const-string v1, "applications"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 281
    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "package"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " NOT LIKE \'"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "com.parrot.mediaplayer:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "%\'"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 287
    const-string v1, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "package"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " NOT LIKE \'"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "com.parrot.mediaList:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "%\'"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 293
    const-string v1, " AND "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "package"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " != \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "com.android.settings"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 299
    const-string v1, " ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 300
    const-string v1, "lastUsed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 301
    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 302
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lse/volvocars/acu/db/ApplicationsDatabase;->rawQuery(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method fetchVisibleApplications()Landroid/database/Cursor;
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 341
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "hidden"

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, "=0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    .line 344
    .local v9, "query":Ljava/lang/StringBuffer;
    iget-object v10, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDatabaseLock:Ljava/lang/Object;

    monitor-enter v10

    .line 345
    :try_start_0
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->assertConnectionStatus()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    :try_start_1
    iget-object v0, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "applications"

    const/4 v2, 0x0

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "lastUsed DESC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v10

    .line 353
    :goto_0
    return-object v0

    .line 348
    :catch_0
    move-exception v0

    move-object v8, v0

    .line 349
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    sget-object v0, Lse/volvocars/acu/db/ApplicationsDatabase;->TAG:Ljava/lang/String;

    const-string v1, "Exception while performing query"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    monitor-exit v10

    move-object v0, v11

    .line 353
    goto :goto_0

    .line 351
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public getAllApps()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lse/volvocars/acu/db/StoredAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 310
    .local v8, "apps":Ljava/util/List;, "Ljava/util/List<Lse/volvocars/acu/db/StoredAppInfo;>;"
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->fetchAllApplications()Landroid/database/Cursor;

    move-result-object v9

    .line 312
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 313
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 314
    const-string v0, "_id"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 315
    .local v1, "id":J
    const-string v0, "name"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 316
    .local v3, "name":Ljava/lang/String;
    const-string v0, "package"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 317
    .local v4, "pkg":Ljava/lang/String;
    const-string v0, "hidden"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v5, v0

    .line 318
    .local v5, "hidden":Z
    :goto_1
    const-string v0, "lastUsed"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 319
    .local v6, "lastUsed":J
    new-instance v0, Lse/volvocars/acu/db/StoredAppInfo;

    invoke-direct/range {v0 .. v7}, Lse/volvocars/acu/db/StoredAppInfo;-><init>(JLjava/lang/String;Ljava/lang/String;ZJ)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 320
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 323
    .end local v1    # "id":J
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "pkg":Ljava/lang/String;
    .end local v5    # "hidden":Z
    .end local v6    # "lastUsed":J
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    .line 317
    .restart local v1    # "id":J
    .restart local v3    # "name":Ljava/lang/String;
    .restart local v4    # "pkg":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    move v5, v0

    goto :goto_1

    .line 323
    .end local v1    # "id":J
    .end local v3    # "name":Ljava/lang/String;
    .end local v4    # "pkg":Ljava/lang/String;
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 325
    return-object v8
.end method

.method public isClosed()Z
    .locals 2

    .prologue
    .line 237
    iget-object v0, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDatabaseLock:Ljava/lang/Object;

    monitor-enter v0

    .line 238
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDbHelper:Lse/volvocars/acu/db/ApplicationsOpenHelper;

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    monitor-exit v0

    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 239
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method removeAllApplications()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 420
    invoke-direct {p0, v0, v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->delete(Ljava/lang/String;[Ljava/lang/String;)I

    .line 421
    return-void
.end method

.method public removeApplication(J)I
    .locals 4
    .param p1, "id"    # J

    .prologue
    .line 248
    invoke-direct {p0, p1, p2}, Lse/volvocars/acu/db/ApplicationsDatabase;->assertNotInvalidId(J)V

    .line 249
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 250
    .local v0, "args":[Ljava/lang/String;
    const-string v2, "_id=?"

    invoke-direct {p0, v2, v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->delete(Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 251
    .local v1, "rows":I
    if-lez v1, :cond_0

    .line 252
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->notifyListeners()V

    .line 254
    :cond_0
    return v1
.end method

.method public removeOnDatabaseChangeListener(Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lse/volvocars/acu/db/ApplicationsDatabase$OnDatabaseChangedListener;

    .prologue
    .line 114
    iget-object v0, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mListenersLock:Ljava/lang/Object;

    monitor-enter v0

    .line 115
    :try_start_0
    iget-object v1, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 116
    monitor-exit v0

    .line 117
    return-void

    .line 116
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public reopen()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 165
    iget-object v3, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDatabaseLock:Ljava/lang/Object;

    monitor-enter v3

    .line 166
    :try_start_0
    iget-object v4, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v4, :cond_0

    iget-object v4, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDbHelper:Lse/volvocars/acu/db/ApplicationsOpenHelper;

    if-eqz v4, :cond_1

    .line 167
    :cond_0
    invoke-virtual {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :cond_1
    :try_start_1
    new-instance v4, Lse/volvocars/acu/db/ApplicationsOpenHelper;

    iget-object v5, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lse/volvocars/acu/db/ApplicationsOpenHelper;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDbHelper:Lse/volvocars/acu/db/ApplicationsOpenHelper;

    .line 171
    iget-object v4, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDbHelper:Lse/volvocars/acu/db/ApplicationsOpenHelper;

    invoke-virtual {v4}, Lse/volvocars/acu/db/ApplicationsOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    iput-object v4, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 185
    :goto_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 186
    return-void

    .line 172
    :catch_0
    move-exception v4

    move-object v1, v4

    .line 176
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v6, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/databases/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Applications.db"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    .local v0, "dbPath":Ljava/lang/String;
    invoke-static {v0}, Lse/volvocars/acu/db/DatabaseErrorHandler;->deleteDatabaseFile(Ljava/lang/String;)V

    .line 180
    iget-object v4, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDbHelper:Lse/volvocars/acu/db/ApplicationsOpenHelper;

    invoke-virtual {v4}, Lse/volvocars/acu/db/ApplicationsOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    iput-object v4, p0, Lse/volvocars/acu/db/ApplicationsDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 181
    .end local v0    # "dbPath":Ljava/lang/String;
    :catch_1
    move-exception v4

    move-object v2, v4

    .line 182
    .local v2, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_4
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 185
    .end local v1    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v2    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4
.end method

.method public updateAppNameColumn(Ljava/lang/String;Ljava/lang/String;)I
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;

    .prologue
    .line 409
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 410
    .local v1, "cv":Landroid/content/ContentValues;
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v0, v3

    .line 411
    .local v0, "args":[Ljava/lang/String;
    const-string v3, "name"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    const-string v3, "package=?"

    invoke-direct {p0, v1, v3, v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->update(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 413
    .local v2, "rows":I
    if-lez v2, :cond_0

    .line 414
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->notifyListeners()V

    .line 416
    :cond_0
    return v2
.end method

.method public updateHiddenColumn(JI)I
    .locals 5
    .param p1, "id"    # J
    .param p3, "hidden"    # I

    .prologue
    .line 390
    invoke-direct {p0, p1, p2}, Lse/volvocars/acu/db/ApplicationsDatabase;->assertNotInvalidId(J)V

    .line 391
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 392
    .local v1, "cv":Landroid/content/ContentValues;
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v3

    .line 393
    .local v0, "args":[Ljava/lang/String;
    const-string v3, "hidden"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 394
    const-string v3, "_id=?"

    invoke-direct {p0, v1, v3, v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->update(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 395
    .local v2, "rows":I
    if-lez v2, :cond_0

    .line 396
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->notifyListeners()V

    .line 398
    :cond_0
    return v2
.end method

.method public updateLastUsedColumn(J)I
    .locals 8
    .param p1, "id"    # J

    .prologue
    .line 363
    invoke-virtual {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->isClosed()Z

    move-result v1

    .line 364
    .local v1, "closedAtStart":Z
    invoke-virtual {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->isClosed()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 365
    invoke-virtual {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->reopen()V

    .line 367
    :cond_0
    invoke-direct {p0, p1, p2}, Lse/volvocars/acu/db/ApplicationsDatabase;->assertNotInvalidId(J)V

    .line 368
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 369
    .local v2, "cv":Landroid/content/ContentValues;
    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v5

    .line 370
    .local v0, "args":[Ljava/lang/String;
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->nextLastUsed()I

    move-result v3

    .line 371
    .local v3, "lastUsed":I
    const-string v5, "lastUsed"

    int-to-long v6, v3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 372
    const-string v5, "_id=?"

    invoke-direct {p0, v2, v5, v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->update(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 373
    .local v4, "rows":I
    if-lez v4, :cond_1

    .line 374
    invoke-direct {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->notifyListeners()V

    .line 376
    :cond_1
    if-eqz v1, :cond_2

    .line 377
    invoke-virtual {p0}, Lse/volvocars/acu/db/ApplicationsDatabase;->close()V

    .line 379
    :cond_2
    return v4
.end method
