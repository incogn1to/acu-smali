.class final Lse/volvocars/acu/db/ApplicationsProvider$CursorWithDatabase;
.super Landroid/database/CursorWrapper;
.source "ApplicationsProvider.java"

# interfaces
.implements Landroid/database/CrossProcessCursor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lse/volvocars/acu/db/ApplicationsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CursorWithDatabase"
.end annotation


# instance fields
.field private final mCursor:Landroid/database/CrossProcessCursor;

.field private final mDb:Lse/volvocars/acu/db/ApplicationsDatabase;


# direct methods
.method public constructor <init>(Lse/volvocars/acu/db/ApplicationsDatabase;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "db"    # Lse/volvocars/acu/db/ApplicationsDatabase;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 99
    invoke-direct {p0, p2}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 100
    instance-of v0, p2, Landroid/database/CrossProcessCursor;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Only CrossProcessCursor cursors are supported across process for now"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    check-cast p2, Landroid/database/CrossProcessCursor;

    .end local p2    # "cursor":Landroid/database/Cursor;
    iput-object p2, p0, Lse/volvocars/acu/db/ApplicationsProvider$CursorWithDatabase;->mCursor:Landroid/database/CrossProcessCursor;

    .line 104
    if-nez p1, :cond_1

    .line 105
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input database cannot be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_1
    iput-object p1, p0, Lse/volvocars/acu/db/ApplicationsProvider$CursorWithDatabase;->mDb:Lse/volvocars/acu/db/ApplicationsDatabase;

    .line 108
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 112
    invoke-super {p0}, Landroid/database/CursorWrapper;->close()V

    .line 113
    iget-object v0, p0, Lse/volvocars/acu/db/ApplicationsProvider$CursorWithDatabase;->mDb:Lse/volvocars/acu/db/ApplicationsDatabase;

    invoke-virtual {v0}, Lse/volvocars/acu/db/ApplicationsDatabase;->close()V

    .line 114
    return-void
.end method

.method public fillWindow(ILandroid/database/CursorWindow;)V
    .locals 1
    .param p1, "pos"    # I
    .param p2, "window"    # Landroid/database/CursorWindow;

    .prologue
    .line 118
    iget-object v0, p0, Lse/volvocars/acu/db/ApplicationsProvider$CursorWithDatabase;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v0, p1, p2}, Landroid/database/CrossProcessCursor;->fillWindow(ILandroid/database/CursorWindow;)V

    .line 119
    return-void
.end method

.method public getWindow()Landroid/database/CursorWindow;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lse/volvocars/acu/db/ApplicationsProvider$CursorWithDatabase;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v0}, Landroid/database/CrossProcessCursor;->getWindow()Landroid/database/CursorWindow;

    move-result-object v0

    return-object v0
.end method

.method public onMove(II)Z
    .locals 1
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I

    .prologue
    .line 128
    iget-object v0, p0, Lse/volvocars/acu/db/ApplicationsProvider$CursorWithDatabase;->mCursor:Landroid/database/CrossProcessCursor;

    invoke-interface {v0, p1, p2}, Landroid/database/CrossProcessCursor;->onMove(II)Z

    move-result v0

    return v0
.end method
