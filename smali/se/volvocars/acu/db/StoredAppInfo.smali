.class public final Lse/volvocars/acu/db/StoredAppInfo;
.super Ljava/lang/Object;
.source "StoredAppInfo.java"


# static fields
.field public static final INVALID_ID:I = -0x1


# instance fields
.field private final mHidden:Z

.field private final mId:J

.field private final mLastUsed:J

.field private final mName:Ljava/lang/String;

.field private final mPackage:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;ZJ)V
    .locals 0
    .param p1, "id"    # J
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "pkg"    # Ljava/lang/String;
    .param p5, "hidden"    # Z
    .param p6, "lastUsed"    # J

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-wide p1, p0, Lse/volvocars/acu/db/StoredAppInfo;->mId:J

    .line 33
    iput-object p3, p0, Lse/volvocars/acu/db/StoredAppInfo;->mName:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lse/volvocars/acu/db/StoredAppInfo;->mPackage:Ljava/lang/String;

    .line 35
    iput-boolean p5, p0, Lse/volvocars/acu/db/StoredAppInfo;->mHidden:Z

    .line 36
    iput-wide p6, p0, Lse/volvocars/acu/db/StoredAppInfo;->mLastUsed:J

    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZJ)V
    .locals 8
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "pkg"    # Ljava/lang/String;
    .param p3, "hidden"    # Z
    .param p4, "lastUsed"    # J

    .prologue
    .line 28
    const-wide/16 v1, -0x1

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    invoke-direct/range {v0 .. v7}, Lse/volvocars/acu/db/StoredAppInfo;-><init>(JLjava/lang/String;Ljava/lang/String;ZJ)V

    .line 29
    return-void
.end method


# virtual methods
.method public getId()J
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lse/volvocars/acu/db/StoredAppInfo;->mId:J

    return-wide v0
.end method

.method public getLastUsed()J
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lse/volvocars/acu/db/StoredAppInfo;->mLastUsed:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lse/volvocars/acu/db/StoredAppInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lse/volvocars/acu/db/StoredAppInfo;->mPackage:Ljava/lang/String;

    return-object v0
.end method

.method public isHidden()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lse/volvocars/acu/db/StoredAppInfo;->mHidden:Z

    return v0
.end method
