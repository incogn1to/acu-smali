.class Landroid/opengl/GLSurfaceView2$EglHelper;
.super Ljava/lang/Object;
.source "GLSurfaceView2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/opengl/GLSurfaceView2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EglHelper"
.end annotation


# instance fields
.field mEgl:Ljavax/microedition/khronos/egl/EGL10;

.field mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

.field mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

.field mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

.field mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

.field final synthetic this$0:Landroid/opengl/GLSurfaceView2;


# direct methods
.method public constructor <init>(Landroid/opengl/GLSurfaceView2;)V
    .locals 0

    .prologue
    .line 887
    iput-object p1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889
    return-void
.end method

.method private throwEglException(Ljava/lang/String;)V
    .locals 1
    .param p1, "function"    # Ljava/lang/String;

    .prologue
    .line 1073
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/opengl/GLSurfaceView2$EglHelper;->throwEglException(Ljava/lang/String;I)V

    .line 1074
    return-void
.end method

.method private throwEglException(Ljava/lang/String;I)V
    .locals 0
    .param p1, "function"    # Ljava/lang/String;
    .param p2, "error"    # I

    .prologue
    .line 1084
    return-void
.end method


# virtual methods
.method public createSurface(Landroid/view/SurfaceHolder;)Ljavax/microedition/khronos/opengles/GL;
    .locals 10
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const-string v9, "EglHelper"

    .line 944
    const-string v4, "EglHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "createSurface()  tid="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v9, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    if-nez v4, :cond_0

    .line 950
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "egl not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 952
    :cond_0
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-nez v4, :cond_1

    .line 953
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglDisplay not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 955
    :cond_1
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v4, :cond_2

    .line 956
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "mEglConfig not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 962
    :cond_2
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v4, :cond_3

    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v4, v5, :cond_3

    .line 968
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v5, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v6, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 969
    sget-object v7, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v8, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    .line 968
    invoke-interface {v4, v5, v6, v7, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 970
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mEGLWindowSurfaceFactory:Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;
    invoke-static {v4}, Landroid/opengl/GLSurfaceView2;->access$3(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;

    move-result-object v4

    iget-object v5, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v6, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v7, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v4, v5, v6, v7}, Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;->destroySurface(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)V

    .line 976
    :cond_3
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mEGLWindowSurfaceFactory:Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;
    invoke-static {v4}, Landroid/opengl/GLSurfaceView2;->access$3(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;

    move-result-object v4

    iget-object v5, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    .line 977
    iget-object v6, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v7, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 976
    invoke-interface {v4, v5, v6, v7, p1}, Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;->createWindowSurface(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v4

    iput-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 979
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v4, :cond_4

    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne v4, v5, :cond_6

    .line 980
    :cond_4
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v1

    .line 981
    .local v1, "error":I
    const/16 v4, 0x300b

    if-ne v1, v4, :cond_5

    .line 982
    const-string v4, "EglHelper"

    const-string v4, "createWindowSurface returned EGL_BAD_NATIVE_WINDOW."

    invoke-static {v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 983
    const/4 v4, 0x0

    .line 1012
    .end local v1    # "error":I
    :goto_0
    return-object v4

    .line 985
    .restart local v1    # "error":I
    :cond_5
    const-string v4, "createWindowSurface"

    invoke-direct {p0, v4, v1}, Landroid/opengl/GLSurfaceView2$EglHelper;->throwEglException(Ljava/lang/String;I)V

    .line 992
    .end local v1    # "error":I
    :cond_6
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v5, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v6, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v7, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v8, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v4, v5, v6, v7, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 993
    const-string v4, "eglMakeCurrent"

    invoke-direct {p0, v4}, Landroid/opengl/GLSurfaceView2$EglHelper;->throwEglException(Ljava/lang/String;)V

    .line 996
    :cond_7
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v4}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v2

    .line 997
    .local v2, "gl":Ljavax/microedition/khronos/opengles/GL;
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mGLWrapper:Landroid/opengl/GLSurfaceView2$GLWrapper;
    invoke-static {v4}, Landroid/opengl/GLSurfaceView2;->access$4(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$GLWrapper;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 998
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mGLWrapper:Landroid/opengl/GLSurfaceView2$GLWrapper;
    invoke-static {v4}, Landroid/opengl/GLSurfaceView2;->access$4(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$GLWrapper;

    move-result-object v4

    invoke-interface {v4, v2}, Landroid/opengl/GLSurfaceView2$GLWrapper;->wrap(Ljavax/microedition/khronos/opengles/GL;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v2

    .line 1001
    :cond_8
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mDebugFlags:I
    invoke-static {v4}, Landroid/opengl/GLSurfaceView2;->access$5(Landroid/opengl/GLSurfaceView2;)I

    move-result v4

    and-int/lit8 v4, v4, 0x3

    if-eqz v4, :cond_b

    .line 1002
    const/4 v0, 0x0

    .line 1003
    .local v0, "configFlags":I
    const/4 v3, 0x0

    .line 1004
    .local v3, "log":Ljava/io/Writer;
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mDebugFlags:I
    invoke-static {v4}, Landroid/opengl/GLSurfaceView2;->access$5(Landroid/opengl/GLSurfaceView2;)I

    move-result v4

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_9

    .line 1005
    or-int/lit8 v0, v0, 0x1

    .line 1007
    :cond_9
    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mDebugFlags:I
    invoke-static {v4}, Landroid/opengl/GLSurfaceView2;->access$5(Landroid/opengl/GLSurfaceView2;)I

    move-result v4

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_a

    .line 1008
    new-instance v3, Landroid/opengl/GLSurfaceView2$LogWriter;

    .end local v3    # "log":Ljava/io/Writer;
    invoke-direct {v3}, Landroid/opengl/GLSurfaceView2$LogWriter;-><init>()V

    .line 1010
    .restart local v3    # "log":Ljava/io/Writer;
    :cond_a
    invoke-static {v2, v0, v3}, Landroid/opengl/GLDebugHelper;->wrap(Ljavax/microedition/khronos/opengles/GL;ILjava/io/Writer;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v2

    .end local v0    # "configFlags":I
    .end local v3    # "log":Ljava/io/Writer;
    :cond_b
    move-object v4, v2

    .line 1012
    goto :goto_0
.end method

.method public destroySurface()V
    .locals 5

    .prologue
    .line 1047
    const-string v0, "EglHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "destroySurface()  tid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v0, v1, :cond_0

    .line 1050
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 1051
    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 1052
    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    .line 1050
    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 1053
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mEGLWindowSurfaceFactory:Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;
    invoke-static {v0}, Landroid/opengl/GLSurfaceView2;->access$3(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;

    move-result-object v0

    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v1, v2, v3}, Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;->destroySurface(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)V

    .line 1054
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 1056
    :cond_0
    return-void
.end method

.method public finish()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1060
    const-string v0, "EglHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "finish() tid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1062
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_0

    .line 1063
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mEGLContextFactory:Landroid/opengl/GLSurfaceView2$EGLContextFactory;
    invoke-static {v0}, Landroid/opengl/GLSurfaceView2;->access$2(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$EGLContextFactory;

    move-result-object v0

    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3}, Landroid/opengl/GLSurfaceView2$EGLContextFactory;->destroyContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)V

    .line 1064
    iput-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 1066
    :cond_0
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eqz v0, :cond_1

    .line 1067
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    .line 1068
    iput-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 1070
    :cond_1
    return-void
.end method

.method public start()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const-string v6, "EglHelper"

    .line 897
    const-string v1, "EglHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "start() tid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 902
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v1

    check-cast v1, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    .line 907
    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v1

    iput-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 909
    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v1, v2, :cond_0

    .line 910
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "eglGetDisplay failed"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 916
    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [I

    .line 917
    .local v0, "version":[I
    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 918
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "eglInitialize failed"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 920
    :cond_1
    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView2$EGLConfigChooser;
    invoke-static {v1}, Landroid/opengl/GLSurfaceView2;->access$1(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$EGLConfigChooser;

    move-result-object v1

    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v3}, Landroid/opengl/GLSurfaceView2$EGLConfigChooser;->chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v1

    iput-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 926
    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mEGLContextFactory:Landroid/opengl/GLSurfaceView2$EGLContextFactory;
    invoke-static {v1}, Landroid/opengl/GLSurfaceView2;->access$2(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$EGLContextFactory;

    move-result-object v1

    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v1, v2, v3, v4}, Landroid/opengl/GLSurfaceView2$EGLContextFactory;->createContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v1

    iput-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 927
    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-ne v1, v2, :cond_3

    .line 928
    :cond_2
    iput-object v5, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 929
    const-string v1, "createContext"

    invoke-direct {p0, v1}, Landroid/opengl/GLSurfaceView2$EglHelper;->throwEglException(Ljava/lang/String;)V

    .line 932
    :cond_3
    const-string v1, "EglHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "createContext "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    iput-object v5, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 936
    return-void
.end method

.method public swap()Z
    .locals 5

    .prologue
    .line 1020
    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1028
    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .line 1029
    .local v0, "error":I
    packed-switch v0, :pswitch_data_0

    .line 1039
    :pswitch_0
    const-string v1, "eglSwapBuffers"

    invoke-direct {p0, v1, v0}, Landroid/opengl/GLSurfaceView2$EglHelper;->throwEglException(Ljava/lang/String;I)V

    .line 1042
    .end local v0    # "error":I
    :cond_0
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 1031
    .restart local v0    # "error":I
    :pswitch_1
    const/4 v1, 0x0

    goto :goto_1

    .line 1036
    :pswitch_2
    const-string v1, "EglHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "eglSwapBuffers returned EGL_BAD_NATIVE_WINDOW. tid="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1029
    nop

    :pswitch_data_0
    .packed-switch 0x300b
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
