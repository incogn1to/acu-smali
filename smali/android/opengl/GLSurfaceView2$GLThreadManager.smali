.class Landroid/opengl/GLSurfaceView2$GLThreadManager;
.super Ljava/lang/Object;
.source "GLSurfaceView2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/opengl/GLSurfaceView2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GLThreadManager"
.end annotation


# static fields
.field private static TAG:Ljava/lang/String; = null

.field private static final kGLES_20:I = 0x20000

.field private static final kMSM7K_RENDERER_PREFIX:Ljava/lang/String; = "Q3Dimension MSM7500 "


# instance fields
.field private mEglOwner:Landroid/opengl/GLSurfaceView2$GLThread;

.field private mGLESDriverCheckComplete:Z

.field private mGLESVersion:I

.field private mGLESVersionCheckComplete:Z

.field private mMultipleGLESContextsAllowed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1628
    const-string v0, "GLThreadManager"

    sput-object v0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->TAG:Ljava/lang/String;

    .line 1627
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/opengl/GLSurfaceView2$GLThreadManager;)V
    .locals 0

    .prologue
    .line 1627
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2$GLThreadManager;-><init>()V

    return-void
.end method

.method private checkGLESVersion()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1709
    iget-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mGLESVersionCheckComplete:Z

    if-nez v0, :cond_1

    .line 1711
    const-string v0, "ro.opengles.version"

    .line 1712
    const/4 v1, 0x0

    .line 1710
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mGLESVersion:I

    .line 1713
    iget v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mGLESVersion:I

    const/high16 v1, 0x20000

    if-lt v0, v1, :cond_0

    .line 1714
    iput-boolean v3, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mMultipleGLESContextsAllowed:Z

    .line 1717
    :cond_0
    sget-object v0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "checkGLESVersion mGLESVersion = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1718
    iget v2, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mGLESVersion:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mMultipleGLESContextsAllowed = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mMultipleGLESContextsAllowed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1717
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1720
    iput-boolean v3, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mGLESVersionCheckComplete:Z

    .line 1722
    :cond_1
    return-void
.end method


# virtual methods
.method public declared-synchronized checkGLDriver(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/4 v3, 0x1

    .line 1692
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mGLESDriverCheckComplete:Z

    if-nez v1, :cond_1

    .line 1693
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->checkGLESVersion()V

    .line 1694
    iget v1, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mGLESVersion:I

    const/high16 v2, 0x20000

    if-ge v1, v2, :cond_0

    .line 1695
    const/16 v1, 0x1f01

    invoke-interface {p1, v1}, Ljavax/microedition/khronos/opengles/GL10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    .line 1697
    .local v0, "renderer":Ljava/lang/String;
    const-string v1, "Q3Dimension MSM7500 "

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    .line 1696
    :goto_0
    iput-boolean v1, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mMultipleGLESContextsAllowed:Z

    .line 1699
    sget-object v1, Landroid/opengl/GLSurfaceView2$GLThreadManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkGLDriver renderer = \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" multipleContextsAllowed = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1700
    iget-boolean v3, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mMultipleGLESContextsAllowed:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1699
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1702
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1704
    .end local v0    # "renderer":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mGLESDriverCheckComplete:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1706
    :cond_1
    monitor-exit p0

    return-void

    .restart local v0    # "renderer":Ljava/lang/String;
    :cond_2
    move v1, v3

    .line 1697
    goto :goto_0

    .line 1692
    .end local v0    # "renderer":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public releaseEglContextLocked(Landroid/opengl/GLSurfaceView2$GLThread;)V
    .locals 1
    .param p1, "thread"    # Landroid/opengl/GLSurfaceView2$GLThread;

    .prologue
    .line 1673
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mEglOwner:Landroid/opengl/GLSurfaceView2$GLThread;

    if-ne v0, p1, :cond_0

    .line 1674
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mEglOwner:Landroid/opengl/GLSurfaceView2$GLThread;

    .line 1676
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1677
    return-void
.end method

.method public declared-synchronized shouldReleaseEGLContextWhenPausing()Z
    .locals 1

    .prologue
    .line 1683
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return v0
.end method

.method public declared-synchronized shouldTerminateEGLWhenPausing()Z
    .locals 1

    .prologue
    .line 1687
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->checkGLESVersion()V

    .line 1688
    iget-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mMultipleGLESContextsAllowed:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1687
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized threadExiting(Landroid/opengl/GLSurfaceView2$GLThread;)V
    .locals 4
    .param p1, "thread"    # Landroid/opengl/GLSurfaceView2$GLThread;

    .prologue
    .line 1632
    monitor-enter p0

    :try_start_0
    const-string v0, "GLThread"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "exiting tid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1634
    const/4 v0, 0x1

    invoke-static {p1, v0}, Landroid/opengl/GLSurfaceView2$GLThread;->access$0(Landroid/opengl/GLSurfaceView2$GLThread;Z)V

    .line 1635
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mEglOwner:Landroid/opengl/GLSurfaceView2$GLThread;

    if-ne v0, p1, :cond_0

    .line 1636
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mEglOwner:Landroid/opengl/GLSurfaceView2$GLThread;

    .line 1638
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1639
    monitor-exit p0

    return-void

    .line 1632
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public tryAcquireEglContextLocked(Landroid/opengl/GLSurfaceView2$GLThread;)Z
    .locals 2
    .param p1, "thread"    # Landroid/opengl/GLSurfaceView2$GLThread;

    .prologue
    const/4 v1, 0x1

    .line 1649
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mEglOwner:Landroid/opengl/GLSurfaceView2$GLThread;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mEglOwner:Landroid/opengl/GLSurfaceView2$GLThread;

    if-nez v0, :cond_1

    .line 1650
    :cond_0
    iput-object p1, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mEglOwner:Landroid/opengl/GLSurfaceView2$GLThread;

    .line 1651
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    move v0, v1

    .line 1665
    :goto_0
    return v0

    .line 1654
    :cond_1
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->checkGLESVersion()V

    .line 1655
    iget-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mMultipleGLESContextsAllowed:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 1656
    goto :goto_0

    .line 1662
    :cond_2
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mEglOwner:Landroid/opengl/GLSurfaceView2$GLThread;

    if-eqz v0, :cond_3

    .line 1663
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$GLThreadManager;->mEglOwner:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView2$GLThread;->requestReleaseEglContextLocked()V

    .line 1665
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
