.class Landroid/opengl/GLSurfaceView2$DefaultContextFactory;
.super Ljava/lang/Object;
.source "GLSurfaceView2.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView2$EGLContextFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/opengl/GLSurfaceView2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultContextFactory"
.end annotation


# instance fields
.field private EGL_CONTEXT_CLIENT_VERSION:I

.field final synthetic this$0:Landroid/opengl/GLSurfaceView2;


# direct methods
.method private constructor <init>(Landroid/opengl/GLSurfaceView2;)V
    .locals 1

    .prologue
    .line 679
    iput-object p1, p0, Landroid/opengl/GLSurfaceView2$DefaultContextFactory;->this$0:Landroid/opengl/GLSurfaceView2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 680
    const/16 v0, 0x3098

    iput v0, p0, Landroid/opengl/GLSurfaceView2$DefaultContextFactory;->EGL_CONTEXT_CLIENT_VERSION:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/opengl/GLSurfaceView2;Landroid/opengl/GLSurfaceView2$DefaultContextFactory;)V
    .locals 0

    .prologue
    .line 679
    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView2$DefaultContextFactory;-><init>(Landroid/opengl/GLSurfaceView2;)V

    return-void
.end method


# virtual methods
.method public createContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;
    .locals 3
    .param p1, "egl"    # Ljavax/microedition/khronos/egl/EGL10;
    .param p2, "display"    # Ljavax/microedition/khronos/egl/EGLDisplay;
    .param p3, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    .line 683
    const/4 v1, 0x3

    new-array v0, v1, [I

    const/4 v1, 0x0

    iget v2, p0, Landroid/opengl/GLSurfaceView2$DefaultContextFactory;->EGL_CONTEXT_CLIENT_VERSION:I

    aput v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$DefaultContextFactory;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mEGLContextClientVersion:I
    invoke-static {v2}, Landroid/opengl/GLSurfaceView2;->access$0(Landroid/opengl/GLSurfaceView2;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    .line 684
    const/16 v2, 0x3038

    aput v2, v0, v1

    .line 686
    .local v0, "attrib_list":[I
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    .line 687
    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$DefaultContextFactory;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mEGLContextClientVersion:I
    invoke-static {v2}, Landroid/opengl/GLSurfaceView2;->access$0(Landroid/opengl/GLSurfaceView2;)I

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v0

    .line 686
    :goto_0
    invoke-interface {p1, p2, p3, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v1

    return-object v1

    .line 687
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public destroyContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)V
    .locals 4
    .param p1, "egl"    # Ljavax/microedition/khronos/egl/EGL10;
    .param p2, "display"    # Ljavax/microedition/khronos/egl/EGLDisplay;
    .param p3, "context"    # Ljavax/microedition/khronos/egl/EGLContext;

    .prologue
    const-string v3, "DefaultContextFactory"

    .line 692
    invoke-interface {p1, p2, p3}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 693
    const-string v0, "DefaultContextFactory"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "display:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " context: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    const-string v0, "DefaultContextFactory"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tid="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    :cond_0
    return-void
.end method
