.class public Landroid/opengl/GLSurfaceView2;
.super Landroid/view/SurfaceView;
.source "GLSurfaceView2.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/opengl/GLSurfaceView2$BaseConfigChooser;,
        Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;,
        Landroid/opengl/GLSurfaceView2$DefaultContextFactory;,
        Landroid/opengl/GLSurfaceView2$DefaultWindowSurfaceFactory;,
        Landroid/opengl/GLSurfaceView2$EGLConfigChooser;,
        Landroid/opengl/GLSurfaceView2$EGLContextFactory;,
        Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;,
        Landroid/opengl/GLSurfaceView2$EglHelper;,
        Landroid/opengl/GLSurfaceView2$GLThread;,
        Landroid/opengl/GLSurfaceView2$GLThreadManager;,
        Landroid/opengl/GLSurfaceView2$GLWrapper;,
        Landroid/opengl/GLSurfaceView2$LogWriter;,
        Landroid/opengl/GLSurfaceView2$Renderer;,
        Landroid/opengl/GLSurfaceView2$SimpleEGLConfigChooser;
    }
.end annotation


# static fields
.field public static final DEBUG_CHECK_GL_ERROR:I = 0x1

.field public static final DEBUG_LOG_GL_CALLS:I = 0x2

.field private static final DRAW_TWICE_AFTER_SIZE_CHANGED:Z = true

.field private static final LOG_EGL:Z = true

.field private static final LOG_PAUSE_RESUME:Z = true

.field private static final LOG_RENDERER:Z = true

.field private static final LOG_RENDERER_DRAW_FRAME:Z = false

.field private static final LOG_SURFACE:Z = true

.field private static final LOG_THREADS:Z = true

.field public static final RENDERMODE_CONTINUOUSLY:I = 0x1

.field public static final RENDERMODE_WHEN_DIRTY:I

.field private static final sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;


# instance fields
.field private mDebugFlags:I

.field private mEGLConfigChooser:Landroid/opengl/GLSurfaceView2$EGLConfigChooser;

.field private mEGLContextClientVersion:I

.field private mEGLContextFactory:Landroid/opengl/GLSurfaceView2$EGLContextFactory;

.field private mEGLWindowSurfaceFactory:Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;

.field private mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

.field private mGLWrapper:Landroid/opengl/GLSurfaceView2$GLWrapper;

.field private mSizeChanged:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1734
    new-instance v0, Landroid/opengl/GLSurfaceView2$GLThreadManager;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/opengl/GLSurfaceView2$GLThreadManager;-><init>(Landroid/opengl/GLSurfaceView2$GLThreadManager;)V

    sput-object v0, Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;

    .line 158
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 208
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 1735
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/opengl/GLSurfaceView2;->mSizeChanged:Z

    .line 209
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2;->init()V

    .line 210
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 217
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1735
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/opengl/GLSurfaceView2;->mSizeChanged:Z

    .line 218
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2;->init()V

    .line 219
    return-void
.end method

.method static synthetic access$0(Landroid/opengl/GLSurfaceView2;)I
    .locals 1

    .prologue
    .line 1743
    iget v0, p0, Landroid/opengl/GLSurfaceView2;->mEGLContextClientVersion:I

    return v0
.end method

.method static synthetic access$1(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$EGLConfigChooser;
    .locals 1

    .prologue
    .line 1738
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView2$EGLConfigChooser;

    return-object v0
.end method

.method static synthetic access$2(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$EGLContextFactory;
    .locals 1

    .prologue
    .line 1739
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mEGLContextFactory:Landroid/opengl/GLSurfaceView2$EGLContextFactory;

    return-object v0
.end method

.method static synthetic access$3(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;
    .locals 1

    .prologue
    .line 1740
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mEGLWindowSurfaceFactory:Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;

    return-object v0
.end method

.method static synthetic access$4(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$GLWrapper;
    .locals 1

    .prologue
    .line 1741
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLWrapper:Landroid/opengl/GLSurfaceView2$GLWrapper;

    return-object v0
.end method

.method static synthetic access$5(Landroid/opengl/GLSurfaceView2;)I
    .locals 1

    .prologue
    .line 1742
    iget v0, p0, Landroid/opengl/GLSurfaceView2;->mDebugFlags:I

    return v0
.end method

.method static synthetic access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;
    .locals 1

    .prologue
    .line 1734
    sget-object v0, Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;

    return-object v0
.end method

.method static synthetic access$7(Landroid/opengl/GLSurfaceView2;)Z
    .locals 1

    .prologue
    .line 1735
    iget-boolean v0, p0, Landroid/opengl/GLSurfaceView2;->mSizeChanged:Z

    return v0
.end method

.method static synthetic access$8(Landroid/opengl/GLSurfaceView2;Z)V
    .locals 0

    .prologue
    .line 1735
    iput-boolean p1, p0, Landroid/opengl/GLSurfaceView2;->mSizeChanged:Z

    return-void
.end method

.method static synthetic access$9(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$GLThread;
    .locals 1

    .prologue
    .line 1737
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    return-object v0
.end method

.method private checkRenderThreadState()V
    .locals 2

    .prologue
    .line 1621
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    if-eqz v0, :cond_0

    .line 1622
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 1623
    const-string v1, "setRenderer has already been called for this instance."

    .line 1622
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1625
    :cond_0
    return-void
.end method

.method private init()V
    .locals 1

    .prologue
    .line 224
    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView2;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 225
    .local v0, "holder":Landroid/view/SurfaceHolder;
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 233
    return-void
.end method


# virtual methods
.method public getDebugFlags()I
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Landroid/opengl/GLSurfaceView2;->mDebugFlags:I

    return v0
.end method

.method public getRenderMode()I
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView2$GLThread;->getRenderMode()I

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 533
    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    .line 534
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView2$GLThread;->requestExitAndWait()V

    .line 535
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 502
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView2$GLThread;->onPause()V

    .line 503
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView2$GLThread;->onResume()V

    .line 514
    return-void
.end method

.method public queueEvent(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 523
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0, p1}, Landroid/opengl/GLSurfaceView2$GLThread;->queueEvent(Ljava/lang/Runnable;)V

    .line 524
    return-void
.end method

.method public requestRender()V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView2$GLThread;->requestRender()V

    .line 468
    return-void
.end method

.method public setDebugFlags(I)V
    .locals 0
    .param p1, "debugFlags"    # I

    .prologue
    .line 262
    iput p1, p0, Landroid/opengl/GLSurfaceView2;->mDebugFlags:I

    .line 263
    return-void
.end method

.method public setEGLConfigChooser(IIIIII)V
    .locals 8
    .param p1, "redSize"    # I
    .param p2, "greenSize"    # I
    .param p3, "blueSize"    # I
    .param p4, "alphaSize"    # I
    .param p5, "depthSize"    # I
    .param p6, "stencilSize"    # I

    .prologue
    .line 393
    new-instance v0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    .line 394
    invoke-direct/range {v0 .. v7}, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;-><init>(Landroid/opengl/GLSurfaceView2;IIIIII)V

    .line 393
    invoke-virtual {p0, v0}, Landroid/opengl/GLSurfaceView2;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView2$EGLConfigChooser;)V

    .line 395
    return-void
.end method

.method public setEGLConfigChooser(Landroid/opengl/GLSurfaceView2$EGLConfigChooser;)V
    .locals 0
    .param p1, "configChooser"    # Landroid/opengl/GLSurfaceView2$EGLConfigChooser;

    .prologue
    .line 356
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2;->checkRenderThreadState()V

    .line 357
    iput-object p1, p0, Landroid/opengl/GLSurfaceView2;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView2$EGLConfigChooser;

    .line 358
    return-void
.end method

.method public setEGLConfigChooser(Z)V
    .locals 1
    .param p1, "needDepth"    # Z

    .prologue
    .line 375
    new-instance v0, Landroid/opengl/GLSurfaceView2$SimpleEGLConfigChooser;

    invoke-direct {v0, p0, p1}, Landroid/opengl/GLSurfaceView2$SimpleEGLConfigChooser;-><init>(Landroid/opengl/GLSurfaceView2;Z)V

    invoke-virtual {p0, v0}, Landroid/opengl/GLSurfaceView2;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView2$EGLConfigChooser;)V

    .line 376
    return-void
.end method

.method public setEGLContextClientVersion(I)V
    .locals 0
    .param p1, "version"    # I

    .prologue
    .line 424
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2;->checkRenderThreadState()V

    .line 425
    iput p1, p0, Landroid/opengl/GLSurfaceView2;->mEGLContextClientVersion:I

    .line 426
    return-void
.end method

.method public setEGLContextFactory(Landroid/opengl/GLSurfaceView2$EGLContextFactory;)V
    .locals 0
    .param p1, "factory"    # Landroid/opengl/GLSurfaceView2$EGLContextFactory;

    .prologue
    .line 325
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2;->checkRenderThreadState()V

    .line 326
    iput-object p1, p0, Landroid/opengl/GLSurfaceView2;->mEGLContextFactory:Landroid/opengl/GLSurfaceView2$EGLContextFactory;

    .line 327
    return-void
.end method

.method public setEGLWindowSurfaceFactory(Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;)V
    .locals 0
    .param p1, "factory"    # Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;

    .prologue
    .line 339
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2;->checkRenderThreadState()V

    .line 340
    iput-object p1, p0, Landroid/opengl/GLSurfaceView2;->mEGLWindowSurfaceFactory:Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;

    .line 341
    return-void
.end method

.method public setGLWrapper(Landroid/opengl/GLSurfaceView2$GLWrapper;)V
    .locals 0
    .param p1, "glWrapper"    # Landroid/opengl/GLSurfaceView2$GLWrapper;

    .prologue
    .line 249
    iput-object p1, p0, Landroid/opengl/GLSurfaceView2;->mGLWrapper:Landroid/opengl/GLSurfaceView2$GLWrapper;

    .line 250
    return-void
.end method

.method public setRenderMode(I)V
    .locals 1
    .param p1, "renderMode"    # I

    .prologue
    .line 445
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0, p1}, Landroid/opengl/GLSurfaceView2$GLThread;->setRenderMode(I)V

    .line 446
    return-void
.end method

.method public setRenderer(Landroid/opengl/GLSurfaceView2$Renderer;)V
    .locals 3
    .param p1, "renderer"    # Landroid/opengl/GLSurfaceView2$Renderer;

    .prologue
    const/4 v2, 0x0

    .line 300
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2;->checkRenderThreadState()V

    .line 301
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView2$EGLConfigChooser;

    if-nez v0, :cond_0

    .line 302
    new-instance v0, Landroid/opengl/GLSurfaceView2$SimpleEGLConfigChooser;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Landroid/opengl/GLSurfaceView2$SimpleEGLConfigChooser;-><init>(Landroid/opengl/GLSurfaceView2;Z)V

    iput-object v0, p0, Landroid/opengl/GLSurfaceView2;->mEGLConfigChooser:Landroid/opengl/GLSurfaceView2$EGLConfigChooser;

    .line 304
    :cond_0
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mEGLContextFactory:Landroid/opengl/GLSurfaceView2$EGLContextFactory;

    if-nez v0, :cond_1

    .line 305
    new-instance v0, Landroid/opengl/GLSurfaceView2$DefaultContextFactory;

    invoke-direct {v0, p0, v2}, Landroid/opengl/GLSurfaceView2$DefaultContextFactory;-><init>(Landroid/opengl/GLSurfaceView2;Landroid/opengl/GLSurfaceView2$DefaultContextFactory;)V

    iput-object v0, p0, Landroid/opengl/GLSurfaceView2;->mEGLContextFactory:Landroid/opengl/GLSurfaceView2$EGLContextFactory;

    .line 307
    :cond_1
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mEGLWindowSurfaceFactory:Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;

    if-nez v0, :cond_2

    .line 308
    new-instance v0, Landroid/opengl/GLSurfaceView2$DefaultWindowSurfaceFactory;

    invoke-direct {v0, v2}, Landroid/opengl/GLSurfaceView2$DefaultWindowSurfaceFactory;-><init>(Landroid/opengl/GLSurfaceView2$DefaultWindowSurfaceFactory;)V

    iput-object v0, p0, Landroid/opengl/GLSurfaceView2;->mEGLWindowSurfaceFactory:Landroid/opengl/GLSurfaceView2$EGLWindowSurfaceFactory;

    .line 310
    :cond_2
    new-instance v0, Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-direct {v0, p0, p1}, Landroid/opengl/GLSurfaceView2$GLThread;-><init>(Landroid/opengl/GLSurfaceView2;Landroid/opengl/GLSurfaceView2$Renderer;)V

    iput-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    .line 311
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView2$GLThread;->start()V

    .line 312
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 492
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0, p3, p4}, Landroid/opengl/GLSurfaceView2$GLThread;->onWindowResize(II)V

    .line 493
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 475
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView2$GLThread;->surfaceCreated()V

    .line 476
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 484
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView2$GLThread;->surfaceDestroyed()V

    .line 485
    return-void
.end method
