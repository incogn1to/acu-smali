.class Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;
.super Landroid/opengl/GLSurfaceView2$BaseConfigChooser;
.source "GLSurfaceView2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/opengl/GLSurfaceView2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComponentSizeChooser"
.end annotation


# instance fields
.field protected mAlphaSize:I

.field protected mBlueSize:I

.field protected mDepthSize:I

.field protected mGreenSize:I

.field protected mRedSize:I

.field protected mStencilSize:I

.field private mValue:[I

.field final synthetic this$0:Landroid/opengl/GLSurfaceView2;


# direct methods
.method public constructor <init>(Landroid/opengl/GLSurfaceView2;IIIIII)V
    .locals 4
    .param p2, "redSize"    # I
    .param p3, "greenSize"    # I
    .param p4, "blueSize"    # I
    .param p5, "alphaSize"    # I
    .param p6, "depthSize"    # I
    .param p7, "stencilSize"    # I

    .prologue
    const/4 v3, 0x1

    .line 808
    iput-object p1, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->this$0:Landroid/opengl/GLSurfaceView2;

    .line 809
    const/16 v0, 0xd

    new-array v0, v0, [I

    const/4 v1, 0x0

    .line 810
    const/16 v2, 0x3024

    aput v2, v0, v1

    aput p2, v0, v3

    const/4 v1, 0x2

    .line 811
    const/16 v2, 0x3023

    aput v2, v0, v1

    const/4 v1, 0x3

    aput p3, v0, v1

    const/4 v1, 0x4

    .line 812
    const/16 v2, 0x3022

    aput v2, v0, v1

    const/4 v1, 0x5

    aput p4, v0, v1

    const/4 v1, 0x6

    .line 813
    const/16 v2, 0x3021

    aput v2, v0, v1

    const/4 v1, 0x7

    aput p5, v0, v1

    const/16 v1, 0x8

    .line 814
    const/16 v2, 0x3025

    aput v2, v0, v1

    const/16 v1, 0x9

    aput p6, v0, v1

    const/16 v1, 0xa

    .line 815
    const/16 v2, 0x3026

    aput v2, v0, v1

    const/16 v1, 0xb

    aput p7, v0, v1

    const/16 v1, 0xc

    .line 816
    const/16 v2, 0x3038

    aput v2, v0, v1

    invoke-direct {p0, p1, v0}, Landroid/opengl/GLSurfaceView2$BaseConfigChooser;-><init>(Landroid/opengl/GLSurfaceView2;[I)V

    .line 817
    new-array v0, v3, [I

    iput-object v0, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mValue:[I

    .line 818
    iput p2, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mRedSize:I

    .line 819
    iput p3, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mGreenSize:I

    .line 820
    iput p4, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mBlueSize:I

    .line 821
    iput p5, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mAlphaSize:I

    .line 822
    iput p6, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mDepthSize:I

    .line 823
    iput p7, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mStencilSize:I

    .line 824
    return-void
.end method

.method private findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I
    .locals 2
    .param p1, "egl"    # Ljavax/microedition/khronos/egl/EGL10;
    .param p2, "display"    # Ljavax/microedition/khronos/egl/EGLDisplay;
    .param p3, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;
    .param p4, "attribute"    # I
    .param p5, "defaultValue"    # I

    .prologue
    .line 855
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mValue:[I

    invoke-interface {p1, p2, p3, p4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 856
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mValue:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 858
    :goto_0
    return v0

    :cond_0
    move v0, p5

    goto :goto_0
.end method


# virtual methods
.method public chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 15
    .param p1, "egl"    # Ljavax/microedition/khronos/egl/EGL10;
    .param p2, "display"    # Ljavax/microedition/khronos/egl/EGLDisplay;
    .param p3, "configs"    # [Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    .line 829
    move-object/from16 v0, p3

    array-length v0, v0

    move v13, v0

    const/4 v1, 0x0

    move v14, v1

    :goto_0
    if-lt v14, v13, :cond_0

    .line 849
    const/4 v1, 0x0

    :goto_1
    return-object v1

    .line 829
    :cond_0
    aget-object v4, p3, v14

    .line 831
    .local v4, "config":Ljavax/microedition/khronos/egl/EGLConfig;
    const/16 v5, 0x3025

    const/4 v6, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 830
    invoke-direct/range {v1 .. v6}, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v9

    .line 833
    .local v9, "d":I
    const/16 v5, 0x3026

    const/4 v6, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 832
    invoke-direct/range {v1 .. v6}, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v12

    .line 834
    .local v12, "s":I
    iget v1, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mDepthSize:I

    if-lt v9, v1, :cond_1

    iget v1, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mStencilSize:I

    if-lt v12, v1, :cond_1

    .line 836
    const/16 v5, 0x3024

    const/4 v6, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 835
    invoke-direct/range {v1 .. v6}, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v11

    .line 838
    .local v11, "r":I
    const/16 v5, 0x3023

    const/4 v6, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 837
    invoke-direct/range {v1 .. v6}, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v10

    .line 840
    .local v10, "g":I
    const/16 v5, 0x3022

    const/4 v6, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 839
    invoke-direct/range {v1 .. v6}, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v8

    .line 842
    .local v8, "b":I
    const/16 v5, 0x3021

    const/4 v6, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    .line 841
    invoke-direct/range {v1 .. v6}, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v7

    .line 843
    .local v7, "a":I
    iget v1, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mRedSize:I

    if-ne v11, v1, :cond_1

    iget v1, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mGreenSize:I

    if-ne v10, v1, :cond_1

    .line 844
    iget v1, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mBlueSize:I

    if-ne v8, v1, :cond_1

    iget v1, p0, Landroid/opengl/GLSurfaceView2$ComponentSizeChooser;->mAlphaSize:I

    if-ne v7, v1, :cond_1

    move-object v1, v4

    .line 845
    goto :goto_1

    .line 829
    .end local v7    # "a":I
    .end local v8    # "b":I
    .end local v10    # "g":I
    .end local v11    # "r":I
    :cond_1
    add-int/lit8 v1, v14, 0x1

    move v14, v1

    goto :goto_0
.end method
