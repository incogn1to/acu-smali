.class public interface abstract Landroid/opengl/GLSurfaceView2$EGLConfigChooser;
.super Ljava/lang/Object;
.source "GLSurfaceView2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/opengl/GLSurfaceView2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EGLConfigChooser"
.end annotation


# virtual methods
.method public abstract chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
.end method
