.class Landroid/opengl/GLSurfaceView2$GLThread;
.super Ljava/lang/Thread;
.source "GLSurfaceView2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/opengl/GLSurfaceView2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GLThread"
.end annotation


# instance fields
.field private mEglHelper:Landroid/opengl/GLSurfaceView2$EglHelper;

.field private mEventQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private mExited:Z

.field private mHasSurface:Z

.field private mHaveEglContext:Z

.field private mHaveEglSurface:Z

.field private mHeight:I

.field private mPaused:Z

.field private mRenderComplete:Z

.field private mRenderMode:I

.field private mRenderer:Landroid/opengl/GLSurfaceView2$Renderer;

.field private mRequestPaused:Z

.field private mRequestRender:Z

.field private mShouldExit:Z

.field private mShouldReleaseEglContext:Z

.field private mWaitingForSurface:Z

.field private mWidth:I

.field final synthetic this$0:Landroid/opengl/GLSurfaceView2;


# direct methods
.method constructor <init>(Landroid/opengl/GLSurfaceView2;Landroid/opengl/GLSurfaceView2$Renderer;)V
    .locals 3
    .param p2, "renderer"    # Landroid/opengl/GLSurfaceView2$Renderer;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1105
    iput-object p1, p0, Landroid/opengl/GLSurfaceView2$GLThread;->this$0:Landroid/opengl/GLSurfaceView2;

    .line 1106
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1579
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mEventQueue:Ljava/util/ArrayList;

    .line 1107
    iput v1, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mWidth:I

    .line 1108
    iput v1, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHeight:I

    .line 1109
    iput-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRequestRender:Z

    .line 1110
    iput v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderMode:I

    .line 1111
    iput-object p2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderer:Landroid/opengl/GLSurfaceView2$Renderer;

    .line 1112
    return-void
.end method

.method static synthetic access$0(Landroid/opengl/GLSurfaceView2$GLThread;Z)V
    .locals 0

    .prologue
    .line 1566
    iput-boolean p1, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mExited:Z

    return-void
.end method

.method private guardedRun()V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1155
    new-instance v14, Landroid/opengl/GLSurfaceView2$EglHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->this$0:Landroid/opengl/GLSurfaceView2;

    move-object v15, v0

    invoke-direct {v14, v15}, Landroid/opengl/GLSurfaceView2$EglHelper;-><init>(Landroid/opengl/GLSurfaceView2;)V

    move-object v0, v14

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/opengl/GLSurfaceView2$GLThread;->mEglHelper:Landroid/opengl/GLSurfaceView2$EglHelper;

    .line 1156
    const/4 v14, 0x0

    move v0, v14

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglContext:Z

    .line 1157
    const/4 v14, 0x0

    move v0, v14

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglSurface:Z

    .line 1159
    const/4 v7, 0x0

    .line 1160
    .local v7, "gl":Ljavax/microedition/khronos/opengles/GL10;
    const/4 v3, 0x0

    .line 1161
    .local v3, "createEglContext":Z
    const/4 v4, 0x0

    .line 1162
    .local v4, "createEglSurface":Z
    const/4 v9, 0x0

    .line 1163
    .local v9, "lostEglContext":Z
    const/4 v10, 0x0

    .line 1164
    .local v10, "sizeChanged":Z
    const/4 v13, 0x0

    .line 1165
    .local v13, "wantRenderNotification":Z
    const/4 v5, 0x0

    .line 1166
    .local v5, "doRenderNotification":Z
    const/4 v2, 0x0

    .line 1167
    .local v2, "askedToReleaseEglContext":Z
    const/4 v12, 0x0

    .line 1168
    .local v12, "w":I
    const/4 v8, 0x0

    .line 1169
    .local v8, "h":I
    const/4 v6, 0x0

    .line 1172
    .local v6, "event":Ljava/lang/Runnable;
    :cond_0
    :goto_0
    :try_start_0
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v14

    monitor-enter v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1174
    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mShouldExit:Z

    move v15, v0

    if-eqz v15, :cond_1

    .line 1175
    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1386
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v14

    monitor-enter v14

    .line 1387
    :try_start_2
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglSurfaceLocked()V

    .line 1388
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglContextLocked()V

    .line 1386
    monitor-exit v14

    .line 1391
    :goto_2
    return-void

    .line 1386
    :catchall_0
    move-exception v15

    monitor-exit v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v15

    .line 1178
    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object v15, v0

    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_2

    .line 1179
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mEventQueue:Ljava/util/ArrayList;

    move-object v15, v0

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Ljava/lang/Runnable;

    move-object v6, v0

    .line 1172
    :goto_3
    monitor-exit v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1331
    if-eqz v6, :cond_11

    .line 1332
    :try_start_4
    invoke-interface {v6}, Ljava/lang/Runnable;->run()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1333
    const/4 v6, 0x0

    .line 1334
    goto :goto_0

    .line 1184
    :cond_2
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mPaused:Z

    move v15, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mRequestPaused:Z

    move/from16 v16, v0

    move v0, v15

    move/from16 v1, v16

    if-eq v0, v1, :cond_3

    .line 1185
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mRequestPaused:Z

    move v15, v0

    move v0, v15

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/opengl/GLSurfaceView2$GLThread;->mPaused:Z

    .line 1186
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Object;->notifyAll()V

    .line 1188
    const-string v15, "GLThread"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "mPaused is now "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mPaused:Z

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " tid="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v17

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1193
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mShouldReleaseEglContext:Z

    move v15, v0

    if-eqz v15, :cond_4

    .line 1195
    const-string v15, "GLThread"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "releasing EGL context because asked to tid="

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v17

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglSurfaceLocked()V

    .line 1198
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglContextLocked()V

    .line 1199
    const/4 v15, 0x0

    move v0, v15

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/opengl/GLSurfaceView2$GLThread;->mShouldReleaseEglContext:Z

    .line 1200
    const/4 v2, 0x1

    .line 1204
    :cond_4
    if-eqz v9, :cond_5

    .line 1205
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglSurfaceLocked()V

    .line 1206
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglContextLocked()V

    .line 1207
    const/4 v9, 0x0

    .line 1211
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglSurface:Z

    move v15, v0

    if-eqz v15, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mPaused:Z

    move v15, v0

    if-eqz v15, :cond_7

    .line 1213
    const-string v15, "GLThread"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "releasing EGL surface because paused tid="

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v17

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1215
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglSurfaceLocked()V

    .line 1216
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    invoke-virtual {v15}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->shouldReleaseEGLContextWhenPausing()Z

    move-result v15

    if-eqz v15, :cond_6

    .line 1217
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglContextLocked()V

    .line 1219
    const-string v15, "GLThread"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "releasing EGL context because paused tid="

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v17

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1222
    :cond_6
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    invoke-virtual {v15}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->shouldTerminateEGLWhenPausing()Z

    move-result v15

    if-eqz v15, :cond_7

    .line 1223
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mEglHelper:Landroid/opengl/GLSurfaceView2$EglHelper;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/opengl/GLSurfaceView2$EglHelper;->finish()V

    .line 1225
    const-string v15, "GLThread"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "terminating EGL because paused tid="

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v17

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1231
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHasSurface:Z

    move v15, v0

    if-nez v15, :cond_9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mWaitingForSurface:Z

    move v15, v0

    if-nez v15, :cond_9

    .line 1233
    const-string v15, "GLThread"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "noticed surfaceView surface lost tid="

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v17

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1235
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglSurface:Z

    move v15, v0

    if-eqz v15, :cond_8

    .line 1236
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglSurfaceLocked()V

    .line 1238
    :cond_8
    const/4 v15, 0x1

    move v0, v15

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/opengl/GLSurfaceView2$GLThread;->mWaitingForSurface:Z

    .line 1239
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Object;->notifyAll()V

    .line 1243
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHasSurface:Z

    move v15, v0

    if-eqz v15, :cond_a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mWaitingForSurface:Z

    move v15, v0

    if-eqz v15, :cond_a

    .line 1245
    const-string v15, "GLThread"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "noticed surfaceView surface acquired tid="

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v17

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1247
    const/4 v15, 0x0

    move v0, v15

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/opengl/GLSurfaceView2$GLThread;->mWaitingForSurface:Z

    .line 1248
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Object;->notifyAll()V

    .line 1251
    :cond_a
    if-eqz v5, :cond_b

    .line 1253
    const-string v15, "GLThread"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "sending render notification tid="

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v17

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1255
    const/4 v13, 0x0

    .line 1256
    const/4 v5, 0x0

    .line 1257
    const/4 v15, 0x1

    move v0, v15

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderComplete:Z

    .line 1258
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Object;->notifyAll()V

    .line 1262
    :cond_b
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->readyToDraw()Z

    move-result v15

    if-eqz v15, :cond_10

    .line 1265
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglContext:Z

    move v15, v0

    if-nez v15, :cond_c

    .line 1266
    if-eqz v2, :cond_e

    .line 1267
    const/4 v2, 0x0

    .line 1282
    :cond_c
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglContext:Z

    move v15, v0

    if-eqz v15, :cond_d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglSurface:Z

    move v15, v0

    if-nez v15, :cond_d

    .line 1283
    const/4 v15, 0x1

    move v0, v15

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglSurface:Z

    .line 1284
    const/4 v4, 0x1

    .line 1285
    const/4 v10, 0x1

    .line 1288
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglSurface:Z

    move v15, v0

    if-eqz v15, :cond_10

    .line 1289
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->this$0:Landroid/opengl/GLSurfaceView2;

    move-object v15, v0

    # getter for: Landroid/opengl/GLSurfaceView2;->mSizeChanged:Z
    invoke-static {v15}, Landroid/opengl/GLSurfaceView2;->access$7(Landroid/opengl/GLSurfaceView2;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 1290
    const/4 v10, 0x1

    .line 1291
    move-object/from16 v0, p0

    iget v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mWidth:I

    move v12, v0

    .line 1292
    move-object/from16 v0, p0

    iget v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHeight:I

    move v8, v0

    .line 1293
    const/4 v13, 0x1

    .line 1295
    const-string v15, "GLThread"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "noticing that we want render notification tid="

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v17

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1305
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->this$0:Landroid/opengl/GLSurfaceView2;

    move-object v15, v0

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Landroid/opengl/GLSurfaceView2;->access$8(Landroid/opengl/GLSurfaceView2;Z)V

    .line 1309
    :goto_5
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Object;->notifyAll()V

    goto/16 :goto_3

    .line 1172
    :catchall_1
    move-exception v15

    monitor-exit v14
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v15
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1382
    :catchall_2
    move-exception v14

    .line 1386
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    monitor-enter v15

    .line 1387
    :try_start_7
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglSurfaceLocked()V

    .line 1388
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglContextLocked()V

    .line 1386
    monitor-exit v15
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 1390
    throw v14

    .line 1268
    :cond_e
    :try_start_8
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    move-object v0, v15

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->tryAcquireEglContextLocked(Landroid/opengl/GLSurfaceView2$GLThread;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v15

    if-eqz v15, :cond_c

    .line 1270
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mEglHelper:Landroid/opengl/GLSurfaceView2$EglHelper;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/opengl/GLSurfaceView2$EglHelper;->start()V
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1275
    const/4 v15, 0x1

    :try_start_a
    move v0, v15

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglContext:Z

    .line 1276
    const/4 v3, 0x1

    .line 1278
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Object;->notifyAll()V

    goto/16 :goto_4

    .line 1271
    :catch_0
    move-exception v15

    move-object v11, v15

    .line 1272
    .local v11, "t":Ljava/lang/RuntimeException;
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    move-object v0, v15

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->releaseEglContextLocked(Landroid/opengl/GLSurfaceView2$GLThread;)V

    .line 1273
    throw v11

    .line 1307
    .end local v11    # "t":Ljava/lang/RuntimeException;
    :cond_f
    const/4 v15, 0x0

    move v0, v15

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/opengl/GLSurfaceView2$GLThread;->mRequestRender:Z

    goto :goto_5

    .line 1316
    :cond_10
    const-string v15, "GLThread"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "waiting tid="

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v17

    invoke-virtual/range {v16 .. v18}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 1317
    const-string v17, " mHaveEglContext: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglContext:Z

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 1318
    const-string v17, " mHaveEglSurface: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglSurface:Z

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 1319
    const-string v17, " mPaused: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mPaused:Z

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 1320
    const-string v17, " mHasSurface: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHasSurface:Z

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 1321
    const-string v17, " mWaitingForSurface: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mWaitingForSurface:Z

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 1322
    const-string v17, " mWidth: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mWidth:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 1323
    const-string v17, " mHeight: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mHeight:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 1324
    const-string v17, " mRequestRender: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mRequestRender:Z

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 1325
    const-string v17, " mRenderMode: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderMode:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 1316
    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1327
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Object;->wait()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    .line 1337
    :cond_11
    if-eqz v4, :cond_13

    .line 1339
    :try_start_b
    const-string v14, "GLThread"

    const-string v15, "egl createSurface"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1341
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mEglHelper:Landroid/opengl/GLSurfaceView2$EglHelper;

    move-object v14, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->this$0:Landroid/opengl/GLSurfaceView2;

    move-object v15, v0

    invoke-virtual {v15}, Landroid/opengl/GLSurfaceView2;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/opengl/GLSurfaceView2$EglHelper;->createSurface(Landroid/view/SurfaceHolder;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Ljavax/microedition/khronos/opengles/GL10;

    move-object v7, v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 1342
    if-nez v7, :cond_12

    .line 1386
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v14

    monitor-enter v14

    .line 1387
    :try_start_c
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglSurfaceLocked()V

    .line 1388
    invoke-direct/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->stopEglContextLocked()V

    .line 1386
    monitor-exit v14

    goto/16 :goto_2

    :catchall_3
    move-exception v15

    monitor-exit v14
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    throw v15

    .line 1346
    :cond_12
    :try_start_d
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v14

    invoke-virtual {v14, v7}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->checkGLDriver(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1347
    const/4 v4, 0x0

    .line 1350
    :cond_13
    if-eqz v3, :cond_14

    .line 1352
    const-string v14, "GLThread"

    const-string v15, "onSurfaceCreated"

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1354
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderer:Landroid/opengl/GLSurfaceView2$Renderer;

    move-object v14, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mEglHelper:Landroid/opengl/GLSurfaceView2$EglHelper;

    move-object v15, v0

    iget-object v15, v15, Landroid/opengl/GLSurfaceView2$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v14, v7, v15}, Landroid/opengl/GLSurfaceView2$Renderer;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 1355
    const/4 v3, 0x0

    .line 1358
    :cond_14
    if-eqz v10, :cond_15

    .line 1360
    const-string v14, "GLThread"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "onSurfaceChanged("

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderer:Landroid/opengl/GLSurfaceView2$Renderer;

    move-object v14, v0

    invoke-interface {v14, v7, v12, v8}, Landroid/opengl/GLSurfaceView2$Renderer;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1363
    const/4 v10, 0x0

    .line 1369
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderer:Landroid/opengl/GLSurfaceView2$Renderer;

    move-object v14, v0

    invoke-interface {v14, v7}, Landroid/opengl/GLSurfaceView2$Renderer;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1370
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/opengl/GLSurfaceView2$GLThread;->mEglHelper:Landroid/opengl/GLSurfaceView2$EglHelper;

    move-object v14, v0

    invoke-virtual {v14}, Landroid/opengl/GLSurfaceView2$EglHelper;->swap()Z

    move-result v14

    if-nez v14, :cond_16

    .line 1372
    const-string v14, "GLThread"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "egl context lost tid="

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 1374
    const/4 v9, 0x1

    .line 1377
    :cond_16
    if-eqz v13, :cond_0

    .line 1378
    const/4 v5, 0x1

    .line 1171
    goto/16 :goto_0

    .line 1386
    :catchall_4
    move-exception v14

    :try_start_e
    monitor-exit v15
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    throw v14
.end method

.method private readyToDraw()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1398
    iget-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mPaused:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHasSurface:Z

    if-eqz v0, :cond_1

    .line 1399
    iget v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mWidth:I

    if-lez v0, :cond_1

    iget v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHeight:I

    if-lez v0, :cond_1

    .line 1400
    iget-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRequestRender:Z

    if-nez v0, :cond_0

    iget v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderMode:I

    if-ne v0, v1, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    .line 1398
    goto :goto_0
.end method

.method private stopEglContextLocked()V
    .locals 1

    .prologue
    .line 1146
    iget-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglContext:Z

    if-eqz v0, :cond_0

    .line 1147
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mEglHelper:Landroid/opengl/GLSurfaceView2$EglHelper;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView2$EglHelper;->finish()V

    .line 1148
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglContext:Z

    .line 1149
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->releaseEglContextLocked(Landroid/opengl/GLSurfaceView2$GLThread;)V

    .line 1151
    :cond_0
    return-void
.end method

.method private stopEglSurfaceLocked()V
    .locals 1

    .prologue
    .line 1135
    iget-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglSurface:Z

    if-eqz v0, :cond_0

    .line 1136
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglSurface:Z

    .line 1137
    iget-object v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mEglHelper:Landroid/opengl/GLSurfaceView2$EglHelper;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView2$EglHelper;->destroySurface()V

    .line 1139
    :cond_0
    return-void
.end method


# virtual methods
.method public ableToDraw()Z
    .locals 1

    .prologue
    .line 1394
    iget-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglContext:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHaveEglSurface:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2$GLThread;->readyToDraw()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRenderMode()I
    .locals 2

    .prologue
    .line 1414
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1415
    :try_start_0
    iget v1, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderMode:I

    monitor-exit v0

    return v1

    .line 1414
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onPause()V
    .locals 6

    .prologue
    .line 1463
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1465
    :try_start_0
    const-string v2, "GLThread"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onPause tid="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1467
    const/4 v2, 0x1

    iput-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRequestPaused:Z

    .line 1468
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 1469
    :goto_0
    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mExited:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mPaused:Z

    if-eqz v2, :cond_1

    .line 1463
    :cond_0
    monitor-exit v1

    .line 1480
    return-void

    .line 1471
    :cond_1
    const-string v2, "Main thread"

    const-string v3, "onPause waiting for mPaused."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1474
    :try_start_1
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1475
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 1476
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1463
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 1483
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1485
    :try_start_0
    const-string v2, "GLThread"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onResume tid="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1487
    const/4 v2, 0x0

    iput-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRequestPaused:Z

    .line 1488
    const/4 v2, 0x1

    iput-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRequestRender:Z

    .line 1489
    const/4 v2, 0x0

    iput-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderComplete:Z

    .line 1490
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 1491
    :goto_0
    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mExited:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mPaused:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderComplete:Z

    if-eqz v2, :cond_1

    .line 1483
    :cond_0
    monitor-exit v1

    .line 1502
    return-void

    .line 1493
    :cond_1
    const-string v2, "Main thread"

    const-string v3, "onResume waiting for !mPaused."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1496
    :try_start_1
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1497
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 1498
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1483
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public onWindowResize(II)V
    .locals 6
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 1505
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1506
    :try_start_0
    iput p1, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mWidth:I

    .line 1507
    iput p2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHeight:I

    .line 1508
    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->this$0:Landroid/opengl/GLSurfaceView2;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Landroid/opengl/GLSurfaceView2;->access$8(Landroid/opengl/GLSurfaceView2;Z)V

    .line 1509
    const/4 v2, 0x1

    iput-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRequestRender:Z

    .line 1510
    const/4 v2, 0x0

    iput-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderComplete:Z

    .line 1511
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 1514
    :goto_0
    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mExited:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mPaused:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderComplete:Z

    if-nez v2, :cond_0

    .line 1515
    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;
    invoke-static {v2}, Landroid/opengl/GLSurfaceView2;->access$9(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$GLThread;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;
    invoke-static {v2}, Landroid/opengl/GLSurfaceView2;->access$9(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$GLThread;

    move-result-object v2

    invoke-virtual {v2}, Landroid/opengl/GLSurfaceView2$GLThread;->ableToDraw()Z

    move-result v2

    .line 1514
    if-nez v2, :cond_1

    .line 1505
    :cond_0
    monitor-exit v1

    .line 1526
    return-void

    .line 1517
    :cond_1
    const-string v2, "Main thread"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onWindowResize waiting for render complete from tid="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Landroid/opengl/GLSurfaceView2$GLThread;->this$0:Landroid/opengl/GLSurfaceView2;

    # getter for: Landroid/opengl/GLSurfaceView2;->mGLThread:Landroid/opengl/GLSurfaceView2$GLThread;
    invoke-static {v4}, Landroid/opengl/GLSurfaceView2;->access$9(Landroid/opengl/GLSurfaceView2;)Landroid/opengl/GLSurfaceView2$GLThread;

    move-result-object v4

    invoke-virtual {v4}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1520
    :try_start_1
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1521
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 1522
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1505
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public queueEvent(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 1554
    if-nez p1, :cond_0

    .line 1555
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "r must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1557
    :cond_0
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1558
    :try_start_0
    iget-object v1, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mEventQueue:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1559
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1557
    monitor-exit v0

    .line 1561
    return-void

    .line 1557
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public requestExitAndWait()V
    .locals 3

    .prologue
    .line 1531
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1532
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mShouldExit:Z

    .line 1533
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 1534
    :goto_0
    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mExited:Z

    if-eqz v2, :cond_0

    .line 1531
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1542
    return-void

    .line 1536
    :cond_0
    :try_start_1
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1537
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 1538
    .local v0, "ex":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1531
    .end local v0    # "ex":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public requestReleaseEglContextLocked()V
    .locals 1

    .prologue
    .line 1545
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mShouldReleaseEglContext:Z

    .line 1546
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1547
    return-void
.end method

.method public requestRender()V
    .locals 2

    .prologue
    .line 1420
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1421
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRequestRender:Z

    .line 1422
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1420
    monitor-exit v0

    .line 1424
    return-void

    .line 1420
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1116
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GLThread "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/opengl/GLSurfaceView2$GLThread;->setName(Ljava/lang/String;)V

    .line 1118
    const-string v0, "GLThread"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "starting tid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1122
    :try_start_0
    invoke-direct {p0}, Landroid/opengl/GLSurfaceView2$GLThread;->guardedRun()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1126
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->threadExiting(Landroid/opengl/GLSurfaceView2$GLThread;)V

    .line 1128
    :goto_0
    return-void

    .line 1123
    :catch_0
    move-exception v0

    .line 1126
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->threadExiting(Landroid/opengl/GLSurfaceView2$GLThread;)V

    goto :goto_0

    .line 1125
    :catchall_0
    move-exception v0

    .line 1126
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/opengl/GLSurfaceView2$GLThreadManager;->threadExiting(Landroid/opengl/GLSurfaceView2$GLThread;)V

    .line 1127
    throw v0
.end method

.method public setRenderMode(I)V
    .locals 2
    .param p1, "renderMode"    # I

    .prologue
    .line 1404
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    .line 1405
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1407
    :cond_1
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v0

    monitor-enter v0

    .line 1408
    :try_start_0
    iput p1, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mRenderMode:I

    .line 1409
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 1407
    monitor-exit v0

    .line 1411
    return-void

    .line 1407
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public surfaceCreated()V
    .locals 6

    .prologue
    .line 1427
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1429
    :try_start_0
    const-string v2, "GLThread"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "surfaceCreated tid="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1431
    const/4 v2, 0x1

    iput-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHasSurface:Z

    .line 1432
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 1433
    :goto_0
    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mWaitingForSurface:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mExited:Z

    if-eqz v2, :cond_1

    .line 1427
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1441
    return-void

    .line 1435
    :cond_1
    :try_start_1
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1436
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 1437
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1427
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public surfaceDestroyed()V
    .locals 6

    .prologue
    .line 1444
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v1

    monitor-enter v1

    .line 1446
    :try_start_0
    const-string v2, "GLThread"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "surfaceDestroyed tid="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/opengl/GLSurfaceView2$GLThread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1448
    const/4 v2, 0x0

    iput-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mHasSurface:Z

    .line 1449
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    .line 1450
    :goto_0
    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mWaitingForSurface:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Landroid/opengl/GLSurfaceView2$GLThread;->mExited:Z

    if-eqz v2, :cond_1

    .line 1444
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1458
    return-void

    .line 1452
    :cond_1
    :try_start_1
    # getter for: Landroid/opengl/GLSurfaceView2;->sGLThreadManager:Landroid/opengl/GLSurfaceView2$GLThreadManager;
    invoke-static {}, Landroid/opengl/GLSurfaceView2;->access$6()Landroid/opengl/GLSurfaceView2$GLThreadManager;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1453
    :catch_0
    move-exception v2

    move-object v0, v2

    .line 1454
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1444
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method
